﻿using System;
using System.Net;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace RetailerAcademy
{
	public class JsonService
	{
		private IWebServiceDelegate webServiceDelegate;
		public WebClient client;

		public JsonService ()
		{
		}

		/// <summary>
		/// Consumes the service.
		/// </summary>
		/// <param name="url">The web api url passed from respective page.</param>
		/// <param name="jsonObject">Json object for the required params.</param>
		/// <param name="webServiceDelegate">Web service delegate interface passed from.</param>
		public void consumeService (string url, string jsonObject, IWebServiceDelegate webServiceDelegate)
		{
			try {
				this.webServiceDelegate = webServiceDelegate;
				using (client = new WebClient ()) {
					client.Headers [HttpRequestHeader.ContentType] = "application/json";
					if (jsonObject != null) {
						client.UploadStringAsync (new Uri (url), "POST", jsonObject);
						client.UploadStringCompleted += UploadContentCompleted;
					} else {
						client.DownloadStringAsync (new Uri (url), "GET");		
						client.DownloadStringCompleted += DownloadContentCompleted;
					}
				}
			} catch (WebException e) {
				Console.WriteLine ("WebException " + e.StackTrace);
			} catch (IOException e) {
				Console.WriteLine ("IOException " + e.StackTrace);
			} catch (Java.Lang.NullPointerException e) {
				Console.WriteLine ("NullPointerException " + e.StackTrace);
			} catch (Exception e) {
				Console.WriteLine ("Exception " + e.StackTrace);
			}
		}

		/// <summary>
		/// Uploads the content completed result of the POST method.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void UploadContentCompleted (object sender, UploadStringCompletedEventArgs e)
		{
			try {
				if (e.Error == null) {
					webServiceDelegate.onResponse (e.Result);
				} else {
					webServiceDelegate.onResponseFailed (CommonSharedStrings.SLOW_NETWORK);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Downloads the content completed result of the GET method.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void DownloadContentCompleted (object sender, DownloadStringCompletedEventArgs e)
		{
			try {
				if (e.Error == null) {
					webServiceDelegate.onResponse (e.Result);
				} else {
					webServiceDelegate.onResponseFailed (CommonSharedStrings.SLOW_NETWORK);
				}
			} catch (Exception) {				
			}
		}
	}
}