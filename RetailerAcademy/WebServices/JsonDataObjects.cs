﻿using System;
using System.Collections.Generic;

namespace RetailerAcademy
{
	/// <summary>
	/// Forgot password Model Class.
	/// </summary>
	public class ForgotPassword
	{
		public ForgotPassword()
		{
		}

		public string emailid
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Login data Model Class.
	/// </summary>
	public class LoginData
	{
		public LoginData()
		{
		}

		public string username
		{
			get;
			set;
		}

		public string password
		{
			get;
			set;
		}

		public string deviceToken
		{
			get;
			set;
		}

		public string platform
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Change password data Model Class.
	/// </summary>
	public class ChangePasswordData
	{
		public ChangePasswordData()
		{

		}

		public string Currentpassword
		{
			get;
			set;
		}

		public string Newpassword
		{
			get;
			set;
		}

		public string UserId
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Edit profile image data Model Class.
	/// </summary>
	public class EditProfileImageData
	{
		public EditProfileImageData()
		{

		}

		public int userid
		{
			get;
			set;
		}

		public string imagebytes
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Edit profile data Model Class.
	/// </summary>
	public class EditProfileData
	{
		public EditProfileData()
		{

		}

		public int userid
		{
			get;
			set;
		}

		public string firstname
		{
			get;
			set;
		}

		public string lastname
		{
			get;
			set;
		}

		public string phonenumber
		{
			get;
			set;
		}

		public string mobilenumber
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Content upload req Model Class.
	/// </summary>
	public class ContentUploadReq
	{
		public ContentUploadReq()
		{

		}

		public int userid
		{
			get;
			set;
		}

		public string file
		{
			get;
			set;
		}

		public string filename 
		{
			get;
			set;
		}

		public string contenttitle
		{
			get;
			set;
		}

		public string description
		{
			get;
			set;
		}

		public string extension {
			get;
			set;
		}

	}

	/// <summary>
	/// Content folder cls Model Class.
	/// </summary>
	public class ContentFolderCls
	{
		public ContentFolderCls()
		{

		}

		public int folderid
		{
			get;
			set;
		}

		public string foldername
		{
			get;
			set;
		}

		public int filenumber
		{
			get;
			set;
		}

		public string folderDesc
		{
			get;
			set;
		}

		public string passCode
		{
			get;
			set;
		}

		public bool isFolderRead {
			get;
			set;
		}
	}

	/// <summary>
	/// Content file Model Class.
	/// </summary>
	public class ContentFile
	{
		public ContentFile()
		{

		}

		public int folderid
		{
			get;
			set;
		}

		public int userid
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Content file with tag Model Class.
	/// </summary>
	public class ContentFileWithTag
	{
		public ContentFileWithTag()
		{

		}

		public int userid
		{
			get;
			set;
		}

		public int tagid
		{
			get;
			set;
		}

		public string brandGuid
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Comments Model Class.
	/// </summary>
	public class Comments
	{
		public Comments()
		{

		}

		public int fileid
		{
			get;
			set;
		}

		public int userid
		{
			get;
			set;
		}

		public string comment
		{
			get;
			set;
		}

		public string afterdate
		{
			get;
			set;
		}

		public int lastcommentid {
			get;
			set;
		}
	}

	/// <summary>
	/// Comments cls Model Class.
	/// </summary>
	public class CommentsCls
	{
		public CommentsCls()
		{

		}

		public int commentid
		{
			get;
			set;
		}

		public int userId
		{
			get;
			set;
		}

		public string userfullname
		{
			get;
			set;
		}

		public string Comment
		{
			get;
			set;
		}

		public string dateofcomment
		{
			get;
			set;
		}

		public string userImg
		{
			get;
			set;
		}

		public string commentAttachmentImg {
			get;
			set;
		}

	}

	/// <summary>
	/// Content delete Model Class.
	/// </summary>
	public class ContentDelete
	{
		public ContentDelete()
		{

		}

		public int userid
		{
			get;
			set;
		}

		public int mycontentid
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Comments delete activity stream Model Class.
	/// </summary>
	public class CommentsDeleteActivityStream
	{
		public CommentsDeleteActivityStream()
		{

		}

		public int commentId
		{
			get;
			set;
		}

		public int userid
		{
			get;
			set;
		}

		public int activityStreamId {
			get;
			set;
		}
	}

	/// <summary>
	/// Comments delete blogs Model Class.
	/// </summary>
	public class CommentsDeleteBlogs
	{
		public CommentsDeleteBlogs()
		{

		}

		public int blogid
		{
			get;
			set;
		}

		public int commentId
		{
			get;
			set;
		}

		public int userid
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Comments delete Model Class.
	/// </summary>
	public class CommentsDelete
	{
		public CommentsDelete()
		{

		}

		public int fileid {
			get;
			set;
		}

		public int commentId
		{
			get;
			set;
		}

		public int userid
		{
			get;
			set;
		}

	}

	/// <summary>
	/// Activity comments Model Class.
	/// </summary>
	public class ActivityComments
	{
		public ActivityComments()
		{

		}

		public int activityStreamId
		{
			get;
			set;
		}

		public int lastcommentid
		{
			get;
			set;
		}

		public int userid
		{
			get;
			set;
		}

		public string comment
		{
			get;
			set;
		}

		public string afterdate 
		{
			get;
			set;
		}

		public string file 
		{
			get;
			set;
		}

		public string filename
		{
			get;
			set;
		}

		public string extension 
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Activity data compose Model Class.
	/// </summary>
	public class ActivityDataCompose
	{
		public ActivityDataCompose()
		{

		}

		public string brandGuid
		{
			get;
			set;
		}

		public int userId
		{
			get;
			set;
		}

		public string activityStreamText
		{
			get;
			set;
		}

		public string file
		{
			get;
			set; 
		}

		public string filename
		{
			get;
			set; 
		}

		public string extension
		{
			get;
			set; 
		}
	}

	/// <summary>
	/// Blog comments Model Class.
	/// </summary>
	public class BlogComments
	{
		public BlogComments()
		{

		}

		public int blogid
		{
			get;
			set;
		}

		public int userid
		{
			get;
			set;
		}

		public string blogpost
		{
			get;
			set;
		}

		public string afterdate
		{
			get;
			set;
		}

		public int lastcommentid {
			get;
			set;
		}
	}

	/// <summary>
	/// Blog like Model Class.
	/// </summary>
	public class BlogLike
	{
		public BlogLike()
		{

		}

		public int blogid
		{
			get;
			set;
		}

		public int userid
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Content desc Model Class.
	/// </summary>
	public class ContentDesc
	{
		public ContentDesc()
		{

		}

		public int fileid
		{
			get;
			set;
		}

		public int userid
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Content like Model Class.
	/// </summary>
	public class ContentLike
	{
		public ContentLike()
		{

		}

		public int fileid
		{
			get;
			set;
		}

		public int userid
		{
			get;
			set;
		}

		public bool check
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Content add bookmark Model Class.
	/// </summary>
	public class ContentAddBookmark
	{
		public ContentAddBookmark()
		{

		}

		public int fileContentId
		{
			get;
			set;
		}

		public int userId
		{
			get;
			set;
		}

		public bool removeBookMark
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Content tag Model Class.
	/// </summary>
	public class ContentTag
	{
		public ContentTag()
		{

		}

		public int userID
		{
			get;
			set;
		}

		public int fileID
		{
			get;
			set;
		}

		public string brandGuid
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Communication Model Class.
	/// </summary>
	public class Communication
	{
		public Communication()
		{

		}

		public int userID
		{
			get;
			set;
		}

		public string filterDate
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Communication archieve Model Class.
	/// </summary>
	public class CommunicationArchieve
	{
		public CommunicationArchieve()
		{

		}

		public int userID
		{
			get;
			set;
		}

		public string filterDate
		{
			get;
			set;
		}

		public bool isArchived
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Communication moveto archieve Model Class.
	/// </summary>
	public class CommunicationMovetoArchieve
	{
		public CommunicationMovetoArchieve()
		{

		}

		public int userID
		{
			get;
			set;
		}

		public int messageSumID
		{
			get;
			set;
		}

		public bool archive
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Communication message Model Class.
	/// </summary>
	public class CommunicationMessage
	{
		public CommunicationMessage()
		{

		}

		public string subject
		{
			get;
			set;
		}

		public string body
		{
			get;
			set;
		}

		public bool messageIsRead
		{
			get;
			set;

		}

		public string recipientName
		{
			get;
			set;
		}

		public string recipientUserIDs
		{
			get;
			set;
		}

		public string sendDateTime
		{
			get;
			set;
		}

		public int userMessageID
		{
			get;
			set;
		}

		public string senderNickName
		{
			get;
			set;
		}

		public string senderUserID
		{
			get;
			set;
		}

		public string attachment
		{
			get;
			set;
		}

		public string imagebytes
		{
			get;
			set;
		}

		public string fileName
		{
			get;
			set;
		}

		public string extension
		{
			get;
			set;
		}

		public string lastMessageDate
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Communication user list Model Class.
	/// </summary>
	public class CommunicationUserList
	{
		public CommunicationUserList()
		{

		}

		public int userID
		{
			get;
			set;
		}

		public string orgID
		{
			get;
			set;
		}

		public string searchKey
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Blogs post Model Class.
	/// </summary>
	public class BlogsPost
	{
		public BlogsPost()
		{

		}

		public int categoryid
		{
			get;
			set;
		}

		public int userId
		{
			get;
			set;
		}

		public int blogid
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Content tags Model Class.
	/// </summary>
	public class ContentTags
	{
		public ContentTags()
		{

		}

		public int tagID
		{
			get;
			set;
		}

		public string tagName
		{
			get;
			set;
		}

		public int fileCount
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Activity like Model Class.
	/// </summary>
	public class ActivityLike
	{
		public ActivityLike()
		{

		}

		public int activityStreamId
		{
			get;
			set;
		}

		public string orgId
		{
			get;
			set;
		}

		public int userId
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Logout Model Class.
	/// </summary>
	public class Logout
	{
		public Logout()
		{

		}

		public int userid
		{
			get;
			set;
		}

		public string deviceToken
		{
			get;
			set;
		}

		public string platform
		{
			get;
			set;
		}
	}
}