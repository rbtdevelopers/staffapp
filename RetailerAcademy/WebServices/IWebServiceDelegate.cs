﻿using System;

namespace RetailerAcademy
{
	/// <summary>
	/// IWebServiceDelegate used in all the service responses to get the json as the result in all the screens.
	/// </summary>
	public interface IWebServiceDelegate
	{
        void onResponse(string response);
        void onResponseFailed(string response);
	}
}