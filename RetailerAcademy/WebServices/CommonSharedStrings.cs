﻿using System;


namespace RetailerAcademy
{
	public class CommonSharedStrings
	{
		/// <summary>
		/// The CURRENT VERSION.
		/// </summary>
		public static string CURRENT_VERSION = "Current version V1.2.2";

		/// <summary>
		/// Common validation messages
		/// </summary>
		public static string USERNAME_BLANK_MESSAGE = "Username should not be blank.";
		public static string USERNAME_INVALID_MESSAGE = "Please enter valid username.";

		public static string PASSWORD_BLANK_MESSAGE = "Password should not be blank.";
		public static string PASSWORD_INVALID_MESSAGE = "Password should be minimum of 8 characters.";
		/// <summary>
		/// Forgot password
		/// </summary>
		public static string FORGOTPASSWORD_BLANK_MESSAGE = "Email address should not be blank.";
		public static string FORGOTPASSWORD_INVALID_MESSAGE = "Please enter a valid email address.";

		/// <summary>
		/// The STATUS ERROR.
		/// </summary>
		public static string SLOW_NETWORK = "Your internet connection seems to be slow or unavailable at this moment. Please try again!";
		public static string STATUS_ERROR = "Something went wrong. Please try again later.";
		public static string FILE_CORRUPT_ERROR = "The file is corrupted and cannot be opened";
		public static string INTERNET_ERROR_MESSAGE = "Please check your network connection.";
		public static string LOADING_TEXT = "Loading. Please wait...";
		public static string NOT_SUPPORTED_TEXT = "Your device can not display the file you are trying to view";

		/// <summary>
		/// Change Password
		/// </summary>
		public static string CONFIRM_PASSWORD_TEXT = "New Passwords are not matching.";
		public static string PASSWORD_CHANGED_TEXT = "Password successfully changed.";
		public static string CURRENT_PASSWORD = "Current password is not matching.";
		public static string CURRENT_PASSWORD_EMPTY = "Current Password should not be blank.";
		public static string NEW_PASSWORD_EMPTY = "New Password should not be blank.";
		public static string CONFIRM_PASSWORD_EMPTY = "Confirm Password should not be blank.";

		public static string NEWPASSWORD_INVALID = "New Password should be minimum of 8 characters.";
		public static string NEWPASSWORD_NO_SPLCHARS = "Your Password should not contain any special characters.";
		public static string CONFIRMPASSWORD_INVALID = "Confirm Password should be minimum of 8 characters.";
		public static string PASSWORD_EXISTS_INVALID = "Password already exists. Please choose different password.";

		/// <summary>
		/// Update profile
		/// </summary>
		public static string FIRSTNAME = "First Name should be minimum of 2 characters.";
		public static string LASTNAME = "Last Name should be minimum of 2 characters.";
		public static string MOBILENUMBER = "Phone number should be minimum of 10 digits.";
		public static string LANDNUMBER = "Mobile number should be minimum of 10 digits.";

		public static string CONTENT_TITLE_ERROR = "Content title should not be blank.";
		public static string CONTENT_FILE_ERROR = "Please select content.";
		public static string COMPOSE_MSG_ERROR = "Message should not be blank.";
		public static string CONTENT_DESC_ERROR = "Content description should not be blank.";

		/// <summary>
		/// The MAIL COMPOSE VALIDATIONS
		/// </summary>
		public static string MAIL_SUBJECT = "Mail subject should not be blank.";
		public static string MAIL_TO = "Please enter valid recipients";
		public static string MAIL_BODY = "Mail body should not be blank.";

		/// <summary>
		/// The EMAIL REGEX.
		/// </summary>
		public static string EMAIL_REGEX = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";

		/// <summary>
		/// web Api Urls
		/// </summary>
		//public static string WEB_URL = "https://retailacademydev.mychemist.com.au/api/";
		public static string WEB_URL = "http://demo19kentico8.raybiztech.com:8081/api/";
		public static string LOGIN_URL = WEB_URL + "userauthentication";
		public static string FORGOT_PASSWORD_URL = WEB_URL + "forgotpassword";
		public static string CHANGE_PASSWORD_URL = WEB_URL + "changepassword";
		public static string USER_DETRAILS = WEB_URL + "userdetailsshow/";
		public static string USER_ORGS = WEB_URL + "UsersOrgInfo/";
		public static string EDIT_PROFILE = WEB_URL + "ProfilePhotoUpdate";
		public static string EDIT_PROFILE_INFO = WEB_URL + "ProfileInfoUpdate";
		public static string STATE_DETAILS = WEB_URL + "UserStateInfo/";
		public static string TEAM_DETAILS = WEB_URL + "UserTeamInfo/";

		public static string QUIZ_ANSWERING = WEB_URL + "QuizQuestions";
		public static string QUIZ_CATAGORY = WEB_URL + "UserQuizCategoryInfo/";

		public static string CONTENT_FOLDERS = WEB_URL + "FolderList/";
		public static string CONTENT_FILES = WEB_URL + "ContentList";
		public static string CONTENT_FILES_TAGLIST = WEB_URL + "ContentListByTag";
		public static string COMMENT = WEB_URL + "ContentComments";
		public static string CONTENT_DISPLAY = WEB_URL + "ContentDisplay";
		public static string CONTENT_LIKE = WEB_URL + "ContentLikes";
		public static string CONTENT_UPLOAD = WEB_URL + "AddMyContent";
		public static string CONTENT_DELETE_COMMENT = WEB_URL + "ContentDeleteComment";
		public static string MYCONTENT_DELETE = WEB_URL + "DeleteMycontent";
		public static string CONTENT_SEARCH = WEB_URL + "SearchFileAndFolder";

		public static string COMMUNICATION_LIST = WEB_URL + "MessageList";
		public static string COMMUNICATION_READ = WEB_URL + "MessageDetails/";
		public static string COMMUNICATION_SENDMESSAGE = WEB_URL + "SendMessage";
		public static string COMMUNICATION_USERSLIST = WEB_URL + "UserEmail";
		public static string COMMUNICATION_SENT_MESSAGE = WEB_URL + "SentMessageList";
		public static string COMMUNICATION_ARCHIEVE_MESSAGE = WEB_URL + "MessageList";
		public static string COMMUNICATION_MOVETOARCHIEVE = WEB_URL + "MessageArchive";
		public static string COMMUNICATION_REPLYLIST = WEB_URL + "ReplyToMessage";

		public static string LEADERBOARDCATEGORY = WEB_URL + "LeaderBoard/";
		public static string LEADERBOARD_LIST = WEB_URL + "LeaderBoardUser";
		public static string LEADERBOARD_NEWSERVICE = WEB_URL + "RetailStatus";

		public static string ACTIVITYSTREAM_LIST = WEB_URL + "ActivityStreamList/";
		public static string ACTIVITYSTREAM_LIKE = WEB_URL + "ActivityStreamLike/";
		public static string ACTIVITYSTREAM_COMMENTSLIST = WEB_URL + "ActivityStreamCommentList";
		public static string ACTIVITYSTREAM_COMPOSE = WEB_URL + "ComposeActivityStream";
		public static string DELETECOMMENT = WEB_URL + "ActivityStreamDeleteComment";

		public static string BLOG_CATEGORIES = WEB_URL + "BlogCategory";
		public static string BLOG_LIST = WEB_URL + "Blog";
		public static string BLOG_LIKE = WEB_URL + "BlogLikes";
		public static string BLOG_COMMENTS = WEB_URL + "AddBlogPost";
		public static string BLOG_DELETE_COMMENTS = WEB_URL+"BlogDeleteComment";

		public static string BOOKMARK_ADDREMOVE = WEB_URL + "ContentAddRemoveBookMark";
		public static string BOOKMARK_LIST = WEB_URL + "ContentBookMarkList/";

		public static string TAG_LIST = WEB_URL + "ContentTagsList";

		public static string LOGOUT = WEB_URL + "Logout";
		public static string NOTIFICATIONLIST = WEB_URL+"NotificationList";
		public static string RECENTACTIVITY = WEB_URL+"RecentUserActivity";

		public static string ISACTIVEORINACTIVE = WEB_URL+ "UserStatus";
		public static string TERMSANDCONDITION = WEB_URL+ "TermsAndConditions";

		public static string CHANGE_IN_USER_ROLE = "You are not authorised to perform this action, please contact support team";
		public static string COMMENT_EMPTY = "Comment should not be empty";
	}
}