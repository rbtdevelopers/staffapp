﻿using System;
using UIKit;
using CoreGraphics;
using CoreAnimation;

namespace RetailerAcademy.iOS
{
	public class BaseController: UIViewController
	{
		public Toast toast = new Toast();
		public ActivityLoader activity = new ActivityLoader();
		public NetworkStatus internetstatus = Reachability.InternetConnectionStatus();
		public BaseController ()
		{
			
		}

		public BaseController (IntPtr handle) : base (handle)
		{		
			
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			CAGradientLayer gradient = new CAGradientLayer ();
			gradient.Frame = this.View.Bounds;
			gradient.Colors = new CGColor[]{ UIColor.FromRGB(3, 97, 197).CGColor, UIColor.FromRGB(0, 73, 150).CGColor };
			gradient.Locations = new Foundation.NSNumber[]{ 0.0, 0.5 };
			this.View.Layer.InsertSublayer (gradient, 0);

		}
	}
}