// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RetailerAcademy.iOS
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView backgroundView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		GradientView containerView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		EmptyButton forgotPwd { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomButton loginBtn { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomTextField name { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomTextField password { get; set; }

		[Action ("forgotPwd_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void forgotPwd_TouchUpInside (EmptyButton sender);

		[Action ("loginBtn_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void loginBtn_TouchUpInside (CustomButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (backgroundView != null) {
				backgroundView.Dispose ();
				backgroundView = null;
			}
			if (containerView != null) {
				containerView.Dispose ();
				containerView = null;
			}
			if (forgotPwd != null) {
				forgotPwd.Dispose ();
				forgotPwd = null;
			}
			if (loginBtn != null) {
				loginBtn.Dispose ();
				loginBtn = null;
			}
			if (name != null) {
				name.Dispose ();
				name = null;
			}
			if (password != null) {
				password.Dispose ();
				password = null;
			}
		}
	}
}
