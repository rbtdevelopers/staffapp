using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ObjCRuntime;

namespace RetailerAcademy.iOS
{
	partial class ForgotPasswordController : BaseController, IWebServiceDelegate
	{
		public ForgotPasswordController (IntPtr handle) : base (handle)
		{		
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
			tap.AddTarget (
				() => View.EndEditing (true));
			View.AddGestureRecognizer (tap);

			this.emailTxt.ShouldReturn += (textField) => {
				textField.ResignFirstResponder ();
				return true;
			};
		}

		partial void cancelBtn_TouchUpInside (CustomButton sender)
		{			
			this.NavigationController.PopToRootViewController (false);
		}

		partial void requestBtn_TouchUpInside (CustomButton sender)
		{
			emailTxt.ResignFirstResponder ();
			string frgtPasswordUrl = CommonSharedStrings.FORGOT_PASSWORD_URL;
			string pwdTxt = this.emailTxt.Text;

			string emailRegex = CommonSharedStrings.EMAIL_REGEX;
			bool isEmail = Regex.IsMatch (pwdTxt, emailRegex, RegexOptions.IgnoreCase);
			internetstatus = Reachability.InternetConnectionStatus ();

			if (string.IsNullOrEmpty (pwdTxt)) {
				toast.makeToast (CommonSharedStrings.FORGOTPASSWORD_BLANK_MESSAGE, this);
			} else if (!isEmail) {
				toast.makeToast (CommonSharedStrings.FORGOTPASSWORD_INVALID_MESSAGE, this);
			} else if (internetstatus == NetworkStatus.NotReachable) {	
				toast.makeToast (CommonSharedStrings.INTERNET_ERROR_MESSAGE, this);
			} else {
				Console.WriteLine ("text {0}", pwdTxt);
				ForgotPassword dataObjects = new ForgotPassword ();
				dataObjects.emailid = pwdTxt;
				string requestParameter = JsonConvert.SerializeObject (dataObjects);
				var jsonService = new JsonService ();
				jsonService.consumeService (frgtPasswordUrl, requestParameter, this);
				activity.startActivity (this);
			}
		}

		#region IWebServiceDelegate implementation

		public void onResponse (string response)
		{
			activity.stopActivity ();
			this.emailTxt.ResignFirstResponder ();

			try {
				JObject responseObj = JObject.Parse (response);
				if (Convert.ToBoolean (responseObj.SelectToken ("status"))) {
					//navigate to next view
					toast.makeToast (Convert.ToString (responseObj.SelectToken ("message")), this);
					this.PerformSelector (new Selector ("backToLogin"), Self, 2);
				} else {
					//display toast message
					toast.makeToast (Convert.ToString (responseObj.SelectToken ("message")), this);
				}
			} catch (Exception e) {
				Console.WriteLine (e);
				toast.makeToast (CommonSharedStrings.STATUS_ERROR, this);
			}
		}

		[Export ("backToLogin")]
		private void backToLogin ()
		{
			this.NavigationController.PopViewController (false);
		}

		#endregion
	}
}
