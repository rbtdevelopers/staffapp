// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RetailerAcademy.iOS
{
	[Register ("ViewProfilePage")]
	partial class ViewProfilePage
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomLabel email_id { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomLabel landNumber { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomLabel mobileNumber { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIImageView userImage { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomLabel userName { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (email_id != null) {
				email_id.Dispose ();
				email_id = null;
			}
			if (landNumber != null) {
				landNumber.Dispose ();
				landNumber = null;
			}
			if (mobileNumber != null) {
				mobileNumber.Dispose ();
				mobileNumber = null;
			}
			if (userImage != null) {
				userImage.Dispose ();
				userImage = null;
			}
			if (userName != null) {
				userName.Dispose ();
				userName = null;
			}
		}
	}
}
