// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RetailerAcademy.iOS
{
	[Register ("SwitchViewController")]
	partial class SwitchViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomButton proceedBtn { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (proceedBtn != null) {
				proceedBtn.Dispose ();
				proceedBtn = null;
			}
		}
	}
}
