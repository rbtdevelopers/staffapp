// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RetailerAcademy.iOS
{
	[Register ("DashboardController")]
	partial class DashboardController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView animateView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton changePass { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		GradientView containerView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITableView dashboardTable { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView dashboardView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView mainMenuListView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton menuBtn { get; set; }

		[Action ("changePass_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void changePass_TouchUpInside (UIButton sender);

		[Action ("menuBtn_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void menuBtn_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (animateView != null) {
				animateView.Dispose ();
				animateView = null;
			}
			if (changePass != null) {
				changePass.Dispose ();
				changePass = null;
			}
			if (containerView != null) {
				containerView.Dispose ();
				containerView = null;
			}
			if (dashboardTable != null) {
				dashboardTable.Dispose ();
				dashboardTable = null;
			}
			if (dashboardView != null) {
				dashboardView.Dispose ();
				dashboardView = null;
			}
			if (mainMenuListView != null) {
				mainMenuListView.Dispose ();
				mainMenuListView = null;
			}
			if (menuBtn != null) {
				menuBtn.Dispose ();
				menuBtn = null;
			}
		}
	}
}
