﻿using System;
using UIKit;
using Foundation;
using ObjCRuntime;
using CoreGraphics;

namespace RetailerAcademy.iOS
{
	public class DashboardTableSource: UITableViewSource
	{

		private string[] tableItems;
		private DashboardController currentView;
		private string[] imageArray;


		public DashboardTableSource (string[] tableItems,string[] imageArray, DashboardController currentView)
		{
			this.tableItems = tableItems;
			this.currentView = currentView;
			this.imageArray = imageArray;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override nint RowsInSection (UITableView tableView, nint section)
		{
			return tableItems.Length;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			NSString cellIdentifier = (NSString)"cell";
			var cell = tableView.DequeueReusableCell (cellIdentifier) as DashboardTableCell;
			if (cell == null)
			{
				cell = new DashboardTableCell (cellIdentifier);
			}

			if (tableView.RespondsToSelector(new Selector("setLayoutMargins:")))
			{
				tableView.LayoutMargins = UIEdgeInsets.Zero;
			}

			if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
			{
					if (cell.RespondsToSelector(new Selector("setLayoutMargins:")))
					{
						cell.LayoutMargins = UIEdgeInsets.Zero;
					}
			}

			cell.UpdateCell (tableItems[indexPath.Row], UIImage.FromFile ("images/" + imageArray[indexPath.Row]));

			return cell;
		}

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			currentView.selectedRow(indexPath.Row);
		}
	}
}

