// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RetailerAcademy.iOS
{
	[Register ("ForgotPasswordController")]
	partial class ForgotPasswordController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomButton cancelBtn { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomTextField emailTxt { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomButton requestBtn { get; set; }

		[Action ("cancelBtn_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void cancelBtn_TouchUpInside (CustomButton sender);

		[Action ("requestBtn_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void requestBtn_TouchUpInside (CustomButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (cancelBtn != null) {
				cancelBtn.Dispose ();
				cancelBtn = null;
			}
			if (emailTxt != null) {
				emailTxt.Dispose ();
				emailTxt = null;
			}
			if (requestBtn != null) {
				requestBtn.Dispose ();
				requestBtn = null;
			}
		}
	}
}
