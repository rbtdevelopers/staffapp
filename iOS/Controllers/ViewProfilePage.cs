using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;
using System.Drawing;
using RetailerAcademy.iOS;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RetailerAcademy.iOS
{
	partial class ViewProfilePage : BaseController,IWebServiceDelegate
	{
		string userId = NSUserDefaults.StandardUserDefaults.StringForKey("USERID"); 

		public ViewProfilePage (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			var jsonService = new JsonService();
			string url = CommonSharedStrings.USER_DETRAILS + userId;
			jsonService.consumeService(url,null,this);
			activity.startActivity(this);
		}

		#region IWebServiceDelegate implementation
		public void onResponse (string response)
		{
			activity.stopActivity ();
			JObject responseObj=JObject.Parse(response);
			if(Convert.ToBoolean(responseObj.SelectToken("status")))
			{
				this.userName.Text = Convert.ToString(responseObj.SelectToken ("UserFullName"));
				this.email_id.Text =  Convert.ToString(responseObj.SelectToken ("emailid"));
				this.mobileNumber.Text =  Convert.ToString(responseObj.SelectToken ("mobilenumber"));
				this.landNumber.Text =  Convert.ToString(responseObj.SelectToken ("landnumber"));

				byte[] encodedDataAsBytes = System.Convert.FromBase64String (Convert.ToString(responseObj.SelectToken ("avatarimage")));
				NSData data = NSData.FromArray (encodedDataAsBytes);
				this.userImage.Image = UIImage.LoadFromData (data);
				this.userImage.Layer.CornerRadius = 100;
				this.userImage.Layer.BorderWidth = 2;
				this.userImage.Layer.BorderColor =  UIColor.FromRGB(191, 191, 191).CGColor;

			}
			else
			{
				//display toast message
				toast.makeToast (Convert.ToString(responseObj.SelectToken ("message")), this);
			}
		}
		#endregion


	}
}
