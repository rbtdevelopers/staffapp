﻿using System;
using UIKit;
using CoreGraphics;
using Carousels;
using System.Linq;

namespace RetailerAcademy.iOS
{
	public class CarouselDataSource: iCarouselDataSource
	{
		int[] items;

		public CarouselDataSource()
		{
			// create our amazing data source
			items = Enumerable.Range (0, 100).ToArray ();
		}

		// let the carousel know how many items to render
		public override nint GetNumberOfItems (iCarousel carousel)
		{
			// return the number of items in the data
			return items.Length;
		}

		// create the view each item in the carousel
		public override UIView GetViewForItem (iCarousel carousel, nint index, UIView view)
		{
			UILabel label = null;
			UIImageView imageView = null;

			if (view == null)
			{
				// create new view if no view is available for recycling
				imageView = new UIImageView(new CGRect(0, 0, 200.0f, 200.0f));
				imageView.Image = UIImage.FromBundle ("images/content.png");
				imageView.ContentMode = UIViewContentMode.Center;
				imageView.BackgroundColor = UIColor.Red;
				imageView.Layer.BorderColor = UIColor.White.CGColor;
				imageView.Layer.BorderWidth = 1;
				imageView.Layer.CornerRadius = 20;


				label = new UILabel(imageView.Bounds);
				label.BackgroundColor = UIColor.Clear;
				label.TextAlignment = UITextAlignment.Center;
				label.Font = label.Font.WithSize(50);
				label.Tag = 1;
				imageView.AddSubview(label);


			}
			else
			{
				// get a reference to the label in the recycled view
				imageView = (UIImageView)view;
				label = (UILabel)view.ViewWithTag(1);
			}

			// set the values of the view
			label.Text = items [index].ToString ();

			Console.WriteLine (label.Text);

			return imageView;
		}
	}
}

