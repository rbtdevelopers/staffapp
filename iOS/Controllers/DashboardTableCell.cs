﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace RetailerAcademy.iOS
{
	public class DashboardTableCell: UITableViewCell
	{
		private CustomLabel headingLabel=new CustomLabel();
		private UIImageView imageView = new UIImageView();
		public DashboardTableCell (NSString cellID) : base (UITableViewCellStyle.Default, cellID)
		{
			SelectionStyle = UITableViewCellSelectionStyle.Gray;
			ContentView.BackgroundColor = UIColor.FromRGB (235, 235, 235);
			ContentView.AddSubviews(new UIView[] {headingLabel, imageView});
		}

		public void UpdateCell (string caption, UIImage image)
		{
			imageView.Image = image;
			headingLabel.Text = caption;
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			imageView.Frame = new CoreGraphics.CGRect (8, 8, 30, 30);

			headingLabel.Frame = new CoreGraphics.CGRect (46, 0, ContentView.Bounds.Width - 50, 46);
			headingLabel.TextAlignment = UITextAlignment.Left;
			headingLabel.TextColor = UIColor.FromRGB (94, 99, 106);
		}
	}
}