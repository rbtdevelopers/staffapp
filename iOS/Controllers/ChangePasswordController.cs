using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;
using System.Drawing;
using RetailerAcademy.iOS;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ObjCRuntime;



namespace RetailerAcademy.iOS
{
	partial class ChangePasswordController : BaseController ,IWebServiceDelegate
	{

		private UIView activeview;
		private float scrollamount = 0.0f;    // amount to scroll 
		private float bottom = 0.0f;           // bottom point
		private float offset = 80.0f;          // extra offset
		private bool moveViewUp = false;

		public ChangePasswordController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Keyboard popup
			NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.DidShowNotification,KeyBoardUpNotification);

			// Keyboard Down
			NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.WillHideNotification,KeyBoardDownNotification);



		}

		private void KeyBoardUpNotification(NSNotification notification)
		{
			// get the keyboard size
			RectangleF r = (RectangleF)UIKeyboard.BoundsFromNotification (notification);

			// Find what opened the keyboard
			foreach (var view in this.containerView.Subviews) {
				if (view.IsFirstResponder)
					this.activeview = view;
			}

			try 
			{
				// Bottom of the controller = initial position + height + offset      
				bottom = ((float)activeview.Frame.Y + (float)activeview.Frame.Height + offset);

				// Calculate how far we need to scroll
				scrollamount = (r.Height - ((float)View.Frame.Size.Height - bottom ) ) ;

				// Perform the scrolling
				if (scrollamount > 0) {
					moveViewUp = true;
					ScrollTheView (moveViewUp);
				} else {
					moveViewUp = false;
				}
			}
			catch(Exception e) {
				Console.WriteLine (e);
			}
		}

		private void KeyBoardDownNotification(NSNotification notification)
		{
			if(moveViewUp){ScrollTheView(false);}
		}


		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			NSNotificationCenter.DefaultCenter.RemoveObserver (UIKeyboard.DidShowNotification);
			// Keyboard Down
			NSNotificationCenter.DefaultCenter.RemoveObserver (UIKeyboard.WillHideNotification);
		}

		public override void DidReceiveMemoryWarning ()
		{		
			base.DidReceiveMemoryWarning ();		
			// Release any cached data, images, etc that aren't in use.		
		}

		private void ScrollTheView(bool move)
		{
			// scroll the view up or down
			UIView.BeginAnimations (string.Empty, System.IntPtr.Zero);
			UIView.SetAnimationDuration (0.3);

			RectangleF frame = (RectangleF)View.Frame;

			if (move) {
				frame.Y -= scrollamount;
			} else {
				frame.Y += scrollamount;
				scrollamount = 0;
			}

			View.Frame = frame;
			UIView.CommitAnimations();
		}

		partial void changePasswordButton_TouchUpInside (CustomButton sender)
		{	
			string currentPasswordString = this.currentPassword.Text;
			string newPasswordStr =this.newPassword.Text;
			string confirmPassword =this.confirmPassword.Text;
			string changedPasswordString ="";
			string userId = NSUserDefaults.StandardUserDefaults.StringForKey("USERID"); 
			internetstatus = Reachability.InternetConnectionStatus();
			string currentPassword = NSUserDefaults.StandardUserDefaults.StringForKey("PASSWORD"); 

			if (string.IsNullOrEmpty(currentPasswordString))
			{
				toast.makeToast(CommonSharedStrings.CURRENT_PASSWORD_EMPTY, this);
			}
			else if (currentPasswordString.Length < 8)
			{
				toast.makeToast(CommonSharedStrings.CURRENTPASSWORD_INVALID, this);
			}
			else if (string.IsNullOrEmpty(newPasswordStr))
			{
				toast.makeToast(CommonSharedStrings.NEW_PASSWORD_EMPTY, this);
			}
			else if (newPasswordStr.Length < 8)
			{
				toast.makeToast(CommonSharedStrings.NEWPASSWORD_INVALID, this);
			}

			else if (string.IsNullOrEmpty(confirmPassword))
			{
				toast.makeToast(CommonSharedStrings.CONFIRM_PASSWORD_EMPTY, this);
			}
			else if (confirmPassword.Length < 8)
			{
				toast.makeToast(CommonSharedStrings.CONFIRMPASSWORD_INVALID, this);
			}
			else if (currentPasswordString != currentPassword )
			{
				toast.makeToast(CommonSharedStrings.CURRENT_PASSWORD, this);
			}
			else if (this.newPassword.Text != this.confirmPassword.Text)
			{
				toast.makeToast(CommonSharedStrings.CONFIRM_PASSWORD_TEXT, this);
			}

			else if (this.newPassword.Text == currentPasswordString)
			{
				toast.makeToast(CommonSharedStrings.PASSWORD_EXITS_INVALID, this);
			}

			else if (internetstatus == NetworkStatus.NotReachable)
			{
				toast.makeToast (CommonSharedStrings.INTERNET_ERROR_MESSAGE, this);
			}

			else
			{
					 changedPasswordString = this.confirmPassword.Text;
					changePasswordData dataObjects=new changePasswordData();
					dataObjects.Currentpassword = currentPasswordString;
					dataObjects.Newpassword = changedPasswordString;
					dataObjects.UserId = userId.ToString();
					string requestParameter=JsonConvert.SerializeObject(dataObjects);
					var jsonService = new JsonService();
				jsonService.consumeService(CommonSharedStrings.CHANGE_PASSWORD_URL,requestParameter,this);
					activity.startActivity(this);
			}

		}

		#region IWebServiceDelegate implementation

		public void onResponse (string response)
		{
			activity.stopActivity ();
			JObject responseObj=JObject.Parse(response);
			if(Convert.ToBoolean(responseObj.SelectToken("status")))
			{
				toast.makeToast(CommonSharedStrings.PASSWORD_CHANGED_TEXT, this);

				this.PerformSelector (new Selector ("backToLogin"), Self, 2);
			}
			else
			{
				//display toast message
				toast.makeToast (Convert.ToString(responseObj.SelectToken ("message")), this);
				this.currentPassword.Text ="";
				this.newPassword.Text ="";
				this.confirmPassword.Text ="";
			}		
		}

		[Export ("backToLogin")]
		private void backToLogin()
		{
			this.NavigationController.PopToRootViewController (false);
		}

		#endregion
	}
}
