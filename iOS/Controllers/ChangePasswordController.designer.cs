// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RetailerAcademy.iOS
{
	[Register ("ChangePasswordController")]
	partial class ChangePasswordController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView ChangePassword { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomButton changePasswordButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomTextField confirmPassword { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		GradientView containerView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomTextField currentPassword { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomTextField newPassword { get; set; }

		[Action ("changePasswordButton_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void changePasswordButton_TouchUpInside (CustomButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (ChangePassword != null) {
				ChangePassword.Dispose ();
				ChangePassword = null;
			}
			if (changePasswordButton != null) {
				changePasswordButton.Dispose ();
				changePasswordButton = null;
			}
			if (confirmPassword != null) {
				confirmPassword.Dispose ();
				confirmPassword = null;
			}
			if (containerView != null) {
				containerView.Dispose ();
				containerView = null;
			}
			if (currentPassword != null) {
				currentPassword.Dispose ();
				currentPassword = null;
			}
			if (newPassword != null) {
				newPassword.Dispose ();
				newPassword = null;
			}
		}
	}
}
