﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;
using System.Drawing;
using RetailerAcademy.iOS;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RetailerAcademy.iOS
{
	public partial class ViewController : BaseController, IWebServiceDelegate
	{
		private UIView activeview;
		private float scrollamount = 0.0f;
		// amount to scroll
		private float bottom = 0.0f;
		// bottom point
		private float offset = 130.0f;
		// extra offset
		private bool moveViewUp = false;
		string passwordString;

		public ViewController (IntPtr handle) : base (handle)
		{	
			
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Keyboard popup
			NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.DidShowNotification, KeyBoardUpNotification);

			// Keyboard Down
			NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.WillHideNotification, KeyBoardDownNotification);
		}

		private void KeyBoardUpNotification (NSNotification notification)
		{
			// get the keyboard size
			RectangleF r = (RectangleF)UIKeyboard.BoundsFromNotification (notification);

			// Find what opened the keyboard
			foreach (var view in this.backgroundView.Subviews) {
				if (view.IsFirstResponder)
					this.activeview = view;
			}
			try {
				// Bottom of the controller = initial position + height + offset      
				bottom = ((float)activeview.Frame.Y + (float)activeview.Frame.Height + offset);

				// Calculate how far we need to scroll
				scrollamount = (r.Height - ((float)View.Frame.Size.Height - bottom));

				// Perform the scrolling
				if (scrollamount > 0) {
					moveViewUp = true;
					ScrollTheView (moveViewUp);
				} else {
					moveViewUp = false;
				}
			} catch (Exception e) {
				Console.WriteLine (e);
			}
		}

		private void KeyBoardDownNotification (NSNotification notification)
		{
			if (moveViewUp) {
				ScrollTheView (false);
			}
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
//			var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
//			tap.AddTarget(
//				() => View.EndEditing(true));
//			View.AddGestureRecognizer(tap);

			//this.name.Text = "";
			this.password.Text = "";

			this.name.ShouldReturn += (textField) => {
				textField.ResignFirstResponder ();
				return true;
			};

			this.password.ShouldReturn += (textField) => {
				textField.ResignFirstResponder ();
				return true;
			};
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			NSNotificationCenter.DefaultCenter.RemoveObserver (UIKeyboard.DidShowNotification);
			// Keyboard Down
			NSNotificationCenter.DefaultCenter.RemoveObserver (UIKeyboard.WillHideNotification);
		}

		public override void DidReceiveMemoryWarning ()
		{		
			base.DidReceiveMemoryWarning ();		
			// Release any cached data, images, etc that aren't in use.		
		}

		private void ScrollTheView (bool move)
		{
			// scroll the view up or down
//			UIView.BeginAnimations (string.Empty, System.IntPtr.Zero);
//			UIView.SetAnimationDuration (0.3);

			UIView.Animate (0.3, () => {

				RectangleF frame = (RectangleF)backgroundView.Frame;

				if (move) {
					frame.Y -= scrollamount;
				} else {
					frame.Y += scrollamount;
					scrollamount = 0;
				}
			                                                                                                                                   
				backgroundView.Frame = frame;

			});
			//	UIView.CommitAnimations();
		}

		partial void loginBtn_TouchUpInside (CustomButton sender)
		{
			View.EndEditing (true);
			string username = this.name.Text;
			passwordString = this.password.Text;
			internetstatus = Reachability.InternetConnectionStatus ();

			if (string.IsNullOrEmpty (username)) {
				toast.makeToast (CommonSharedStrings.USERNAME_BLANK_MESSAGE, this);
			} else if (username.Length < 3) {
				toast.makeToast (CommonSharedStrings.USERNAME_INVALID_MESSAGE, this);
			} else if (string.IsNullOrEmpty (passwordString)) {
				toast.makeToast (CommonSharedStrings.PASSWORD_BLANK_MESSAGE, this);
			} else if (passwordString.Length < 8) {
				toast.makeToast (CommonSharedStrings.PASSWORD_INVALID_MESSAGE, this);
			} else if (internetstatus == NetworkStatus.NotReachable) {
				toast.makeToast (CommonSharedStrings.INTERNET_ERROR_MESSAGE, this);
			} else {
				LoginData dataObjects = new LoginData ();
				dataObjects.username = username;
				dataObjects.password = passwordString;
				string requestParameter = JsonConvert.SerializeObject (dataObjects);
				var jsonService = new JsonService ();
				jsonService.consumeService (CommonSharedStrings.LOGIN_URL, requestParameter, this);
				activity.startActivity (this);
			}
		}

		partial void forgotPwd_TouchUpInside (EmptyButton sender)
		{
			var storyBoard = UIStoryboard.FromName ("Main", null);
			var switchController = storyBoard.InstantiateViewController ("ForgotPasswordController"); 
			this.NavigationController.PushViewController (switchController, false);	
		}

		#region IWebServiceDelegate implementation

		public void onResponse (string response)
		{
			activity.stopActivity ();
			try {
				JObject responseObj = JObject.Parse (response);
				if (Convert.ToBoolean (responseObj.SelectToken ("status"))) {
					//navigate to next view
					var storyBoard = UIStoryboard.FromName ("Main", null);
					var switchController = storyBoard.InstantiateViewController ("DashboardController"); 
					this.NavigationController.PushViewController (switchController, false);

					NSUserDefaults.StandardUserDefaults.SetString (Convert.ToString (responseObj.SelectToken ("userid")), "USERID"); 
					NSUserDefaults.StandardUserDefaults.SetString (passwordString.ToString (), "PASSWORD"); 
					NSUserDefaults.StandardUserDefaults.Synchronize ();

				} else {
					//display toast message
					toast.makeToast (Convert.ToString (responseObj.SelectToken ("message")), this);
					this.name.Text = "";
					this.password.Text = "";
				}
			} catch (Exception e) {
				Console.WriteLine (e);
				toast.makeToast (CommonSharedStrings.STATUS_ERROR, this);
			}
		}

		#endregion
	}
}
