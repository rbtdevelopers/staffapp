// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RetailerAcademy.iOS
{
	[Register ("ChangePassword")]
	partial class ChangePassword
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton backButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomButton changePassword { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomTextField confirmPassword { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		GradientView containerView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomTextField currentPassword { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		CustomTextField newPassword { get; set; }

		[Action ("backButton_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void backButton_TouchUpInside (UIButton sender);

		[Action ("changePassword_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void changePassword_TouchUpInside (CustomButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (backButton != null) {
				backButton.Dispose ();
				backButton = null;
			}
			if (changePassword != null) {
				changePassword.Dispose ();
				changePassword = null;
			}
			if (confirmPassword != null) {
				confirmPassword.Dispose ();
				confirmPassword = null;
			}
			if (containerView != null) {
				containerView.Dispose ();
				containerView = null;
			}
			if (currentPassword != null) {
				currentPassword.Dispose ();
				currentPassword = null;
			}
			if (newPassword != null) {
				newPassword.Dispose ();
				newPassword = null;
			}
		}
	}
}
