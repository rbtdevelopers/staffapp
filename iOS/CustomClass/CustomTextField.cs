using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RetailerAcademy.iOS
{
	partial class CustomTextField : UITextField
	{
		private int horizontalPadding = 0;
		private int verticalPadding = 0;

		public CustomTextField (IntPtr handle) : base (handle)
		{
			this.Layer.CornerRadius = 5;
			this.Layer.BorderColor = UIColor.FromRGB(191, 191, 191).CGColor;
			this.Layer.BorderWidth = 1;
			this.TextColor = UIColor.FromRGB (153,153,153);
		}

		public override CoreGraphics.CGRect TextRect (CoreGraphics.CGRect forBounds)
		{
			horizontalPadding = 10;
			verticalPadding = 0;

			float xAxis = (float)forBounds.Location.X + horizontalPadding;
			float yAxis = (float)forBounds.Location.Y;
			float width = (float)forBounds.Size.Width - (horizontalPadding * 4);
			float height = (float)forBounds.Size.Height - (verticalPadding * 2);

			System.Drawing.RectangleF txtRect = new System.Drawing.RectangleF (xAxis, yAxis, width, height);
			return base.TextRect (txtRect);
		}

		public override CoreGraphics.CGRect RightViewRect (CoreGraphics.CGRect forBounds)
		{
			float xAxis = (float)forBounds.Location.X - 5;

			CoreGraphics.CGRect txtRect = new CoreGraphics.CGRect (xAxis, forBounds.Location.Y, forBounds.Size.Width, forBounds.Size.Height);

			return base.RightViewRect (txtRect);
		}

		public override CoreGraphics.CGRect EditingRect (CoreGraphics.CGRect forBounds)
		{
			return this.TextRect (forBounds);
		}
	}
}
