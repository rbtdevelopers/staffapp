using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreAnimation;
using CoreGraphics;

namespace RetailerAcademy.iOS
{
	partial class HeaderGradient : UIView
	{
		public HeaderGradient (IntPtr handle) : base (handle)
		{
		}

		public override void Draw (CoreGraphics.CGRect rect)
		{
			base.Draw (rect);
			CAGradientLayer gradient = new CAGradientLayer ();
			gradient.Frame = this.Bounds;
			gradient.Colors = new CGColor[]{ UIColor.FromRGB(3, 97, 197).CGColor, UIColor.FromRGB(0, 73, 150).CGColor };
			gradient.Locations = new Foundation.NSNumber[]{ 0.0, 0.9 };
			this.Layer.InsertSublayer (gradient, 0);
		}
	}
}
