﻿using System;
using UIKit;
using CoreGraphics;
using Foundation;
using System.Threading;
using System.Diagnostics;

namespace RetailerAcademy.iOS
{
	public class Toast : UIView
	{
		private string message = "";
		CustomLabel msgLbl = new CustomLabel ();
		private UIViewController currentController;
		public Toast ()
		{
			
		}

		private void OnTimerDone(object state)
		{
			try
			{
				InvokeOnMainThread (delegate {  

					hideToastMessage(); 
				});
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
			}
		}
	
		public void makeToast(string msg, UIViewController controller)
		{
			this.message = msg;
			this.currentController = controller;
			Timer myTimer = new Timer (OnTimerDone);
			myTimer.Change (2000, Timeout.Infinite);

			showToastMessage ();

		}

		public void showToastMessage()
		{
			CGRect mainFrame = UIScreen.MainScreen.Bounds;
			this.BackgroundColor = UIColor.FromRGB(1, 83, 169);
			this.Frame = new CGRect (10, mainFrame.Size.Height-100, mainFrame.Size.Width - 20, 50);
			this.Layer.CornerRadius = 5;
			this.Layer.MasksToBounds = true;
			this.Layer.BorderColor = UIColor.White.CGColor;
			this.Layer.BorderWidth = 1;

			msgLbl.Frame = new CGRect (10, 0, this.Frame.Size.Width-20, 50);
			msgLbl.TextColor = UIColor.White;
			msgLbl.Text = this.message;
			msgLbl.TextAlignment = UITextAlignment.Center;
			var size = this.message.StringSize (msgLbl.Font, new CGSize (this.Frame.Size.Width-20, 500), UILineBreakMode.WordWrap);

			var msgLblHeight = size.Height;

			if (msgLblHeight < 50) {
				msgLblHeight = 50;
			}

			this.Frame = new CGRect (10, mainFrame.Size.Height-100, mainFrame.Size.Width - 20, msgLblHeight+10);
			msgLbl.Frame = new CGRect (10, 0, this.Frame.Size.Width - 20, msgLblHeight+10);
			AddSubview (msgLbl);

			this.currentController.View.AddSubview (this);
		}

		public void hideToastMessage()
		{
			this.RemoveFromSuperview ();
		}
	}

}

