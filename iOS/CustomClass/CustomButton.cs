using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RetailerAcademy.iOS
{
	partial class CustomButton : UIButton
	{
		public CustomButton (IntPtr handle) : base (handle)
		{
			
		}

		public override void Draw (CoreGraphics.CGRect rect)
		{
			base.Draw (rect);

			var fontSize = Font.PointSize;
			Font = UIFont.FromName("Oswald-Regular", fontSize);

			this.Layer.CornerRadius = 5;
			this.Layer.MasksToBounds = true;
			this.Layer.BorderColor = UIColor.FromRGB (0, 159, 227).CGColor;
			this.Layer.BorderWidth = 1;
		}
	}
}
