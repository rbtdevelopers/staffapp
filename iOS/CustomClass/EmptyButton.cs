using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace RetailerAcademy.iOS
{
	partial class EmptyButton : UIButton
	{
		public EmptyButton (IntPtr handle) : base (handle)
		{
		}

		public override void Draw (CoreGraphics.CGRect rect)
		{
			base.Draw (rect);

			var fontSize = Font.PointSize;
			Font = UIFont.FromName("Oswald-Regular", fontSize);

		}
	}
}
