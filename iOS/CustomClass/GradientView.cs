using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreAnimation;
using CoreGraphics;

namespace RetailerAcademy.iOS
{
	partial class GradientView : UIView
	{
		public GradientView (IntPtr handle) : base (handle)
		{
		}

		public override void Draw (CoreGraphics.CGRect rect)
		{
			base.Draw (rect);
			CAGradientLayer gradient = new CAGradientLayer ();
			gradient.Frame = this.Bounds;
			gradient.Colors = new CGColor[]{ UIColor.White.CGColor, UIColor.FromRGB(237, 237, 237).CGColor };
			gradient.Locations = new Foundation.NSNumber[]{ 0.2, 1.0 };
			this.Layer.InsertSublayer (gradient, 0);
		}
	}
}
