﻿using System;
using UIKit;
using CoreGraphics;

namespace RetailerAcademy.iOS
{
	public class ActivityLoader: UIView
	{
		public UIActivityIndicatorView activityView;
		private UIViewController currentController;
		CustomLabel loaderLbl = new CustomLabel ();

		public ActivityLoader ()
		{	
			
		}

		public void startActivity(UIViewController current)
		{
			this.currentController = current;
			CGRect mainFrame = UIScreen.MainScreen.Bounds;
			UIView view = new UIView (mainFrame);

			this.Frame = new CGRect (0, 0, 200, 40);
			this.Center = view.Center;
			this.Layer.CornerRadius = 5;
			this.BackgroundColor = UIColor.White;
			this.Layer.BorderColor = UIColor.FromRGB (1, 83, 169).CGColor;
			this.Layer.BorderWidth = 1;

			activityView = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray);
			activityView.Frame = new CGRect (10, 5, 30, 30);
			AddSubview (activityView);				

			loaderLbl.Frame = new CGRect (50, 0, 200, 40);
			loaderLbl.TextColor = UIColor.FromRGB (153, 153, 153);
			loaderLbl.Text = CommonSharedStrings.LOADING_TEXT;
			AddSubview (loaderLbl);

			this.currentController.View.AddSubview (this);
			activityView.StartAnimating ();
		}

		public void stopActivity()
		{
			activityView.StopAnimating ();
			this.RemoveFromSuperview ();
		}

	}
}

