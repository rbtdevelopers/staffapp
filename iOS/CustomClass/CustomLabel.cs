using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
                 
namespace RetailerAcademy.iOS
{
	partial class CustomLabel : UILabel
	{
		public CustomLabel(IntPtr handle) : base (handle)
		{

		}
		
		public CustomLabel ()
		{
			var fontSize = Font.PointSize;
			Font = UIFont.FromName("Oswald-Regular", fontSize);
			this.Lines = 0;

			this.LineBreakMode = UILineBreakMode.WordWrap;
			this.SizeToFit();
		}

		public override void AwakeFromNib ()
		{
			base.AwakeFromNib ();
			var fontSize = Font.PointSize;
			Font = UIFont.FromName("Oswald-Regular", fontSize);
		}
	}
}
