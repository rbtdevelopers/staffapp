using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;

namespace RetailerAcademy.Droid.Source.Adapters
{
	class MessageListAdapter: BaseAdapter<CommunicationMessage>
	{
		public List<CommunicationMessage> messageListItems;
		public List<CommunicationMessage> tempMessageListItems;
		private Context context;
		private LayoutInflater inflater = null;
		public bool isSentList = false;
		public int setPosition = -1;
		private TextView moveToArchiveInboxTV;
		private RelativeLayout messageItemLayout;
		private int resource;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.Source.Adapters.MessageListAdapter"/> class, and loading the default image with Universal image loader.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="messageListItems">Message list items.</param>
		public MessageListAdapter (Context context, int resource, List<CommunicationMessage> messageListItems)
		{        
			this.context = context;
			this.messageListItems = messageListItems;
			tempMessageListItems = new List<CommunicationMessage> ();
			tempMessageListItems.AddRange (messageListItems);
			this.resource = resource;
			inflater = LayoutInflater.From (context);
		}

		/// <summary>
		/// Searchs the list item.
		/// </summary>
		/// <param name="searchCharacter">Search character.</param>
		public void SearchListItem (string searchCharacter){	
			messageListItems.Clear ();			
			if (!string.IsNullOrEmpty (searchCharacter)) {
				foreach (CommunicationMessage msg in tempMessageListItems) {
					if (msg.subject.ToLower ().Contains (searchCharacter.ToLower ()) || msg.body.ToLower ().Contains (searchCharacter.ToLower ()) || msg.senderNickName.ToLower ().Contains (searchCharacter.ToLower ())) {
						messageListItems.Add (msg);
					}
				}
			} else {
				messageListItems.AddRange (tempMessageListItems);
			}
			NotifyDataSetChanged ();
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) { // no view to re-use, create new
				holder = new ViewHolder ();
				convertView = inflater.Inflate (resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
				holder.titleMsgTVId = convertView.FindViewById<TextView> (Resource.Id.titleMsgTVId);
				holder.senderMsgTVId = convertView.FindViewById<TextView> (Resource.Id.senderMsgTVId);
				holder.subMsgTVId = convertView.FindViewById<TextView> (Resource.Id.subMsgTVId);
				holder.msgDotIVId = convertView.FindViewById<TextView> (Resource.Id.msgDotIVId);
				holder.titleMsgTVId.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.subMsgTVId.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.senderMsgTVId.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				convertView.Tag = holder;
			} else {
				holder = (ViewHolder)convertView.Tag;
			}
			try {
				if (messageListItems != null && messageListItems.Count > 0) {
					CommunicationMessage item = messageListItems [position];
					holder.titleMsgTVId.Text = item.subject;
					holder.subMsgTVId.Text = item.body;
					if (!isSentList) {
						holder.senderMsgTVId.Text = item.senderNickName.ToUpper();
						if (item.messageIsRead) {
							holder.msgDotIVId.Visibility = ViewStates.Invisible;
						} else {
							holder.msgDotIVId.Visibility = ViewStates.Visible;
						}
					} else {
						holder.senderMsgTVId.Text = item.recipientName.ToUpper();
						holder.msgDotIVId.Visibility = ViewStates.Invisible;
					}
					moveToArchiveInboxTV = (TextView)convertView.FindViewById (Resource.Id.moveToArchiveInboxTV);
					messageItemLayout = (RelativeLayout)convertView.FindViewById (Resource.Id.messageItemLayout);
					if (position == setPosition) {
						messageItemLayout.Visibility = ViewStates.Gone;
						moveToArchiveInboxTV.Visibility = ViewStates.Visible;
					} else {
						messageItemLayout.Visibility = ViewStates.Visible;
						moveToArchiveInboxTV.Visibility = ViewStates.Gone;
					}         
				}
			} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
			} catch (InvalidCastException) {
			} catch (Java.Lang.Exception) {
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object
		{
			public TextView titleMsgTVId, subMsgTVId, msgDotIVId, senderMsgTVId;
		}

		/// <summary>
		/// Removes the item.
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="userMsgId">User message identifier.</param>
		public void removeItem (int position, int userMsgId)
		{
			messageListItems.RemoveAt (position);
			for (int arrPos = 0; arrPos < tempMessageListItems.Count; arrPos++) {
				if (tempMessageListItems[arrPos].userMessageID == userMsgId) {
					tempMessageListItems.RemoveAt (arrPos);
				}
			}
			setPosition = -1;
			NotifyDataSetChanged ();
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.Source.Adapters.MessageListAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override CommunicationMessage this [int position] {
			get { return this.messageListItems [position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count {
			get { return messageListItems.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId (int position)
		{
			return position;
		}
	}
}