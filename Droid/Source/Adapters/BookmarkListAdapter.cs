﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Graphics;
using Android.Content;
using System.Text.RegularExpressions;

namespace RetailerAcademy.Droid
{
	public class BookmarkListAdapter : BaseAdapter<BookmarkListDto>
	{
		public List<BookmarkListDto> bookmarkListItems;
		private Context context;
		private LayoutInflater inflater = null;
		private int resource;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.BookmarkListAdapter"/> class, and loading the default image with Universal image loader.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="bookmarkListItems">Bookmark list items.</param>
		public BookmarkListAdapter(Context context, int resource, List<BookmarkListDto> bookmarkListItems)
		{        
			this.context = context;
			this.bookmarkListItems = bookmarkListItems;
			this.resource = resource;
			inflater = LayoutInflater.From(context);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) // no view to re-use, create new
			{
				holder = new ViewHolder();
				convertView = inflater.Inflate(resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset(context.Assets, "Fonts/oswald_regular.ttf");
				holder.bookmarkTitleTV = convertView.FindViewById<TextView>(Resource.Id.bookmarkTitleTV);
				holder.bookmarkUrlTV = convertView.FindViewById<TextView>(Resource.Id.bookmarkUrlTV);
				holder.bookmarkTitleTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				holder.bookmarkUrlTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				convertView.Tag = holder;
			}
			else
			{
				holder = (ViewHolder)convertView.Tag;
			}
			try{
			if (bookmarkListItems != null && bookmarkListItems.Count > 0) {
				var item = bookmarkListItems [position];
				var contentFileName = Regex.Replace (item.bookmarkName, @"[^0-9a-zA-Z .]+", " ");
				holder.bookmarkTitleTV.Text = contentFileName;
				holder.bookmarkUrlTV.Text = item.bookmarkUrl;  
				BookmarkListDto bookmarkListDto = item;
				holder.bookmarkTitleTV.Tag = bookmarkListDto;
			}
			} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
			} catch (InvalidCastException) {
			} catch (Java.Lang.Exception) {
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object 
		{
			public TextView bookmarkTitleTV, bookmarkUrlTV;
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.BookmarkListAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override BookmarkListDto this[int position]
		{
			get { return this.bookmarkListItems[position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count
		{
			get { return bookmarkListItems.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId(int position)
		{
			return position;
		}
	}
}