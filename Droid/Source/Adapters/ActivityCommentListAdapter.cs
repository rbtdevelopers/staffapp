﻿using System;
using Android.Widget;
using Android.App;
using System.Collections.Generic;
using Android.Views;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Com.Nostra13.Universalimageloader.Core;
using Android.Content;
using Android.Text.Util;

namespace RetailerAcademy.Droid
{
	public class ActivityCommentListAdapter : BaseAdapter<CommentsCls>, View.IOnClickListener
	{
		public List<CommentsCls> activityStreamCommentListItems;
		private Context context;
		private LayoutInflater inflater = null;
		private int resource;
		public int setPosition = -1;
		private ImageLoader imageLoader;
		private DisplayImageOptions options;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ActivityCommentListAdapter"/> class, and loading the default image with Universal image loader.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="activityStreamCommentListItems">Activity stream comment list items.</param>
		public ActivityCommentListAdapter (Context context, int resource, List<CommentsCls> activityStreamCommentListItems)
		{        
			this.context = context;
			this.activityStreamCommentListItems = activityStreamCommentListItems;
			this.resource = resource;
			inflater = LayoutInflater.From (context);
			imageLoader = ImageLoader.Instance;
			options = CommonMethods.ReturnDisplayOptions (Resource.Drawable.no_image);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) { // no view to re-use, create new
				holder = new ViewHolder ();
				convertView = inflater.Inflate (resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
				holder.userNameTV = convertView.FindViewById<TextView> (Resource.Id.userNameTV);
				holder.userActivityDescTV = convertView.FindViewById<TextView> (Resource.Id.activityDescTV);
				holder.dateFormatTV = convertView.FindViewById<TextView> (Resource.Id.dateTV);
				holder.userImageIV = convertView.FindViewById<ImageView> (Resource.Id.userImageView);
				holder.removeBGTV = convertView.FindViewById<TextView> (Resource.Id.moveToArchiveInboxTV);
				holder.commentItemRL = convertView.FindViewById<RelativeLayout> (Resource.Id.itemRLId);
				holder.commentAttachmentIV = convertView.FindViewById<ImageView> (Resource.Id.commentAttachmentIV);
				holder.userNameTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.userActivityDescTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.dateFormatTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.commentAttachmentIV.SetOnClickListener (this);
				convertView.Tag = holder;
			} else {
				holder = (ViewHolder)convertView.Tag;
			}
			if (position == setPosition) {
				holder.commentItemRL.Visibility = ViewStates.Gone;
				holder.removeBGTV.Visibility = ViewStates.Visible;
			} else {
				holder.commentItemRL.Visibility = ViewStates.Visible;
				holder.removeBGTV.Visibility = ViewStates.Gone;
			}
			try {
				CommentsCls item = null;
				if (activityStreamCommentListItems != null && activityStreamCommentListItems.Count > 0) {
					item = activityStreamCommentListItems [position];
					holder.userNameTV.Text = item.userfullname;
					holder.userActivityDescTV.Text = item.Comment;
					holder.dateFormatTV.Text = item.dateofcomment;
					var userImageUrl = item.userImg;
					var commentAttachment = item.commentAttachmentImg;
					holder.commentAttachmentIV.Tag = activityStreamCommentListItems [position].commentAttachmentImg;
					if (!string.IsNullOrEmpty (userImageUrl)) {
						imageLoader.DisplayImage (userImageUrl, holder.userImageIV, options);
					} else {
						holder.userImageIV.SetImageResource (Resource.Drawable.no_image);
					} 
					if (!string.IsNullOrEmpty (commentAttachment)) {
						holder.commentAttachmentIV.Visibility = ViewStates.Visible;
						imageLoader.DisplayImage (commentAttachment, holder.commentAttachmentIV, options);
					} else {
						holder.commentAttachmentIV.Visibility = ViewStates.Gone;
					}
				}
			} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
			} catch (InvalidCastException) {
			} catch (Java.Lang.Exception) {
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object
		{
			public TextView userNameTV, userActivityDescTV, dateFormatTV, removeBGTV;
			public ImageView userImageIV, commentAttachmentIV;
			public RelativeLayout commentItemRL;
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.ActivityCommentListAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override CommentsCls this [int position] {
			get { return this.activityStreamCommentListItems [position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count {
			get { return activityStreamCommentListItems.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId (int position)
		{
			return position;
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				var id = v.Id;
				switch (id) {
				case Resource.Id.commentAttachmentIV:
					ImageView activityImage = (ImageView)v.FindViewById (Resource.Id.commentAttachmentIV);
					Intent intent = new Intent (context, typeof(ImageDisplayActivity));
					intent.PutExtra ("activityAttachment", (string)activityImage.Tag);
					context.StartActivity (intent);
					break;
				}
			} catch (Exception) {				
			}
		}
	}
}