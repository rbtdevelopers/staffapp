﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Graphics;
using Android.Content;

namespace RetailerAcademy.Droid
{
	public class ContentTagListAdapter : BaseAdapter<ContentTags>
	{
		public List<ContentTags> contentTagListItems;
		private Context context;
		private LayoutInflater inflater = null;
		private int resource;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ContentTagListAdapter"/> class, and loading the default image with Universal image loader.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="contentTagListItems">Content tag list items.</param>
		public ContentTagListAdapter(Context context, int resource, List<ContentTags> contentTagListItems)
		{        
			this.context = context;
			this.contentTagListItems = contentTagListItems;
			this.resource = resource;
			inflater = LayoutInflater.From(context);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) // no view to re-use, create new
			{
				holder = new ViewHolder();
				convertView = inflater.Inflate(resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset(context.Assets, "Fonts/oswald_regular.ttf");
				holder.contentTagNameTV = convertView.FindViewById<TextView>(Resource.Id.contentTagNameTV);
				holder.contentFileCountTV = convertView.FindViewById<TextView>(Resource.Id.contentFileCountTV);
				holder.contentTagNameTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				holder.contentFileCountTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				convertView.Tag = holder;
			}
			else
			{
				holder = (ViewHolder)convertView.Tag;
			}
			var item = contentTagListItems[position];

			holder.contentTagNameTV.Text = item.tagName;
			holder.contentFileCountTV.Text = item.fileCount +"";      
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object 
		{
			public TextView contentTagNameTV, contentFileCountTV;
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.ContentTagListAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override ContentTags this[int position]
		{
			get { return this.contentTagListItems[position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count
		{
			get { return contentTagListItems.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId(int position)
		{
			return position;
		}
	}
}