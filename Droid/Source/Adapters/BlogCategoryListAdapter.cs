﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.ContentDTO;
using Android.Graphics;

namespace RetailerAcademy.Droid
{
	public class BlogCategoryListAdapter : ArrayAdapter<BlogDto>
	{
		public List<BlogDto> blogCategoryListItems;
		private Context context;
		private LayoutInflater inflater = null;
		private Typeface OswaldRegular;
		private int resource;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.BlogCategoryListAdapter"/> class, and loading the default image with Universal image loader.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="blogCategoryListItems">Blog category list items.</param>
		public BlogCategoryListAdapter (Context context, int resource, List<BlogDto> blogCategoryListItems) : base (context, resource, blogCategoryListItems)
		{
			this.context = context;
			this.blogCategoryListItems = blogCategoryListItems;
			this.resource = resource;
			OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
			inflater = LayoutInflater.From (context);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) { // no view to re-use, create new
				holder = new ViewHolder ();
				convertView = inflater.Inflate (resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
				holder.itemNameTV = convertView.FindViewById<TextView> (Resource.Id.itemTVId);
				holder.readUnreadDotTV = convertView.FindViewById<TextView> (Resource.Id.contentDotIVId);
				holder.itemNameTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				convertView.Tag = holder;
			} else {
				holder = (ViewHolder)convertView.Tag;
			}
			try {
				BlogDto item = null;
				if (blogCategoryListItems != null && blogCategoryListItems.Count > 0) {
					item = blogCategoryListItems [position];
					holder.itemNameTV.Text = item.categoryName;
					holder.itemNameTV.Tag = item;
					if (item.hasviewed) {
						holder.readUnreadDotTV.Visibility = ViewStates.Invisible;
					} else {
						holder.readUnreadDotTV.Visibility = ViewStates.Visible;
					}
				}
			} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
			} catch (InvalidCastException) {
			} catch (Java.Lang.Exception) {
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object
		{
			public TextView readUnreadDotTV, itemNameTV;
		}
	}
}