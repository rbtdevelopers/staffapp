﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.ContentDTO;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Com.Nostra13.Universalimageloader.Core;

namespace RetailerAcademy.Droid
{		
	public class LeaderBoardUserListAdapter : ArrayAdapter<string>
	{
		private Context context;
		private LayoutInflater inflater = null;
		private int resource;
		private Typeface OswaldRegular;
		private Dictionary<string, List<LeaderBoardUserDto>> dictionary;
		private List<LeaderBoardUserDto> uniqueLeaderboardList;
		private ImageLoader imageLoader;
		private DisplayImageOptions options;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.LeaderBoardUserListAdapter"/> class, and loading the default image with Universal image loader.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="keysList">Keys list.</param>
		public LeaderBoardUserListAdapter (Context context, int resource, List<string> keysList)
			: base(context, resource, keysList)
		{
			this.context = context;
			this.resource = resource;
			inflater = LayoutInflater.From (context);
			OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
			dictionary = new Dictionary<string, List<LeaderBoardUserDto>> ();
			imageLoader = ImageLoader.Instance;
			options = CommonMethods.ReturnDisplayOptions(Resource.Drawable.no_image);
		}

		/// <summary>
		/// Sets the data.
		/// </summary>
		/// <param name="dictionary">Dictionary.</param>
		public void setData(Dictionary<string, List<LeaderBoardUserDto>> dictionary){
			this.dictionary = dictionary; 
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) { // no view to re-use, create new
				holder = new ViewHolder ();
				convertView = inflater.Inflate (resource, null); 
				holder.headerTV = convertView.FindViewById<TextView> (Resource.Id.headerTVId);
				holder.containerlayout = convertView.FindViewById<LinearLayout> (Resource.Id.conatiner_layout);
				convertView.Tag = holder;
			} else {
				holder = (ViewHolder)convertView.Tag;
			}
			string key = GetItem(position);
			dictionary.TryGetValue (key, out uniqueLeaderboardList);
			holder.headerTV.Text = key;
			holder.headerTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			if(holder.headerTV.Tag == null ||(Convert.ToInt32(holder.headerTV.Tag) != position))
			{
					renderData (holder.containerlayout, uniqueLeaderboardList);
					holder.headerTV.Tag = position;
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object
		{
			public TextView headerTV;
			public LinearLayout containerlayout;
		}

		/// <summary>
		/// Renders the data.
		/// </summary>
		/// <param name="containerLayout">Container layout.</param>
		/// <param name="list">List.</param>
		private void renderData(LinearLayout containerLayout, List<LeaderBoardUserDto> list){
			int sno = 0;
			foreach (LeaderBoardUserDto dto in list) {
				View view = inflater.Inflate (Resource.Layout.LeaderBoardUserInnerListItem, null); 
				TextView numberTV = view.FindViewById<TextView> (Resource.Id.numberTV);
				TextView userNameTV = view.FindViewById<TextView> (Resource.Id.userNameTV);
				TextView pointsTV = view.FindViewById<TextView> (Resource.Id.pointsTV);
				ImageView userImgView = view.FindViewById<ImageView> (Resource.Id.userImageView);
				sno = sno + 1;
				numberTV.Text = sno +"";
				userNameTV.Text = dto.userName;
				pointsTV.Text = dto.points+" points";

				/* Assign Font style */
				numberTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				userNameTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				pointsTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				containerLayout.AddView (view);

				/* Get image */
				if (!string.IsNullOrEmpty(dto.profileImageUrl))
				{
					imageLoader.DisplayImage(dto.profileImageUrl, userImgView, options);
				}
				else
				{
					userImgView.SetImageResource(Resource.Drawable.no_image);
				}
			}
		}
	}
}