﻿using System;
using Android.Widget;
using Android.App;
using System.Collections.Generic;
using Android.Views;
using Android.Graphics;
using Android.Content;
using System.Text.RegularExpressions;

namespace RetailerAcademy.Droid
{
	public class ContentFolderListAdapter : BaseAdapter<ContentFolderCls>
	{
		public List<ContentFolderCls> contentFolderListItems;
		private Context context;
		private LayoutInflater inflater = null;
		private int resource;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ContentFolderListAdapter"/> class, and loading the default image with Universal image loader.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="contentFolderListItems">Content folder list items.</param>
		public ContentFolderListAdapter (Context context, int resource, List<ContentFolderCls> contentFolderListItems)
		{        
			this.context = context;
			this.contentFolderListItems = contentFolderListItems;
			this.resource = resource;
			inflater = LayoutInflater.From (context);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) { // no view to re-use, create new
				holder = new ViewHolder ();
				convertView = inflater.Inflate (resource, null); 
				Typeface OswaldLight = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_light.ttf");
				Typeface OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
				holder.folderTitleTVId = convertView.FindViewById<TextView> (Resource.Id.folderTitleTVId);
				holder.folderDescTVId = convertView.FindViewById<TextView> (Resource.Id.folderDescTVId);
				holder.contentDotIVId = convertView.FindViewById<TextView> (Resource.Id.contentDotIVId);
				holder.folderImageView = convertView.FindViewById<ImageView> (Resource.Id.folderImageView);
				holder.folderTitleTVId.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.folderDescTVId.SetTypeface (OswaldLight, TypefaceStyle.Normal);
				convertView.Tag = holder;
			} else {
				holder = (ViewHolder)convertView.Tag;
			}
			try {
				if (contentFolderListItems != null && contentFolderListItems.Count > 0) {
					ContentFolderCls item = contentFolderListItems [position];
					var contentFileName = Regex.Replace (item.foldername, @"[^0-9a-zA-Z .]+", " ");
					holder.folderTitleTVId.Text = contentFileName;
					holder.folderDescTVId.Text = item.folderDesc;
					if (item.folderid != 0) {
						holder.folderImageView.SetImageResource (Resource.Drawable.content_folder);
					} else {
						holder.folderImageView.SetImageResource (Resource.Drawable.my_content_folder);
					}
					if (item.isFolderRead) {
						holder.contentDotIVId.Visibility = ViewStates.Invisible;
					} else {
						holder.contentDotIVId.Visibility = ViewStates.Visible;
					}
				}
			} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
			} catch (InvalidCastException) {
			} catch (Java.Lang.Exception) {
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object
		{
			public TextView folderTitleTVId, folderDescTVId, contentDotIVId;
			public ImageView folderImageView;
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.ContentFolderListAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override ContentFolderCls this [int position] {
			get { return this.contentFolderListItems [position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count {
			get { return contentFolderListItems.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId (int position)
		{
			return position;
		}
	}
}