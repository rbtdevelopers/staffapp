﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.ContentDTO;
using Android.Support.V4.View;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;

namespace RetailerAcademy.Droid
{
	class LeaderBoardAdapter : ArrayAdapter<LeaderBoardDto>
	{
		public List<LeaderBoardDto> listItems;
		private Context context;
		private LayoutInflater inflater = null;
		private Typeface OswaldRegular;
		private int resource;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.LeaderBoardAdapter"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="listItems">List items.</param>
		public LeaderBoardAdapter(Context context,int resource, List<LeaderBoardDto> listItems) : base(context,Resource.Layout.LeaderBoardListItem ,listItems)
		{
			this.context = context;
			this.listItems = listItems;
			this.resource = resource;
			OswaldRegular = Typeface.CreateFromAsset(context.Assets, "Fonts/oswald_regular.ttf");
			inflater = LayoutInflater.From(context);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			convertView = inflater.Inflate(resource, null);
			TextView itemNameTV = (TextView)convertView.FindViewById (Resource.Id.itemTVId);
			itemNameTV.Text = listItems [position].leaderBoardName;
			itemNameTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
			itemNameTV.Tag = listItems [position];
			return convertView;
		}
	}
}

