using System;
using Android.Content;
using System.Collections.Generic;
using Android.Widget;
using Android.Views;
using Android.Graphics;
using Android.Views.Animations;
using Android.App;
using System.Linq;
using RetailerAcademy.Droid.Source.ContentDTO;

namespace RetailerAcademy.Droid
{
	public class QuizExpandableListAdapter : BaseExpandableListAdapter, IFilterable
	{
		private Context context;
		public List<string> expandableListTitle;
		public Dictionary<string, string> expandableListDetail;
		private Typeface OswaldRegular;
		public Filter filter;
		string[] matchItems;
		public List<QuizCategoryDto> quizCategoryResponseList;
		private IQuizCategoryPosition iQuizCategoryPosition;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.QuizExpandableListAdapter"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="expandableListTitle">Expandable list title.</param>
		/// <param name="expandableListDetail">Expandable list detail.</param>
		/// <param name="quizCategoryResponseList">Quiz category response list.</param>
		/// <param name="iQuizCategoryPosition">I quiz category position.</param>
		public QuizExpandableListAdapter(Context context, List<string> expandableListTitle, Dictionary<string, string> expandableListDetail, List<QuizCategoryDto> quizCategoryResponseList, IQuizCategoryPosition iQuizCategoryPosition)
		{
			this.context = context;
			this.expandableListTitle = expandableListTitle;
			this.expandableListDetail = expandableListDetail;
			this.matchItems = expandableListTitle.ToArray();
			this.quizCategoryResponseList = quizCategoryResponseList;
			this.iQuizCategoryPosition = iQuizCategoryPosition;
			filter = new SuggestionsFilter(this);
			OswaldRegular = Typeface.CreateFromAsset(context.Assets, "Fonts/oswald_regular.ttf");
		}

		/// <param name="groupPosition">the position of the group that the child resides in</param>
		/// <param name="childPosition">the position of the child with respect to other
		///  children in the group</param>
		/// <summary>
		/// Gets the data associated with the given child within the given group.
		/// </summary>
		/// <returns>To be added.</returns>
		public override Java.Lang.Object GetChild(int groupPosition, int childPosition)
		{
			string value;
			expandableListDetail.TryGetValue(this.expandableListTitle[groupPosition], out value);
			return value;
		}

		/// <param name="groupPosition">the position of the group that contains the child</param>
		/// <param name="childPosition">the position of the child within the group for which
		///  the ID is wanted</param>
		/// <summary>
		/// Gets the ID for the given child within the given group.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetChildId(int groupPosition, int childPosition)
		{
			return childPosition;
		}

		/// <param name="groupPosition">the position of the group that contains the child</param>
		/// <param name="childPosition">the position of the child (for which the View is
		///  returned) within the group</param>
		/// <param name="isLastChild">Whether the child is the last child within the group</param>
		/// <summary>
		/// Gets the child view.
		/// </summary>
		/// <returns>The child view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override Android.Views.View GetChildView(int groupPosition, int childPosition, bool isLastChild, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			string expandedListText = (string)GetChild(groupPosition, childPosition);
			if (convertView == null)
			{
				LayoutInflater layoutInflater = (LayoutInflater)this.context.GetSystemService(Context.LayoutInflaterService);
				convertView = layoutInflater.Inflate(Resource.Layout.QuizCategoryChildListItem, null);
			}
			TextView expandedListTextView = (TextView)convertView.FindViewById(Resource.Id.childListItemTV);
			expandedListTextView.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
			expandedListTextView.Text = expandedListText;
			expandedListTextView.Click	+= delegate 
			{	
				iQuizCategoryPosition.GetQuizCategoryPosition(groupPosition);
			};
			return convertView;
		}

		/// <param name="groupPosition">the position of the group for which the children
		///  count should be returned</param>
		/// <summary>
		/// Gets the number of children in a specified group.
		/// </summary>
		/// <returns>To be added.</returns>
		public override int GetChildrenCount(int groupPosition)
		{
			string checkValue;
			string keyName = expandableListTitle[groupPosition].ToString();
			expandableListDetail.TryGetValue(keyName, out checkValue);
			if (!string.IsNullOrEmpty(checkValue))
				return 1;
			else
				return 0;
		}

		/// <param name="groupPosition">the position of the group</param>
		/// <summary>
		/// Gets the data associated with the given group.
		/// </summary>
		/// <returns>To be added.</returns>
		public override Java.Lang.Object GetGroup(int groupPosition)
		{
			return this.expandableListTitle[groupPosition];
		}

		/// <param name="groupPosition">the position of the group for which the ID is wanted</param>
		/// <summary>
		/// Gets the ID for the group at the given position.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetGroupId(int groupPosition)
		{
			return groupPosition;
		}

		/// <param name="groupPosition">the position of the group for which the View is
		///  returned</param>
		/// <param name="isExpanded">whether the group is expanded or collapsed</param>
		/// <summary>
		/// Gets the group view.
		/// </summary>
		/// <returns>The group view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override Android.Views.View GetGroupView(int groupPosition, bool isExpanded, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			string listTitle = (string)GetGroup(groupPosition);
			if (convertView == null)
			{
				LayoutInflater layoutInflater = (LayoutInflater)this.context.GetSystemService(Context.LayoutInflaterService);
				convertView = layoutInflater.Inflate(Resource.Layout.QuizCategoryListItem, null);
			}
			TextView listTitleTextView = (TextView)convertView.FindViewById(Resource.Id.quizCategoryTextTVId);
			RelativeLayout listTitleBackgroundRL = (RelativeLayout)convertView.FindViewById (Resource.Id.listTitleBackgroundRL);
			TextView readUnreadDotTV = convertView.FindViewById<TextView>(Resource.Id.contentDotIVId);
			var rightArrow = (ImageView)convertView.FindViewById(Resource.Id.rightIVId);
			var downArrow = (ImageView)convertView.FindViewById(Resource.Id.downIVID);
			listTitleTextView.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
			listTitleTextView.Text = listTitle;
			if (isExpanded)
			{
				rightArrow.Visibility = ViewStates.Gone;
				downArrow.Visibility = ViewStates.Visible;
			}
			else
			{
				downArrow.Visibility = ViewStates.Gone;
				rightArrow.Visibility = ViewStates.Visible;
			}
			if (quizCategoryResponseList != null && quizCategoryResponseList.Count > 0) {
				var quizResponseListObj = quizCategoryResponseList [groupPosition];
				if (quizResponseListObj.hasViewed) 
				{
					readUnreadDotTV.Visibility = ViewStates.Invisible;
				} 
				else 
				{
					readUnreadDotTV.Visibility = ViewStates.Visible;
				}
				if (quizResponseListObj.hasattempted)
				{
					listTitleBackgroundRL.SetBackgroundColor (Color.ParseColor("#F2F2F2"));
				} 
				else 
				{
					listTitleBackgroundRL.SetBackgroundColor (Color.ParseColor("#E7EFDB"));
				}
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object 
		{
			public TextView readUnreadDotTV, itemNameTV;
		}

		/// <summary>
		/// Gets the number of groups.
		/// </summary>
		/// <value>To be added.</value>
		public override int GroupCount
		{
			get { return this.expandableListTitle.Count; }
		}

		/// <summary>
		/// Indicates whether the child and group IDs are stable across changes to the
		///  underlying data.
		/// </summary>
		/// <value>To be added.</value>
		public override bool HasStableIds
		{
			get { return false; }
		}

		/// <param name="groupPosition">the position of the group that contains the child</param>
		/// <param name="childPosition">the position of the child within the group</param>
		/// <summary>
		/// Whether the child at the specified position is selectable.
		/// </summary>
		/// <returns>To be added.</returns>
		public override bool IsChildSelectable(int groupPosition, int childPosition)
		{
			return true;
		}

		/// <summary>
		/// Resets the search.
		/// </summary>
		public void ResetSearch()
		{
			matchItems = expandableListTitle.ToArray();
			NotifyDataSetChanged();
		}

		/// <summary>
		/// Suggestions filter.
		/// </summary>
		class SuggestionsFilter : Filter
		{
			readonly QuizExpandableListAdapter _adapter;

			/// <summary>
			/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.QuizExpandableListAdapter+SuggestionsFilter"/> class.
			/// </summary>
			/// <param name="adapter">Adapter.</param>
			public SuggestionsFilter(QuizExpandableListAdapter adapter)
				: base()
			{
				_adapter = adapter;
			}

			/// <param name="constraint">the constraint used to filter the data</param>
			/// <summary>
			/// Filters the search list item from the expandable list view and displays the same.
			/// </summary>
			/// <returns>To be added.</returns>
			protected override Filter.FilterResults PerformFiltering(Java.Lang.ICharSequence constraint)
			{
				FilterResults results = new FilterResults();
				if (!String.IsNullOrEmpty(constraint.ToString()))
				{
					var searchFor = constraint.ToString();
					var matchList = new List<string>();

					var matches =
						from i in _adapter.expandableListTitle
							where i.IndexOf(searchFor, StringComparison.InvariantCultureIgnoreCase) >= 0
						select i;

					foreach (var match in matches)
					{
						matchList.Add(match);
					}

					_adapter.matchItems = matchList.ToArray();

					Java.Lang.Object[] matchObjects;
					matchObjects = new Java.Lang.Object[matchList.Count];
					for (int i = 0; i < matchList.Count; i++)
					{
						matchObjects[i] = new Java.Lang.String(matchList[i]);
					}

					results.Values = matchObjects;
					results.Count = matchList.Count;
				}
				else
				{
					_adapter.ResetSearch();
				}
				return results;
			}

			/// <summary>
			/// Publishs the results.
			/// </summary>
			/// <param name="constraint">Constraint.</param>
			/// <param name="results">Results.</param>
			protected override void PublishResults(Java.Lang.ICharSequence constraint, Filter.FilterResults results)
			{
				_adapter.NotifyDataSetChanged();
			}
		}

		public Filter Filter
		{
			get
			{
				return filter;
			}
		}
	}
}