using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.ContentDTO;
using Android.Graphics;

namespace RetailerAcademy.Droid.Source.Adapters
{
    class TeamDetailsListAdapter: ArrayAdapter<TeamDetailsDto>
    {
        private List<TeamDetailsDto> items;
		private Context context;
        private LayoutInflater inflater = null;
        private int resource;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.Source.Adapters.TeamDetailsListAdapter"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="items">Items.</param>
		public TeamDetailsListAdapter(Context context, int resource, List<TeamDetailsDto> items)
            : base(context,Resource.Layout.DetailsListViewItem,items)
        {        
            this.context = context;
            this.items = items;
            this.resource = resource;
            inflater = LayoutInflater.From(context);
        }
        
		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            ViewHolder holder = null;
            if (convertView == null) // no view to re-use, create new
            {
                holder = new ViewHolder();
                convertView = inflater.Inflate(resource, null); 
                Typeface OswaldRegular = Typeface.CreateFromAsset(context.Assets, "Fonts/oswald_regular.ttf");
                holder.numberTV = convertView.FindViewById<TextView>(Resource.Id.numberTV);
                holder.stateNameTV = convertView.FindViewById<TextView>(Resource.Id.brandNameTV);
                holder.brandImgView = convertView.FindViewById<ImageView>(Resource.Id.brandImageView);
                holder.numberTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
                holder.stateNameTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
                convertView.Tag = holder;
            }
            else
            {
                holder = (ViewHolder)convertView.Tag;
            }
           
            var item = items[position];
            holder.stateNameTV.Text = item.teamName;
            holder.numberTV.Text = position + 1 + "";
            holder.brandImgView.Visibility = ViewStates.Gone;
            return convertView;
        }

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
        public class ViewHolder : Java.Lang.Object 
        {
           public TextView numberTV, stateNameTV;
           public ImageView brandImgView;
        }
    }   
}