using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;

namespace RetailerAcademy.Droid.Source.Adapters
{
	class MenuListAdapter : BaseAdapter<NavigationListDTO>
	{
		private Context context;
		private LayoutInflater inflater = null;
		private List<NavigationListDTO> navMenuList;
		private int resource;
		public int save = -1;
		private ViewHolder holder = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.Source.Adapters.MenuListAdapter"/> class, and loading the default image with Universal image loader.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="navMenuList">Nav menu list.</param>
		public MenuListAdapter (Context context, int resource, List<NavigationListDTO> navMenuList)
		{
			this.context = context;
			this.navMenuList = navMenuList;
			this.resource = resource;
			inflater = LayoutInflater.From (context);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			if (convertView == null) { // no view to re-use, create new
				holder = new ViewHolder ();
				convertView = inflater.Inflate (resource, null);
				Typeface OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
				holder.dashboardMenuTV = convertView.FindViewById<TextView> (Resource.Id.dashboardMenuTV);
				holder.notificationCountTV = convertView.FindViewById<TextView> (Resource.Id.notificationCountTV);
				holder.dashboardMenuIV = convertView.FindViewById<ImageView> (Resource.Id.dashboardMenuIV);
				holder.dashboardMenuTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.notificationCountTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.rLayoutId = convertView.FindViewById<RelativeLayout> (Resource.Id.rLayoutId);
				convertView.Tag = holder;
			} else {
				holder = (ViewHolder)convertView.Tag;
			}
			NavigationListDTO navDto = navMenuList [position];
			holder.dashboardMenuTV.Text = navDto.itemName;
			if (navDto.itemName == context.GetString (Resource.String.menu_settings_text) || navDto.itemName.Contains (context.GetString (Resource.String.menu_currentversion_text))) {
				holder.dashboardMenuIV.Visibility = ViewStates.Invisible;
				if (navDto.itemName == context.GetString (Resource.String.menu_settings_text)) {
					holder.rLayoutId.Background = new Android.Graphics.Drawables.ColorDrawable (Color.Rgb (209, 209, 209));
				} else {
					holder.rLayoutId.Background = new Android.Graphics.Drawables.ColorDrawable (Color.Rgb (235, 235, 235));
				}
			} else if (save == position) {
				holder.rLayoutId.Background = new Android.Graphics.Drawables.ColorDrawable (Color.Rgb (209, 209, 209));
				holder.dashboardMenuIV.SetImageResource (navDto.itemImgResource);
			} else {
				if (navDto.itemName == context.GetString (Resource.String.menu_settings_text) || navDto.itemName.Contains (context.GetString (Resource.String.menu_currentversion_text))) {
					holder.dashboardMenuIV.Visibility = ViewStates.Invisible;
					if (navDto.itemName == context.GetString (Resource.String.menu_settings_text)) {
						holder.rLayoutId.Background = new Android.Graphics.Drawables.ColorDrawable (Color.Rgb (209, 209, 209));
					} else {
						holder.rLayoutId.Background = new Android.Graphics.Drawables.ColorDrawable (Color.Rgb (235, 235, 235));
					}
				} else {
					holder.dashboardMenuIV.Visibility = ViewStates.Visible;
					holder.dashboardMenuIV.SetImageResource (navDto.itemImgResource);
					holder.rLayoutId.Background = new Android.Graphics.Drawables.ColorDrawable (Color.Rgb (235, 235, 235));
				}
			}
			var notificationCount = PreferenceConnector.ReadInteger (this.context, "notificationCount", 0);
			var newContentCount = PreferenceConnector.ReadInteger (this.context, "newContentCount", 0);
			var newQuizCount = PreferenceConnector.ReadInteger (this.context, "newQuizCount", 0);
			var newBlogsCount = PreferenceConnector.ReadInteger (this.context, "newBlogsCount", 0);
			if (("Notifications".Equals (navDto.itemName) && notificationCount != 0) || ("Quiz Master".Equals (navDto.itemName) && newQuizCount != 0) || ("What's Hot".Equals (navDto.itemName) && newBlogsCount != 0) || ("Knowledge".Equals (navDto.itemName) && newContentCount != 0)) {
				switch (navDto.itemName) {
				case "Notifications":
					holder.notificationCountTV.Visibility = ViewStates.Visible;
					CommonMethods.CommentsOrLikesCountDisplay (notificationCount, holder.notificationCountTV);
					break;
				case "Knowledge":
					holder.notificationCountTV.Visibility = ViewStates.Visible;
					CommonMethods.CommentsOrLikesCountDisplay (newContentCount, holder.notificationCountTV);
					break;
				case "Quiz Master":
					holder.notificationCountTV.Visibility = ViewStates.Visible;
					CommonMethods.CommentsOrLikesCountDisplay (newQuizCount, holder.notificationCountTV);
					break;
				case "What's Hot":
					holder.notificationCountTV.Visibility = ViewStates.Visible;
					CommonMethods.CommentsOrLikesCountDisplay (newBlogsCount, holder.notificationCountTV);
					break;
				default:
					holder.notificationCountTV.Visibility = ViewStates.Invisible;
					break;
				}
			} else {
				holder.notificationCountTV.Visibility = ViewStates.Invisible;
			}
			return convertView;
		}

		/// <summary>
		/// Resets the color of the menu list.
		/// </summary>
		public void ResetMenuListColor ()
		{
			if (holder == null) {
				holder = new ViewHolder ();
			}
			foreach (NavigationListDTO name in navMenuList) {
				if (name.itemName == context.GetString (Resource.String.menu_settings_text)) {
					holder.rLayoutId.Background = new Android.Graphics.Drawables.ColorDrawable (Color.Rgb (209, 209, 209));
				} else {
					holder.rLayoutId.Background = new Android.Graphics.Drawables.ColorDrawable (Color.Rgb (235, 235, 235));
				}
			}
			NotifyDataSetChanged ();
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object
		{
			public TextView dashboardMenuTV, notificationCountTV;
			public ImageView dashboardMenuIV;
			public RelativeLayout rLayoutId;
		}

		#region implemented abstract members of BaseAdapter
		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId (int position)
		{
			return position;
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count {
			get { return navMenuList.Count; }
		}

		#endregion

		#region implemented abstract members of BaseAdapter
		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.Source.Adapters.MenuListAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override NavigationListDTO this [int position] {
			get { return this.navMenuList [position]; }
		}

		#endregion
	}
}