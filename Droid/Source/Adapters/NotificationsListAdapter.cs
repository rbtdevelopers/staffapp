﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Graphics;
using Android.Content;
using Android.Support.V4.Content;

namespace RetailerAcademy.Droid
{
	public class NotificationsListAdapter: BaseAdapter<NotificationListObjDto>
	{
		private List<NotificationListObjDto> notificationListItems;
		private List<NotificationListObjDto> tempMessageListItems;
		private Context context;
		private LayoutInflater inflater = null;
		private int resource;
		private ViewHolder holder = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.NotificationsListAdapter"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="notificationListItems">Notification list items.</param>
		public NotificationsListAdapter (Context context, int resource, List<NotificationListObjDto> notificationListItems)
		{        
			this.context = context;
			this.notificationListItems = notificationListItems;
			tempMessageListItems = new List<NotificationListObjDto> ();
			tempMessageListItems.AddRange (notificationListItems);
			this.resource = resource;
			inflater = LayoutInflater.From (context);
		}

		/// <summary>
		/// Searchs the list item.
		/// </summary>
		/// <param name="searchCharacter">Search character.</param>
		public void SearchListItem (string searchCharacter)
		{	
			notificationListItems.Clear ();			
			if (!string.IsNullOrEmpty (searchCharacter)) {
				foreach (NotificationListObjDto msg in tempMessageListItems) {
					Console.WriteLine (msg.message);
					if (msg.message.ToLower ().Contains (searchCharacter.ToLower ())) {
						notificationListItems.Add (msg);
					}
				}
			} else {
				notificationListItems.AddRange (tempMessageListItems);
			}
			NotifyDataSetChanged ();
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			if (convertView == null) { // no view to re-use, create new
				holder = new ViewHolder ();
				convertView = inflater.Inflate (resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
				holder.notificationTitleTV = convertView.FindViewById<TextView> (Resource.Id.bookmarkTitleTV);
				holder.notificationMessageTV = convertView.FindViewById<TextView> (Resource.Id.bookmarkUrlTV);
				holder.itemContainer = convertView.FindViewById<LinearLayout> (Resource.Id.itemContainer);
				holder.notificationTitleTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.notificationMessageTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				convertView.Tag = holder;
			} else {
				holder = (ViewHolder)convertView.Tag;
			}
			try {
				NotificationListObjDto item = null;
				if (notificationListItems != null && notificationListItems.Count > 0) {
					item = notificationListItems [position];
					holder.notificationTitleTV.Text = item.message;
					holder.notificationMessageTV.Text = item.DateAndTime; 
					holder.notificationTitleTV.Tag = item;
					if (!item.isRead) {
						holder.itemContainer.SetBackgroundColor (Color.ParseColor ("#E7EFDB"));
					} else {
						holder.itemContainer.SetBackgroundColor (Color.ParseColor ("#FEFEFE"));
					}
				}
			} catch (System.IndexOutOfRangeException e) {
				Console.WriteLine ("System Error: Index out of range exception " + e.StackTrace);
				throw new System.ArgumentOutOfRangeException ("index parameter is out of range.", e);
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		private class ViewHolder : Java.Lang.Object
		{
			public TextView notificationTitleTV, notificationMessageTV;
			public LinearLayout itemContainer;
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.NotificationsListAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override NotificationListObjDto this [int position] {
			get { return this.notificationListItems [position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count {
			get { return notificationListItems.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId (int position)
		{
			return position;
		}
	}
}