﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Java.Text;
using RetailerAcademy.Droid.Source.Utilities;

namespace RetailerAcademy.Droid
{
	public class BlogListAdapter : BaseAdapter<BlogListObjectsDTO>, View.IOnClickListener
	{
		private Context context;
		public List<BlogListObjectsDTO> blogsList;
		private Typeface OswaldRegular;
		private int resource;
		private LayoutInflater inflater = null;
		public TextView likeTV, commentTV;
		private BlogServiceForLike blogLikeInterface;
		public int listPosition = -1;
		private BlogListObjectsDTO blogListDto;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.BlogListAdapter" class, and loading the default image with Universal image loader./> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="blogsList">Blogs list.</param>
		/// <param name="blogLikeInterface">Blog like interface.</param>
		public BlogListAdapter (Context context, int resource, List<BlogListObjectsDTO> blogsList, BlogServiceForLike blogLikeInterface)
		{
			this.context = context;
			this.blogsList = blogsList;
			this.resource = resource;
			OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
			inflater = LayoutInflater.From (context);
			this.blogLikeInterface = blogLikeInterface;
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) { // no view to re-use, create new
				holder = new ViewHolder ();
				convertView = inflater.Inflate (resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
				holder.itemNameTV = convertView.FindViewById<TextView> (Resource.Id.itemTVId);
				holder.likeCountTV = convertView.FindViewById<TextView> (Resource.Id.likeCountTVId);
				holder.commentCountTV = convertView.FindViewById<TextView> (Resource.Id.commentCountTVId);
				holder.dateTV = convertView.FindViewById<TextView> (Resource.Id.dateTVId);
				holder.contentDotIVId = convertView.FindViewById<TextView> (Resource.Id.contentDotIVId);
				holder.likeCountTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.commentCountTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.dateTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.itemNameTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				convertView.Tag = holder;
			} else {
				holder = (ViewHolder)convertView.Tag;
			}
			try {
				if (blogsList != null && blogsList.Count > 0) {
					BlogListObjectsDTO item = blogsList [position];
					holder.likeCountTV.Tag = item;
					holder.commentCountTV.Tag = item;
					holder.itemNameTV.Text = item.blogtitle;
					var blogListCommentCount = item.commnetcount;
					var blogListLikeCount = item.likecount;
					CommonMethods.CommentsOrLikesCountDisplay (blogListCommentCount, holder.commentCountTV);
					CommonMethods.CommentsOrLikesCountDisplay (blogListLikeCount, holder.likeCountTV);
					holder.dateTV.Text = item.date;
					if (item.hasLiked) {
						holder.likeCountTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_checked, 0, 0, 0);
					} else {
						holder.likeCountTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_unchecked, 0, 0, 0);
					}
					if (item.hasviewed) {
						holder.contentDotIVId.Visibility = ViewStates.Invisible;
					} else {
						holder.contentDotIVId.Visibility = ViewStates.Visible;
					}
					holder.likeCountTV.SetOnClickListener (this);
					holder.likeCountTV.SetTag (resource, item.blogid + "|" + position);
					holder.commentCountTV.SetOnClickListener (this);
					holder.commentCountTV.SetTag (resource, item.blogid + "|" + position);
				}
			} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
			} catch (InvalidCastException) {
			} catch (Java.Lang.Exception) {
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object
		{
			public TextView itemNameTV, likeCountTV, commentCountTV, dateTV, contentDotIVId;
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				string idPositionStr = (string)v.GetTag (resource);
				string[] str = idPositionStr.Split ('|');
				int blogId = Convert.ToInt32 (str [0]); 
				int itemPosition = Convert.ToInt32 (str [1]);
				switch (id) {
				case Resource.Id.likeCountTVId:
					blogListDto = (BlogListObjectsDTO)v.Tag;
					blogLikeInterface.BlogServiceForLike (blogListDto.blogid, context, "like", itemPosition, blogListDto);
					break;
				case Resource.Id.commentCountTVId:
					blogListDto = (BlogListObjectsDTO)v.Tag;					
					blogLikeInterface.BlogServiceForLike (blogListDto.blogid, context, "comment", itemPosition, blogListDto);
					break;
				}
			} catch (Exception) {				
			}
		}

		#region implemented abstract members of BaseAdapter
		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId (int position)
		{
			return position;
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count {
			get { return blogsList.Count; }
		}
		#endregion

		#region implemented abstract members of BaseAdapter
		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.BlogListAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override BlogListObjectsDTO this [int position] {
			get { return this.blogsList [position]; }
		}
		#endregion
	}
}