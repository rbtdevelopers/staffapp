using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V13.App;
using RetailerAcademy.Droid.Source.Fragments;
using RetailerAcademy.Droid.Source.ContentDTO;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Android.Support.V4.View;
using Com.Nostra13.Universalimageloader.Core;

namespace RetailerAcademy.Droid.Source.Adapters
{
	public class SwitchBrandsPagerAdapter : BaseAdapter<BrandOrgDetailsDto>
    {
		public List<BrandOrgDetailsDto> brandDetails;
		private Context context;
		private LayoutInflater inflater = null;
		private int resource;
        private ImageLoader imageLoader;
        private DisplayImageOptions options;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.Source.Adapters.SwitchBrandsPagerAdapter"/> class, and loading the default image with Universal image loader.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="brandDetails">Brand details.</param>
		public SwitchBrandsPagerAdapter(Context context, int resource, List<BrandOrgDetailsDto> brandDetails)
        {
            this.brandDetails = brandDetails;
			this.context = context;
			this.resource = resource;
			inflater = LayoutInflater.From(context);
            imageLoader = ImageLoader.Instance;
			options = CommonMethods.ReturnDisplayOptionsFastLoading(Resource.Drawable.no_image);
        }

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) // no view to re-use, create new
			{
				holder = new ViewHolder();
				convertView = inflater.Inflate(resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset(context.Assets, "Fonts/oswald_regular.ttf");
				holder.switchBrandImageIV = convertView.FindViewById<ImageView>(Resource.Id.switchBrandImageIV);
				holder.numberTV = convertView.FindViewById<TextView>(Resource.Id.numberTV);
				holder.brandNameTV = convertView.FindViewById<TextView>(Resource.Id.brandNameTV);
				holder.numberTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				holder.brandNameTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				convertView.Tag = holder;
			}
			else
			{
				holder = (ViewHolder)convertView.Tag;
			}
			try{
			if (brandDetails != null && brandDetails.Count > 0) {
				BrandOrgDetailsDto dto = brandDetails [position];
				holder.brandNameTV.Text = dto.brandName;
				holder.numberTV.Text = position + 1 + "";
				var imageUrl = dto.brandImageURL;
				if (!string.IsNullOrEmpty (imageUrl)) {
					imageLoader.DisplayImage (dto.brandImageURL, holder.switchBrandImageIV, options);
				} else {
					holder.switchBrandImageIV.SetImageDrawable (context.GetDrawable (Resource.Drawable.no_image));
				}
			}
			} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
			} catch (InvalidCastException) {
			} catch (Java.Lang.Exception) {
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object 
		{
			public ImageView switchBrandImageIV;
			public TextView numberTV, brandNameTV;
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.Source.Adapters.SwitchBrandsPagerAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override BrandOrgDetailsDto this[int position]
		{
			get { return this.brandDetails[position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count
		{
			get { return brandDetails.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId(int position)
		{
			return position;
		}        
    }
}