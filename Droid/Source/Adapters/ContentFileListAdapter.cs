﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Graphics;
using Android.Content;
using System.Text.RegularExpressions;

namespace RetailerAcademy.Droid
{
	public class ContentFileListAdapter : BaseAdapter<ContentFileDto>
	{
		public List<ContentFileDto> contentFileListItems;
		private Context context;
		private LayoutInflater inflater = null;
		private int resource;
		public int setPosition = -1;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ContentFileListAdapter"/> class, and loading the default image with Universal image loader.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="contentFileListItems">Content file list items.</param>
		public ContentFileListAdapter (Context context, int resource, List<ContentFileDto> contentFileListItems)
		{        
			this.context = context;
			this.contentFileListItems = contentFileListItems;
			this.resource = resource;
			inflater = LayoutInflater.From (context);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) { // no view to re-use, create new
				holder = new ViewHolder ();
				convertView = inflater.Inflate (resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
				Typeface OswaldLight = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_light.ttf");
				holder.fileTitleTVId = convertView.FindViewById<TextView> (Resource.Id.fileTitleTVId);
				holder.contentDotIVId = convertView.FindViewById<TextView> (Resource.Id.contentDotIVId);
				holder.contentFileItemRL = convertView.FindViewById<RelativeLayout> (Resource.Id.fileItemRLId);
				holder.deleteTV = convertView.FindViewById<TextView> (Resource.Id.deleteTV);
				holder.fileImageView = convertView.FindViewById<ImageView> (Resource.Id.fileImageView);
				holder.folderDescTVId = convertView.FindViewById<TextView> (Resource.Id.folderDescTVId);
				holder.fileTitleTVId.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.folderDescTVId.SetTypeface (OswaldLight, TypefaceStyle.Normal);
				convertView.Tag = holder;
			} else {
				holder = (ViewHolder)convertView.Tag;
			}
			try {
				if (contentFileListItems != null && contentFileListItems.Count > 0) {
					ContentFileDto item = contentFileListItems [position];
					var contentFileName = Regex.Replace (item.filesname, @"[^0-9a-zA-Z .]+", " ");
					holder.fileTitleTVId.Text = contentFileName;
					holder.contentDotIVId.Tag = item;

					if (position == setPosition) {
						holder.contentFileItemRL.Visibility = ViewStates.Gone;
						holder.deleteTV.Visibility = ViewStates.Visible;
					} else {
						holder.contentFileItemRL.Visibility = ViewStates.Visible;
						holder.deleteTV.Visibility = ViewStates.Gone;
					} 

					if (item.hasread) {
						holder.contentDotIVId.Visibility = ViewStates.Invisible;
					} else {
						holder.contentDotIVId.Visibility = ViewStates.Visible;
					}

					if (item.type != null) {
						if (item.type.Equals ("folder")) {
							holder.folderDescTVId.Visibility = ViewStates.Visible;
							holder.folderDescTVId.Text = item.description;
							holder.fileImageView.SetImageResource (Resource.Drawable.content_folder);
						} else {
							holder.folderDescTVId.Visibility = ViewStates.Gone;
							holder.fileImageView.SetImageResource (Resource.Drawable.content_file);
						}
					}
				}
			} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
			} catch (InvalidCastException) {
			} catch (Java.Lang.Exception) {
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object
		{
			public TextView fileTitleTVId, contentDotIVId, deleteTV, folderDescTVId;
			public RelativeLayout contentFileItemRL;
			public ImageView fileImageView;
		}

		/// <summary>
		/// Removes the item.
		/// </summary>
		/// <param name="position">Position.</param>
		public void removeItem (int position)
		{
			contentFileListItems.RemoveAt (position);
			setPosition = -1;
			NotifyDataSetChanged ();
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.ContentFileListAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override ContentFileDto this [int position] {
			get { return this.contentFileListItems [position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count {
			get { return contentFileListItems.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId (int position)
		{
			return position;
		}
	}
}