﻿using System;
using Android.Widget;
using Android.App;
using System.Collections.Generic;
using Android.Views;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Com.Nostra13.Universalimageloader.Core;
using Android.Content;
using Android.Text.Util;
using Android.Webkit;

namespace RetailerAcademy.Droid
{
	public class MessageRepliesAdapter : BaseAdapter<MessageRepliesDTO>, View.IOnClickListener
	{
		public List<MessageRepliesDTO> repliesListItems;
		private Context context;
		private LayoutInflater inflater = null;
		private int resource;
		public int setPosition = -1;
		private string commentAttachment;
		private static ProgressBar dialogProgressBar;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.MessageRepliesAdapter"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="repliesListItems">Replies list items.</param>
		public MessageRepliesAdapter(Context context, int resource, List<MessageRepliesDTO> repliesListItems)
		{        
			this.context = context;
			this.repliesListItems = repliesListItems;
			this.resource = resource;
			inflater = LayoutInflater.From(context);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			string fileUrl = null;
			if (convertView == null) // no view to re-use, create new
			{
				holder = new ViewHolder();
				convertView = inflater.Inflate(resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset(context.Assets, "Fonts/oswald_regular.ttf");
				holder.replySentByTV = convertView.FindViewById<TextView>(Resource.Id.replySentByTV);
				holder.replyMessageTV = convertView.FindViewById<TextView>(Resource.Id.replyMessageTV);
				holder.dateFormatTV = convertView.FindViewById<TextView> (Resource.Id.dateTV);
				holder.removeBGTV = convertView.FindViewById<TextView> (Resource.Id.moveToArchiveInboxTV);
				holder.commentItemRL = convertView.FindViewById<RelativeLayout> (Resource.Id.itemRLId);
				holder.replySubjectTV = convertView.FindViewById<TextView> (Resource.Id.replySubjectTV);
				holder.attachmentImgWV = convertView.FindViewById<Android.Webkit.WebView> (Resource.Id.attachmentImgWV);
				holder.videoImageViewRL = convertView.FindViewById<RelativeLayout> (Resource.Id.videoImageViewRL);
				dialogProgressBar = convertView.FindViewById<ProgressBar> (Resource.Id.dialogProgressBar);

				holder.replySubjectTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				holder.replySentByTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				holder.replyMessageTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				holder.dateFormatTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				holder.attachmentImgWV.SetOnClickListener (this);
				holder.videoImageViewRL.SetOnClickListener (this);
				convertView.Tag = holder;
			}
			else
			{
				holder = (ViewHolder)convertView.Tag;
			}

			if (position == setPosition)
			{
				holder.commentItemRL.Visibility = ViewStates.Gone;
				holder.removeBGTV.Visibility = ViewStates.Visible;
			}
			else
			{
				holder.commentItemRL.Visibility = ViewStates.Visible;
				holder.removeBGTV.Visibility = ViewStates.Gone;
			}
			try{
				MessageRepliesDTO item = null;
				if (repliesListItems != null && repliesListItems.Count > 0) {
					item = repliesListItems [position];
					holder.replySentByTV.Text = item.senderNickName;
					holder.replySubjectTV.Text = item.subject;
					holder.replyMessageTV.Text = item.body;
					holder.dateFormatTV.Text = item.sendDateTime;
					commentAttachment = item.attachment;
					if (!string.IsNullOrEmpty (commentAttachment)) 
					{
						if (commentAttachment.Contains (".pdf")) {
							holder.attachmentImgWV.Visibility = ViewStates.Visible;
							holder.videoImageViewRL.Visibility = ViewStates.Gone;
							fileUrl = "https://docs.google.com/gview?embedded=true&url=" + commentAttachment;
							holder.attachmentImgWV.SetWebViewClient (new MyWebViewClient ());
							holder.attachmentImgWV.Settings.JavaScriptEnabled = true;
							holder.attachmentImgWV.LoadUrl (fileUrl);
						} else if (commentAttachment.Contains (".mp4") || commentAttachment.Contains (".wmv") || commentAttachment.Contains (".mov")) {
							holder.attachmentImgWV.Visibility = ViewStates.Gone;
							holder.videoImageViewRL.Visibility = ViewStates.Visible;
						} else {
							holder.attachmentImgWV.Tag = commentAttachment;
							holder.attachmentImgWV.Visibility = ViewStates.Visible;
							holder.videoImageViewRL.Visibility = ViewStates.Gone;
							holder.attachmentImgWV.Settings.JavaScriptEnabled = true;
							holder.attachmentImgWV.SetWebViewClient (new MyWebViewClient ());
							holder.attachmentImgWV.LoadData ("<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} img{width:100%25;} </style></head><body><img src='" + commentAttachment + "'/></body></html>", "text/html", "UTF-8");
						}

					} else {
						holder.attachmentImgWV.Visibility = ViewStates.Gone;
						holder.videoImageViewRL.Visibility = ViewStates.Gone;
					}
				}
			} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
			} catch (InvalidCastException) {
			} catch (Java.Lang.Exception) {
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object 
		{
			public TextView replySentByTV, replySubjectTV, replyMessageTV, dateFormatTV, removeBGTV;
			public RelativeLayout commentItemRL, videoImageViewRL;
			public Android.Webkit.WebView attachmentImgWV;
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.MessageRepliesAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override MessageRepliesDTO this[int position]
		{
			get { return this.repliesListItems[position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count
		{
			get { return repliesListItems.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId(int position)
		{
			return position;
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				var id = v.Id;
				switch (id) {
				case Resource.Id.attachmentImgWV:
					Android.Webkit.WebView activityImage = (Android.Webkit.WebView)v.FindViewById (Resource.Id.attachmentImgWV);
					Intent intent = new Intent (context, typeof(ImageDisplayActivity));
					intent.PutExtra ("activityAttachment", (string)activityImage.Tag);
					context.StartActivity (intent);
					break;
				case Resource.Id.videoImageViewRL:
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						if (!string.IsNullOrEmpty (commentAttachment)) {
							Intent videoIntent = new Intent (this.context, typeof(VideoPlayerActivity));
							videoIntent.PutExtra ("videoUrl", commentAttachment);
							this.context.StartActivity (videoIntent);
						} else {
							CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.FILE_CORRUPT_ERROR);
						}
					}
					break;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// My web view client class to make progress bar visible and invisible on loading of content.
		/// </summary>
		private class MyWebViewClient : WebViewClient
		{
			public override bool ShouldOverrideUrlLoading (WebView view, string url)
			{
				view.LoadUrl (url);
				return true;
			}

			public override void OnPageStarted (WebView view, string url, Android.Graphics.Bitmap favicon)
			{
				MessageRepliesAdapter.dialogProgressBar.Visibility = ViewStates.Visible;
				base.OnPageStarted (view, url, favicon);
			}

			public override void OnPageFinished (WebView view, string url)
			{
				MessageRepliesAdapter.dialogProgressBar.Visibility = ViewStates.Gone;
				base.OnPageFinished (view, url);
			}
		}
	}
}