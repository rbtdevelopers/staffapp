﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Com.Nostra13.Universalimageloader.Core;
using Android.Content;
using Java.Text;

namespace RetailerAcademy.Droid
{
	public class ActivityStreamListAdapter : BaseAdapter<ActivityStreamDto>, View.IOnClickListener
	{
		public List<ActivityStreamDto> activityStreamListItems;
		private Context context;
		private LayoutInflater inflater = null;
		private int resource;
		private IActivityStreamLikeServiceCall activityStreamLikeServiceCall;
		private ImageLoader imageLoader;
		private DisplayImageOptions options;
		private ViewHolder holder = null;
		public ActivityStreamDto activityStreamDto;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ActivityStreamListAdapter"/> class, and loading the default image with Universal image loader.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="activityStreamListItems">Activity stream list items.</param>
		/// <param name="activityStreamLikeServiceCall">Activity stream like service call.</param>
		public ActivityStreamListAdapter (Context context, int resource, List<ActivityStreamDto> activityStreamListItems, IActivityStreamLikeServiceCall activityStreamLikeServiceCall)
		{        
			this.context = context;
			this.activityStreamListItems = activityStreamListItems;
			this.resource = resource;
			this.activityStreamLikeServiceCall = activityStreamLikeServiceCall;
			inflater = LayoutInflater.From (context);
			imageLoader = ImageLoader.Instance;
			options = CommonMethods.ReturnDisplayOptions (Resource.Drawable.no_image);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			if (convertView == null) { // no view to re-use, create new
				holder = new ViewHolder ();
				convertView = inflater.Inflate (resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
				holder.userNameTV = convertView.FindViewById<TextView> (Resource.Id.userNameTV);
				holder.userActivityDescTV = convertView.FindViewById<TextView> (Resource.Id.activityDescTV);
				holder.userLikeTV = convertView.FindViewById<TextView> (Resource.Id.likeTV);
				holder.userCommentTV = convertView.FindViewById<TextView> (Resource.Id.commentTV);
				holder.dateFormatTV = convertView.FindViewById<TextView> (Resource.Id.dateTV);
				holder.userImageIV = convertView.FindViewById<ImageView> (Resource.Id.userImageView);
				holder.commentAttachmentIV = convertView.FindViewById<ImageView> (Resource.Id.commentAttachmentIV);
				holder.userNameTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.userActivityDescTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.userLikeTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.userCommentTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.dateFormatTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.commentAttachmentIV.SetOnClickListener (this);
				convertView.Tag = holder;
			} else {
				holder = (ViewHolder)convertView.Tag;
			}
			try {
				ActivityStreamDto item = null;
				if (activityStreamListItems != null && activityStreamListItems.Count > 0) {
					item = activityStreamListItems [position];
					holder.userLikeTV.SetOnClickListener (this);
					holder.userLikeTV.Tag = item;
					holder.userNameTV.Text = item.userName;
					holder.userActivityDescTV.Text = item.activityStreamText;
					holder.userLikeTV.Text = item.likeCount + "";
					CommonMethods.CommentsOrLikesCountDisplay (item.likeCount, holder.userLikeTV);
					CommonMethods.CommentsOrLikesCountDisplay (item.commentCount, holder.userCommentTV);
					holder.dateFormatTV.Text = item.date;
					var hasLiked = item.hasLiked;
					var userImageUrl = item.profileImage;
					var activityImage = item.activityimage;
					holder.commentAttachmentIV.Tag = activityStreamListItems [position].activityimage;
					if (!string.IsNullOrEmpty (activityImage)) {
						holder.commentAttachmentIV.Visibility = ViewStates.Visible;
						imageLoader.DisplayImage (activityImage, holder.commentAttachmentIV, options);
					} else {
						holder.commentAttachmentIV.Visibility = ViewStates.Gone;
					}
					if (!string.IsNullOrEmpty (userImageUrl)) {
						imageLoader.DisplayImage (userImageUrl, holder.userImageIV, options);
					} else {
						holder.userImageIV.SetImageResource (Resource.Drawable.no_image);
					}
					if (!hasLiked) {
						holder.userLikeTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_unchecked, 0, 0, 0);
					} else {
						holder.userLikeTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_checked, 0, 0, 0);
					}       
				}
			} catch (System.IndexOutOfRangeException e) {
				Console.WriteLine ("System Error: Index out of range exception " + e.StackTrace);
				throw new System.ArgumentOutOfRangeException ("index parameter is out of range.", e);
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object
		{
			public TextView userNameTV, userActivityDescTV, userLikeTV, userCommentTV, dateFormatTV;
			public ImageView userImageIV, commentAttachmentIV;
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.ActivityStreamListAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override ActivityStreamDto this [int position] {
			get { return this.activityStreamListItems [position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count {
			get { return activityStreamListItems.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId (int position)
		{
			return position;
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				switch (id) {
				case Resource.Id.likeTV:
					activityStreamDto = (ActivityStreamDto)v.Tag;
					activityStreamLikeServiceCall.WebServiceCallForActivityLike (activityStreamDto.activityStreamId);
					break;
				case Resource.Id.commentAttachmentIV:
					ImageView activityImage = (ImageView)v.FindViewById (Resource.Id.commentAttachmentIV);
					Intent intent = new Intent (context, typeof(ImageDisplayActivity));
					intent.PutExtra ("activityAttachment", (string)activityImage.Tag);
					context.StartActivity (intent);
					break;
				}
			} catch (Exception) {				
			}
		}
	}
}