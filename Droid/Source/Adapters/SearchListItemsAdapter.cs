﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Graphics;
using System.Text.RegularExpressions;

namespace RetailerAcademy.Droid
{
	public class SearchListItemsAdapter : BaseAdapter<ContentSearchTermsDto>
	{
		public List<ContentSearchTermsDto> contentSearchListItems;
		private Context context;
		private LayoutInflater inflater = null;
		private int resource;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.SearchListItemsAdapter"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="contentSearchListItems">Content search list items.</param>
		public SearchListItemsAdapter (Context context, int resource, List<ContentSearchTermsDto> contentSearchListItems)
		{        
			this.context = context;
			this.contentSearchListItems = contentSearchListItems;
			this.resource = resource;
			inflater = LayoutInflater.From (context);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) { // no view to re-use, create new
				holder = new ViewHolder ();
				convertView = inflater.Inflate (resource, null); 
				Typeface OswaldLight = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_light.ttf");
				Typeface OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
				holder.folderTitleTVId = convertView.FindViewById<TextView> (Resource.Id.fileTitleTVId);
				holder.folderDescTVId = convertView.FindViewById<TextView> (Resource.Id.folderDescTVId);
				holder.contentDotIVId = convertView.FindViewById<TextView> (Resource.Id.contentDotIVId);
				holder.folderImageView = convertView.FindViewById<ImageView> (Resource.Id.fileImageView);
				holder.folderTitleTVId.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.folderDescTVId.SetTypeface (OswaldLight, TypefaceStyle.Normal);
				convertView.Tag = holder;
			} else {
				holder = (ViewHolder)convertView.Tag;
			}
			try {
				if (contentSearchListItems != null && contentSearchListItems.Count > 0) {
					ContentSearchTermsDto item = contentSearchListItems [position];
					var contentFileName = Regex.Replace (item.filesname, @"[^0-9a-zA-Z .]+", " ");
					holder.folderTitleTVId.Text = contentFileName;
					holder.contentDotIVId.Tag = item;
					if (item.fileid != 0) {
						holder.folderImageView.SetImageResource (Resource.Drawable.content_folder);
					} else {
						holder.folderImageView.SetImageResource (Resource.Drawable.my_content_folder);
					}
					if (item.hasread) {
						holder.contentDotIVId.Visibility = ViewStates.Invisible;
					} else {
						holder.contentDotIVId.Visibility = ViewStates.Visible;
					}
					if(item.type.Equals("folder")){
						holder.folderImageView.SetImageResource(Resource.Drawable.content_folder);
						if(!string.IsNullOrEmpty(item.description)){
							holder.folderDescTVId.Text = item.description;
							holder.folderDescTVId.Visibility = ViewStates.Visible;
						} else {
							holder.folderDescTVId.Visibility = ViewStates.Gone;
						}
					} else {
						holder.folderDescTVId.Visibility = ViewStates.Gone;						
						holder.folderImageView.SetImageResource(Resource.Drawable.content_file);
					}
				}
			} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
			} catch (InvalidCastException) {
			} catch (Java.Lang.Exception) {
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object
		{
			public TextView folderTitleTVId, folderDescTVId, contentDotIVId;
			public ImageView folderImageView;
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.SearchListItemsAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override ContentSearchTermsDto this [int position] {
			get { return this.contentSearchListItems [position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count {
			get { return contentSearchListItems.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId (int position)
		{
			return position;
		}
	}
}