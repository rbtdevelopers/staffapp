﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Com.Nostra13.Universalimageloader.Core;
using Android.Text.Util;
using Android.Text;

namespace RetailerAcademy.Droid
{		
	public class BlogCommentsAdapter : BaseAdapter<CommentsCls>
	{
		private Context context;
		public List<CommentsCls> blogsCommentListItems;
		private int resource;
		public int setPosition = -1;
		private LayoutInflater inflater = null;
		private ImageLoader imageLoader;
		private DisplayImageOptions options;     

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.BlogCommentsAdapter"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="blogsCommentListItems">Blogs comment list items.</param>
		public BlogCommentsAdapter(Context context, int resource, List<CommentsCls> blogsCommentListItems) 
		{
			this.context = context;
			this.blogsCommentListItems = blogsCommentListItems;
			this.resource = resource;
			inflater = LayoutInflater.From(context);
			imageLoader = ImageLoader.Instance;
			options = CommonMethods.ReturnDisplayOptions(Resource.Drawable.no_image);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) // no view to re-use, create new
			{
				holder = new ViewHolder();
				convertView = inflater.Inflate(resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset(context.Assets, "Fonts/oswald_regular.ttf");
				holder.nameTV = convertView.FindViewById<TextView>(Resource.Id.nameTVId);
				holder.msgTV = convertView.FindViewById<TextView>(Resource.Id.msgTVId);
				holder.dateTV = convertView.FindViewById<TextView>(Resource.Id.dateTVId);
				holder.profileIV = convertView.FindViewById<ImageView> (Resource.Id.imageViewId);
				holder.CommentListItemRL = convertView.FindViewById<RelativeLayout> (Resource.Id.itemRLId);
				holder.archieveRemoveTV = convertView.FindViewById<TextView> (Resource.Id.moveToArchiveInboxTVId);
				holder.archieveRemoveTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				holder.msgTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				holder.nameTV.SetTypeface(OswaldRegular, TypefaceStyle.Normal);
				holder.dateTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				convertView.Tag = holder;
			}
			else
			{
				holder = (ViewHolder)convertView.Tag;
			}

			if (position == setPosition)
			{
				holder.CommentListItemRL.Visibility = ViewStates.Gone;
				holder.archieveRemoveTV.Visibility = ViewStates.Visible;
			}
			else
			{
				holder.CommentListItemRL.Visibility = ViewStates.Visible;
				holder.archieveRemoveTV.Visibility = ViewStates.Gone;
			} 
			try{
			if (blogsCommentListItems != null && blogsCommentListItems.Count > 0) {
				var item = blogsCommentListItems [position];
				holder.nameTV.Text = item.userfullname;
				holder.msgTV.Text = item.Comment; 
				holder.dateTV.Text = item.dateofcomment;
				var profileImgUrl = item.userImg;
				if (!string.IsNullOrEmpty (profileImgUrl)) {
					imageLoader.DisplayImage (profileImgUrl, holder.profileIV, options);
				} else {
					holder.profileIV.SetImageResource (Resource.Drawable.no_image);
				}
			}
			} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
			} catch (InvalidCastException) {
			} catch (Java.Lang.Exception) {
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object
		{
			public TextView nameTV, msgTV, dateTV, archieveRemoveTV;
			public ImageView profileIV;
			public RelativeLayout CommentListItemRL;
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.BlogCommentsAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override CommentsCls this[int position]
		{
			get { return this.blogsCommentListItems[position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count
		{
			get { return blogsCommentListItems.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId(int position)
		{
			return position;
		}
	}
}

