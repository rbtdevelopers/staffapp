﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Com.Nostra13.Universalimageloader.Core;
using Android.Content;

namespace RetailerAcademy.Droid
{
	public class ContentCommentListAdapter: BaseAdapter<CommentsCls>
	{
		public List<CommentsCls> contentCommentListItems;
		private Context context;
		private LayoutInflater inflater = null;
		private ImageLoader imageLoader;
		private DisplayImageOptions options;
		public int setPosition = -1, resource;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ContentCommentListAdapter"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="resource">Resource.</param>
		/// <param name="contentCommentListItems">Content comment list items.</param>
		public ContentCommentListAdapter (Context context, int resource, List<CommentsCls> contentCommentListItems)
		{        
			this.context = context;
			this.contentCommentListItems = contentCommentListItems;
			this.resource = resource;
			inflater = LayoutInflater.From (context);
			imageLoader = ImageLoader.Instance;
			options = CommonMethods.ReturnDisplayOptions (Resource.Drawable.no_image);
		}

		/// <param name="position">The position of the item within the adapter's data set of the item whose view
		///  we want.</param>
		/// <summary>
		/// Gets the view.
		/// </summary>
		/// <returns>The view.</returns>
		/// <param name="convertView">Convert view.</param>
		/// <param name="parent">Parent.</param>
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			ViewHolder holder = null;
			if (convertView == null) { // no view to re-use, create new
				holder = new ViewHolder ();
				convertView = inflater.Inflate (resource, null); 
				Typeface OswaldRegular = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_regular.ttf");
				holder.userNameTV = convertView.FindViewById<TextView> (Resource.Id.userNameTV);
				holder.userActivityDescTV = convertView.FindViewById<TextView> (Resource.Id.activityDescTV);
				holder.dateFormatTV = convertView.FindViewById<TextView> (Resource.Id.dateTV);
				holder.userImageIV = convertView.FindViewById<ImageView> (Resource.Id.userImageView);
				holder.removeBGTV = convertView.FindViewById<TextView> (Resource.Id.moveToArchiveInboxTV);
				holder.commentItemRL = convertView.FindViewById<RelativeLayout> (Resource.Id.itemRLId);
				holder.removeBGTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.userNameTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.userActivityDescTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				holder.dateFormatTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				convertView.Tag = holder;
			} else {
				holder = (ViewHolder)convertView.Tag;
			}
			try {
				if (position == setPosition) {
					holder.commentItemRL.Visibility = ViewStates.Gone;
					holder.removeBGTV.Visibility = ViewStates.Visible;
				} else {
					holder.commentItemRL.Visibility = ViewStates.Visible;
					holder.removeBGTV.Visibility = ViewStates.Gone;
				} 
				if (contentCommentListItems != null && contentCommentListItems.Count > 0) {
					var item = contentCommentListItems [position];
					holder.userNameTV.Text = item.userfullname;
					holder.userActivityDescTV.Text = item.Comment;
					holder.dateFormatTV.Text = item.dateofcomment;
					var userImageUrl = item.userImg;
					if (!string.IsNullOrEmpty (userImageUrl)) {
						imageLoader.DisplayImage (userImageUrl, holder.userImageIV, options);
					} else {
						holder.userImageIV.SetImageResource (Resource.Drawable.no_image);
					} 
				}
			} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
			} catch (InvalidCastException) {
			} catch (Java.Lang.Exception) {
			}
			return convertView;
		}

		/// <summary>
		/// View holder class to declare the list items UI.
		/// </summary>
		public class ViewHolder : Java.Lang.Object
		{
			public TextView userNameTV, userActivityDescTV, dateFormatTV, removeBGTV;
			public ImageView userImageIV;
			public RelativeLayout commentItemRL;
		}

		/// <summary>
		/// Gets the <see cref="RetailerAcademy.Droid.ContentCommentListAdapter"/> with the specified position.
		/// </summary>
		/// <param name="position">Position.</param>
		public override CommentsCls this [int position] {
			get { return this.contentCommentListItems [position]; }
		}

		/// <summary>
		/// How many items are in the data set represented by this Adapter.
		/// </summary>
		/// <value>To be added.</value>
		public override int Count {
			get { return contentCommentListItems.Count; }
		}

		/// <param name="position">The position of the item within the adapter's data set whose row id we want.</param>
		/// <summary>
		/// Get the row id associated with the specified position in the list.
		/// </summary>
		/// <returns>To be added.</returns>
		public override long GetItemId (int position)
		{
			return position;
		}
	}
}