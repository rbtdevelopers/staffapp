using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Util;

namespace RetailerAcademy.Droid.Source.Utilities
{

    public class CircularView : ImageView
    {
        public CircularView(Context context) : base(context)
        {
            Initialize();
        }

        public CircularView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Initialize();
        }

        public CircularView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            Initialize();
        }

        void Initialize()
        {
        }

		/// <param name="canvas">the canvas on which the background will be drawn</param>
		/// <summary>
		/// Implement this to do your drawing.
		/// </summary>
        protected override void OnDraw(Canvas canvas)
        {
			try {
				BitmapDrawable drawable = (BitmapDrawable)Drawable;
            	
				if (drawable == null) {
					return;
				}
            	
				if (Width == 0 || Height == 0) {
					return;
				}
            	
				Bitmap fullSizeBitmap = drawable.Bitmap;
            	
				int scaledWidth = MeasuredWidth;
				int scaledHeight = MeasuredHeight;
            	
				Bitmap mScaledBitmap;
				if (scaledWidth == fullSizeBitmap.Width && scaledHeight == fullSizeBitmap.Height) {
					mScaledBitmap = fullSizeBitmap;
				} else {
					mScaledBitmap = Bitmap.CreateScaledBitmap (fullSizeBitmap, scaledWidth, scaledHeight, true /* filter */);
				}
            	
				Bitmap circleBitmap = getCircledBitmap (mScaledBitmap);
				canvas.DrawBitmap (circleBitmap, 0, 0, null);
			} catch (Exception) {
			}
        }

		/// <summary>
		/// Gets the rounded corner bitmap.
		/// </summary>
		/// <returns>The rounded corner bitmap.</returns>
		/// <param name="context">Context.</param>
		/// <param name="input">Input.</param>
		/// <param name="pixels">Pixels.</param>
		/// <param name="w">The width.</param>
		/// <param name="h">The height.</param>
		/// <param name="squareTL">If set to <c>true</c> square T.</param>
		/// <param name="squareTR">If set to <c>true</c> square T.</param>
		/// <param name="squareBL">If set to <c>true</c> square B.</param>
		/// <param name="squareBR">If set to <c>true</c> square B.</param>
        public Bitmap getRoundedCornerBitmap(Context context, Bitmap input, int pixels, int w, int h, bool squareTL, bool squareTR, bool squareBL, bool squareBR)
        {
			try {
				Bitmap output = Bitmap.CreateBitmap (w, h, Android.Graphics.Bitmap.Config.Argb8888);
				Canvas canvas = new Canvas (output);
				float densityMultiplier = context.Resources.DisplayMetrics.Density;
				Paint paint = new Paint ();
				Rect rect = new Rect (0, 0, w, h);
				RectF rectF = new RectF (rect);
            	
				// make sure that our rounded corner is scaled appropriately
				float roundPx = pixels * densityMultiplier;
            	
				paint.AntiAlias = true;
				canvas.DrawARGB (0, 0, 0, 0);
				paint.Color = Color.White;
				canvas.DrawRoundRect (rectF, roundPx, roundPx, paint);
            	
				// draw rectangles over the corners we want to be square
				if (squareTL) {
					canvas.DrawRect (0, 0, w / 2, h / 2, paint);
				}
				if (squareTR) {
					canvas.DrawRect (w / 2, 0, w, h / 2, paint);
				}
				if (squareBL) {
					canvas.DrawRect (0, h / 2, w / 2, h, paint);
				}
				if (squareBR) {
					canvas.DrawRect (w / 2, h / 2, w, h, paint);
				}
            	
				paint.SetXfermode (new PorterDuffXfermode (PorterDuff.Mode.SrcIn));
				canvas.DrawBitmap (input, 0, 0, paint);
				return output;
			} catch (Exception) {
				return null;
			}
        }

		/// <summary>
		/// Gets the circled bitmap.
		/// </summary>
		/// <returns>The circled bitmap.</returns>
		/// <param name="bitmap">Bitmap.</param>
        Bitmap getCircledBitmap(Bitmap bitmap)
        {
			try {
				Bitmap result = Bitmap.CreateBitmap (bitmap.Width, bitmap.Height, Bitmap.Config.Argb8888);
				Canvas canvas = new Canvas (result);
				Paint paint = new Paint ();
				Rect rect = new Rect (0, 0, bitmap.Width, bitmap.Height);
				paint.AntiAlias = true;
				canvas.DrawARGB (0, 0, 0, 0);
				paint.Color = Color.Blue;
				canvas.DrawCircle (bitmap.Width / 2, bitmap.Height / 2, bitmap.Height / 2, paint);
				paint.SetXfermode (new PorterDuffXfermode (Android.Graphics.PorterDuff.Mode.SrcIn));
				canvas.DrawBitmap (bitmap, rect, rect, paint);
				return result;
			} catch (Exception) {
				return null;
			}
        }
    }
}
