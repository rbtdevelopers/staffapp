using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RetailerAcademy.Droid.Source.Utilities
{
	public class MyApplication : Application
	{ 
		public static string UserId { get; set; }
		public static string CurrentPassword { get; set; }
		public static string CurrentUserId { get; set; }
		public static string FirstName { get; set; }
		public static string LastName { get; set; }
		public static string MobilePhone { get; set; }
		public static string LandPhone { get; set; }
		public static string AvatarURL { get; set; }
		public static string OrgId { get; set; }
		public static string UserRole { get; set;}
		public static string OrgName { get; set;}
		public static string DeviceToken { get;	set;}
		public static bool IsLogin{ get; set;}
		public static string StartDate { get; set;}
		public static string EndDate { get; set;}
		public static bool isComingFromUpload { get; set;}
		public static bool isKeyBoardVisible { get; set;}
		public static bool isActivityUploaded { get; set;}
	}
}