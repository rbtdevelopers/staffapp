﻿using System;

namespace RetailerAcademy.Droid
{
	public enum DialogListenerTypesEnum
	{
		OkTypeAppClose, CancelTypeDismissAlert, OkTypeFailure
	}
}

