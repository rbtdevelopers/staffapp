using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Net;
using Android.Graphics;
using Java.Net;
using Java.IO;
using System.Net;
using Android.Webkit;
using System.Threading.Tasks;
using System.ComponentModel;
using Com.Nostra13.Universalimageloader.Core;
using Com.Nostra13.Universalimageloader.Core.Assist;
using Com.Nostra13.Universalimageloader.Core.Display;
using System.Net.Http;
using Java.Text;
using RetailerAcademy.Droid.Source.Fragments;
using RetailerAcademy.Droid.StaffApp;
using RetailerAcademy.Droid.Source.StaffApp;
using Android.Content.PM;

namespace RetailerAcademy.Droid.Source.Utilities
{
	public static class CommonMethods
	{
		private static MessagesListFragment messagesFrag = null;
		private static DashboardFragment dashboardFragment = null;
		private static ContentFolderListFragment contentFolderFrag = null;
		private static QuizCategoryListFragment quizCategoryListFragment = null;
		private static LeaderBoardNewUserFragment leaderBoardNewUserFragment = null;
		private static ActivityStreamListFragment activityStreamListFrag = null;
		private static BlogCategoryListFragment blogCategoryListFrag = null;
		private static ViewProfileFragment viewProfileFrag = null;
		private static BrandDetailsFragment brandDetailsFrag = null;
		private static StateDetailsFragment stateDetailsFrag = null;
		private static TeamDetailsFragment teamDetailsFrag = null;
		private static CommunicationDisplayFragment communicationFrag = null;
		private static EditProfileFragment editProfileFragment = null;
		private static ChangePasswordFragment changePasswordFragment = null;
		public static Dialog permissionsDialog = null;

		/// <summary>
		/// Determines if is internet connected the specified context.
		/// </summary>
		/// <returns><c>true</c> if is internet connected the specified context; otherwise, <c>false</c>.</returns>
		/// <param name="context">Context.</param>
		public static bool IsInternetConnected (Context context)
		{
			var connectivityManager = (ConnectivityManager)context.GetSystemService (Context.ConnectivityService);
			var activeConnection = connectivityManager.ActiveNetworkInfo;
			if ((activeConnection != null) && activeConnection.IsConnected) {
				return true;
			} else {
				return false;
			}
		}

		/// <summary>
		/// Shows the alert dialog.
		/// </summary>
		/// <returns>The alert dialog.</returns>
		/// <param name="context">Context.</param>
		/// <param name="message">Message.</param>
		public static Dialog ShowAlertDialog (Context context, string message)
		{
			AlertDialog.Builder alert = new AlertDialog.Builder (context);
			alert.SetTitle ("Alert");
			alert.SetCancelable (false);
			alert.SetMessage (message);
			Dialog dialog = null;
			alert.SetPositiveButton ("OK", (senderAlert, args) => {
				if (dialog != null) {
					dialog.Dismiss ();
				}
			});
			dialog = alert.Create ();
			dialog.Show ();
			return dialog;
		}

		/// <summary>
		/// Shows the custom alert dialog.
		/// </summary>
		/// <returns>The alert dialog.</returns>
		/// <param name="context">Context.</param>
		/// <param name="message">Message.</param>
		public static Dialog ShowCustomAlertDialog (Context context, string message, string title, string okBtnTxt, string cancelBtnTxt, bool setCancel, DialogListenerTypesEnum okType)
		{
			AlertDialog.Builder alert = new AlertDialog.Builder (context);
			alert.SetTitle (title);
			alert.SetCancelable (setCancel);
			alert.SetMessage (message);
			Dialog dialog = null;
			alert.SetPositiveButton (okBtnTxt, (senderAlert, args) => {
				AlertDialogBtnHandler.OkTypeCases(context, dialog, okType);
			});
			if (!string.IsNullOrEmpty (cancelBtnTxt)) {
				alert.SetNegativeButton (cancelBtnTxt, (senderAlert, args) => {
					AlertDialogBtnHandler.CancelTypeCases(context, dialog, okType);
				});
			}
			dialog = alert.Create ();
			dialog.Show ();
			return dialog;
		}

		/// <summary>
		/// Displays the progress dialog.
		/// </summary>
		/// <returns>The progress dialog.</returns>
		/// <param name="context">Context.</param>
		/// <param name="txt">Text.</param>
		public static Dialog GetProgressDialog (Context context, string txt)
		{
			Typeface OswaldLight = Typeface.CreateFromAsset (context.Assets, "Fonts/oswald_light.ttf");
			Dialog dialog = new Dialog (context);
			dialog.Window.RequestFeature (WindowFeatures.NoTitle);
			dialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
			dialog.SetContentView (Resource.Layout.ProgressDialog);
			TextView tv = (TextView)dialog.FindViewById (Resource.Id.dialog_text);
			tv.SetTypeface (OswaldLight, TypefaceStyle.Normal);
			tv.Text = txt;
			dialog.SetCancelable (false);
			dialog.Show ();
			return dialog;
		}

		/**
	     * Display image options for Universal image loader lazy loading
	     * @return image options for lazy loading
	     */
		/// <summary>
		/// Display image options for Universal image loader lazy loading.
		/// </summary>
		/// <returns> Image options for lazy loading.</returns>
		/// <param name="defaultImage">Default image.</param>
		public static DisplayImageOptions ReturnDisplayOptions (int defaultImage)
		{
			try {
				DisplayImageOptions Options = new DisplayImageOptions.Builder ()
				                 .ShowImageForEmptyUri (defaultImage)
				                 .ShowImageOnFail (defaultImage)
				                 .ShowImageOnLoading (defaultImage)
						.CacheInMemory (true)
				                 .ResetViewBeforeLoading (true)
				                 .ImageScaleType (ImageScaleType.Exactly)
				                 .BitmapConfig (Bitmap.Config.Rgb565)
				                 .ConsiderExifParams (true)
				                 .Displayer (new FadeInBitmapDisplayer (300))
				                 .Build ();
				return Options;
			} catch (Exception) {
				return null;
			}
		}

		/// <summary>
		/// Display image options for Universal image loader fast loading.
		/// </summary>
		/// <returns>Image options for lazy loading.</returns>
		/// <param name="defaultImage">Default image.</param>
		public static DisplayImageOptions ReturnDisplayOptionsFastLoading (int defaultImage)
		{
			try {
				DisplayImageOptions Options = new DisplayImageOptions.Builder ()
					.ShowImageForEmptyUri (defaultImage)
					.ShowImageOnFail (defaultImage)
					.ShowImageOnLoading (defaultImage)
					.CacheInMemory (false)
					.ResetViewBeforeLoading (true)
					.ImageScaleType (ImageScaleType.Exactly)
					.BitmapConfig (Bitmap.Config.Rgb565)
					.ConsiderExifParams (true)
					.Displayer (new FadeInBitmapDisplayer (300))
					.Build ();
				return Options;
			} catch (Exception) {
				return null;
			}
		}

		/// <summary>
		/// Comments or likes count display.
		/// </summary>
		/// <param name="countValue">Count value.</param>
		/// <param name="textView">Text view.</param>
		public static void CommentsOrLikesCountDisplay (int countValue, TextView textView)
		{
			try {
				if (countValue > 99 && countValue < 1000) {
					textView.Text = "99+";
				} else if (countValue > 999) {
					var count = countValue / 1000.0;    
					textView.Text = new DecimalFormat ("##.#").Format (count) + "K";
				} else {
					textView.Text = countValue + "";
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Determines if is package installed the specified packageName context.
		/// </summary>
		/// <returns><c>true</c> if is package installed the specified packageName context; otherwise, <c>false</c>.</returns>
		/// <param name="packageName">Package name.</param>
		/// <param name="context">Context.</param>
		public static bool IsPackageInstalled(string packageName, Context context) 
		{
			PackageManager pm = context.PackageManager;
			try {
				pm.GetPackageInfo(packageName, PackageInfoFlags.Activities);
				return true;
			} catch (PackageManager.NameNotFoundException) {
				return false;
			}
		}

		/// <summary>
		/// Navigates to respective fragment.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="fragmentManager">Fragment manager.</param>
		/// <param name="moduleName">Module name.</param>
		/// <param name="moduleid">Moduleid.</param>
		/// <param name="blogcategoryid">Blogcategoryid.</param>
		public static void NavigateToRespectiveFragment (Context context, FragmentManager fragmentManager, string moduleName, string moduleid, string blogcategoryid)
		{
			try {
				var moduleNameLowerCase = moduleName.ToLower ();
				switch (moduleNameLowerCase) {
				case "dashboard":
					if (dashboardFragment == null) {
						dashboardFragment = new DashboardFragment (context);
					}
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, dashboardFragment, "dashboard_fragment").AddToBackStack ("dashboard_fragment").Commit ();
					break;
				case "message": 
					messagesFrag = new MessagesListFragment (context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, messagesFrag, "messages_list_fragment").AddToBackStack ("messages_list_fragment").Commit ();
					break;
				case "content":
					if (!string.IsNullOrEmpty (moduleid) && !moduleid.Equals ("0")) {
						Bundle args = new Bundle ();
						ContentFileDto contentFileDtoObj = new ContentFileDto ();
						int contentFileId = 0;
						contentFileId = Convert.ToInt32 (moduleid);
						contentFileDtoObj.filesid = contentFileId;
						ContentViewFragment contentViewFragment = new ContentViewFragment (context, "pushNotificationApp");
						args.PutSerializable ("contentFileCls", contentFileDtoObj);
						args.PutInt ("folderId", 1);
						contentViewFragment.Arguments = args;
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentViewFragment, "content_view_fragment").AddToBackStack ("content_view_fragment").Commit ();
					} else {	
						contentFolderFrag = new ContentFolderListFragment (context);
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentFolderFrag, "content_folder_list_fragment").AddToBackStack ("content_folder_list_fragment").Commit ();	
					}
					break;
				case "quiz":
					quizCategoryListFragment = new QuizCategoryListFragment (context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, quizCategoryListFragment, "quiz_category_list_fragment").AddToBackStack ("quiz_category_list_fragment").Commit ();
					break;
				case "retail status":
					leaderBoardNewUserFragment = new LeaderBoardNewUserFragment (context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, leaderBoardNewUserFragment, "leaderboard_status_fragment").AddToBackStack ("leaderboard_status_fragment").Commit ();
					break;
				case "activity stream":
					activityStreamListFrag = new ActivityStreamListFragment (context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, activityStreamListFrag, "activity_stream_list_fragment").AddToBackStack ("activity_stream_list_fragment").Commit ();
					break;
				case "newsfeed":
					activityStreamListFrag = new ActivityStreamListFragment (context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, activityStreamListFrag, "activity_stream_list_fragment").AddToBackStack ("activity_stream_list_fragment").Commit ();
					break;
				case "whatshot":				
					if (!string.IsNullOrEmpty (moduleid) && !moduleid.Equals ("0") && !string.IsNullOrEmpty (blogcategoryid) && !blogcategoryid.Equals ("0")) {
						BlogListObjectsDTO blogListDto = new BlogListObjectsDTO ();
						blogListDto.blogid = Convert.ToInt32 (moduleid);
						BlogsListDetailsFragment blogListDetailsFrag = new BlogsListDetailsFragment (context, blogListDto, "What's Hot", Convert.ToInt32 (blogcategoryid));
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, blogListDetailsFrag, "blogs_list_details_fragment").AddToBackStack ("blogs_list_details_fragment").Commit ();
					} else {
						blogCategoryListFrag = new BlogCategoryListFragment (context);
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, blogCategoryListFrag, "blogs_category_list").AddToBackStack ("blogs_category_list").Commit ();
					}
					break;
				case "profile":
					viewProfileFrag = new ViewProfileFragment (context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, viewProfileFrag, "view_profile_fragment").AddToBackStack ("view_profile_fragment").Commit ();
					break;
				case "brand details":
					brandDetailsFrag = new BrandDetailsFragment (context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, brandDetailsFrag, "brand_details_fragment").AddToBackStack ("brand_details_fragment").Commit ();
					break;
				case "state details":
					stateDetailsFrag = new StateDetailsFragment (context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, stateDetailsFrag, "state_details_fragment").AddToBackStack ("state_details_fragment").Commit ();
					break;
				case "team details":
					teamDetailsFrag = new TeamDetailsFragment (context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, teamDetailsFrag, "team_details_fragment").AddToBackStack ("team_details_fragment").Commit ();
					break;
				case "login":
					((MainActivity)context).LogoutService ();
					break;
				case "logout":
					((MainActivity)context).LogoutService ();
					break;
				case "switch brands":
					((MainActivity)context).switchBrands = true;
					((MainActivity)context).WebServiceCall ();
					break;
				case "communication":
					communicationFrag = new CommunicationDisplayFragment (context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, communicationFrag, "communication_display_fragment").AddToBackStack ("communication_display_fragment").Commit ();
					break;
				case "edit profile":
					editProfileFragment = new EditProfileFragment (context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, editProfileFragment, "edit_profile_fragment").AddToBackStack ("edit_profile_fragment").Commit ();
					break;
				case "change password":
					changePasswordFragment = new ChangePasswordFragment (context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, changePasswordFragment, "change_password_fragment").AddToBackStack ("change_password_fragment").Commit ();
					break;
				default:
					break;
				}
			} catch (Exception) {
				
			}
		}

		/// <summary>
		/// Alerts the dialog for permissions.
		/// </summary>
		/// <returns>The dialog for permissions.</returns>
		/// <param name="context">Context.</param>
		/// <param name="typeface">Typeface.</param>
		/// <param name="clickListener">Click listener.</param>
		/// <param name="isCancelable">If set to <c>true</c> is cancelable.</param>
		public static Dialog AlertDialogForPermissions(Context context, Typeface typeface, View.IOnClickListener clickListener, bool isCancelable){
			permissionsDialog = new Dialog (context);
			permissionsDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
			permissionsDialog.Window.RequestFeature (WindowFeatures.NoTitle);
			permissionsDialog.SetContentView (Resource.Layout.AlertDialogOK);
			TextView alertTV = permissionsDialog.FindViewById<TextView> (Resource.Id.okAlertTextTV);
			TextView alertMessageTV = permissionsDialog.FindViewById<TextView> (Resource.Id.okAlertMessageTV);
			TextView allowPermissionTV = permissionsDialog.FindViewById<TextView> (Resource.Id.okAlertOKTV);
			alertMessageTV.Text = context.GetString (Resource.String.message_marshmallow);
			alertTV.SetTypeface (typeface, TypefaceStyle.Normal);
			alertMessageTV.SetTypeface (typeface, TypefaceStyle.Normal);
			allowPermissionTV.SetTypeface (typeface, TypefaceStyle.Bold);
			allowPermissionTV.SetOnClickListener (clickListener);
			permissionsDialog.SetCancelable (isCancelable);
			permissionsDialog.Show ();
			return permissionsDialog;
		}

		/// <summary>
		/// Taskbar app theme with the custom actionbar.
		/// </summary>
		/// <param name="context">Context.</param>
		public static void TaskBarAppTheme(Activity context){
			if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop) {
				Window window = context.Window;
				window.AddFlags (WindowManagerFlags.DrawsSystemBarBackgrounds);
				window.ClearFlags (WindowManagerFlags.TranslucentStatus);
				window.SetStatusBarColor (Color.ParseColor ("#004996"));
			}
		}
	}
}