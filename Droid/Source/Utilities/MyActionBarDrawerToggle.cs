using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Views.Animations;

namespace RetailerAcademy.Droid.Source.Utilities
{
	public class ActionBarDrawerEventArgs : EventArgs
	{
		public View DrawerView { get; set; }

		public float SlideOffset { get; set; }

		public int NewState { get; set; }
	}

	/// <summary>
	/// Action bar drawer changed event handler.
	/// </summary>
	public delegate void ActionBarDrawerChangedEventHandler (object s, ActionBarDrawerEventArgs e);

	public class MyActionBarDrawerToggle : ActionBarDrawerToggle
	{
		private Activity mHostActivity;
		private int mOpenedResource;
		private int mClosedResource;
		private float lastTranslate = 0.0f;
		private ListView mDrawerList;
		private FrameLayout frame;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.Source.Utilities.MyActionBarDrawerToggle"/> class.
		/// </summary>
		/// <param name="host">Host.</param>
		/// <param name="drawerLayout">Drawer layout.</param>
		/// <param name="toolbar">Toolbar.</param>
		/// <param name="openedResource">Opened resource.</param>
		/// <param name="closedResource">Closed resource.</param>
		/// <param name="mDrawerList">M drawer list.</param>
		/// <param name="frame">Frame.</param>
		public MyActionBarDrawerToggle (Activity host, DrawerLayout drawerLayout, Android.Support.V7.Widget.Toolbar toolbar, int openedResource, int closedResource, ListView mDrawerList, FrameLayout frame)
			: base (host, drawerLayout, toolbar, openedResource, closedResource)
		{
			mHostActivity = host;
			mOpenedResource = openedResource;
			mClosedResource = closedResource;
			this.mDrawerList = mDrawerList;
			this.frame = frame;
		}

		public event ActionBarDrawerChangedEventHandler DrawerClosed;
		public event ActionBarDrawerChangedEventHandler DrawerOpened;
		public event ActionBarDrawerChangedEventHandler DrawerSlide;

		/// <Docs>Drawer view that is now open</Docs>
		/// <remarks>Called when a drawer has settled in a completely open state.
		///  The drawer is interactive at this point.</remarks>
		/// <format type="text/html">[Android Documentation]</format>
		/// <since version=""></since>
		/// <summary>
		/// Raises the drawer opened event.
		/// </summary>
		/// <param name="drawerView">Drawer view.</param>
		public override void OnDrawerOpened (Android.Views.View drawerView)
		{
			if (null != DrawerOpened)
				DrawerOpened (this, new ActionBarDrawerEventArgs { DrawerView = drawerView });
			base.OnDrawerOpened (drawerView);
		}

		/// <Docs>Drawer view that is now closed</Docs>
		/// <remarks>Called when a drawer has settled in a completely closed state.</remarks>
		/// <format type="text/html">[Android Documentation]</format>
		/// <since version=""></since>
		/// <summary>
		/// Raises the drawer closed event.
		/// </summary>
		/// <param name="drawerView">Drawer view.</param>
		public override void OnDrawerClosed (Android.Views.View drawerView)
		{
			if (null != DrawerClosed)
				DrawerClosed (this, new ActionBarDrawerEventArgs { DrawerView = drawerView });
			base.OnDrawerClosed (drawerView);
		}

		/// <Docs>The child view that was moved</Docs>
		/// <summary>
		/// Called when a drawer's position changes.
		/// </summary>
		/// <para tool="javadoc-to-mdoc">Called when a drawer's position changes.</para>
		/// <format type="text/html">[Android Documentation]</format>
		/// <since version=""></since>
		/// <param name="drawerView">Drawer view.</param>
		/// <param name="slideOffset">Slide offset.</param>
		public override void OnDrawerSlide (Android.Views.View drawerView, float slideOffset)
		{
			if (null != DrawerSlide)
				DrawerSlide (this, new ActionBarDrawerEventArgs {
					DrawerView = drawerView,
					SlideOffset = slideOffset
				});
			float moveFactor = (mDrawerList.Width * slideOffset);
			if (Build.VERSION.SdkInt >= BuildVersionCodes.Honeycomb) {
				frame.TranslationX = (moveFactor);
			} else {
				TranslateAnimation anim = new TranslateAnimation (lastTranslate, moveFactor, lastTranslate, lastTranslate);
				anim.Duration = 0;
				anim.FillAfter = true;
				frame.StartAnimation (anim);
				lastTranslate = moveFactor;
			}
		}
	}
}