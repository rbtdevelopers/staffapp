﻿using System;
using Android.Content;
using Android.App;
using RetailerAcademy.Droid.StaffApp;

namespace RetailerAcademy.Droid
{
	public class AlertDialogBtnHandler
	{
		public AlertDialogBtnHandler ()
		{
		}

		public static void OkTypeCases(Context context, Dialog dialog, DialogListenerTypesEnum okTypeEnum){
			switch(okTypeEnum){
			case DialogListenerTypesEnum.OkTypeAppClose:
				if (dialog != null) {
					dialog.Dismiss ();
					Android.OS.Process.KillProcess (Android.OS.Process.MyPid ());
					((Activity)context).Finish ();
				}
				break;
			}
		}

		public static void CancelTypeCases(Context context, Dialog dialog, DialogListenerTypesEnum cancelTypeEnum){
			switch(cancelTypeEnum){
			case DialogListenerTypesEnum.OkTypeAppClose:
			case DialogListenerTypesEnum.OkTypeFailure:
				if (dialog != null) {
					dialog.Dismiss ();
				}
				break;
			}
		}
	}
}

