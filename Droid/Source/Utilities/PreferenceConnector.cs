﻿using System;
using Android.Content;

namespace RetailerAcademy.Droid
{
	public class PreferenceConnector
	{
		public static readonly string PREF_NAME = "StaffApp";
		public static readonly string ID = "deviceId";

		public PreferenceConnector ()
		{
		}

		public static void WriteBoolean(Context context, string key, bool value) {
			GetEditor(context).PutBoolean(key, value).Commit();
		}

		public static bool ReadBoolean(Context context, string key, bool defValue) {
			return GetPreferences(context).GetBoolean(key, defValue);
		}

		public static void WriteInteger(Context context, string key, int value) {
			GetEditor(context).PutInt(key, value).Commit();
		}

		public static int ReadInteger(Context context, string key, int defValue) {
			return GetPreferences(context).GetInt(key, defValue);
		}

		public static string WriteString(Context context, string key, string value) {
			GetEditor(context).PutString(key, value).Commit();
			return key;
		}

		public static string ReadString(Context context, string key, string defValue) {
			return GetPreferences(context).GetString(key, defValue);
		}

		public static void WriteFloat(Context context, string key, float value) {
			GetEditor(context).PutFloat(key, value).Commit();
		}

		public static float ReadFloat(Context context, string key, float defValue) {
			return GetPreferences(context).GetFloat(key, defValue);
		}

		public static void WriteLong(Context context, string key, long value) {
			GetEditor(context).PutLong(key, value).Commit();
		}

		public static long ReadLong(Context context, string key, long defValue) {
			return GetPreferences(context).GetLong(key, defValue);
		}

		private static ISharedPreferences GetPreferences(Context context) {
			return context.GetSharedPreferences(PREF_NAME, FileCreationMode.Private);
		}

		private static ISharedPreferencesEditor GetEditor(Context context) {
			return GetPreferences(context).Edit();
		}

		public static void ResetPreferences(Context context) {
			GetEditor(context).Clear().Commit();
		}
	}
}

