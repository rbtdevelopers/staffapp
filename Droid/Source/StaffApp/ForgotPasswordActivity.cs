using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.StaffApp;
using Android.Content.PM;
using System.Text.RegularExpressions;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Android.Graphics;
using Android.Views.InputMethods;
using System.Threading;

namespace RetailerAcademy.Droid.Source.StaffApp
{
	[Activity (Label = "ForgotPasswordActivity", ScreenOrientation = ScreenOrientation.Portrait)]
	public class ForgotPasswordActivity : Activity, IWebServiceDelegate, View.IOnClickListener
	{
		private TextView headerTextTV, forgotPasswordTV, emailAddressReqTV;
		private EditText emailAddressET;
		private Button cancelButton, requestButton;
		private Dialog progressDialog, alertDialog;
		public Typeface OswaldRegular;

		/// <summary>
		/// Raises the create event.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.ForgotPassword);
			CommonMethods.TaskBarAppTheme (this);
			OswaldRegular = Typeface.CreateFromAsset (Assets, "Fonts/oswald_regular.ttf");
			LoadUI ();
		}

		/// <summary>
		/// Loads the U.
		/// </summary>
		private void LoadUI ()
		{
			FindViewById (Resource.Id.headerMenu).Visibility = ViewStates.Gone;
			FindViewById (Resource.Id.headerOrgs).Visibility = ViewStates.Gone;
			headerTextTV = (TextView)FindViewById (Resource.Id.headerText);
			forgotPasswordTV = (TextView)FindViewById (Resource.Id.forgotPasswordTV);
			emailAddressReqTV = (TextView)FindViewById (Resource.Id.forgotPasswordRequestTV);
			headerTextTV.Text = GetString (Resource.String.forgot_password_text);
			emailAddressET = (EditText)FindViewById (Resource.Id.emailAddress);
			cancelButton = (Button)FindViewById (Resource.Id.cancelButton);
			requestButton = (Button)FindViewById (Resource.Id.requestButton);
			cancelButton.SetOnClickListener (this);
			requestButton.SetOnClickListener (this);
			headerTextTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			emailAddressET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			cancelButton.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			requestButton.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			forgotPasswordTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			emailAddressReqTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			int id = v.Id;
			View view = this.CurrentFocus;
			if (view != null) {  
				InputMethodManager imm = (InputMethodManager)GetSystemService (Context.InputMethodService);
				imm.HideSoftInputFromWindow (view.WindowToken, 0);
			}
			switch (id) {
			case Resource.Id.cancelButton:
				Finish ();
				break;
			case Resource.Id.requestButton:
				WebServiceCall ();  
				break;
			}
		}

		/// <summary>
		/// Webservice call for requesting server to retrieve the password.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				string frgtPasswordUrl = CommonSharedStrings.FORGOT_PASSWORD_URL;
				string pwdTxt = this.emailAddressET.Text;
				string emailRegex = CommonSharedStrings.EMAIL_REGEX;
				bool isEmail = Regex.IsMatch (pwdTxt, emailRegex, RegexOptions.IgnoreCase);
				if (string.IsNullOrEmpty (pwdTxt)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.FORGOTPASSWORD_BLANK_MESSAGE);
				} else if (!isEmail) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.FORGOTPASSWORD_INVALID_MESSAGE);
				} else if (!CommonMethods.IsInternetConnected (this)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this, "Loading, please wait...");
					}
					ForgotPassword dataObjects = new ForgotPassword ();
					dataObjects.emailid = pwdTxt;
					string requestParameter = JsonConvert.SerializeObject (dataObjects);
					var jsonService = new JsonService ();
					jsonService.consumeService (frgtPasswordUrl, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (status) {
						RunOnUiThread (() => {
							if (!string.IsNullOrEmpty (statusMessage)) {
								AlertDialog.Builder alert = new AlertDialog.Builder (this);
								alert.SetTitle ("Alert");
								alert.SetCancelable (false);
								alert.SetMessage (statusMessage);
								Dialog dialog = null;
								alert.SetPositiveButton ("OK", (senderAlert, args) => {
									if (dialog != null) {
										dialog.Dismiss ();
										Finish ();
									}
								});
								dialog = alert.Create ();
								dialog.Show ();
							} else {
								Finish ();
							}
						});
					} else {
						if (!string.IsNullOrEmpty (statusMessage)) {
							RunOnUiThread (() => {
								if (alertDialog != null) {
									alertDialog.Dismiss ();
								}
								alertDialog = CommonMethods.ShowAlertDialog (this, statusMessage);
							});
						}
					}
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Exception) {
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this, response);
			});
		}
	}
}