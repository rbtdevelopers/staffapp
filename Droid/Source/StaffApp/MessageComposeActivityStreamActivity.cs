﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.StaffApp;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Android.Content.PM;
using Android.Graphics;
using System.IO;
using Android.Provider;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Support.V4.Content;
using Android;
using Android.Support.V4.App;

namespace RetailerAcademy.Droid
{
	[Activity (Label = "MessageComposeActivityStreamActivity", ScreenOrientation = ScreenOrientation.Portrait, ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.KeyboardHidden)]			
	public class MessageComposeActivityStreamActivity : BaseActivity, IWebServiceDelegate, View.IOnClickListener
	{
		private TextView cancelTV, sendTVId, messageHeaderTV, messageColonTV, cancelAttachmentTV;
		private EditText bodyET;
		private Dialog progressDialog, photoDialog, alertDialog, permissionsDialog = null;
		private ImageView commentUploadImageView, photoUploadImageView;
		private RelativeLayout commentPictureBox;
		private View footerLineView;
		private Java.IO.File _file;
		private Java.IO.File _dir;
		private const int CROP_IMAGE_REQUEST_CODE = 111;
		private const int FROM_CAMERA_INTENT_CODE = 222;
		private const int MY_PERMISSIONS_REQUEST_CAMERA = 333;
		private const int PERMISSIONS_REQUEST_WRITE_STORAGE = 444;
		private const int PERMISSIONS_REQUEST_READ_STORAGE = 555;
		private string encodedImage = null, fileName, extension;

		/// <summary>
		/// Raises the create event.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.MessageComposeActivityStream);
			CommonMethods.TaskBarAppTheme (this);
			LoadUI ();
		}

		/// <summary>
		/// Loads the UI.
		/// </summary>
		private void LoadUI ()
		{
			cancelTV = (TextView)FindViewById (Resource.Id.cancelFinishTV);
			messageHeaderTV = (TextView)FindViewById (Resource.Id.messageHeaderTV);
			sendTVId = (TextView)FindViewById (Resource.Id.sendTVId);
			messageColonTV = (TextView)FindViewById (Resource.Id.messageLabelTV);
			bodyET = (EditText)FindViewById (Resource.Id.bodyET);
			cancelAttachmentTV = (TextView)FindViewById (Resource.Id.cancelAttachmentTV);
			commentUploadImageView = (ImageView)FindViewById (Resource.Id.commentUploadImageView);
			photoUploadImageView = (ImageView)FindViewById (Resource.Id.photoUploadImageView);
			commentPictureBox = (RelativeLayout)FindViewById (Resource.Id.commentPictureBox);
			footerLineView = (View)FindViewById (Resource.Id.footerLineView);

			sendTVId.SetOnClickListener (this);
			cancelTV.SetOnClickListener (this);
			photoUploadImageView.SetOnClickListener (this);
			cancelAttachmentTV.SetOnClickListener (this);

			messageColonTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			cancelTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			messageHeaderTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			sendTVId.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			bodyET.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
		}

		/// <summary>
		/// Service call to upload activities.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (bodyET.Text.Trim ().Length == 0) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.COMPOSE_MSG_ERROR);
				} else if (!CommonMethods.IsInternetConnected (this)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this, "org_id", null);
					ActivityDataCompose dataObjects = new ActivityDataCompose ();
					dataObjects.userId = Convert.ToInt32 (userId);
					dataObjects.brandGuid = orgId;
					dataObjects.activityStreamText = bodyET.Text.Trim ();
					dataObjects.file = encodedImage;
					dataObjects.filename = fileName;
					dataObjects.extension = extension;
					string requestParameter = JsonConvert.SerializeObject (dataObjects);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.ACTIVITYSTREAM_COMPOSE, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		#region IWebServiceDelegate implementation
		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (status) {
						MyApplication.isActivityUploaded = true;
						if (!string.IsNullOrEmpty (statusMessage)) {
							RunOnUiThread (() => {
								AlertDialog.Builder alert = new AlertDialog.Builder (this);
								alert.SetTitle ("Alert");
								alert.SetCancelable (false);
								alert.SetMessage (statusMessage);
								Dialog dialog = null;
								alert.SetPositiveButton ("OK", (senderAlert, args) => {
									if (dialog != null) {
										dialog.Dismiss ();
										Finish ();
									}
								});
								dialog = alert.Create ();
								dialog.Show ();
							});
						} else {
							Finish ();
						}
					} else {
						//display toast message
						if (!string.IsNullOrEmpty (statusMessage)) {
							RunOnUiThread (() => {
								if (alertDialog != null) {
									alertDialog.Dismiss ();
								}
								alertDialog = CommonMethods.ShowAlertDialog (this, statusMessage);
							});
						}
					}
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Exception) {
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this, response);
			});
		}

		#endregion
		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				switch (id) {
				case Resource.Id.cancelFinishTV:
					Finish ();
					break;
				case Resource.Id.sendTVId:
					var userRole = PreferenceConnector.ReadString (this, "user_role", null);
					if (userRole != "SuperAdmin" && userRole != "Admin") {
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.CHANGE_IN_USER_ROLE);
					} else {
						WebServiceCall ();
					}
					break;
				case Resource.Id.photoLibraryTV:
					photoDialog.Dismiss ();
					RequestReadStoragePermission ();
					break;
				case Resource.Id.photoUploadImageView:
					photoDialog = new Dialog (this);
					photoDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
					photoDialog.Window.RequestFeature (WindowFeatures.NoTitle);
					photoDialog.SetContentView (Resource.Layout.PhotoDialogBox);
					TextView cameraTV = (TextView)photoDialog.FindViewById (Resource.Id.cameraTV);
					TextView photoLibraryTV = (TextView)photoDialog.FindViewById (Resource.Id.photoLibraryTV);
					TextView cancelTV = (TextView)photoDialog.FindViewById (Resource.Id.cancelTV);
					cameraTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
					photoLibraryTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
					cancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
					photoDialog.Show ();
					cameraTV.SetOnClickListener (this);
					photoLibraryTV.SetOnClickListener (this);
					cancelTV.SetOnClickListener (this);
					break;
				case Resource.Id.cancelAttachmentTV:
					commentPictureBox.Visibility = ViewStates.Gone;
					footerLineView.Visibility = ViewStates.Gone;
					encodedImage = null;
					break;
				case Resource.Id.cameraTV:
					photoDialog.Dismiss ();
					RequestWriteStoragePermission ();
					break;
				case Resource.Id.cancelTV:
					photoDialog.Dismiss ();
					break;
				case Resource.Id.okAlertOKTV:
					if (permissionsDialog != null) {
						permissionsDialog.Dismiss ();
					}
					break;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Intent call for photo library.
		/// </summary>
		private void IntentCallForPhotoLibrary ()
		{
			try {
				int height = 450;
				int width = Resources.DisplayMetrics.WidthPixels;
				Intent intent = new Intent (Intent.ActionPick, Android.Provider.MediaStore.Images.Media.ExternalContentUri);	
				intent.SetType ("image/*");
				intent.PutExtra ("crop", "true");  
				intent.PutExtra ("aspectX", 1);  
				intent.PutExtra ("aspectY", 1);  
				intent.PutExtra ("outputX", width);  
				intent.PutExtra ("outputY", height);  
				intent.PutExtra ("noFaceDetection", true);  
				intent.PutExtra ("return-data", true); 
				intent.PutExtra ("scale", true);   
				intent.PutExtra("scaleUpIfNeeded", true);
				intent.PutExtra("outputFormat", Bitmap.CompressFormat.Jpeg.ToString());
				StartActivityForResult (intent, CROP_IMAGE_REQUEST_CODE);
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Requests the write storage permission.
		/// </summary>
		private void RequestWriteStoragePermission ()
		{
			try {
				// Here, thisActivity is the current activity
				if (ContextCompat.CheckSelfPermission (this, Manifest.Permission.WriteExternalStorage) != Permission.Granted) {
					// Should we show an explanation?
					if (ActivityCompat.ShouldShowRequestPermissionRationale (this, Manifest.Permission.WriteExternalStorage)) {
						// Show an expanation to the user *asynchronously* -- don't block
						// this thread waiting for the user's response! After the user
						// sees the explanation, try again to request the permission.
						ActivityCompat.RequestPermissions (this, new String[]{ Manifest.Permission.WriteExternalStorage }, PERMISSIONS_REQUEST_WRITE_STORAGE);
					} else {
						// No explanation needed, we can request the permission.
						ActivityCompat.RequestPermissions (this, new String[]{ Manifest.Permission.WriteExternalStorage }, PERMISSIONS_REQUEST_WRITE_STORAGE);
						// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
						// app-defined int constant. The callback method gets the
						// result of the request.
					}
				} else {
					//Call camera directly, permissions are already accepted.
					if (IsThereAnAppToTakePictures ()) {
						CreateDirectoryForPictures ();
						CallCamera ();
					}
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Requests the read storage permission.
		/// </summary>
		private void RequestReadStoragePermission ()
		{
			try {
				// Here, thisActivity is the current activity
				if (ContextCompat.CheckSelfPermission (this, Manifest.Permission.ReadExternalStorage) != Permission.Granted) {
					// Should we show an explanation?
					if (ActivityCompat.ShouldShowRequestPermissionRationale (this, Manifest.Permission.ReadExternalStorage)) {
						// Show an expanation to the user *asynchronously* -- don't block
						// this thread waiting for the user's response! After the user
						// sees the explanation, try again to request the permission.
						ActivityCompat.RequestPermissions (this, new String[]{ Manifest.Permission.ReadExternalStorage }, PERMISSIONS_REQUEST_READ_STORAGE);
					} else {
						// No explanation needed, we can request the permission.
						ActivityCompat.RequestPermissions (this, new String[]{ Manifest.Permission.ReadExternalStorage }, PERMISSIONS_REQUEST_READ_STORAGE);
						// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
						// app-defined int constant. The callback method gets the
						// result of the request.
					}
				} else {
					//Call camera directly, permissions are already accepted.
					IntentCallForPhotoLibrary ();
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Raises the request permissions result event.
		/// </summary>
		/// <param name="requestCode">Request code.</param>
		/// <param name="permissions">Permissions.</param>
		/// <param name="grantResults">Grant results.</param>
		public override void OnRequestPermissionsResult (int requestCode, string[] permissions, Permission[] grantResults)
		{
			try {
				switch (requestCode) {
				case PERMISSIONS_REQUEST_WRITE_STORAGE:
					{
						// If request is cancelled, the result arrays are empty.
						if (grantResults.Length > 0	&& grantResults [0] == Permission.Granted) {
							// permission was granted, yay! Do the
							// camera task you need to do.
							if (IsThereAnAppToTakePictures ()) {
								CreateDirectoryForPictures ();
								CallCamera ();
							}
						} else {
							// permission denied, boo! Disable the
							// functionality that depends on this permission.
							if (permissionsDialog != null) {
								permissionsDialog.Dismiss ();
							}
							permissionsDialog = CommonMethods.AlertDialogForPermissions (this, Arial, this, true);
						}
						return;
					}
				case PERMISSIONS_REQUEST_READ_STORAGE:
					{
						// If request is cancelled, the result arrays are empty.
						if (grantResults.Length > 0	&& grantResults [0] == Permission.Granted) {
							// permission was granted, yay! Do the
							// camera task you need to do.
							IntentCallForPhotoLibrary ();
						} else {
							// permission denied, boo! Disable the
							// functionality that depends on this permission.
							if (permissionsDialog != null) {
								permissionsDialog.Dismiss ();
							}
							permissionsDialog = CommonMethods.AlertDialogForPermissions (this, Arial, this, true);
						}
						return;
					}
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Determines whether this instance is there an app to take pictures.
		/// </summary>
		/// <returns><c>true</c> if this instance is there an app to take pictures; otherwise, <c>false</c>.</returns>
		private bool IsThereAnAppToTakePictures ()
		{
			try {
				Intent intent = new Intent (MediaStore.ActionImageCapture);
				IList<ResolveInfo> availableActivities = PackageManager.QueryIntentActivities (intent, PackageInfoFlags.MatchDefaultOnly);
				return availableActivities != null && availableActivities.Count > 0;
			} catch (Exception) {
				return false;
			}
		}

		/// <summary>
		/// Creates the directory for pictures.
		/// </summary>
		private void CreateDirectoryForPictures ()
		{
			try {
				_dir = new Java.IO.File (Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/RetailAcademyData/");
				if (!_dir.Exists ()) {
					_dir.Mkdirs ();
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Calls the camera.
		/// </summary>
		private void CallCamera ()
		{
			try {
				Intent intent = new Intent (MediaStore.ActionImageCapture);
				_file = new Java.IO.File (_dir, System.String.Format ("StaffApp_{0}.jpg", Guid.NewGuid ()));
				intent.PutExtra (MediaStore.ExtraOutput, Android.Net.Uri.FromFile (_file));
				StartActivityForResult (intent, 222);
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Gets the real path from UR.
		/// </summary>
		/// <returns>The real path from UR.</returns>
		/// <param name="contentUri">Content URI.</param>
		public string GetRealPathFromURI (Android.Net.Uri contentUri)
		{
			try {
				string[] filePathColumn = { MediaStore.MediaColumns.Data };
				var cursor = ContentResolver.Query (contentUri, filePathColumn, null, null, null);
				cursor.MoveToFirst ();
				int columnIndex = cursor.GetColumnIndex (filePathColumn [0]);
				string picturePath = cursor.GetString (columnIndex);
				cursor.Close ();
				return picturePath;
			} catch (Exception) {	
				return null;
			}
		}

		/// <summary>
		/// Gets the image URI.
		/// </summary>
		/// <returns>The image URI.</returns>
		/// <param name="inImage">In image.</param>
		public Android.Net.Uri GetImageUri (Bitmap inImage)
		{
			try {
				MemoryStream bytes = new MemoryStream ();
				inImage.Compress (Bitmap.CompressFormat.Jpeg, 50, bytes);
				string path = Android.Provider.MediaStore.Images.Media.InsertImage (ContentResolver, inImage, "Title", null);
				return Android.Net.Uri.Parse (path);
			} catch (Exception) {
				return null;
			}
		}

		/// <param name="requestCode">The integer request code originally supplied to
		///  startActivityForResult(), allowing you to identify who this
		///  result came from.</param>
		/// <param name="resultCode">The integer result code returned by the child activity
		///  through its setResult().</param>
		/// <param name="data">An Intent, which can return result data to the caller
		///  (various data can be attached to Intent "extras").</param>
		/// <summary>
		/// Called when an activity you launched exits, giving you the requestCode
		///  you started it with, the resultCode it returned, and any additional
		///  data from it.
		/// </summary>
		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			try {
				base.OnActivityResult (requestCode, resultCode, data);
				if (resultCode == Result.Canceled) {
					return;
				} else {
					// make it available in the gallery
					Android.Net.Uri contentUri = null;
					string imagePath = null;
					Intent mediaScanIntent = new Intent (Intent.ActionMediaScannerScanFile);
					if (_file != null && requestCode == FROM_CAMERA_INTENT_CODE) {
						contentUri = Android.Net.Uri.FromFile (_file);
						mediaScanIntent.SetData (contentUri);
						SendBroadcast (mediaScanIntent);
						int height = commentUploadImageView.Height;
						int width = Resources.DisplayMetrics.WidthPixels;
						if (height <= 0) {
							height = 450;
						}
						Bitmap bitmap = null;
						using (bitmap = _file.Path.LoadAndResizeBitmapCommentsActivityStream (width, height)) {
							commentPictureBox.Visibility = ViewStates.Visible;
							commentUploadImageView.SetImageBitmap (bitmap);
							MemoryStream stream = new MemoryStream ();
							bitmap.Compress (Bitmap.CompressFormat.Jpeg, 50, stream);
							byte[] imageByteArray = stream.ToArray ();
							encodedImage = Convert.ToBase64String (imageByteArray);
						}
						_file = null;
					} else if (data != null) {
						Bitmap thumbnailBitmap = null;
						if (requestCode == CROP_IMAGE_REQUEST_CODE && data.Extras != null) {
							Bundle bundle = data.Extras;
							thumbnailBitmap = (Bitmap)bundle.GetParcelable ("data");
							contentUri = GetImageUri (thumbnailBitmap);
						} else {
							contentUri = data.Data;
							thumbnailBitmap = Android.Provider.MediaStore.Images.Media.GetBitmap (ContentResolver, contentUri);
						}
						commentPictureBox.Visibility = ViewStates.Visible;
						commentUploadImageView.SetImageBitmap (thumbnailBitmap);
						MemoryStream stream = new MemoryStream ();
						thumbnailBitmap.Compress (Bitmap.CompressFormat.Jpeg, 50, stream);
						byte[] imageByteArray = stream.ToArray ();
						encodedImage = Convert.ToBase64String (imageByteArray);
					}
					if (contentUri != null) {
						if (!contentUri.ToString ().Contains ("file")) {
							imagePath = GetRealPathFromURI (contentUri);
						} else {
							imagePath = contentUri.ToString ();
						}
						if(!string.IsNullOrEmpty(imagePath)){
							var imagefileName = imagePath.Substring (imagePath.LastIndexOf ("/") + 1);
							string[] imageNameArray = imagefileName.Split ('.');
							fileName = imageNameArray [0];
							extension = imagefileName.Substring (imagefileName.LastIndexOf ("."));
						}
					}
				}
			} catch (Exception) {				
			}
		}
	}

	public static class BitmapHelpersComposeActivity
	{
		public static Bitmap LoadAndResizeBitmapComposeActivity (this string fileName, int width, int height)
		{
			try {
				// First we get the the dimensions of the file on disk
				BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
				BitmapFactory.DecodeFile (fileName, options);

				// Next we calculate the ratio that we need to resize the image by
				// in order to fit the requested dimensions.
				int outHeight = options.OutHeight;
				int outWidth = options.OutWidth;
				int inSampleSize = 1;

				try {
					if (outHeight > height || outWidth > width) {
						inSampleSize = outWidth > outHeight	? outHeight / height : outWidth / width;
					}
				} catch (System.ArithmeticException) {
				} catch (System.Exception) {
				}

				// Now we will load the image and have BitmapFactory resize it for us.
				options.InSampleSize = inSampleSize;
				options.InJustDecodeBounds = false;
				Bitmap resizedBitmap = null;
				resizedBitmap = BitmapFactory.DecodeFile (fileName, options);

				return resizedBitmap;
			} catch (Java.Lang.ArithmeticException) {
				return null;
			} catch (Java.Lang.IllegalArgumentException) {
				return null;
			} catch (Java.Lang.Exception) {
				return null;
			}
		}
	}
}