using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using System.Timers;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Android.Preferences;
using Android.Gms.Common;

namespace RetailerAcademy.Droid.StaffApp
{
	[Activity(Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true, ScreenOrientation = ScreenOrientation.Portrait)]
	public class SplashScreenActivity : Activity, IWebServiceDelegate
	{
		private string lastUserLoginId, lastUserPassword;

		/// <summary>
		/// Raises the create event.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			lastUserLoginId = PreferenceConnector.ReadString(this, "last_user_login", null);
			lastUserPassword = PreferenceConnector.ReadString(this, "last_user_password", null);
			timer_Elapsed();
		}

		/// <summary>
		/// Timers elapsed event.
		/// </summary>
		private void timer_Elapsed()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this)) {
					StartActivity (typeof(LoginActivity));
				} else {
					if (!string.IsNullOrEmpty (lastUserLoginId) && !string.IsNullOrEmpty (lastUserPassword)) {
						LoginData dataObjects = new LoginData ();
						dataObjects.username = lastUserLoginId;
						dataObjects.password = lastUserPassword;
						string requestParameter = JsonConvert.SerializeObject (dataObjects);
						var jsonService = new JsonService ();
						jsonService.consumeService (CommonSharedStrings.LOGIN_URL, requestParameter, this);
					} else {
						StartActivity (typeof(LoginActivity));
					}
				}
			} catch (Exception) {	
				StartActivity (typeof(LoginActivity));
			}
		}

		/// <summary>
		/// Called when the activity has detected the user's press of the back
		///  key.
		/// </summary>
		public override void OnBackPressed()
		{
			base.OnBackPressed();
			Finish();
		}

		#region IWebServiceDelegate implementation
		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse(string response)
		{
			try
			{
				if (!string.IsNullOrEmpty(response)) {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					string userId = null, userRole = null;
					if(responseObj["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if(responseObj["userid"] != null) {
							userId = (string)responseObj.SelectToken ("userid");
						}
						if(responseObj["role"] != null) {
							userRole = (string)responseObj.SelectToken ("role");
						}
						MyApplication.UserId = userId;
						MyApplication.UserRole = userRole;
						MyApplication.CurrentPassword = lastUserPassword;
						MyApplication.CurrentUserId = lastUserLoginId;
						MyApplication.IsLogin = true;
						PreferenceConnector.WriteString(this, "last_user_id", userId);
						PreferenceConnector.WriteString(this, "last_user_password", lastUserPassword);
						PreferenceConnector.WriteBoolean(this, "isLogin", true);
						PreferenceConnector.WriteString(this, "user_role", userRole);
						RunOnUiThread(() => StartActivity(typeof(MainActivity)));
					} else {
						userId = (string)responseObj.SelectToken("userid");
						userRole = (string)responseObj.SelectToken ("role");
						MyApplication.CurrentPassword = "";
						MyApplication.CurrentUserId = "";
						MyApplication.IsLogin = false;
						PreferenceConnector.WriteString(this, "last_user_id", null);
						PreferenceConnector.WriteString(this, "last_user_password", null);
						PreferenceConnector.WriteBoolean(this, "isLogin", false);
						PreferenceConnector.WriteString(this, "user_role", userRole);
						RunOnUiThread(() => StartActivity(typeof(LoginActivity)));
					}
				}
			} catch (Exception)	{
				RunOnUiThread(() => StartActivity(typeof(LoginActivity)));
			}
		}

		#endregion
		/// <summary>
		/// On response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed(string response) 
		{
			RunOnUiThread(() => StartActivity(typeof(LoginActivity)));
		}
	}
}