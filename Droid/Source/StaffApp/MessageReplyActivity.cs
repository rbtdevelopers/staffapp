﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.StaffApp;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using Android.Graphics;
using System.IO;
using Android.Provider;
using Newtonsoft.Json.Linq;
using Android.Content.PM;
using Android.Support.V4.Content;
using Android;
using Android.Support.V4.App;

namespace RetailerAcademy.Droid
{
	[Activity (Label = "MessageReplyActivity", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class MessageReplyActivity : BaseActivity, IWebServiceDelegate, View.IOnClickListener
	{
		private TextView cancelTV, messageHeaderTV, sendTVId, fromTV, subjectTV, attachmentTV, attachmentImgTV;
		private EditText bodyET, subjectET;
		private MultiAutoCompleteTextView fromACT;
		private Dialog progressDialog, dialog = null, permissionsDialog = null;
		private const int FROM_GALLERY_INTENT_CODE = 111;
		private Android.Net.Uri imageUri;
		private ImageView attachmentIV;
		private string senderNickName, subject, recipientName, encodedAttachedImage, imageFileName, imageFileExtension, imagePath;
		private int userMessageID, recipientUserID;
		private const int PERMISSIONS_REQUEST_READ_STORAGE = 555;

		/// <summary>
		/// Raises the create event.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.MessageReply);
			CommonMethods.TaskBarAppTheme (this);
			userMessageID = Intent.GetIntExtra ("userMessageID", 0);
			recipientUserID = Intent.GetIntExtra ("recipientUserID", 0);
			subject = Intent.GetStringExtra ("subject");
			senderNickName = Intent.GetStringExtra ("senderName");
			recipientName = Intent.GetStringExtra ("recipientName");
			LoadUI ();
		}

		/// <summary>
		/// Loads the U.
		/// </summary>
		private void LoadUI ()
		{
			cancelTV = (TextView)FindViewById (Resource.Id.cancelTV);
			messageHeaderTV = (TextView)FindViewById (Resource.Id.messageHeaderTV);
			sendTVId = (TextView)FindViewById (Resource.Id.sendTVId);
			subjectTV = (TextView)FindViewById (Resource.Id.subjectTV);
			subjectET = (EditText)FindViewById (Resource.Id.subjectET);
			fromTV = (TextView)FindViewById (Resource.Id.fromTV);
			fromACT = (MultiAutoCompleteTextView)FindViewById (Resource.Id.fromACT);
			attachmentIV = (ImageView)FindViewById (Resource.Id.attachmentIV);
			attachmentTV = (TextView)FindViewById (Resource.Id.attachmentTV);
			attachmentImgTV = (TextView)FindViewById (Resource.Id.attachmentImgTV);
			bodyET = (EditText)FindViewById (Resource.Id.bodyET);

			fromACT.Text = recipientName;

			if (!string.IsNullOrEmpty (subject) && subject.Contains ("Re:")) {
				subjectET.Text = subject;
			} else {
				subjectET.Text = "Re: " + subject;
			}

			fromACT.Enabled = false;
			cancelTV.SetOnClickListener (this);
			sendTVId.SetOnClickListener (this);
			attachmentIV.SetOnClickListener (this);
			attachmentImgTV.SetOnClickListener (this);

			cancelTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			messageHeaderTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			sendTVId.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			subjectTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			fromTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			bodyET.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			subjectET.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			fromACT.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			attachmentTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			attachmentImgTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
		}

		/// <summary>
		/// Webservice call for reply.
		/// </summary>
		private void WebServiceCallForReply ()
		{
			try {				
				if (string.IsNullOrEmpty (fromACT.Text)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.MAIL_TO);
				} else if (subjectET.Text.Trim ().Length == 0) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.MAIL_SUBJECT);
				} else if (bodyET.Text.Trim ().Length == 0) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.MAIL_BODY);
				} else if (!CommonMethods.IsInternetConnected (this)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this, "last_user_id", null);
					MessageReplyDTO dataObjects = new MessageReplyDTO ();
					dataObjects.userMessageID = userMessageID;
					dataObjects.senderUserID = Convert.ToInt32 (userId);
					dataObjects.senderNickName = "";
					dataObjects.subject = subjectET.Text;
					dataObjects.body = bodyET.Text;
					dataObjects.recipientName = recipientName;
					dataObjects.recipientUserIDs = recipientUserID;
					if (!string.IsNullOrEmpty (attachmentImgTV.Text)) {
						Bitmap thumbnailBitmap = null;
						thumbnailBitmap = Android.Provider.MediaStore.Images.Media.GetBitmap (ContentResolver, imageUri);
						MemoryStream stream = new MemoryStream ();
						thumbnailBitmap.Compress (Bitmap.CompressFormat.Jpeg, 50, stream);
						byte[] imageByteArray = stream.ToArray ();
						encodedAttachedImage = Convert.ToBase64String (imageByteArray);
						dataObjects.fileName = imageFileName;
						dataObjects.extension = imageFileExtension;
						dataObjects.imagebytes = encodedAttachedImage;
					} else {
						dataObjects.fileName = "";
						dataObjects.extension = "";
						dataObjects.imagebytes = "";
					}
					sendTVId.Enabled = false;
					string requestParameter = JsonConvert.SerializeObject (dataObjects);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.COMMUNICATION_REPLYLIST, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		#region IWebServiceDelegate implementation
		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				sendTVId.Enabled = true;
				try {
					JObject responseObj = JObject.Parse (response);
					string statusMessage = null;
					if (responseObj ["message"] != null) {
						statusMessage = (string)responseObj.SelectToken ("message");
						if (!string.IsNullOrEmpty (statusMessage)) {
							AlertDialog.Builder alertMessage = new AlertDialog.Builder (this);
							alertMessage.SetTitle ("Alert");
							alertMessage.SetCancelable (false);
							alertMessage.SetMessage (statusMessage);
							alertMessage.SetPositiveButton ("OK", (senderAlert, args) => {
								if (dialog != null) {
									Finish ();
								}
							});
							dialog = alertMessage.Create ();
							dialog.Show ();
						}
					}
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
				} catch (Exception) {	
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				sendTVId.Enabled = true;
				RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this, response);
			});
		}

		#endregion
		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				switch (id) {
				case Resource.Id.attachmentIV:
					RequestReadStoragePermission ();
					break;
				case Resource.Id.sendTVId:
					WebServiceCallForReply ();
					break;
				case Resource.Id.cancelTV:
					AlertOnCancelMessage ();
					break;
				case Resource.Id.attachmentImgTV:
					attachmentImgTV.Text = "";
					attachmentImgTV.Visibility = ViewStates.Invisible;
					break;
				case Resource.Id.okAlertOKTV:
					if (permissionsDialog != null) {
						permissionsDialog.Dismiss ();
					}
					break;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Requests the read storage permission.
		/// </summary>
		private void RequestReadStoragePermission ()
		{
			// Here, thisActivity is the current activity
			if (ContextCompat.CheckSelfPermission (this, Manifest.Permission.ReadExternalStorage) != Permission.Granted) {
				// Should we show an explanation?
				if (ActivityCompat.ShouldShowRequestPermissionRationale (this, Manifest.Permission.ReadExternalStorage)) {
					// Show an expanation to the user *asynchronously* -- don't block
					// this thread waiting for the user's response! After the user
					// sees the explanation, try again to request the permission.
					ActivityCompat.RequestPermissions (this, new String[]{ Manifest.Permission.ReadExternalStorage }, PERMISSIONS_REQUEST_READ_STORAGE);
				} else {
					// No explanation needed, we can request the permission.
					ActivityCompat.RequestPermissions (this, new String[]{ Manifest.Permission.ReadExternalStorage }, PERMISSIONS_REQUEST_READ_STORAGE);
					// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
					// app-defined int constant. The callback method gets the
					// result of the request.
				}
			} else {
				//Call camera directly, permissions are already accepted.
				Intent i = new Intent (Intent.ActionPick, Android.Provider.MediaStore.Images.Media.ExternalContentUri);
				StartActivityForResult (i, FROM_GALLERY_INTENT_CODE);
			}
		}

		/// <summary>
		/// Raises the request permissions result event.
		/// </summary>
		/// <param name="requestCode">Request code.</param>
		/// <param name="permissions">Permissions.</param>
		/// <param name="grantResults">Grant results.</param>
		public override void OnRequestPermissionsResult (int requestCode, string[] permissions, Permission[] grantResults)
		{
			switch (requestCode) {
			case PERMISSIONS_REQUEST_READ_STORAGE:
				{
					// If request is cancelled, the result arrays are empty.
					if (grantResults.Length > 0	&& grantResults [0] == Permission.Granted) {
						// permission was granted, yay! Do the
						// camera task you need to do.
						Intent i = new Intent (Intent.ActionPick, Android.Provider.MediaStore.Images.Media.ExternalContentUri);
						StartActivityForResult (i, FROM_GALLERY_INTENT_CODE);
					} else {
						// permission denied, boo! Disable the
						// functionality that depends on this permission.
						if (permissionsDialog != null) {
							permissionsDialog.Dismiss ();
						}
						permissionsDialog = CommonMethods.AlertDialogForPermissions (this, Arial, this, true);
					}
					return;
				}
			}
		}

		/// <summary>
		/// Alert Dialog display on cancel message.
		/// </summary>
		private void AlertOnCancelMessage ()
		{
			AlertDialog.Builder alert = new AlertDialog.Builder (this);
			alert.SetTitle ("Alert");
			alert.SetCancelable (false);
			alert.SetMessage ("Are you sure you want to cancel Message?");
			alert.SetPositiveButton ("OK", (senderAlert, args) => {
				if (dialog != null) {
					Finish ();
				}
			});
			alert.SetNegativeButton ("CANCEL", (senderAlert, args) => {
				if (dialog != null) {
					dialog.Dismiss ();
				}
			});
			dialog = alert.Create ();
			dialog.Show ();

		}

		/// <param name="requestCode">The integer request code originally supplied to
		///  startActivityForResult(), allowing you to identify who this
		///  result came from.</param>
		/// <param name="resultCode">The integer result code returned by the child activity
		///  through its setResult().</param>
		/// <param name="data">An Intent, which can return result data to the caller
		///  (various data can be attached to Intent "extras").</param>
		/// <summary>
		/// Called when an activity you launched exits, giving you the requestCode
		///  you started it with, the resultCode it returned, and any additional
		///  data from it.
		/// </summary>
		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			try {
				if (data != null) {
					imageUri = data.Data;
					if (imageUri != null) {
						if (!imageUri.ToString ().Contains ("file")) {							
							imagePath = GetRealPathFromURI (imageUri);
						} else {
							imagePath = imageUri.ToString ();
						}
						if (!string.IsNullOrEmpty (imagePath)) {
							var fileName = imagePath.Substring (imagePath.LastIndexOf ("/") + 1);
							string[] imageNameArray = fileName.Split ('.');
							imageFileName = imageNameArray [0];
							imageFileExtension = fileName.Substring (fileName.LastIndexOf ("."));
							if (!string.IsNullOrEmpty (fileName)) {
								attachmentImgTV.Text = fileName;
								attachmentImgTV.Visibility = ViewStates.Visible;
							} else {
								attachmentImgTV.Text = "";
								attachmentImgTV.Visibility = ViewStates.Invisible;
							}
						} else {
							CommonMethods.ShowAlertDialog (this, CommonSharedStrings.FILE_CORRUPT_ERROR);
						}
					} else {
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.FILE_CORRUPT_ERROR);
					}
				}
			} catch (Exception) {
				CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
			}
		}

		/// <summary>
		/// Gets the real path from UR.
		/// </summary>
		/// <returns>The real path from UR.</returns>
		/// <param name="contentUri">Content URI.</param>
		public string GetRealPathFromURI (Android.Net.Uri contentUri)
		{
			try {
				string[] filePathColumn = { MediaStore.MediaColumns.Data };
				var cursor = ContentResolver.Query (contentUri, filePathColumn, null, null, null);
				cursor.MoveToFirst ();
				int columnIndex = cursor.GetColumnIndex (filePathColumn [0]);
				string picturePath = cursor.GetString (columnIndex);
				cursor.Close ();
				return picturePath;
			} catch (Exception) {
				return null;
			}
		}
	}
}