﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.StaffApp;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;

namespace RetailerAcademy.Droid
{
	[Activity (Label = "VideoPlayerActivity", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]			
	public class VideoPlayerActivity : BaseActivity, Android.Media.MediaPlayer.IOnCompletionListener, Android.Media.MediaPlayer.IOnPreparedListener
	{
		private string videoUrl;
		private VideoView videoView;
		private ProgressBar progressBar;
		private TextView loadingTV;
		private int seekPosition;
		private bool isVideoPaused;

		/// <summary>
		/// Raises the create event.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.VideoView);
			CommonMethods.TaskBarAppTheme (this);
			videoUrl = Intent.GetStringExtra ("videoUrl");
			videoView = (VideoView)FindViewById (Resource.Id.videoView);
			progressBar = (ProgressBar)FindViewById (Resource.Id.pBar);
			loadingTV = (TextView)FindViewById (Resource.Id.loadingTV);
			loadingTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			videoView.SetOnCompletionListener (this);
			videoView.SetOnPreparedListener (this);
			LoadVideo (videoUrl);
		}

		/// <summary>
		/// Loads the video.
		/// </summary>
		/// <param name="video_url">Video URL.</param>
		private void LoadVideo(string video_url)
		{
			if (!string.IsNullOrEmpty (video_url)) {
				MediaController mediaController = new MediaController (this);
				mediaController.SetAnchorView (videoView);
				Android.Net.Uri video = Android.Net.Uri.Parse (video_url);
				videoView.SetMediaController (mediaController);
				videoView.SetVideoURI (video);
				videoView.Start ();
			} else {
				CommonMethods.ShowAlertDialog (this, CommonSharedStrings.FILE_CORRUPT_ERROR);
			}
		}

		/// <summary>
		/// Raises the completion event.
		/// </summary>
		/// <param name="mp">Mp.</param>
		public void OnCompletion (Android.Media.MediaPlayer mp)
		{
			this.Finish();
		}

		/// <summary>
		/// Raises the prepared event.
		/// </summary>
		/// <param name="mp">Mp.</param>
		public void OnPrepared (Android.Media.MediaPlayer mp)
		{
			progressBar.Visibility = ViewStates.Invisible;
			loadingTV.Visibility = ViewStates.Invisible;
		}

		/// <summary>
		/// Called as part of the activity lifecycle when an activity is going into
		///  the background, but has not (yet) been killed.
		/// </summary>
		protected override void OnPause ()
		{
			base.OnPause ();
			seekPosition = videoView.CurrentPosition;
			videoView.Pause ();
			isVideoPaused = true;
		}

		/// <summary>
		/// Called when the activity has detected the user's press of the back
		///  key.
		/// </summary>
		public override void OnBackPressed ()
		{
			base.OnBackPressed ();
		}

		/// <summary>
		/// Raises the resume event.
		/// </summary>
		protected override void OnResume ()
		{
			base.OnResume ();
			if (isVideoPaused) {
				isVideoPaused = false;
				videoView.SeekTo (seekPosition);
				videoView.Start ();
			}
		}
	}
}