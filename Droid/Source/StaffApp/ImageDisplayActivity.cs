﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.StaffApp;
using Android.Webkit;
using Com.Nostra13.Universalimageloader.Core;
using RetailerAcademy.Droid.Source.Utilities;
using Android.Graphics;

namespace RetailerAcademy.Droid
{
	[Activity (Label = "ImageDisplayActivity", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]			
	public class ImageDisplayActivity : Activity, Android.Views.View.IOnClickListener
	{
		private Android.Webkit.WebView contentViewWV;
		private static LinearLayout progressBarContainer;
		private ImageLoader imageLoader;
		private DisplayImageOptions options;
		private TextView imageCrossTV;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ImageDisplayActivity"/> class.
		/// </summary>
		public ImageDisplayActivity(){
			imageLoader = ImageLoader.Instance;
			options = CommonMethods.ReturnDisplayOptions(Resource.Drawable.user_profile);
		}

		/// <summary>
		/// Raises the create event.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView(Resource.Layout.ImageDisplay);
			CommonMethods.TaskBarAppTheme (this);
			var imageUrl = Intent.GetStringExtra ("activityAttachment");
			imageCrossTV = (TextView)FindViewById (Resource.Id.imageCrossTV);
			contentViewWV = FindViewById<Android.Webkit.WebView> (Resource.Id.contentViewWV);
			progressBarContainer = FindViewById<LinearLayout> (Resource.Id.progressBarContainer);
			imageCrossTV.SetOnClickListener (this);
			contentViewWV.Settings.BuiltInZoomControls = true;
			contentViewWV.Settings.LoadWithOverviewMode = true;
			contentViewWV.Settings.UseWideViewPort = true;
			contentViewWV.Settings.DisplayZoomControls = false;
			contentViewWV.SetWebViewClient (new MyWebViewClient ());
			contentViewWV.LoadData("<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} img{width:100%25;} </style></head><body><img src='"+imageUrl+"'/></body></html>" ,"text/html",  "UTF-8");
		}

		/// <summary>
		/// Called when the activity has detected the user's press of the back
		///  key.
		/// </summary>
		public override void OnBackPressed ()
		{
			base.OnBackPressed ();
		}

		private class MyWebViewClient : WebViewClient
		{
			public override bool ShouldOverrideUrlLoading (WebView view, string url)
			{
				view.LoadUrl (url);
				return true;
			}

			public override void OnPageStarted (WebView view, string url, Android.Graphics.Bitmap favicon)
			{
				ImageDisplayActivity.progressBarContainer.Visibility = ViewStates.Visible;
				base.OnPageStarted (view, url, favicon);
			}

			public override void OnPageFinished (WebView view, string url)
			{
				ImageDisplayActivity.progressBarContainer.Visibility = ViewStates.Gone;
				base.OnPageFinished (view, url);
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			int id = v.Id;
			switch (id) {
			case Resource.Id.imageCrossTV:
				Finish ();
				break;
			}
		}
	}
}