﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using RetailerAcademy.Droid.Source.Fragments;
using RetailerAcademy.Droid.Source.Utilities;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Java.Lang;
using Android.Preferences;
using RetailerAcademy.Droid.Source.Adapters;
using RetailerAcademy.Droid.Source.ContentDTO;
using Newtonsoft.Json.Linq;
using Android.Graphics;
using Newtonsoft.Json;
using Com.Nostra13.Universalimageloader.Core;
using Android.Gms.Common;
using Java.Text;
using Java.Util;
using Android.Views.InputMethods;
using Android.Graphics.Drawables;
using Android.Text;
using Android.Text.Method;


namespace RetailerAcademy.Droid.StaffApp
{
	[Activity (Label = "MainActivity", ScreenOrientation = ScreenOrientation.Portrait, ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.ScreenSize | ConfigChanges.KeyboardHidden, WindowSoftInputMode = SoftInput.AdjustResize, HardwareAccelerated = true)]
	public class MainActivity : BaseActivity, View.IOnClickListener, AdapterView.IOnItemClickListener, IWebServiceDelegate
	{
		private DrawerLayout leftMenuSlider;
		private ListView drawerList, switchBrandsLV;
		private FrameLayout frameLayout;
		private ImageButton menuImageButton, orgsImageButton;
		private TextView notificationCountTV, dialogTitleTV, dialogDescTV, dialogAcceptTV;
		private MyActionBarDrawerToggle mDrawerToggle;
		private SwitchBrandsPagerAdapter pagerAdapter;
		private MenuListAdapter menuListAdapter;
		private Dialog progressDialog, dialog = null, switchBrandsDialog, alertDialog, termsConditionDialog;
		private List<BrandOrgDetailsDto> brandOrgsList;
		private bool logoutService, hasDefaultBrand;
		private Android.App.Fragment currentVisibleFragment;
		private MessagesListFragment messagesFrag = null;
		private DashboardFragment dashboardFragment = null;
		private ContentFolderListFragment contentFolderFrag = null;
		private QuizCategoryListFragment quizCategoryListFragment = null;
		private LeaderBoardNewUserFragment leaderBoardNewUserFragment = null;
		private ActivityStreamListFragment activityStreamListFrag = null;
		private BlogCategoryListFragment blogCategoryListFrag = null;
		private NotificationsListFragment notificationsListFrag = null;
		private ViewProfileFragment viewProfileFrag = null;
		private BrandDetailsFragment brandDetailsFrag = null;
		private StateDetailsFragment stateDetailsFrag = null;
		private TeamDetailsFragment teamDetailsFrag = null;
		private int badgeCountState;
		private string titleText, detailsText, buttonText, brandOrgId, brandName, moduleName, moduleid, blogcategoryid;
		private AlertDialog.Builder alert = null;
		private NavigationListDTO navigationListItemDto;
		private View activityRootLayout;
		public bool switchBrands, termsAndCondition;
		public Android.App.FragmentManager fragmentManager;
		public static MainActivity Instance;
		public static bool appStatus;

		/// <summary>
		/// Raises the create event.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Main);
			CommonMethods.TaskBarAppTheme (this);
			MainActivity.Instance = this;
			appStatus = true;
			try {
				fragmentManager = FragmentManager;
				if (!CommonMethods.IsInternetConnected (this)) {
					if (alert == null) {
						alert = new AlertDialog.Builder (this);
					}
					alert.SetTitle ("Alert");
					alert.SetCancelable (true);
					alert.SetMessage (CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					alert.SetPositiveButton ("Refresh", (senderAlert, args) => {
						if (dialog != null) {
							dialog.Dismiss ();
							this.Recreate ();
						}
					});
					dialog = alert.Create ();
					dialog.Show ();
					return;
				} else {
					LoadUI ();
					LoadNavList ();
					ImageLoader imageLoader = ImageLoader.Instance;
					imageLoader.Init (ImageLoaderConfiguration.CreateDefault (this));
					brandOrgId = Intent.GetStringExtra ("brandOrgId");
					brandName = Intent.GetStringExtra ("brandName");
					moduleName = Intent.GetStringExtra ("moduleName");
					moduleid = Intent.GetStringExtra ("moduleid");
					blogcategoryid = Intent.GetStringExtra ("blogcategoryid"); 
					WebServiceForTermsConditions (0);
					if (IsPlayServicesAvailable ()) {
						var intent = new Intent (this, typeof(RegistrationIntentService));
						StartService (intent);
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Webservice for terms conditions dialog to display or not.
		/// </summary>
		/// <param name="key">Key.</param>
		private void WebServiceForTermsConditions (int key)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {				
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this, "Loading, please wait...");
					}
					termsAndCondition = true;
					var userId = PreferenceConnector.ReadString (this, "last_user_id", null);
					TermsConditionsDTO termsConditionsDTO = new TermsConditionsDTO ();
					termsConditionsDTO.userid = Convert.ToInt32 (userId);
					termsConditionsDTO.key = key;
					string requestParameter = JsonConvert.SerializeObject (termsConditionsDTO);
					JsonService jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.TERMSANDCONDITION, requestParameter, this);
				}
			} catch (System.Exception) {			
			}
		}

		/// <summary>
		/// Dashboard count field updation.
		/// </summary>
		public void DashboardCountUpdation ()
		{
			try {
				if (dashboardFragment != null) {
					dashboardFragment.UpdateCountValues ();
				}
				if (dashboardFragment.communicationDisplayFragment != null) {
					dashboardFragment.communicationDisplayFragment.UpdateCountInCommunicate ();
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Updates the notification count.
		/// </summary>
		public void UpdateNotificationCount ()
		{
			try {
				badgeCountState = PreferenceConnector.ReadInteger (this, "notificationCount", 0);
				if (notificationCountTV != null) {
					if (badgeCountState > 0) {				
						notificationCountTV.Visibility = ViewStates.Visible;
						CommonMethods.CommentsOrLikesCountDisplay (badgeCountState, notificationCountTV);
					} else {
						notificationCountTV.Visibility = ViewStates.Gone;
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the resume event.
		/// </summary>
		protected override void OnResume ()
		{
			base.OnResume ();
			UpdateNotificationCount ();
		}

		/// <summary>
		/// Called as part of the activity lifecycle when an activity is going into
		///  the background, but has not (yet) been killed.
		/// </summary>
		protected override void OnPause ()
		{
			base.OnPause ();
		}

		/// <summary>
		/// Webservice call for user orgs/brands to display the list of the brands associated to the user.
		/// </summary>
		public void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {				
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this, "Loading, please wait...");
					}
					var userIdSend = MyApplication.UserId;
					if (string.IsNullOrEmpty (userIdSend)) {
						userIdSend = PreferenceConnector.ReadString (this, "last_user_id", null);
					}
					JsonService jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.USER_ORGS + userIdSend, null, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Switchbrands popup dialog display.
		/// </summary>
		/// <param name="brandDetailsList">Brand details list.</param>
		/// <param name="alertDisplay">If set to <c>true</c> alert display.</param>
		/// <param name="isCancelable">If set to <c>true</c> is cancelable.</param>
		private void SwitchBrandsPopup (List<BrandOrgDetailsDto> brandDetailsList, bool alertDisplay, bool isCancelable)
		{
			try {
				switchBrandsDialog = new Dialog (this);
				switchBrandsDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
				switchBrandsDialog.Window.RequestFeature (WindowFeatures.NoTitle);
				switchBrandsDialog.SetContentView (Resource.Layout.SwitchBrandsFragment);
				switchBrandsLV = switchBrandsDialog.FindViewById<ListView> (Resource.Id.switchBrandsLV);
				if (alertDisplay) {
					switchBrandsLV.ItemClick += SwitchBrandsLV_ItemClick;
					switchBrandsDialog.SetCancelable (isCancelable);
					switchBrandsDialog.Show ();
				}
				pagerAdapter = new SwitchBrandsPagerAdapter (this, Resource.Layout.SwitchBrandsListItem, brandDetailsList);
				switchBrandsLV.Adapter = pagerAdapter;
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Terms and conditions popup dialog.
		/// </summary>
		/// <param name="titleText">Title text.</param>
		/// <param name="detailsText">Details text.</param>
		/// <param name="buttonText">Button text.</param>
		private void TermsAndConditionsPopup (string titleText, string detailsText, string buttonText)
		{
			try {
				termsConditionDialog = new Dialog (this);
				termsConditionDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
				termsConditionDialog.Window.RequestFeature (WindowFeatures.NoTitle);
				termsConditionDialog.SetContentView (Resource.Layout.TermsAndConditionsDialog);
				termsConditionDialog.SetCancelable (false);
				dialogTitleTV = termsConditionDialog.FindViewById<TextView> (Resource.Id.dialogTitleTV);
				dialogDescTV = termsConditionDialog.FindViewById<TextView> (Resource.Id.dialogDescTV);
				dialogAcceptTV = termsConditionDialog.FindViewById<TextView> (Resource.Id.dialogAcceptTV);
				dialogTitleTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				dialogDescTV.SetTypeface (OswaldLight, TypefaceStyle.Normal);
				dialogAcceptTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				dialogAcceptTV.SetOnClickListener (this);
				dialogDescTV.MovementMethod = new ScrollingMovementMethod ();
				dialogTitleTV.Text = titleText ?? "IMPORTANT";
				ISpanned spanned = Html.FromHtml (detailsText);
				dialogDescTV.SetText (spanned, TextView.BufferType.Spannable);
				dialogAcceptTV.Text = buttonText ?? "I UNDERSTAND";
				termsConditionDialog.Show ();
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Loads the navigation menu list.
		/// </summary>
		private void LoadNavList ()
		{
			try {
				List<NavigationListDTO> navMenuList = new List<NavigationListDTO> ();
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "Dashboard";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_dashboard;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "Messages";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_messages;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "Knowledge";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_content;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "Quiz Master";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_quiz_master;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "Retail Status";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_retail_status_trophy;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "Trending";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_news_feed;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "What's Hot";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_whats_hot;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "Notifications";
				navigationListItemDto.itemImgResource = Resource.Drawable.notification;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "SETTINGS";
				navigationListItemDto.itemImgResource = 1;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "Profile";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_profile;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "Brand Details";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_brand_details;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "State Details";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_state_details;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "Team Details";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_group_details;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "Switch Brands";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_switch_brands;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = "Logout";
				navigationListItemDto.itemImgResource = Resource.Drawable.menu_logout;
				navMenuList.Add (navigationListItemDto);
				navigationListItemDto = new NavigationListDTO ();
				navigationListItemDto.itemName = CommonSharedStrings.CURRENT_VERSION;
				navigationListItemDto.itemImgResource = 0;
				navMenuList.Add (navigationListItemDto);
				menuListAdapter = new MenuListAdapter (this, Resource.Layout.MenuListItem, navMenuList);
				drawerList.Adapter = menuListAdapter;
				drawerList.OnItemClickListener = this;
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Called when the activity has detected the user's press of the back
		///  key.
		/// </summary>
		public override void OnBackPressed ()
		{
			try {
				if (leftMenuSlider.IsDrawerOpen (GravityCompat.Start)) {
					leftMenuSlider.CloseDrawer (GravityCompat.Start);
				} else {
					menuListAdapter.ResetMenuListColor ();
					menuListAdapter.save = -1;
					if ((FragmentManager.BackStackEntryCount > 0)) {
						currentVisibleFragment = (Android.App.Fragment)FragmentManager.FindFragmentByTag ("dashboard_fragment");
						if ("dashboard_fragment".Equals (FragmentManager.GetBackStackEntryAt (FragmentManager.BackStackEntryCount - 1).Name) || (currentVisibleFragment != null && currentVisibleFragment.IsVisible)) {
							RunOnUiThread (() => {
								Finish ();
							});
						} else if ("quiz_radio_btns_fragment".Equals (FragmentManager.GetBackStackEntryAt (FragmentManager.BackStackEntryCount - 1).Name)) {
							if (alert == null) {
								alert = new AlertDialog.Builder (this);
							}
							alert.SetTitle ("Alert");
							alert.SetCancelable (false);
							alert.SetMessage ("Are you sure you want to cancel Quiz?");
							alert.SetPositiveButton ("OK", (senderAlert, args) => {
								if (dialog != null) {
									dialog.Dismiss ();
									this.FragmentManager.PopBackStack ();
								}
							});
							alert.SetNegativeButton ("CANCEL", (senderAlert, args) => {
								if (dialog != null) {
									dialog.Dismiss ();
								}
							});
							dialog = alert.Create ();
							dialog.Show ();
						} else {
							this.FragmentManager.PopBackStack ();
						}
					} else {
						Finish ();
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Disables the navigation drawer.
		/// </summary>
		public void DisableNavigationDrawer ()
		{
			leftMenuSlider.SetDrawerLockMode (DrawerLayout.LockModeLockedClosed);
		}

		/// <summary>
		/// Enables the navigation drawer.
		/// </summary>
		public void EnableNavigationDrawer ()
		{
			leftMenuSlider.SetDrawerLockMode (DrawerLayout.LockModeUnlocked);
		}

		/// <summary>
		/// Logout service ends the user session.
		/// </summary>
		public void LogoutService ()
		{
			try {
				if (progressDialog == null) {
					progressDialog = CommonMethods.GetProgressDialog (this, "Loading, please wait...");
				}
				logoutService = true;
				var userId = MyApplication.UserId;
				var deviceToken = MyApplication.DeviceToken;
				if (string.IsNullOrEmpty (deviceToken)) {
					deviceToken = PreferenceConnector.ReadString (this, "device_token", null);
				}
				if (string.IsNullOrEmpty (userId)) {
					userId = PreferenceConnector.ReadString (this, "last_user_id", null);
				}
				JsonService jsonService = new JsonService ();
				Logout logout = new Logout ();
				logout.userid = Convert.ToInt32 (userId);
				logout.deviceToken = deviceToken;
				logout.platform = "android";
				string requestParameter = JsonConvert.SerializeObject (logout);
				jsonService.consumeService (CommonSharedStrings.LOGOUT, requestParameter, this);
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Logout popup.
		/// </summary>
		private void LogOutPopup ()
		{
			try {
				if (alert == null) {
					alert = new AlertDialog.Builder (this);
				}
				alert.SetTitle ("Alert");
				alert.SetCancelable (false);
				alert.SetMessage ("Are you sure you want to logout?");
				alert.SetPositiveButton ("OK", (senderAlert, args) => {
					if (dialog != null) {
						dialog.Dismiss ();
						LogoutService ();
					}
				});
				alert.SetNegativeButton ("CANCEL", (senderAlert, args) => {
					if (dialog != null) {
						dialog.Dismiss ();
					}
				});
				dialog = alert.Create ();
				dialog.Show ();
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Loads the UI.
		/// </summary>
		private void LoadUI ()
		{
			try {
				leftMenuSlider = FindViewById<DrawerLayout> (Resource.Id.drawer_layout);
				drawerList = (ListView)FindViewById (Resource.Id.left_drawer);
				frameLayout = (FrameLayout)FindViewById (Resource.Id.childContainer);
				menuImageButton = (ImageButton)FindViewById (Resource.Id.headerMenu);
				orgsImageButton = (ImageButton)FindViewById (Resource.Id.headerOrgs);
				notificationCountTV = (TextView)FindViewById (Resource.Id.notificationCountTV);
				activityRootLayout = (View)FindViewById (Resource.Id.rootViewLayout);
				notificationCountTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);		
				mDrawerToggle = new MyActionBarDrawerToggle (this, leftMenuSlider, new Android.Support.V7.Widget.Toolbar (this), Resource.String.open_text, Resource.String.close_text, drawerList, frameLayout);
				this.leftMenuSlider.SetDrawerListener (this.mDrawerToggle);
				menuImageButton.SetOnClickListener (this);
				orgsImageButton.SetOnClickListener (this);
				dashboardFragment = new DashboardFragment (this);
				fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, dashboardFragment).Commit ();
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Switchbrands Listview item click.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void SwitchBrandsLV_ItemClick (object sender, AdapterView.ItemClickEventArgs e)
		{
			try {
				var orgId = pagerAdapter.brandDetails [e.Position].numberCount;
				var orgName = pagerAdapter.brandDetails [e.Position].brandName.ToUpper ();
				var userRole = pagerAdapter.brandDetails [e.Position].userRole;
				var totalNewBlogs = pagerAdapter.brandDetails [e.Position].totalNewBlogs;
				var totalNewContent = pagerAdapter.brandDetails [e.Position].totalNewContent;
				var totalNewQuiz = pagerAdapter.brandDetails [e.Position].totalNewQuiz;
				PreferenceConnector.WriteInteger (this, "newContentCount", totalNewContent);
				PreferenceConnector.WriteInteger (this, "newQuizCount", totalNewQuiz);
				PreferenceConnector.WriteInteger (this, "newBlogsCount", totalNewBlogs);
				DashboardCountUpdation ();
				MyApplication.OrgId = orgId;
				MyApplication.OrgName = orgName;
				MyApplication.UserRole = userRole;
				PreferenceConnector.WriteString (this, "org_id", orgId);
				PreferenceConnector.WriteString (this, "org_name", orgName);
				PreferenceConnector.WriteString (this, "user_role", userRole);
				UpdateDashboardOrgName (orgName);
				switchBrandsDialog.Dismiss ();
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Updates the name of the org on the dashboard and communication display fragment label.
		/// </summary>
		/// <param name="orgName">Org name.</param>
		public void UpdateDashboardOrgName (string orgName)
		{
			try {
				if (dashboardFragment.communicationDisplayFragment != null) {
					dashboardFragment.communicationDisplayFragment.orgsTitleTVId.Text = orgName;
				}
				if (dashboardFragment != null) {
					dashboardFragment.orgsTitleTVId.Text = orgName;
				}
			} catch (System.Exception) {				
			}				
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				View view = this.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				int id = v.Id;
				switch (id) {
				case Resource.Id.headerMenu:
					if (leftMenuSlider.IsDrawerOpen (GravityCompat.Start)) {
						leftMenuSlider.CloseDrawer (GravityCompat.Start);
					} else {
						leftMenuSlider.OpenDrawer (GravityCompat.Start);
						menuListAdapter.NotifyDataSetChanged ();
					}
					break;
				case Resource.Id.headerOrgs:
					if (leftMenuSlider.IsDrawerOpen (GravityCompat.Start)) {
						leftMenuSlider.CloseDrawer (GravityCompat.Start);
					} else {
						if ((FragmentManager.BackStackEntryCount > 0)) {						
							menuListAdapter.ResetMenuListColor ();
							menuListAdapter.save = -1;
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, dashboardFragment, "dashboard_fragment").AddToBackStack ("dashboard_fragment").Commit ();
						}
					}
					break;
				case Resource.Id.dialogAcceptTV:
					WebServiceForTermsConditions (1);
					break;
				default:
					break;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the item click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			try {
				if (position != 8 && position != 15) {
					leftMenuSlider.CloseDrawer (GravityCompat.Start);
					Thread.Sleep (300);
					menuListAdapter.save = position;
					fragmentManager.PopBackStackImmediate (null, Android.App.PopBackStackFlags.Inclusive);
					switch (position) {
					case 0:
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, dashboardFragment, "dashboard_fragment").AddToBackStack ("dashboard_fragment").Commit ();
						break;
					case 1: 
						currentVisibleFragment = (Android.App.Fragment)FragmentManager.FindFragmentByTag ("messages_list_fragment");
						if (currentVisibleFragment != null && currentVisibleFragment.IsVisible) {
							leftMenuSlider.CloseDrawer (GravityCompat.Start);
						} else {
							if (messagesFrag == null) {
								messagesFrag = new MessagesListFragment (this);
							}
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, messagesFrag, "messages_list_fragment").AddToBackStack ("messages_list_fragment").Commit ();
						}
						break;
					case 2:
						currentVisibleFragment = (Android.App.Fragment)FragmentManager.FindFragmentByTag ("content_folder_list_fragment");
						if (currentVisibleFragment != null && currentVisibleFragment.IsVisible) {
							leftMenuSlider.CloseDrawer (GravityCompat.Start);
						} else {
							if (contentFolderFrag == null) {
								contentFolderFrag = new ContentFolderListFragment (this);
							}
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentFolderFrag, "content_folder_list_fragment").AddToBackStack ("content_folder_list_fragment").Commit ();
						}
						break;
					case 3:
						currentVisibleFragment = (Android.App.Fragment)FragmentManager.FindFragmentByTag ("quiz_category_list_fragment");
						if (currentVisibleFragment != null && currentVisibleFragment.IsVisible) {
							leftMenuSlider.CloseDrawer (GravityCompat.Start);
						} else {
							if (quizCategoryListFragment == null) {
								quizCategoryListFragment = new QuizCategoryListFragment (this);
							}
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, quizCategoryListFragment, "quiz_category_list_fragment").AddToBackStack ("quiz_category_list_fragment").Commit ();
						}
						break;
					case 4:
						currentVisibleFragment = (Android.App.Fragment)FragmentManager.FindFragmentByTag ("leaderboard_status_fragment");
						if (currentVisibleFragment != null && currentVisibleFragment.IsVisible) {
							leftMenuSlider.CloseDrawer (GravityCompat.Start);
						} else {
							if (leaderBoardNewUserFragment == null) {
								leaderBoardNewUserFragment = new LeaderBoardNewUserFragment (this);
							}
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, leaderBoardNewUserFragment, "leaderboard_status_fragment").AddToBackStack ("leaderboard_status_fragment").Commit ();
						}
						break;
					case 5:
						currentVisibleFragment = (Android.App.Fragment)FragmentManager.FindFragmentByTag ("activity_stream_list_fragment");
						if (currentVisibleFragment != null && currentVisibleFragment.IsVisible) {
							leftMenuSlider.CloseDrawer (GravityCompat.Start);
						} else {
							if (activityStreamListFrag == null) {
								activityStreamListFrag = new ActivityStreamListFragment (this);
							}
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, activityStreamListFrag, "activity_stream_list_fragment").AddToBackStack ("activity_stream_list_fragment").Commit ();
						}
						break;
					case 6:
						currentVisibleFragment = (Android.App.Fragment)FragmentManager.FindFragmentByTag ("blogs_category_list");
						if (currentVisibleFragment != null && currentVisibleFragment.IsVisible) {
							leftMenuSlider.CloseDrawer (GravityCompat.Start);
						} else {
							if (blogCategoryListFrag == null) {
								blogCategoryListFrag = new BlogCategoryListFragment (this);
							}
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, blogCategoryListFrag, "blogs_category_list").AddToBackStack ("blogs_category_list").Commit ();
						}
						break;
					case 7:
						currentVisibleFragment = (Android.App.Fragment)FragmentManager.FindFragmentByTag ("notifications_list_fragment");
						if (currentVisibleFragment != null && currentVisibleFragment.IsVisible) {
							leftMenuSlider.CloseDrawer (GravityCompat.Start);
						} else {
							if (notificationsListFrag == null) {
								notificationsListFrag = new NotificationsListFragment (this);
							}
							notificationsListFrag.isComingBack = false;
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, notificationsListFrag, "notifications_list_fragment").AddToBackStack ("notifications_list_fragment").Commit ();
						}
						break;
					case 9:
						currentVisibleFragment = (Android.App.Fragment)FragmentManager.FindFragmentByTag ("view_profile_fragment");
						if (currentVisibleFragment != null && currentVisibleFragment.IsVisible) {
							leftMenuSlider.CloseDrawer (GravityCompat.Start);
						} else {
							if (viewProfileFrag == null) {
								viewProfileFrag = new ViewProfileFragment (this);
							}
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, viewProfileFrag, "view_profile_fragment").AddToBackStack ("view_profile_fragment").Commit ();
						}
						break;
					case 10:
						currentVisibleFragment = (Android.App.Fragment)FragmentManager.FindFragmentByTag ("brand_details_fragment");
						if (currentVisibleFragment != null && currentVisibleFragment.IsVisible) {
							leftMenuSlider.CloseDrawer (GravityCompat.Start);
						} else {
							if (brandDetailsFrag == null) {
								brandDetailsFrag = new BrandDetailsFragment (this);
							}
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, brandDetailsFrag, "brand_details_fragment").AddToBackStack ("brand_details_fragment").Commit ();
						}
						break;
					case 11:
						currentVisibleFragment = (Android.App.Fragment)FragmentManager.FindFragmentByTag ("state_details_fragment");
						if (currentVisibleFragment != null && currentVisibleFragment.IsVisible) {
							leftMenuSlider.CloseDrawer (GravityCompat.Start);
						} else {
							if (stateDetailsFrag == null) {
								stateDetailsFrag = new StateDetailsFragment (this);
							}
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, stateDetailsFrag, "state_details_fragment").AddToBackStack ("state_details_fragment").Commit ();
						}
						break;
					case 12:
						currentVisibleFragment = (Android.App.Fragment)FragmentManager.FindFragmentByTag ("team_details_fragment");
						if (currentVisibleFragment != null && currentVisibleFragment.IsVisible) {
							leftMenuSlider.CloseDrawer (GravityCompat.Start);
						} else {
							if (teamDetailsFrag == null) {
								teamDetailsFrag = new TeamDetailsFragment (this);
							}
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, teamDetailsFrag, "team_details_fragment").AddToBackStack ("team_details_fragment").Commit ();
						}
						break;
					case 13:
						switchBrands = true;
						WebServiceCall ();
						break;
					case 14:
						LogOutPopup ();
						break;
					default:
						break;
					}
					menuListAdapter.NotifyDataSetChanged ();
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Dos the logout.
		/// </summary>
		private void DoLogout ()
		{
			try {
				dialog.Dismiss ();
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				StartActivity (typeof(LoginActivity));
				Finish ();
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Logout service popup after the service response.
		/// </summary>
		/// <param name="message">Message.</param>
		public void LogoutServicePopup (string message)
		{
			try {
				PreferenceConnector.WriteString (this, "last_user_password", null);
				PreferenceConnector.WriteBoolean (this, "isLogin", false); 
				MyApplication.CurrentPassword = "";
				MyApplication.IsLogin = false;	
				MyApplication.OrgId = "";
				MyApplication.OrgName = "";
				logoutService = false;
				if (!string.IsNullOrEmpty (message)) {
					AlertDialog.Builder	alert = new AlertDialog.Builder (this);
					alert.SetTitle ("Alert");
					alert.SetCancelable (false);
					alert.SetMessage (message);
					alert.SetPositiveButton ("OK", (senderAlert, args) => {										
						if (dialog != null) {
							dialog.Dismiss ();
							DoLogout ();
						}
					});
					dialog = alert.Create ();
					dialog.Show ();
				} else {
					DoLogout ();
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Respective page navigation on the pushnotification arrival.
		/// </summary>
		private void RespectivePageNavigation ()
		{
			var moduleNameLowerCase = moduleName.ToLower ();
			switch (moduleNameLowerCase) {
			case "content":	
				CommonMethods.NavigateToRespectiveFragment (this, fragmentManager, moduleName, moduleid, "");
				break;
			case "whatshot": 
				CommonMethods.NavigateToRespectiveFragment (this, fragmentManager, moduleName, moduleid, blogcategoryid);
				break;
			default:
				CommonMethods.NavigateToRespectiveFragment (this, fragmentManager, moduleName, "", "");
				break;
			}
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					brandOrgsList = new List<BrandOrgDetailsDto> ();
					bool status = false;
					string statusMessage = null;
					JArray brandsListArray = null;
					int badgeCount = 0;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = (string)responseObj.SelectToken ("message");
					}
					if (status) {
						if (termsAndCondition) {
							titleText = (string)responseObj.SelectToken ("titleText");
							detailsText = (string)responseObj.SelectToken ("detailsText");
							buttonText = (string)responseObj.SelectToken ("buttonText");
							RunOnUiThread (() => TermsAndConditionsPopup (titleText, detailsText, buttonText)); 
							termsAndCondition = false;
							RunOnUiThread (() => {
								if (progressDialog != null) {
									progressDialog.Dismiss ();
									progressDialog = null;
								}
							});
							return;
						}
						if (logoutService) {							
							RunOnUiThread (() => LogoutServicePopup (statusMessage));
							RunOnUiThread (() => {
								if (progressDialog != null) {
									progressDialog.Dismiss ();
									progressDialog = null;
								}
							});
							return;
						}
						if (responseObj ["brands"] != null) {
							brandsListArray = (JArray)responseObj.SelectToken ("brands");
						}
						if (responseObj ["badge"] != null) {
							badgeCount = (int)responseObj.SelectToken ("badge");
						}
						PreferenceConnector.WriteInteger (this, "notificationCount", badgeCount);
						RunOnUiThread (() => UpdateNotificationCount ());
						if (brandsListArray != null && brandsListArray.Count > 0) {
							foreach (JObject item in brandsListArray) {
								BrandOrgDetailsDto brandOrgsMain = new BrandOrgDetailsDto ();
								brandOrgsMain.numberCount = (string)item.SelectToken ("orgid");
								brandOrgsMain.brandImageURL = (string)item.SelectToken ("orglogo");
								brandOrgsMain.brandName = (string)item.SelectToken ("orgname");
								brandOrgsMain.userRole = (string)item.SelectToken ("role");
								brandOrgsMain.totalNewBlogs = (int)item.SelectToken ("totalNewBlogs");
								brandOrgsMain.totalNewContent = (int)item.SelectToken ("totalNewContent");
								brandOrgsMain.totalNewQuiz = (int)item.SelectToken ("totalNewQuiz");
								bool defaultBrand = (bool)item.SelectToken ("defaultbrand");
								brandOrgsMain.defaultBrand = defaultBrand;
								brandOrgsList.Add (brandOrgsMain);
								if (defaultBrand && !switchBrands) {
									hasDefaultBrand = true;
									var orgId = brandOrgsMain.numberCount;
									var orgName = brandOrgsMain.brandName.ToUpper ();
									var role = brandOrgsMain.userRole;
									MyApplication.OrgId = orgId;
									MyApplication.OrgName = orgName;
									PreferenceConnector.WriteString (this, "org_id", orgId);
									PreferenceConnector.WriteString (this, "org_name", orgName);
									PreferenceConnector.WriteString (this, "user_role", role);
									PreferenceConnector.WriteInteger (this, "newContentCount", brandOrgsMain.totalNewContent);
									PreferenceConnector.WriteInteger (this, "newQuizCount", brandOrgsMain.totalNewQuiz);
									PreferenceConnector.WriteInteger (this, "newBlogsCount", brandOrgsMain.totalNewBlogs);
									RunOnUiThread (() => DashboardCountUpdation ());
									RunOnUiThread (() => dashboardFragment.orgsTitleTVId.Text = orgName);
								}
							}
						}
						var itemCount = brandOrgsList.Count;
						var organizationId = MyApplication.OrgId;
						if (hasDefaultBrand && !switchBrands) {
							RunOnUiThread (() => SwitchBrandsPopup (brandOrgsList, false, true));
							if (!string.IsNullOrEmpty (moduleName) && !string.IsNullOrEmpty (organizationId)) {
								RunOnUiThread (() => RespectivePageNavigation ());
							}
							RunOnUiThread (() => {
								if (progressDialog != null) {
									progressDialog.Dismiss ();
									progressDialog = null;
								}
							});
							return;
						}
						if (itemCount > 1 || switchBrands) {
							if (switchBrands) {
								RunOnUiThread (() => SwitchBrandsPopup (brandOrgsList, true, true));
							} else {
								if (!string.IsNullOrEmpty (MyApplication.OrgId) && !string.IsNullOrEmpty (moduleName)) {
									RunOnUiThread (() => RespectivePageNavigation ());
								} else {
									RunOnUiThread (() => SwitchBrandsPopup (brandOrgsList, true, false));
								}
							}
							switchBrands = false;
						} else {
							var orgId = brandOrgsList [0].numberCount;
							var orgName = brandOrgsList [0].brandName.ToUpper ();
							var role = brandOrgsList [0].userRole;
							MyApplication.OrgId = orgId;
							MyApplication.OrgName = orgName;
							MyApplication.UserRole = role;
							PreferenceConnector.WriteString (this, "org_id", orgId);
							PreferenceConnector.WriteString (this, "org_name", orgName);
							PreferenceConnector.WriteString (this, "user_role", role);
							RunOnUiThread (() => {
								dashboardFragment.orgsTitleTVId.Text = orgName;
								SwitchBrandsPopup (brandOrgsList, false, false);
							});
						}
					} else {
						if (termsAndCondition) {
							if (responseObj ["titleText"] != null && responseObj ["detailsText"] != null && responseObj ["buttonText"] != null) {
								titleText = (string)responseObj.SelectToken ("titleText");
								detailsText = (string)responseObj.SelectToken ("detailsText");
								buttonText = (string)responseObj.SelectToken ("buttonText");
								if (string.IsNullOrEmpty (titleText) && string.IsNullOrEmpty (detailsText) && string.IsNullOrEmpty (buttonText)) {
									termsAndCondition = false;
									RunOnUiThread (() => WebServiceCall ());
									if (termsConditionDialog != null) {
										RunOnUiThread (() => {
											termsConditionDialog.Dismiss ();
											termsConditionDialog = null;
										});
									}
								}
							}
						} else {
							RunOnUiThread (() => {
								if (!string.IsNullOrEmpty (statusMessage)) {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this, statusMessage);
								}
							});
						}
					}
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.Exception) {
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this, response);
			});
		}

		/// <summary>
		/// Determines whether this instance has play services available.
		/// </summary>
		/// <returns><c>true</c> if this instance is play services available; otherwise, <c>false</c>.</returns>
		public bool IsPlayServicesAvailable ()
		{
			try {
				GoogleApiAvailability googleAPI = GoogleApiAvailability.Instance;
				int result = googleAPI.IsGooglePlayServicesAvailable (this);
				if (result != ConnectionResult.Success) {
					if (googleAPI.IsUserResolvableError (result)) {
						googleAPI.GetErrorDialog (this, result, 10).Show ();
					} else {
						CommonMethods.ShowAlertDialog (this, "Sorry, your device does not support remote notifications");
					}
					return false;
				}
				return true;
			} catch (System.Exception) {	
				return false;
			}
		}

		/// <summary>
		/// My broadcast receiver to receive the notification count.
		/// </summary>
		[BroadcastReceiver (Enabled = true), IntentFilter (new [] { "com.retaileracademy.receiver" })]
		public class MyBroadcastReceiver : BroadcastReceiver
		{
			public override void OnReceive (Context context, Intent intent)
			{
				try {
					if (intent.Action.Equals ("com.retaileracademy.receiver")) {
						MainActivity.Instance.UpdateNotificationCount ();
					}
				} catch (Java.Lang.Exception e) {
					Console.WriteLine ("Push notification error :::::::::: " + e.StackTrace);
				}
			}
		}
	}
}