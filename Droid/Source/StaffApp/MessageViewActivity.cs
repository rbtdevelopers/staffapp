using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.StaffApp;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using RetailerAcademy.Droid.Source.ContentDTO;
using Android.Content.PM;
using Android.Graphics;
using System.IO;
using Android.Provider;
using Android.Database;
using RetailerAcademy.Droid.Source.Adapters;
using Android.Graphics.Drawables;
using Java.IO;
using Android.Views.InputMethods;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Webkit;
using System.Windows.Input;
using Android.Text.Util;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Java.Util;
using Android.Text.Method;
using Android.Support.V4.Content;
using Android;
using Android.Support.V4.App;

namespace RetailerAcademy.Droid.Source.StaffApp
{
	[Activity (Label = "MessageViewActivity", ScreenOrientation = ScreenOrientation.Portrait)]
	public class MessageViewActivity : BaseActivity, IWebServiceDelegate, View.IOnClickListener, Android.Text.ITextWatcher, AdapterView.IOnItemClickListener
	{
		private TextView cancelTV, messageHeaderTV, sendTVId, fromTV, subjectTV, attachmentTV, attachmentImgTV, bodyTV, downloadAttachTV, replyTV, subjectContentTV;
		private EditText bodyET, subjectET;
		private MultiAutoCompleteTextView fromACT;
		private Dialog progressDialog, dialog = null, alertDialog, permissionsDialog = null;
		private ImageView attachmentIV, headerOrgs;
		private Android.Webkit.WebView attachmentImgWV, tapToOpenPdf;
		private RelativeLayout attachmentLayout, videoImageViewRL, listLayout;
		private View attachmentLayoutView, listLayoutView;
		private string isUserIdAvailable, messageType, attachment, attachmentFromSent, fromQueryString, encodedAttachedImage, imageFileName, imagePath, imageFileExtension, attachmentName, imageFilePath, filePath, recipientName, recipientUserIDs;
		private string sendDateTime, strFrom, strSubject, strBody, strAttachment, attachmentFromInbox;
		private int userMessageID, senderUserID;
		private List<int> recipientUserIds = new List<int> ();
		private List<string> recipientMailIds;
		private List<MessageComposeModel> selectedUserList = new List<MessageComposeModel> ();
		private List<MessageComposeModel> userList;
		private List<string> selectedReceipentNames = new List<string> ();
		private List<MessageRepliesDTO> repliesList = new List<MessageRepliesDTO> ();
		private bool isCompose;
		private static ProgressBar dialogProgressBar;
		private const int FROM_CAMERA_INTENT_CODE = 222;
		private const int FROM_GALLERY_INTENT_CODE = 111;
		private const int PERMISSIONS_REQUEST_WRITE_STORAGE = 444;
		private const int PERMISSIONS_REQUEST_READ_STORAGE = 555;
		private Android.Net.Uri imageUri;
		public Android.App.FragmentManager fragmentManager;
		private int mailToListCount = 0;
		private WebClient client;
		private Java.IO.File finalPath, dir;
		private ListView replyListLV;
		private MessageRepliesAdapter messageRepliesAdapter = null;

		/// <summary>
		/// Raises the create event.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.MessageView);
			CommonMethods.TaskBarAppTheme (this);
			this.Window.SetSoftInputMode (SoftInput.StateAlwaysHidden);
			fragmentManager = FragmentManager;
			LoadUI ();
			dir = new Java.IO.File (Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/RetailAcademyData/");
		}

		/// <summary>
		/// Raises the resume event.
		/// </summary>
		protected override void OnResume ()
		{
			base.OnResume ();
			if (!string.IsNullOrEmpty (attachmentName)) {
				try {
					finalPath = new Java.IO.File (dir, userMessageID + attachmentName);
					if (finalPath.Exists ()) {
						downloadAttachTV.SetTextColor (Color.Rgb (0, 163, 224));
						downloadAttachTV.SetCompoundDrawablesWithIntrinsicBounds (0, 0, Resource.Drawable.message_dowload_checked, 0);
					} else {
						downloadAttachTV.SetTextColor (Color.Rgb (167, 167, 167));
						downloadAttachTV.SetCompoundDrawablesWithIntrinsicBounds (0, 0, Resource.Drawable.message_download, 0);
					}
				} catch (Exception) {					
				}
			}
		}

		/// <summary>
		/// Loads the UI.
		/// </summary>
		private void LoadUI ()
		{
			cancelTV = (TextView)FindViewById (Resource.Id.cancelTV);
			messageHeaderTV = (TextView)FindViewById (Resource.Id.messageHeaderTV);
			sendTVId = (TextView)FindViewById (Resource.Id.sendTVId);
			subjectTV = (TextView)FindViewById (Resource.Id.subjectTV);
			fromTV = (TextView)FindViewById (Resource.Id.fromTV);
			attachmentTV = (TextView)FindViewById (Resource.Id.attachmentTV);
			attachmentImgTV = (TextView)FindViewById (Resource.Id.attachmentImgTV);
			downloadAttachTV = (TextView)FindViewById (Resource.Id.downloadContentTV);
			tapToOpenPdf = (Android.Webkit.WebView)FindViewById (Resource.Id.tapToOpenPdf);
			subjectContentTV = (TextView)FindViewById (Resource.Id.subjectContentTV);

			attachmentIV = (ImageView)FindViewById (Resource.Id.attachmentIV);
			headerOrgs = (ImageView)FindViewById (Resource.Id.headerOrgs);

			attachmentImgWV = (Android.Webkit.WebView)FindViewById (Resource.Id.attachmentImgWV);
			attachmentImgWV.Settings.DefaultZoom = Android.Webkit.WebSettings.ZoomDensity.Far;
			attachmentImgWV.Settings.BuiltInZoomControls = true;
			attachmentImgWV.Settings.DisplayZoomControls = false;
			attachmentImgWV.Settings.UseWideViewPort = true;
			attachmentImgWV.Settings.LoadWithOverviewMode = true;

			attachmentLayoutView = (View)FindViewById (Resource.Id.attachmentLayoutView);
			attachmentLayout = (RelativeLayout)FindViewById (Resource.Id.attachmentLayout);

			videoImageViewRL = (RelativeLayout)FindViewById (Resource.Id.videoImageViewRL);
			videoImageViewRL.SetOnClickListener (this);

			bodyET = (EditText)FindViewById (Resource.Id.bodyET);
			bodyTV = (TextView)FindViewById (Resource.Id.bodyTV);
			bodyTV.MovementMethod = new ScrollingMovementMethod ();
			subjectET = (EditText)FindViewById (Resource.Id.subjectET);
			subjectContentTV.MovementMethod = new ScrollingMovementMethod ();
			fromACT = (MultiAutoCompleteTextView)FindViewById (Resource.Id.fromACT);

			dialogProgressBar = FindViewById<ProgressBar> (Resource.Id.dialogProgressBar);

			/*Reply content UI*/
			replyListLV = (ListView)FindViewById (Resource.Id.replyListLV);
			listLayout = (RelativeLayout)FindViewById (Resource.Id.listLayout);
			replyTV = (TextView)FindViewById (Resource.Id.replyTV);
			listLayoutView = (View)FindViewById (Resource.Id.listLayoutView);

			replyTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			cancelTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			messageHeaderTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			sendTVId.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			subjectTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			fromTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			bodyET.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			bodyTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			subjectET.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			subjectContentTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			fromACT.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			attachmentTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			attachmentImgTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			downloadAttachTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			downloadAttachTV.SetOnClickListener (this);

			try {
				messageType = Intent.GetStringExtra ("messageType");				
				if (messageType == "sent") {
					fromTV.Text = "TO:"; 
					bodyET.Visibility = ViewStates.Gone;
					replyTV.Visibility = ViewStates.Gone;
					subjectET.Visibility = ViewStates.Gone;
					cancelTV.Visibility = ViewStates.Invisible;
					sendTVId.Visibility = ViewStates.Invisible;
					bodyTV.Visibility = ViewStates.Visible;
					subjectContentTV.Visibility = ViewStates.Visible;
					headerOrgs.Visibility = ViewStates.Visible;
					bodyTV.SetMaxLines (100);
					fromACT.Enabled = false;
					subjectET.Enabled = false;
					var fromIds = Intent.GetStringExtra ("fromIds");
					var subject = Intent.GetStringExtra ("subject");
					var body = Intent.GetStringExtra ("body");
					attachmentFromSent = Intent.GetStringExtra ("attachment");
					isUserIdAvailable = Intent.GetStringExtra ("userMessageID");
					userMessageID = Convert.ToInt32 (isUserIdAvailable);
					headerOrgs.SetOnClickListener (this);
					fromACT.Text = fromIds;
					subjectET.Text = subject;
					subjectContentTV.Text = subject;
					bodyTV.Text = body;
					if (!string.IsNullOrEmpty (attachmentFromSent)) {
						attachmentLayout.Visibility = ViewStates.Visible;
						attachmentLayoutView.Visibility = ViewStates.Visible;
						string fileUrl = null;
						if (attachmentFromSent.Contains (".pdf")) {
							downloadAttachTV.Visibility = ViewStates.Gone;
							attachmentImgWV.Visibility = ViewStates.Invisible;
							videoImageViewRL.Visibility = ViewStates.Invisible;
							tapToOpenPdf.Visibility = ViewStates.Visible;
							tapToOpenPdf.Settings.JavaScriptEnabled = true;
							fileUrl = "https://docs.google.com/gview?embedded=true&url=" + attachmentFromSent;
							tapToOpenPdf.LoadUrl (fileUrl);
						} else if (attachmentFromSent.Contains (".mp4")) {
							downloadAttachTV.Visibility = ViewStates.Gone;
							attachmentImgWV.Visibility = ViewStates.Invisible;
							tapToOpenPdf.Visibility = ViewStates.Invisible;
							videoImageViewRL.Visibility = ViewStates.Visible;
						} else if (attachmentFromSent.Contains (".html")) {
							downloadAttachTV.Visibility = ViewStates.Gone;
							attachmentImgWV.Visibility = ViewStates.Invisible;
							tapToOpenPdf.Visibility = ViewStates.Invisible;
							videoImageViewRL.Visibility = ViewStates.Gone;
						} else {
							fileUrl = attachmentFromSent;
							imageFilePath = fileUrl;
							videoImageViewRL.Visibility = ViewStates.Invisible;
							tapToOpenPdf.Visibility = ViewStates.Invisible;
							attachmentImgWV.Visibility = ViewStates.Visible;
							downloadAttachTV.Visibility = ViewStates.Visible;
							attachmentImgWV.Settings.JavaScriptEnabled = true;
							attachmentImgWV.SetWebViewClient (new MyWebViewClient ());
							attachmentImgWV.LoadData ("<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} img{width:100%25;} </style></head><body><img src='" + fileUrl + "'/></body></html>", "text/html", "UTF-8");
						}
						if (!string.IsNullOrEmpty (attachmentFromSent)) {
							attachmentName = attachmentFromSent.Substring (attachmentFromSent.LastIndexOf ("/") + 1);
						}
					} else {
						attachmentLayout.Visibility = ViewStates.Gone;
						attachmentLayoutView.Visibility = ViewStates.Gone;
					}
				} else if (messageType == "inbox" || messageType == "archive") {
					isUserIdAvailable = Intent.GetStringExtra ("userMessageID");
					recipientUserIDs = Intent.GetStringExtra ("recipientUserIDs");
					recipientName = Intent.GetStringExtra ("recipientName");
					attachmentFromInbox = Intent.GetStringExtra ("attachment");
					if (!string.IsNullOrEmpty (attachmentFromInbox)) {
						attachmentName = attachmentFromInbox.Substring (attachmentFromInbox.LastIndexOf ("/") + 1);
					}
					if (!string.IsNullOrEmpty (isUserIdAvailable)) {
						bodyET.Visibility = ViewStates.Gone;
						subjectET.Visibility = ViewStates.Gone;
						cancelTV.Visibility = ViewStates.Invisible;
						sendTVId.Visibility = ViewStates.Invisible;
						bodyTV.Visibility = ViewStates.Visible;
						subjectContentTV.Visibility = ViewStates.Visible;
						headerOrgs.Visibility = ViewStates.Visible;
						replyTV.Visibility = ViewStates.Visible;
						fromACT.Enabled = false;
						subjectET.Enabled = false;
						fromTV.Text = "FROM:";
						headerOrgs.SetOnClickListener (this);
						var userRole = PreferenceConnector.ReadString (this, "user_role", null);
						replyTV.SetOnClickListener (this);
						WebServiceCall ();
					}                
				} else {
					fromACT.Enabled = true;
					subjectET.Enabled = true;
					fromTV.Text = "TO:";
					bodyTV.Visibility = ViewStates.Gone;
					replyTV.Visibility = ViewStates.Gone;
					replyListLV.Visibility = ViewStates.Gone;
					subjectContentTV.Visibility = ViewStates.Gone;
					attachmentImgWV.Visibility = ViewStates.Gone;
					videoImageViewRL.Visibility = ViewStates.Gone;
					tapToOpenPdf.Visibility = ViewStates.Gone;
					downloadAttachTV.Visibility = ViewStates.Gone;
					headerOrgs.Visibility = ViewStates.Invisible;
					subjectET.Visibility = ViewStates.Visible;
					cancelTV.Visibility = ViewStates.Visible;
					sendTVId.Visibility = ViewStates.Visible;
					bodyET.Visibility = ViewStates.Visible;
					attachmentLayout.Visibility = ViewStates.Visible;
					attachmentLayoutView.Visibility = ViewStates.Visible;
					cancelTV.SetOnClickListener (this);
					sendTVId.SetOnClickListener (this);
					bodyET.Text = "";
					bodyTV.Text = "";
					fromACT.Text = "";
					subjectET.Text = "";
					subjectContentTV.Text = "";
					fromACT.Threshold = 1;
					fromACT.AddTextChangedListener (this);
					fromACT.OnItemClickListener = this;
					attachmentIV.SetOnClickListener (this);
					attachmentImgTV.SetOnClickListener (this);
				}
				messageRepliesAdapter = new MessageRepliesAdapter (this, Resource.Layout.MessageReplyItem, repliesList);//changed
				replyListLV.Adapter = messageRepliesAdapter;
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Webservice call to view the message sent from the message list.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					fromQueryString = "read_message";
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this, "last_user_id", null);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.COMMUNICATION_READ + userId + "/" + isUserIdAvailable, null, this);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Loads the pdf.
		/// </summary>
		/// <param name="pdfFileAttachment">Pdf file attachment.</param>
		private void LoadPdf (string pdfFileAttachment)
		{
			try {
				if (pdfFileAttachment.Contains (".pdf")) {
					string fileUrl = null;
					fileUrl = "https://docs.google.com/gview?embedded=true&url=" + pdfFileAttachment;
					tapToOpenPdf.Settings.JavaScriptEnabled = true;
					tapToOpenPdf.LoadUrl (fileUrl);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if (fromQueryString == "read_message") {
							JArray userArray = null;
							if (responseObj ["messagelist"] != null) {
								userArray = (JArray)responseObj.SelectToken ("messagelist");
								if (userArray != null && userArray.Count > 0) {
									foreach (JObject item in userArray) {
										userMessageID = (int)item.SelectToken ("userMessageID");
										senderUserID = (int)item.SelectToken ("senderUserID");
										sendDateTime = (string)item.SelectToken ("sendDateTime");
										strSubject = (string)item.SelectToken ("subject");
										strFrom = (string)item.SelectToken ("senderNickName");
										strAttachment = (string)item.SelectToken ("attachment");
										strBody = (string)item.SelectToken ("body");
										RunOnUiThread (() => {
											subjectContentTV.Text = strSubject;
											subjectET.Text = strSubject;
											fromACT.Text = strFrom;
											bodyET.Text = strBody;
											bodyTV.Text = strBody;
											attachment = strAttachment;
										});
										if (!string.IsNullOrEmpty (attachment)) {
											RunOnUiThread (() => {
												attachmentLayout.Visibility = ViewStates.Visible;
												attachmentLayoutView.Visibility = ViewStates.Visible;
												string fileUrl = null;
												if (attachment.Contains (".pdf")) {
													tapToOpenPdf.Visibility = ViewStates.Visible;
													attachmentImgWV.Visibility = ViewStates.Invisible;
													videoImageViewRL.Visibility = ViewStates.Invisible;
													downloadAttachTV.Visibility = ViewStates.Gone;
													fileUrl = "https://docs.google.com/gview?embedded=true&url=" + attachment;
													tapToOpenPdf.Settings.JavaScriptEnabled = true;
													tapToOpenPdf.LoadUrl (fileUrl);
												} else if (attachment.Contains (".mp4") || attachment.Contains (".mov")) {
													videoImageViewRL.Visibility = ViewStates.Visible;
													attachmentImgWV.Visibility = ViewStates.Invisible;
													tapToOpenPdf.Visibility = ViewStates.Invisible;
													downloadAttachTV.Visibility = ViewStates.Gone;
												} else {
													fileUrl = attachment;
													imageFilePath = fileUrl;
													attachmentImgWV.Visibility = ViewStates.Visible;
													downloadAttachTV.Visibility = ViewStates.Visible;
													videoImageViewRL.Visibility = ViewStates.Invisible;
													tapToOpenPdf.Visibility = ViewStates.Invisible;
													attachmentImgWV.Settings.JavaScriptEnabled = true;
													attachmentImgWV.LoadData ("<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} img{width:100%25;} </style></head><body><img src='" + fileUrl + "'/></body></html>", "text/html", "UTF-8");
												}
												attachmentName = attachment.Substring (attachment.LastIndexOf ("/") + 1);
											});
										} else {
											RunOnUiThread (() => {								
												attachmentLayout.Visibility = ViewStates.Gone;
												attachmentLayoutView.Visibility = ViewStates.Gone;
											});
										}
										JArray repliesArray = null;
										if (item.SelectToken ("reply") != null) {
											repliesArray = (JArray)item.SelectToken ("reply");
											if (repliesArray != null && repliesArray.Count > 0) {
												foreach (JObject replyItem in repliesArray) {
													MessageRepliesDTO replies = new MessageRepliesDTO ();
													replies.attachment = (string)replyItem.SelectToken ("attachment");
													replies.body = (string)replyItem.SelectToken ("body");
													replies.subject = (string)replyItem.SelectToken ("subject");
													replies.sendDateTime = (string)replyItem.SelectToken ("sendDateTime");
													replies.senderNickName = (string)replyItem.SelectToken ("senderNickName");
													replies.senderUserID = (int)replyItem.SelectToken ("senderUserID");
													replies.userMessageID = (int)replyItem.SelectToken ("userMessageID");
													repliesList.Add (replies);
												}
												RunOnUiThread (() => {
													listLayout.Visibility = ViewStates.Visible;
													listLayoutView.Visibility = ViewStates.Visible;
													messageRepliesAdapter.NotifyDataSetChanged ();
												});
											} else {
												RunOnUiThread (() => { 
													listLayout.Visibility = ViewStates.Gone;
													listLayoutView.Visibility = ViewStates.Gone;
												});
											}
										}
									}
								}
							}
						} else if (fromQueryString == "text_change") {
							recipientMailIds = new List<string> ();
							userList = new List<MessageComposeModel> ();
							JArray userArray = null;
							if (responseObj ["emailList"] != null) {
								userArray = (JArray)responseObj.SelectToken ("emailList");
								if (userArray != null && userArray.Count > 0) {
									foreach (JObject item in userArray) {
										MessageComposeModel msgCompose = new MessageComposeModel ();
										msgCompose.userID = (int)item.SelectToken ("userID");
										msgCompose.userEmail = (string)item.SelectToken ("userEmail");
										msgCompose.fullName = (string)item.SelectToken ("fullName");
										this.userList.Add (msgCompose);
										this.recipientMailIds.Add ((string)item.SelectToken ("fullName"));
									}
								}
							}
							RunOnUiThread (() => {
								var adapter = new ArrayAdapter<string> (this, Resource.Layout.AutoCompleteTextResults, recipientMailIds);
								adapter.Filter.InvokeFilter ("");
								fromACT.Adapter = adapter;
								fromACT.SetTokenizer (new MultiAutoCompleteTextView.CommaTokenizer ());
							});
						} else if (fromQueryString == "send_message") {
							var message = Convert.ToString (responseObj.SelectToken ("message"));	
							PreferenceConnector.WriteBoolean (this, "isCompose", false);
							RunOnUiThread (() => {	
								listLayout.Visibility = ViewStates.Gone;
								listLayoutView.Visibility = ViewStates.Gone;
								AlertDialog.Builder alert = new AlertDialog.Builder (this);
								alert.SetTitle ("Alert");
								alert.SetCancelable (false);
								alert.SetMessage (message);
								Dialog dialog = null;
								alert.SetPositiveButton ("OK", (senderAlert, args) => {
									if (dialog != null) {
										sendTVId.Enabled = true;
										dialog.Dismiss ();
										Finish ();
									}
								});
								dialog = alert.Create ();
								dialog.Show ();
							});
						}
					} else {
						if (fromQueryString == "text_change") {
							RunOnUiThread (() => {
								recipientMailIds = new List<string> ();
								var adapter = new ArrayAdapter<string> (this, Resource.Layout.AutoCompleteTextResults, recipientMailIds);
								fromACT.Adapter = adapter;
								fromACT.SetTokenizer (new MultiAutoCompleteTextView.CommaTokenizer ());
							});
						} else {
							string statusMessage = null;
							if (responseObj ["message"] != null) {
								statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
								if (!string.IsNullOrEmpty (statusMessage)) {
									RunOnUiThread (() => {
										if (alertDialog != null) {
											alertDialog.Dismiss ();
										}
										alertDialog = CommonMethods.ShowAlertDialog (this, statusMessage);
									});
								}
							}
						}
					}
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Exception) {
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				var userRole = PreferenceConnector.ReadString (this, "user_role", null);
				switch (id) {
				case Resource.Id.cancelTV:
					AlertOnCancelMessage ();
					break;
				case Resource.Id.sendTVId:
					SendEmail ();
					break;
				case Resource.Id.attachmentIV:
					RequestReadStoragePermission ();
					break;
				case Resource.Id.attachmentImgTV:
					attachmentImgTV.Text = "";
					attachmentImgTV.Visibility = ViewStates.Invisible;
					break;
				case Resource.Id.tapToOpenPdf:
					if (messageType == "sent") {
						LoadPdf (attachmentFromSent);
					} else if (messageType == "inbox" || messageType == "archive") {
						LoadPdf (attachment);
					}
					break;
				case Resource.Id.headerOrgs:				
					SetResult (Result.Ok, null);
					Finish ();
					break;
				case Resource.Id.videoImageViewRL:
					if (!CommonMethods.IsInternetConnected (this)) {
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						if (!"".Equals (attachment) && !"null".Equals (attachment)) {
							Intent videoIntent = new Intent (this, typeof(VideoPlayerActivity));
							videoIntent.PutExtra ("videoUrl", attachment);
							StartActivity (videoIntent);
						}
					}
					break;
				case Resource.Id.downloadContentTV:
					RequestWriteStoragePermission ();
					break;
				case Resource.Id.replyTV:
					Intent intent = new Intent (this, typeof(MessageReplyActivity));
					intent.PutExtra ("userMessageID", userMessageID);
					intent.PutExtra ("recipientUserID", senderUserID);
					intent.PutExtra ("subject", strSubject);
					intent.PutExtra ("senderName", recipientName);
					intent.PutExtra ("recipientName", strFrom);
					StartActivity (intent);
					break;
				case Resource.Id.okAlertOKTV:
					if (permissionsDialog != null) {
						permissionsDialog.Dismiss ();
					}
					break;
				default:
					break;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Requests the write storage permission.
		/// </summary>
		private void RequestWriteStoragePermission ()
		{
			// Here, thisActivity is the current activity
			if (ContextCompat.CheckSelfPermission (this, Manifest.Permission.WriteExternalStorage) != Permission.Granted) {
				// Should we show an explanation?
				if (ActivityCompat.ShouldShowRequestPermissionRationale (this, Manifest.Permission.WriteExternalStorage)) {
					// Show an expanation to the user *asynchronously* -- don't block
					// this thread waiting for the user's response! After the user
					// sees the explanation, try again to request the permission.
					ActivityCompat.RequestPermissions (this, new String[]{ Manifest.Permission.WriteExternalStorage }, PERMISSIONS_REQUEST_WRITE_STORAGE);
				} else {
					// No explanation needed, we can request the permission.
					ActivityCompat.RequestPermissions (this, new String[]{ Manifest.Permission.WriteExternalStorage }, PERMISSIONS_REQUEST_WRITE_STORAGE);
					// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
					// app-defined int constant. The callback method gets the
					// result of the request.
				}
			} else {
				//Call camera directly, permissions are already accepted.
				DownloadImageAsync ();
			}
		}

		/// <summary>
		/// Requests the read storage permission.
		/// </summary>
		private void RequestReadStoragePermission ()
		{
			// Here, thisActivity is the current activity
			if (ContextCompat.CheckSelfPermission (this, Manifest.Permission.ReadExternalStorage) != Permission.Granted) {
				// Should we show an explanation?
				if (ActivityCompat.ShouldShowRequestPermissionRationale (this, Manifest.Permission.ReadExternalStorage)) {
					// Show an expanation to the user *asynchronously* -- don't block
					// this thread waiting for the user's response! After the user
					// sees the explanation, try again to request the permission.
					ActivityCompat.RequestPermissions (this, new String[]{ Manifest.Permission.ReadExternalStorage }, PERMISSIONS_REQUEST_READ_STORAGE);
				} else {
					// No explanation needed, we can request the permission.
					ActivityCompat.RequestPermissions (this, new String[]{ Manifest.Permission.ReadExternalStorage }, PERMISSIONS_REQUEST_READ_STORAGE);
					// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
					// app-defined int constant. The callback method gets the
					// result of the request.
				}
			} else {
				//Call camera directly, permissions are already accepted.
				Intent i = new Intent (Intent.ActionPick, Android.Provider.MediaStore.Images.Media.ExternalContentUri);
				StartActivityForResult (i, FROM_GALLERY_INTENT_CODE);
			}
		}

		/// <summary>
		/// Downloads the image async.
		/// </summary>
		private void DownloadImageAsync ()
		{
			try {
				finalPath = new Java.IO.File (dir, userMessageID + attachmentName);
				if (finalPath.Exists ()) {
					CommonMethods.ShowAlertDialog (this, "File already exists!");
				} else {
					if (!CommonMethods.IsInternetConnected (this)) {
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						if (!string.IsNullOrEmpty (imageFilePath)) {
							if (progressDialog == null) {
								progressDialog = CommonMethods.GetProgressDialog (this, "Download in progress...");
							}
							try {
								new System.Threading.Thread (new System.Threading.ThreadStart (() => {
									client = new WebClient ();
									client.DownloadProgressChanged += new DownloadProgressChangedEventHandler (client_DownloadProgressChanged);
									client.DownloadStringCompleted += Client_DownloadStringCompleted;
									client.DownloadStringAsync (new Uri (imageFilePath));
								})).Start ();
							} catch (WebException) {
								RunOnUiThread (() => {
									if (progressDialog != null) {
										progressDialog.Dismiss ();
										progressDialog = null;
									}
									CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
								});
							}
						}
					}
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Raises the request permissions result event.
		/// </summary>
		/// <param name="requestCode">Request code.</param>
		/// <param name="permissions">Permissions.</param>
		/// <param name="grantResults">Grant results.</param>
		public override void OnRequestPermissionsResult (int requestCode, string[] permissions, Permission[] grantResults)
		{
			switch (requestCode) {
			case PERMISSIONS_REQUEST_WRITE_STORAGE:
				{
					// If request is cancelled, the result arrays are empty.
					if (grantResults.Length > 0	&& grantResults [0] == Permission.Granted) {
						// permission was granted, yay! Do the
						// camera task you need to do.
						DownloadImageAsync ();
					} else {
						// permission denied, boo! Disable the
						// functionality that depends on this permission.
						if (permissionsDialog != null) {
							permissionsDialog.Dismiss ();
						}
						permissionsDialog = CommonMethods.AlertDialogForPermissions (this, Arial, this, true);
					}
					return;
				}
			case PERMISSIONS_REQUEST_READ_STORAGE:
				{
					// If request is cancelled, the result arrays are empty.
					if (grantResults.Length > 0	&& grantResults [0] == Permission.Granted) {
						// permission was granted, yay! Do the
						// camera task you need to do.
						Intent i = new Intent (Intent.ActionPick, Android.Provider.MediaStore.Images.Media.ExternalContentUri);
						StartActivityForResult (i, FROM_GALLERY_INTENT_CODE);
					} else {
						// permission denied, boo! Disable the
						// functionality that depends on this permission.
						if (permissionsDialog != null) {
							permissionsDialog.Dismiss ();
						}
						permissionsDialog = CommonMethods.AlertDialogForPermissions (this, Arial, this, true);
					}
					return;
				}
			}
		}

		/// <summary>
		/// Download string completed event get called after the DownloadImageAsync webservice call.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		async void Client_DownloadStringCompleted (object sender, DownloadStringCompletedEventArgs e)
		{
			var text = e.Result;
			/* Creating custom local path for images */
			if (!dir.Exists ()) {
				dir.Mkdirs ();
			}
			filePath = System.IO.Path.Combine (dir.ToString (), userMessageID + attachmentName);
			string localPath = System.IO.Path.Combine (dir.ToString (), filePath);
			if (client != null) {
				client = new WebClient ();
			}
			var url = new Uri (imageFilePath);
			byte[] bytes = null;
			try {
				bytes = await client.DownloadDataTaskAsync (url); 
			} catch (WebException) {
				RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
					return;
				});
			} catch (TaskCanceledException) {
				System.Console.WriteLine ("Task Canceled!");
				return;
			} 
			FileStream fs = new FileStream (localPath, FileMode.OpenOrCreate);
			await fs.WriteAsync (bytes, 0, bytes.Length);
			fs.Close ();
		}

		/// <summary>
		/// Client download progress changed event gets updated upon downloading the content.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void client_DownloadProgressChanged (object sender, DownloadProgressChangedEventArgs e)
		{
			try {
				double bytesIn = Convert.ToInt32 (e.BytesReceived.ToString ());
				double totalBytes = Convert.ToInt32 (e.TotalBytesToReceive.ToString ());
				int percentage = Convert.ToInt32 ((bytesIn / totalBytes) * 100);
				if (percentage == 100) {
					RunOnUiThread (() => {
						if (progressDialog != null) {
							Thread.Sleep (3000);
							if (alertDialog != null) {
								alertDialog.Dismiss ();
							}
							alertDialog = CommonMethods.ShowAlertDialog (this, "Download completed.");
							progressDialog.Dismiss ();
							progressDialog = null;
							downloadAttachTV.SetTextColor (Color.Rgb (0, 163, 224));
							downloadAttachTV.SetCompoundDrawablesWithIntrinsicBounds (0, 0, Resource.Drawable.message_dowload_checked, 0);
						}
					});
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Gets the real path from URI.
		/// </summary>
		/// <returns>The real path from UR.</returns>
		/// <param name="contentUri">Content URI.</param>
		public string GetRealPathFromURI (Android.Net.Uri contentUri)
		{
			try {
				string[] filePathColumn = { MediaStore.MediaColumns.Data };
				var cursor = ContentResolver.Query (contentUri, filePathColumn, null, null, null);
				cursor.MoveToFirst ();
				int columnIndex = cursor.GetColumnIndex (filePathColumn [0]);
				string picturePath = cursor.GetString (columnIndex);
				cursor.Close ();
				return picturePath;
			} catch (Exception) {
				return null;
			}
		}

		/// <param name="requestCode">The integer request code originally supplied to
		///  startActivityForResult(), allowing you to identify who this
		///  result came from.</param>
		/// <param name="resultCode">The integer result code returned by the child activity
		///  through its setResult().</param>
		/// <param name="data">An Intent, which can return result data to the caller
		///  (various data can be attached to Intent "extras").</param>
		/// <summary>
		/// Called when an activity you launched exits, giving you the requestCode
		///  you started it with, the resultCode it returned, and any additional
		///  data from it.
		/// </summary>
		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			try {
				if (data != null) {
					imageUri = data.Data;
					if (imageUri != null) {
						if (!imageUri.ToString ().Contains ("file")) {							
							imagePath = GetRealPathFromURI (imageUri);
						} else {
							imagePath = imageUri.ToString ();
						}
						if (!string.IsNullOrEmpty (imagePath)) {
							var fileName = imagePath.Substring (imagePath.LastIndexOf ("/") + 1);
							string[] imageNameArray = fileName.Split ('.');
							imageFileName = imageNameArray [0];
							imageFileExtension = fileName.Substring (fileName.LastIndexOf ("."));
							if (!string.IsNullOrEmpty (fileName)) {
								attachmentImgTV.Text = fileName;
								attachmentImgTV.Visibility = ViewStates.Visible;
							} else {
								attachmentImgTV.Text = "";
								attachmentImgTV.Visibility = ViewStates.Invisible;
							}
						} else {
							CommonMethods.ShowAlertDialog (this, CommonSharedStrings.FILE_CORRUPT_ERROR);
						}
					} else {
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.FILE_CORRUPT_ERROR);
					}
				}
			} catch (Exception) {
				CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
			}
		}

		/// <summary>
		/// Sends the email.
		/// </summary>
		private void SendEmail ()
		{
			try {
				string[] components = fromACT.Text.Split (new string[] { ", " }, StringSplitOptions.None);
				var lastStrLength = components [components.Length - 1].Length;
				if (lastStrLength == 0) {
					mailToListCount = components.Length - 1;
				} else {
					mailToListCount = components.Length;
				}
				recipientUserIds = new List<int> ();
				foreach (var item in components) {
					if (item.Contains ("@")) {
						foreach (var email in userList) {
							if (item.Trim () == email.userEmail) {
								if (!recipientUserIds.Contains (email.userID)) {
									recipientUserIds.Add (email.userID);
								}
							}
						}
					} else {
						foreach (var name in userList) {
							if (item.Trim () == name.fullName) {
								if (!recipientUserIds.Contains (name.userID)) {
									recipientUserIds.Add (name.userID);
								}
							}
						}
					}
				}
				
				if (recipientUserIds.Count <= 0 || (mailToListCount != recipientUserIds.Count)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.MAIL_TO);
				} else if (subjectET.Text.Trim ().Length == 0) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.MAIL_SUBJECT);
				} else if (bodyET.Text.Trim ().Length == 0) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.MAIL_BODY);
				} else if (!CommonMethods.IsInternetConnected (this)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this, "Loading, please wait...");
					}
					fromQueryString = "send_message";
					var userId = PreferenceConnector.ReadString (this, "last_user_id", null);
					MessageSend dataObjects = new MessageSend ();
					dataObjects.senderUserID = Convert.ToInt32 (userId);
					dataObjects.subject = subjectET.Text;
					dataObjects.body = bodyET.Text;
					dataObjects.recipientUserIDs = string.Join (",", recipientUserIds.ToArray ());
					if (!string.IsNullOrEmpty (attachmentImgTV.Text)) {
						Bitmap thumbnailBitmap = null;
						thumbnailBitmap = Android.Provider.MediaStore.Images.Media.GetBitmap (ContentResolver, imageUri);
						MemoryStream stream = new MemoryStream ();
						thumbnailBitmap.Compress (Bitmap.CompressFormat.Jpeg, 50, stream);
						byte[] imageByteArray = stream.ToArray ();
						encodedAttachedImage = Convert.ToBase64String (imageByteArray);
						dataObjects.fileName = imageFileName;
						dataObjects.extension = imageFileExtension;
						dataObjects.imagebytes = encodedAttachedImage;
					} else {
						dataObjects.fileName = "";
						dataObjects.extension = "";
						dataObjects.imagebytes = "";
					}
					sendTVId.Enabled = false;
					string requestParameter = JsonConvert.SerializeObject (dataObjects);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.COMMUNICATION_SENDMESSAGE, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		/// <param name="s">To be added.</param>
		/// <summary>
		/// Afters the text changed.
		/// </summary>
		public void AfterTextChanged (Android.Text.IEditable s)
		{
			try {
				if (fromACT.Text.Length <= 0 && selectedUserList != null && selectedReceipentNames != null) {
					selectedReceipentNames.Clear ();
					selectedUserList.Clear ();
				}
			} catch (Exception) {				
			}
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <param name="after">To be added.</param>
		/// <summary>
		/// Befores the text changed.
		/// </summary>
		public void BeforeTextChanged (Java.Lang.ICharSequence s, int start, int count, int after)
		{
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="before">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <summary>
		/// Raises the text changed event.
		/// </summary>
		public void OnTextChanged (Java.Lang.ICharSequence s, int start, int before, int count)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					var searchKey = fromACT.Text;
					string searchKeyThreshold;
					if (searchKey.Contains (",")) {
						string[] searchTermArray = searchKey.Split (',');
						searchKeyThreshold = searchTermArray [searchTermArray.Length - 1].Trim ();
					} else {
						searchKeyThreshold = searchKey;
					}
					fromQueryString = "text_change";
					var userId = PreferenceConnector.ReadString (this, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this, "org_id", null);
					var jsonService = new JsonService ();
					CommunicationUserList commnDataObjects = new CommunicationUserList ();
					commnDataObjects.userID = Convert.ToInt32 (userId);
					commnDataObjects.orgID = orgId;
					commnDataObjects.searchKey = searchKeyThreshold;
					string requestParameter = JsonConvert.SerializeObject (commnDataObjects);
					jsonService.consumeService (CommonSharedStrings.COMMUNICATION_USERSLIST, requestParameter, this);
					commnDataObjects = null;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Raises the item click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					var recipientID = userList [position].userID;
					var recipientNameInList = userList [position].fullName;
					var recipientEmail = userList [position].userEmail;
					if (selectedUserList != null && selectedReceipentNames != null) {
						bool isIdAvailable = selectedUserList.Any (recpId => recpId.userID == recipientID);
						if (!isIdAvailable) {
							MessageComposeModel msgComposeUser = new MessageComposeModel ();
							msgComposeUser.userID = recipientID;
							msgComposeUser.userEmail = recipientEmail;
							msgComposeUser.fullName = recipientNameInList;
							selectedUserList.Add (msgComposeUser); 
							selectedReceipentNames.Add (msgComposeUser.fullName);
						} else {
							if (!selectedReceipentNames.Contains (recipientNameInList)) {
								fromACT.Text = string.Join ("", selectedReceipentNames) + recipientNameInList + ", ";
							} else {
								fromACT.Text = string.Join (", ", selectedReceipentNames);
							}
							fromACT.SetSelection (fromACT.Text.Length);
						}
					}
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this, response);
			});
		}

		/// <summary>
		/// Alert dialog display on cancel message.
		/// </summary>
		private void AlertOnCancelMessage ()
		{
			try {
				isCompose = PreferenceConnector.ReadBoolean (this, "isCompose", false);
				if (isCompose) {
					AlertDialog.Builder alert = new AlertDialog.Builder (this);
					alert.SetTitle ("Alert");
					alert.SetCancelable (false);
					alert.SetMessage ("Are you sure you want to cancel Message?");
					alert.SetPositiveButton ("OK", (senderAlert, args) => {
						if (dialog != null) {
							PreferenceConnector.WriteBoolean (this, "isCompose", false);
							Finish ();
						}
					});
					alert.SetNegativeButton ("CANCEL", (senderAlert, args) => {
						if (dialog != null) {
							dialog.Dismiss ();
						}
					});
					dialog = alert.Create ();
					dialog.Show ();
				} else {
					Finish ();
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Called when the activity has detected the user's press of the back
		///  key.
		/// </summary>
		public override void OnBackPressed ()
		{
			AlertOnCancelMessage ();
		}

		private class MyWebViewClient : WebViewClient
		{
			public override bool ShouldOverrideUrlLoading (WebView view, string url)
			{
				view.LoadUrl (url);
				return true;
			}

			public override void OnPageStarted (WebView view, string url, Android.Graphics.Bitmap favicon)
			{
				MessageViewActivity.dialogProgressBar.Visibility = ViewStates.Visible;
				base.OnPageStarted (view, url, favicon);
			}

			public override void OnPageFinished (WebView view, string url)
			{
				MessageViewActivity.dialogProgressBar.Visibility = ViewStates.Gone;
				base.OnPageFinished (view, url);
			}
		}
	}
}