using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Content.PM;
using Newtonsoft.Json;
using RetailerAcademy.Droid.Source.Utilities;
using Android.Net;
using Newtonsoft.Json.Linq;
using RetailerAcademy.Droid.Source.StaffApp;
using System.Threading;
using Android.Preferences;
using Android.Views.InputMethods;

namespace RetailerAcademy.Droid.StaffApp
{
	[Activity (Label = "LoginActivity", ScreenOrientation = ScreenOrientation.Portrait, WindowSoftInputMode = SoftInput.AdjustResize | SoftInput.StateHidden)]
	public class LoginActivity : Activity, IWebServiceDelegate, View.IOnClickListener, TextView.IOnEditorActionListener
	{
		private EditText loginET, passwordET;
		private Button loginBTN;
		private TextView forgotPasswordTV, headerTextTV, networkConnTV;
		private Dialog progressDialog, dialog = null, alertDialog;
		private ScrollView loginScrollParent;
		private string lastUserLoginId, lastUserPassword;
		private AlertDialog.Builder alert = null;
		private Typeface OswaldRegular;

		/// <summary>
		/// Raises the create event.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Login);
			CommonMethods.TaskBarAppTheme (this);
			loginScrollParent = (ScrollView)FindViewById (Resource.Id.loginScrollParent);
			networkConnTV = (TextView)FindViewById (Resource.Id.networkConnTV);
			headerTextTV = (TextView)FindViewById (Resource.Id.headerText);
			FindViewById (Resource.Id.headerMenu).Visibility = ViewStates.Gone;
			FindViewById (Resource.Id.headerOrgs).Visibility = ViewStates.Gone;
			OswaldRegular = Typeface.CreateFromAsset (Assets, "Fonts/oswald_regular.ttf");
			headerTextTV.Text = GetString (Resource.String.login_text);
			headerTextTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			networkConnTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			try {
				if (!CommonMethods.IsInternetConnected (this)) {
					loginScrollParent.Visibility = ViewStates.Gone;
					networkConnTV.Visibility = ViewStates.Visible;
					if (alert == null) {
						alert = new AlertDialog.Builder (this);
					}
					alert.SetTitle ("Alert");
					alert.SetCancelable (true);
					alert.SetMessage (CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					alert.SetPositiveButton ("Refresh", (senderAlert, args) => {
						if (dialog != null) {
							dialog.Dismiss ();
							this.Recreate ();
						}
					});
					dialog = alert.Create ();
					dialog.Show ();
					return;
				} else {
					LoadUI ();
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Raises the resume event.
		/// </summary>
		protected override void OnResume ()
		{
			base.OnResume ();
		}

		/// <summary>
		/// Loads the U.
		/// </summary>
		private void LoadUI ()
		{
			networkConnTV.Visibility = ViewStates.Gone;
			loginET = (EditText)FindViewById (Resource.Id.userName);
			passwordET = (EditText)FindViewById (Resource.Id.password);
			loginBTN = (Button)FindViewById (Resource.Id.loginButton);
			forgotPasswordTV = (TextView)FindViewById (Resource.Id.forgotPasswordTV);
			loginET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			passwordET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			loginBTN.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			forgotPasswordTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			loginBTN.SetOnClickListener (this);
			forgotPasswordTV.SetOnClickListener (this);
			passwordET.SetOnEditorActionListener (this);
			lastUserLoginId = PreferenceConnector.ReadString (this, "last_user_login", null);
			lastUserPassword = PreferenceConnector.ReadString (this, "last_user_password", null);
			try {
				if (!string.IsNullOrEmpty (lastUserLoginId) && !string.IsNullOrEmpty (lastUserPassword)) {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this, "Loading, please wait...");
					}
					LoginData dataObjects = new LoginData ();
					dataObjects.username = lastUserLoginId;
					dataObjects.password = lastUserPassword;
					string requestParameter = JsonConvert.SerializeObject (dataObjects);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.LOGIN_URL, requestParameter, this);
				} else if (!string.IsNullOrEmpty (lastUserLoginId)) {
					loginScrollParent.Visibility = ViewStates.Visible;
					loginET.Text = lastUserLoginId;
				} else {
					loginScrollParent.Visibility = ViewStates.Visible;
					loginET.Text = "";
					passwordET.Text = "";
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			int id = v.Id;
			switch (id) {
			case Resource.Id.loginButton:
				WebServiceCallForLogin ();
				break;
			case Resource.Id.forgotPasswordTV:
				StartActivity (typeof(ForgotPasswordActivity));
				break;
			}
		}

		/// <summary>
		/// Webs the service call for login.
		/// </summary>
		private void WebServiceCallForLogin ()
		{
			try {
				var username = loginET.Text;
				var password = passwordET.Text;
				if (string.IsNullOrEmpty (username)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.USERNAME_BLANK_MESSAGE);
				} else if (username.Length < 3) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.USERNAME_INVALID_MESSAGE);
				} else if (string.IsNullOrEmpty (password)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.PASSWORD_BLANK_MESSAGE);
				} else if (password.Length < 8) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.PASSWORD_INVALID_MESSAGE);
				} else if (!CommonMethods.IsInternetConnected (this)) {
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this, "Loading, please wait...");
					}
					LoginData dataObjects = new LoginData ();
					dataObjects.username = username;
					dataObjects.password = password;
					string requestParameter = JsonConvert.SerializeObject (dataObjects);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.LOGIN_URL, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		#region IWebServiceDelegate implementation
		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					string statusMessage = null, userId = null, userRole = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if (responseObj ["userid"] != null) {
							userId = (string)responseObj.SelectToken ("userid");
						}
						if (responseObj ["role"] != null) {
							userRole = (string)responseObj.SelectToken ("role");
						}
						MyApplication.CurrentUserId = this.loginET.Text;
						MyApplication.CurrentPassword = this.passwordET.Text;
						MyApplication.IsLogin = true;
						MyApplication.UserId = userId;
						MyApplication.UserRole = userRole;
						PreferenceConnector.WriteString (this, "last_user_id", userId);
						PreferenceConnector.WriteString (this, "last_user_login", this.loginET.Text);
						PreferenceConnector.WriteString (this, "last_user_password", this.passwordET.Text);
						PreferenceConnector.WriteString (this, "user_role", userRole);
						PreferenceConnector.WriteBoolean (this, "isLogin", true);
						RunOnUiThread (() => StartActivity (typeof(MainActivity)));
						Finish ();
					} else {
						if (responseObj ["message"] != null) {
							statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
							if (!string.IsNullOrEmpty (statusMessage)) {
								RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this, statusMessage);
								});
							}
						}
						RunOnUiThread (() => this.passwordET.Text = "");
					}
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Exception) {
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		#endregion
		/// <summary>
		/// Called when the activity has detected the user's press of the back
		///  key.
		/// </summary>
		public override void OnBackPressed ()
		{
			try {
				CommonMethods.ShowCustomAlertDialog(this, GetString(Resource.String.msg_app_exit), 
					GetString(Resource.String.alert_text), GetString(Resource.String.ok_text), 
					GetString(Resource.String.cancel_text), false, DialogListenerTypesEnum.OkTypeAppClose);
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// On response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this, response);
			});
		}

		/// <summary>
		/// Raises the editor action event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="actionId">Action identifier.</param>
		/// <param name="e">E.</param>
		public bool OnEditorAction (TextView v, Android.Views.InputMethods.ImeAction actionId, KeyEvent e)
		{
			if ((e != null && (e.KeyCode == Keycode.Enter)) || (actionId == Android.Views.InputMethods.ImeAction.Done)) {
				WebServiceCallForLogin ();
			}    
			return true;
		}
	}
}