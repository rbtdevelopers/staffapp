﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.StaffApp;
using Android.Content.PM;
using RetailerAcademy.Droid.Source.ContentDTO;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using Android.Webkit;
using Newtonsoft.Json.Linq;

namespace RetailerAcademy.Droid
{
	[Activity (Label = "QuizPageActivity", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class QuizPageActivity : BaseActivity, IWebServiceDelegate, CompoundButton.IOnCheckedChangeListener, View.IOnClickListener
	{
		private Button submitButton, cancelButton, OpenMediaContentButton, closeMediaPopupButton;
		private ImageButton orgsImageButton;
		private TextView questionStaticTV, questionHeaderTV, answereStaticTV, questionDynamicTV, headerTextTV;
		private string categoryId, mediaUrl, quizTitleStr, questionType, question, listOfAttempQues = "0", fileUrlForPdf, feedBackMessage;
		private List<int> answersTosubmit = new List<int> ();
		private int questionId = 0, noOfQuestion;
		private LinearLayout answerLL, answerInnerLL;
		private RelativeLayout videoImageViewRL;
		private List<Answers> QuizAns;
		private int attemptedQuestionsCount = 0;
		private RadioGroup rg;
		private Dialog progressDialog, dialog = null, openMediaContentDialog;
		private Android.Webkit.WebView mediaContentWV, tapToOpenPdf;
		private static ProgressBar dialogProgressBar;
		private bool isFirst = true, isQuizEnd;
		private int iterationCount = 0;
		public List<int> ansArrayList;

		/// <summary>
		/// Raises the create event.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.QuizPageActivity);
			CommonMethods.TaskBarAppTheme (this);
			string[] strArray = Intent.GetStringArrayExtra ("categoryId"); 
			if (!strArray.Equals (null)) {
				categoryId = strArray [0];
				quizTitleStr = strArray [1];
			}
			WebServiceCall ("");
			headerTextTV = (TextView)FindViewById (Resource.Id.headerText);
			FindViewById (Resource.Id.headerMenu).Visibility = ViewStates.Gone;
			orgsImageButton = (ImageButton)FindViewById (Resource.Id.headerOrgs);
			orgsImageButton.Visibility = ViewStates.Visible;
			orgsImageButton.SetOnClickListener (this);
			headerTextTV.Text = GetString (Resource.String.quiz_text);
			headerTextTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);

			questionStaticTV = FindViewById<TextView> (Resource.Id.questionStaticTVId);
			questionDynamicTV = FindViewById<TextView> (Resource.Id.questionDynamicTVId);
			questionHeaderTV = FindViewById<TextView> (Resource.Id.questionHeaderTVId);
			answereStaticTV = FindViewById<TextView> (Resource.Id.answereStaticTVId);
			answerLL = FindViewById<LinearLayout> (Resource.Id.answerLLId);
			answerInnerLL = FindViewById<LinearLayout> (Resource.Id.answerInnerLLId);
			rg = FindViewById<RadioGroup> (Resource.Id.radioGroupId);
			submitButton = FindViewById<Button> (Resource.Id.submitBtnId);
			cancelButton = FindViewById<Button> (Resource.Id.cancelBtnId);
			OpenMediaContentButton = FindViewById<Button> (Resource.Id.openMediaContentBtnId);

			questionStaticTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			questionDynamicTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			answereStaticTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			questionHeaderTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);

			cancelButton.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			submitButton.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			OpenMediaContentButton.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);

			/* For UI displaying issue */
			questionStaticTV.Visibility = ViewStates.Gone;
			answereStaticTV.Visibility = ViewStates.Gone;
			cancelButton.Visibility = ViewStates.Gone;
			submitButton.Visibility = ViewStates.Gone;
			OpenMediaContentButton.Visibility = ViewStates.Gone;
			cancelButton.SetOnClickListener (this);
			submitButton.SetOnClickListener (this);
			OpenMediaContentButton.SetOnClickListener (this);
			questionHeaderTV.Text = quizTitleStr;
		}

		/// <summary>
		/// Webservice call for getting the quiz questions and answers.
		/// </summary>
		/// <param name="answers">Answers.</param>
		private void WebServiceCall (string answers)
		{
			if (!CommonMethods.IsInternetConnected (this)) {
				CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
			} else {
				if (progressDialog == null) {
					progressDialog = CommonMethods.GetProgressDialog (this, "Loading, please wait...");
				}
				var userId = PreferenceConnector.ReadString (this, "last_user_id", null);
				QuizQuestionsCallDto quizQuestionCallData = new QuizQuestionsCallDto ();
				quizQuestionCallData.userid = Convert.ToInt32 (userId);
				quizQuestionCallData.questionid = questionId + "";
				quizQuestionCallData.answersubmited = answers;
				quizQuestionCallData.categoryid = categoryId + "";
				quizQuestionCallData.attemptedQuestions = listOfAttempQues + "";
				string requestParameter = JsonConvert.SerializeObject (quizQuestionCallData);
				var jsonService = new JsonService ();
				jsonService.consumeService (CommonSharedStrings.QUIZ_ANSWERING, requestParameter, this);
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				switch (id) {
				case Resource.Id.closeBtnId:
					openMediaContentDialog.Dismiss ();
					break;
				case Resource.Id.videoImageViewRL:
					if (!CommonMethods.IsInternetConnected (this)) {
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						if (!"".Equals (mediaUrl) && !"null".Equals (mediaUrl)) {
							Intent videoIntent = new Intent (this, typeof(VideoPlayerActivity));
							videoIntent.PutExtra ("videoUrl", mediaUrl);
							StartActivity (videoIntent);
						}
					}
					break;
				case Resource.Id.cancelBtnId:
					OnBackPressed ();
					break;
				case Resource.Id.submitBtnId:
					if (SubmitAnswer ()) {
						string answers = "";
						ansArrayList.Sort ();
						for (int i = 0; i < ansArrayList.Count; i++) {
							if (string.IsNullOrEmpty (answers)) {
								answers = ansArrayList [i] + "";
							} else {
								answers = answers + "," + ansArrayList [i] + "";
							}
						}
						attemptedQuestionsCount = attemptedQuestionsCount + 1;
						WebServiceCall (answers);
					} else {
					}
					break;
				case Resource.Id.openMediaContentBtnId:
					OpenMediaContent ();
					break;
				case Resource.Id.headerOrgs:
					OnBackPressed ();
					break;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Opens the MediaContent popup dialog.
		/// </summary>
		private void OpenMediaContent ()
		{
			if (!string.IsNullOrEmpty (mediaUrl)) {
				try {
					openMediaContentDialog = new Dialog (this);
					openMediaContentDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
					openMediaContentDialog.Window.RequestFeature (WindowFeatures.NoTitle);
					openMediaContentDialog.SetContentView (Resource.Layout.OpenMediaContentPopup);
					closeMediaPopupButton = openMediaContentDialog.FindViewById<Button> (Resource.Id.closeBtnId);
					mediaContentWV = openMediaContentDialog.FindViewById<Android.Webkit.WebView> (Resource.Id.quizWebView);
					videoImageViewRL = openMediaContentDialog.FindViewById<RelativeLayout> (Resource.Id.videoImageViewRL);
					tapToOpenPdf = openMediaContentDialog.FindViewById<Android.Webkit.WebView> (Resource.Id.tapToOpenPdf);
					dialogProgressBar = openMediaContentDialog.FindViewById<ProgressBar> (Resource.Id.dialogProgressBar);
					closeMediaPopupButton.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
					mediaContentWV.Settings.BuiltInZoomControls = true;
					mediaContentWV.Settings.LoadWithOverviewMode = true;
					mediaContentWV.Settings.UseWideViewPort = true;
					mediaContentWV.Settings.DisplayZoomControls = false;
					mediaContentWV.SetWebViewClient (new MyWebViewClient ());
					openMediaContentDialog.SetCancelable (false);
					openMediaContentDialog.Show ();
					closeMediaPopupButton.SetOnClickListener (this);
					if (mediaUrl.Contains (".pdf")) {	
						tapToOpenPdf.Visibility = ViewStates.Visible;
						mediaContentWV.Visibility = ViewStates.Gone;
						videoImageViewRL.Visibility = ViewStates.Gone;
						tapToOpenPdf.Settings.JavaScriptEnabled = true;
						fileUrlForPdf = "https://docs.google.com/gview?embedded=true&url=" + mediaUrl;
						tapToOpenPdf.LoadUrl (fileUrlForPdf);
					} else if (mediaUrl.Contains (".mp4") || mediaUrl.Contains (".wmv") || mediaUrl.Contains (".mov")) {
						videoImageViewRL.Visibility = ViewStates.Visible;
						mediaContentWV.Visibility = ViewStates.Gone;
						tapToOpenPdf.Visibility = ViewStates.Gone;
						videoImageViewRL.SetOnClickListener (this);
					} else if (mediaUrl.Contains (".htm") || mediaUrl.Contains (".html")) {
						mediaContentWV.Visibility = ViewStates.Visible;
						videoImageViewRL.Visibility = ViewStates.Gone;
						tapToOpenPdf.Visibility = ViewStates.Gone;
						mediaContentWV.Settings.JavaScriptEnabled = true;
						mediaContentWV.LoadUrl (mediaUrl);
					} else {	
						mediaContentWV.Visibility = ViewStates.Visible;
						videoImageViewRL.Visibility = ViewStates.Gone;
						tapToOpenPdf.Visibility = ViewStates.Gone;
						//mediaContentWV.LoadData ("<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} img{width:100%25; height:100%25;} </style></head><body><img src='" + mediaUrl + "'/></body></html>", "text/html", "UTF-8");
						mediaContentWV.LoadData("<html style='height:100%; width:100%; padding:0; margin:0;'><body style='height:100%; width:100%; padding:0; margin:0;'><table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0'><tr><td style='text-align:center'><img src='" + mediaUrl + "' align='middle' /></td></tr></table></body></html>", "text/html", "UTF-8");
					}
				} catch (Exception) {					
				}
			}
		}

		/// <summary>
		/// Submits the answer.
		/// </summary>
		/// <returns><c>true</c>, if answer was submited, <c>false</c> otherwise.</returns>
		private bool SubmitAnswer ()
		{
			bool validation = false;
			try {
				if (questionType.Equals ("Multiple Choice - Multiple Answers")) {
					if (ansArrayList.Count >= 2) {
						validation = true;
					} else if (ansArrayList.Count == 1) {
						CommonMethods.ShowAlertDialog (this, "Please select two options.");
					} else {
						CommonMethods.ShowAlertDialog (this, "Please select your answer.");
					}
				} else if (questionType.Equals ("Multiple Choice - Single Answer")) {
					if (ansArrayList.Count == 1) {
						validation = true;
					} else {
						CommonMethods.ShowAlertDialog (this, "Please select your answer.");
					}
				}
				return validation;
			} catch (Exception ex) {
				return false;
			}
		}

		/// <summary>
		/// Hides the view till next question.
		/// </summary>
		private void HideViewTillNextQuestion ()
		{
			try {
				answerInnerLL.RemoveAllViews ();
				rg.RemoveAllViews ();
				questionDynamicTV.Text = "";
				isFirst = true;
				iterationCount = 0;
				questionStaticTV.Visibility = ViewStates.Gone;
				answereStaticTV.Visibility = ViewStates.Gone;
				cancelButton.Visibility = ViewStates.Gone;
				submitButton.Visibility = ViewStates.Gone;
				OpenMediaContentButton.Visibility = ViewStates.Gone;
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Makes the visble of all views.
		/// </summary>
		/// <param name="mediaUrl">Media URL.</param>
		private void MakeVisbleAllViews (string mediaUrl)
		{
			try {
				questionStaticTV.Visibility = ViewStates.Visible;
				answereStaticTV.Visibility = ViewStates.Visible;
				cancelButton.Visibility = ViewStates.Visible;
				submitButton.Visibility = ViewStates.Visible;
				OpenMediaContentButton.Visibility = ViewStates.Visible;
				if (!string.IsNullOrEmpty (mediaUrl)) {
					OpenMediaContentButton.Visibility = ViewStates.Visible;
				} else {
					OpenMediaContentButton.Visibility = ViewStates.Gone;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					RunOnUiThread (() => HideViewTillNextQuestion ());
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					JArray quizAnswersArray = null;
					string statusMessage = null;
					if (responseObj ["questionid"] != null) {
						questionId = (int)(responseObj.SelectToken ("questionid"));
					}
					if (responseObj ["question"] != null) {
						question = (string)(responseObj.SelectToken ("question"));
					}
					if (responseObj ["questiontype"] != null) {
						questionType = (string)(responseObj.SelectToken ("questiontype"));
					}
					if (responseObj ["noofquestion"] != null) {
						noOfQuestion = (int)(responseObj.SelectToken ("noofquestion"));
					}
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (responseObj ["feedback"] != null) {
						feedBackMessage = (string)(responseObj.SelectToken ("feedback"));
					}
					if (responseObj ["mediaurl"] != null) {
						mediaUrl = (string)responseObj.SelectToken ("mediaurl");
					}
					if (responseObj ["quizend"] != null) {
						isQuizEnd = Convert.ToBoolean (responseObj.SelectToken ("quizend"));
					}
					if (status) {
						answersTosubmit = new List<int> ();
						if (isQuizEnd) {
							if (!string.IsNullOrEmpty (statusMessage)) {
								RunOnUiThread (() => {
									if (!string.IsNullOrEmpty (statusMessage)) {
										AlertDialog.Builder alert = new AlertDialog.Builder (this);
										alert.SetTitle ("Alert");
										alert.SetCancelable (false);
										alert.SetMessage (statusMessage);
										alert.SetPositiveButton ("OK", (senderAlert, args) => {
											if (dialog != null) {
												dialog.Dismiss ();
												Finish ();
											}
										});
										dialog = alert.Create ();
										dialog.Show ();
									}
								});
							}
							if (!string.IsNullOrEmpty (feedBackMessage)) {
								RunOnUiThread (() => CommonMethods.ShowAlertDialog (this, feedBackMessage));
							}
							RunOnUiThread (() => {
								if (progressDialog != null) {
									progressDialog.Dismiss ();
									progressDialog = null;
								}
							});
							return;
						} else {
							if (!string.IsNullOrEmpty (feedBackMessage)) {
								RunOnUiThread (() => CommonMethods.ShowAlertDialog (this, feedBackMessage));
							}
						}
						RunOnUiThread (() => {
							if (listOfAttempQues == "0") {
								listOfAttempQues = questionId + "";
							} else {
								listOfAttempQues = listOfAttempQues + "," + questionId;
							}
						});
						if (responseObj ["answers"] != null) {
							quizAnswersArray = (JArray)responseObj.SelectToken ("answers");
						}
						if (quizAnswersArray != null && quizAnswersArray.Count > 0) {
							QuizAns = new List<Answers> ();
							for (int i = 0; i < quizAnswersArray.Count; i++) {
								Answers ansArray = new Answers ();
								ansArray.answer = (string)quizAnswersArray [i].SelectToken ("answer");
								ansArray.answerid = (int)quizAnswersArray [i].SelectToken ("answerno");
								QuizAns.Add (ansArray);
							}
						}
						RunOnUiThread (() => {
							questionDynamicTV.Text = question;
							if (questionType.Equals ("Multiple Choice - Multiple Answers")) {
								MultipleChoiceAnswers ();
							} else {
								SingleChoiceAnswers ();
							}
							/* displaying All buttons */
							MakeVisbleAllViews (mediaUrl);
						});
					} else {
						if (!string.IsNullOrEmpty (statusMessage)) {
							RunOnUiThread (() => CommonMethods.ShowAlertDialog (this, statusMessage));
						}
					}
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (InvalidCastException) {
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.Exception) {
					RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// Called when the activity has detected the user's press of the back
		///  key.
		/// </summary>
		public override void OnBackPressed ()
		{
			AlertDialog.Builder alert = new AlertDialog.Builder (this);
			alert.SetTitle ("Alert");
			alert.SetCancelable (false);
			alert.SetMessage ("Are you sure you want to cancel Quiz?");
			alert.SetPositiveButton ("OK", (senderAlert, args) => {
				Finish ();
			});
			alert.SetNegativeButton ("CANCEL", (senderAlert, args) => {
				if (dialog != null) {
					dialog.Dismiss ();
				}
			});
			dialog = alert.Create ();
			dialog.Show ();
		}

		/// <summary>
		/// Multiple choice answers.
		/// </summary>
		private void MultipleChoiceAnswers ()
		{
			try {
				ansArrayList = new List<int> ();
				View view;
				LayoutInflater inflater = LayoutInflater.From (this);
				int size = QuizAns.Count;
				for (int i = 0; i < size; i++) {
					view = inflater.Inflate (Resource.Layout.QuizPageItem, null);
					CheckBox answerCB = view.FindViewById<CheckBox> (Resource.Id.answerCBId);
					answerCB.SetPadding (15, 15, 15, 15);
					answerCB.Text = QuizAns [i].answer;
					answerCB.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
					answerCB.SetTag (Resource.Id.answerCBId, QuizAns [i].answerid);
					answerCB.SetOnCheckedChangeListener (this);
					answerInnerLL.AddView (view);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Single choice answers.
		/// </summary>
		private void SingleChoiceAnswers ()
		{
			try {
				ansArrayList = new List<int> ();
				int size = QuizAns.Count;
				for (int i = 0; i < size; i++) {
					RadioButton optionRB = new RadioButton (this);
					optionRB.SetPadding (15, 15, 15, 15);
					optionRB.SetButtonDrawable (Resource.Drawable.radiobtnsselector);
					optionRB.Text = QuizAns [i].answer;
					optionRB.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
					optionRB.SetTextColor (Color.ParseColor ("#B4B4B4"));
					optionRB.SetTag (Resource.Id.radioGroupId, QuizAns [i].answerid);
					optionRB.SetOnCheckedChangeListener (this); 
					rg.AddView (optionRB);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Raises the checked changed event.
		/// </summary>
		/// <param name="buttonView">Button view.</param>
		/// <param name="isChecked">If set to <c>true</c> is checked.</param>
		public void OnCheckedChanged (CompoundButton buttonView, bool isChecked)
		{
			try {
				var id = 0;
				if (questionType == "Multiple Choice - Multiple Answers") {
					/* Id for getting Check Box answers id's. */
					id = (int)buttonView.GetTag (Resource.Id.answerCBId);
					if (ansArrayList == null) {
						ansArrayList.Add ((int)id);
					} else if (!ansArrayList.Contains ((int)id)) {
						ansArrayList.Add ((int)id);
					} else {
						ansArrayList.Remove ((int)id);
					}
				} else {
					if (isFirst) {
						/* Id for getting Radio Buttons answers id's. */
						id = (int)buttonView.GetTag (Resource.Id.radioGroupId);
						ansArrayList.Clear ();
						ansArrayList.Add ((int)id);
						isFirst = false;
						iterationCount++;
					} else {
						if ((iterationCount % 2) != 0) {
							id = (int)buttonView.GetTag (Resource.Id.radioGroupId);
							ansArrayList.Clear ();
							ansArrayList.Add ((int)id);
							iterationCount++;
						} else {
							iterationCount++;
						}
					}
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this, response);
			});
		}

		private class MyWebViewClient : WebViewClient
		{
			public override bool ShouldOverrideUrlLoading (WebView view, string url)
			{
				view.LoadUrl (url);
				return true;
			}

			public override void OnPageStarted (WebView view, string url, Android.Graphics.Bitmap favicon)
			{
				QuizPageActivity.dialogProgressBar.Visibility = ViewStates.Visible;
				base.OnPageStarted (view, url, favicon);
			}

			public override void OnPageFinished (WebView view, string url)
			{
				QuizPageActivity.dialogProgressBar.Visibility = ViewStates.Invisible;
				base.OnPageFinished (view, url);
			}
		}
	}
}