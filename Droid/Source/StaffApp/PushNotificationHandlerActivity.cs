﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Utilities;
using RetailerAcademy.Droid.StaffApp;
using Android.Content.PM;
using Android.Graphics;

namespace RetailerAcademy.Droid
{
	[Activity (Label = "PushNotificationHandlerActivity", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class PushNotificationHandlerActivity : Activity
	{
		/// <summary>
		/// Raises the create event for navigating push notifications to respective page.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView(Resource.Layout.PushNotificationHandler);
			try {
				CommonMethods.TaskBarAppTheme (this);
				var moduleName = Intent.GetStringExtra ("moduleName");
				var brandOrgId = Intent.GetStringExtra ("brandOrgId");
				var brandName = Intent.GetStringExtra ("brandName");
				var moduleid = Intent.GetStringExtra ("moduleid");
				var blogcategoryid = Intent.GetStringExtra ("blogcategoryid");
				var isLogin = PreferenceConnector.ReadBoolean (this, "isLogin", false);
				MyApplication.UserId = PreferenceConnector.ReadString (this, "last_user_id", "");
				MyApplication.CurrentPassword = PreferenceConnector.ReadString (this, "last_user_password", "");
				MyApplication.OrgId = brandOrgId;
				MyApplication.OrgName = brandName.ToUpper ();
				PreferenceConnector.WriteString (this, "org_id", brandOrgId);
				PreferenceConnector.WriteString (this, "org_name", brandName.ToUpper ());
				Intent intent = null;
				if(!CommonMethods.IsInternetConnected(this)){
					intent = new Intent (this, typeof(LoginActivity));
					intent.AddFlags (ActivityFlags.ClearTop | ActivityFlags.NewTask | ActivityFlags.ClearTask);
				} else if (isLogin) {
					intent = new Intent (this, typeof(MainActivity));
					intent.AddFlags (ActivityFlags.ClearTop | ActivityFlags.NewTask | ActivityFlags.ClearTask);
					intent.PutExtra ("moduleName", moduleName);
					intent.PutExtra ("brandOrgId", brandOrgId);
					intent.PutExtra ("brandName", brandName);
					intent.PutExtra ("moduleid", moduleid);
					intent.PutExtra ("blogcategoryid", blogcategoryid);
				} else {
					intent = new Intent (this, typeof(SplashScreenActivity));
					intent.AddFlags (ActivityFlags.ClearTop | ActivityFlags.NewTask | ActivityFlags.ClearTask);
				}
				StartActivity (intent);
				intent = null;
				Finish ();
			} catch (Exception) {	
				Finish ();			
			}
		}
	}
}