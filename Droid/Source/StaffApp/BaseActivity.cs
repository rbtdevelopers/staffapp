using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Net;
using Android.Graphics;

using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using RetailerAcademy.Droid.Source.Utilities;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Timers;

namespace RetailerAcademy.Droid.StaffApp
{
	[Activity (Label = "BaseActivity", ScreenOrientation = ScreenOrientation.Portrait)]
	public class BaseActivity : Activity
	{
		public Typeface OswaldBold, OswaldLight, OswaldRegular, Arial;
		public bool isLogin, isActiveOrNotResponse, userbelongstobrand;
		private string startDate, userId, orgId, endDate;
		private System.Timers.Timer timer;
		private Dialog dialog = null;
		private AlertDialog.Builder alert = null;

		/// <summary>
		/// Raises the create event used for Initialization of custom font styles.
		/// </summary>
		/// <param name="savedInstanceState">Saved instance state.</param>
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			OswaldBold = Typeface.CreateFromAsset (Assets, "Fonts/oswald_bold.ttf");
			OswaldLight = Typeface.CreateFromAsset (Assets, "Fonts/oswald_light.ttf");
			OswaldRegular = Typeface.CreateFromAsset (Assets, "Fonts/oswald_regular.ttf");
			Arial = Typeface.CreateFromAsset (Assets, "Fonts/arial.ttf");
		}

		/// <summary>
		/// Raises the resume event to check for the exact time.
		/// </summary>
		protected override void OnResume ()
		{
			base.OnResume ();
			try {
				isLogin = PreferenceConnector.ReadBoolean (this, "isLogin", false);//MyApplication.IsLogin;
				if (isLogin) {
					DateTime date = DateTime.Now;
					DateTime utcDatetime = date.ToUniversalTime ();
					string currentTime = string.Format ("{0:yyyy-MM-dd HH:mm:ss}", utcDatetime);
					MyApplication.StartDate = currentTime;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Called as part of the activity lifecycle when an activity is going into
		///  the background, but has not (yet) been killed.
		/// </summary>
		protected override void OnPause ()
		{
			base.OnPause ();
			isLogin = PreferenceConnector.ReadBoolean (this, "isLogin", false);
			try {
				if (isLogin) {
					DateTime date = DateTime.Now;
					DateTime utcDatetime = date.ToUniversalTime ();
					string currentTime = string.Format ("{0:yyyy-MM-dd HH:mm:ss}", utcDatetime);
					MyApplication.EndDate = currentTime;
					startDate = MyApplication.StartDate;
					new Task (() => {
						backgroundTaskForUserRecentActivity ();
					}).Start ();
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Service task which is a service need to run when application is going to background.
		/// </summary>
		private void backgroundTaskForUserRecentActivity ()
		{
			try {
				userId = PreferenceConnector.ReadString (this, "last_user_id", null);
				orgId = MyApplication.OrgId;
				endDate = MyApplication.EndDate;
				if (Convert.ToDateTime (startDate) < Convert.ToDateTime (endDate)) { 
					RecentUserActivity recent = new RecentUserActivity ();
					recent.userId = Convert.ToInt32 (userId);
					recent.brandGuid = orgId;
					recent.startDate = startDate;
					recent.endDate = endDate;
					isActiveOrNotResponse = false;
					string requestParameter = JsonConvert.SerializeObject (recent);
					WebClient client = new WebClient ();
					client.Headers [HttpRequestHeader.ContentType] = "application/json";
					client.UploadStringAsync (new System.Uri (CommonSharedStrings.RECENTACTIVITY), "POST", requestParameter);
					client.UploadStringCompleted += UploadContentCompleted;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// This event gets called when the backgroundTaskForUserRecentActivity gets called.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void UploadContentCompleted (object sender, UploadStringCompletedEventArgs e)
		{
			try {
				if (e.Error == null && !string.IsNullOrEmpty (e.Result)) {
					JObject responseObj = JObject.Parse (e.Result);
					bool status = false;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if (isActiveOrNotResponse) {
							if (!userbelongstobrand) {
								PreferenceConnector.WriteString (this, "org_id", "");
							}
							PreferenceConnector.WriteString (this, "last_user_password", "");
							PreferenceConnector.WriteString (this, "org_name", "");
							PreferenceConnector.WriteBoolean (this, "isLogin", false); 
							MyApplication.CurrentPassword = "";
							MyApplication.IsLogin = false;	
							MyApplication.OrgId = "";
							MyApplication.OrgName = "";
							string message = null;
							if (responseObj ["message"] != null) {
								message = (string)responseObj.SelectToken ("message");
							}
							RunOnUiThread (() => { 
								isActiveOrNotResponse = false;
								timer.Stop ();
								if (alert == null) {
									alert = new AlertDialog.Builder (this);
								}
								alert.SetTitle ("Alert");
								alert.SetCancelable (false);
								alert.SetMessage (message);
								alert.SetPositiveButton ("OK", (senderAlert, args) => {
									if (dialog != null) {
										dialog.Dismiss ();
										Finish ();
										StartActivity (typeof(LoginActivity));
									}
								});
								dialog = alert.Create ();
								if (!((MainActivity)this).IsFinishing) {
									dialog.Show ();
								}
							});
						}				
					}
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Check weather the user is active or inactive. If inactive user is has been logout from app.
		/// </summary>
		/// <param name="userId">User identifier.</param>
		internal void ActiveOrInActive (string userId)
		{
			try {
				var orgId = PreferenceConnector.ReadString (this, "org_id", null);
				if (CommonMethods.IsInternetConnected (this)) {
					new System.Threading.Thread (new System.Threading.ThreadStart (() => {
						WebClient client = new WebClient ();
						client.Headers [HttpRequestHeader.ContentType] = "application/json";
						client.DownloadStringAsync (new System.Uri (CommonSharedStrings.ISACTIVEORINACTIVE + "/" + userId + "/" + orgId), "GET");
						client.DownloadStringCompleted += DownloadStringCompleted;
					})).Start ();
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// This event gets called on ActiveOrInActive webservice call.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		private void DownloadStringCompleted (object sender, DownloadStringCompletedEventArgs e)
		{
			try {
				if (e.Error == null && !string.IsNullOrEmpty (e.Result)) {
					JObject responseObj = JObject.Parse (e.Result);
					bool status = false, passwordChangedInActiveOrNot = false, isActiveOrNot = false;
					int totalNewContent = 0, totalNewQuiz = 0, totalNewBlogs = 0, badge = 0;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if (responseObj ["roleName"] != null) {
							var role = (string)responseObj.SelectToken ("roleName");
							MyApplication.UserRole = role;
							PreferenceConnector.WriteString (this, "user_role", role);
						}
						if (responseObj ["isActive"] != null) {
							isActiveOrNot = (bool)responseObj.SelectToken ("isActive");
						}
						if (responseObj ["passwordChanged"] != null) {
							passwordChangedInActiveOrNot = (bool)responseObj.SelectToken ("passwordChanged");
						}
						if (responseObj ["totalNewContent"] != null) {
							totalNewContent = (int)responseObj.SelectToken ("totalNewContent");
						}
						if (responseObj ["totalNewQuiz"] != null) {
							totalNewQuiz = (int)responseObj.SelectToken ("totalNewQuiz");
						}
						if (responseObj ["totalNewBlogs"] != null) {
							totalNewBlogs = (int)responseObj.SelectToken ("totalNewBlogs");
						}
						if (responseObj ["badge"] != null) {
							badge = (int)responseObj.SelectToken ("badge");
						}
						PreferenceConnector.WriteInteger (this, "newContentCount", totalNewContent);
						PreferenceConnector.WriteInteger (this, "newQuizCount", totalNewQuiz);
						PreferenceConnector.WriteInteger (this, "newBlogsCount", totalNewBlogs);
						PreferenceConnector.WriteInteger (this, "notificationCount", badge);
						if (((MainActivity)this) != null) {
							RunOnUiThread (() => ((MainActivity)this).DashboardCountUpdation ());
							RunOnUiThread (() => ((MainActivity)this).UpdateNotificationCount ());
						}
						if (!isActiveOrNot || passwordChangedInActiveOrNot) {
							DoLogoutFromTheActivation ();
						} 
						var orgId = PreferenceConnector.ReadString (this, "org_id", null);
						if (!string.IsNullOrEmpty (orgId) && responseObj ["userbelongstobrand"] != null) {						
							userbelongstobrand = (bool)responseObj.SelectToken ("userbelongstobrand");
							if (!userbelongstobrand) {
								DoLogoutFromTheActivation ();
							}
						}
					}
				}
			} catch (InvalidCastException) {
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Dos the logout from the activation.
		/// </summary>
		void DoLogoutFromTheActivation ()
		{	
			try {
				if (CommonMethods.IsInternetConnected (this)) {
					isActiveOrNotResponse = true;
					new System.Threading.Thread (new System.Threading.ThreadStart (() => {
						var pushDeviceToken = PreferenceConnector.ReadString (this, "device_token", null);//MyApplication.DeviceToken;
						Logout logout = new Logout ();
						logout.userid = Convert.ToInt32 (userId);
						logout.deviceToken = pushDeviceToken;
						logout.platform = "android";
						string requestParameter = JsonConvert.SerializeObject (logout);
						WebClient client = new WebClient ();
						client.Headers [HttpRequestHeader.ContentType] = "application/json";
						client.UploadStringAsync (new System.Uri (CommonSharedStrings.LOGOUT), "POST", requestParameter);
						client.UploadStringCompleted += UploadContentCompleted;
					})).Start ();
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Raises the start event.
		/// </summary>
		protected override void OnStart ()
		{
			base.OnStart ();
			SetTimerForService ();
		}

		/// <summary>
		/// Called when you are no longer visible to the user.
		/// </summary>
		protected override void OnStop ()
		{
			base.OnStop ();
			timer.Stop ();
		}

		/// <summary>
		/// Sets the timer for service.
		/// </summary>
		private void SetTimerForService ()
		{
			timer = new System.Timers.Timer (15000);
			timer.Elapsed += OnTimedEvent;
			timer.AutoReset = true;
			timer.Enabled = true;
		}

		/// <summary>
		/// Raises the timed event on timer time out.
		/// </summary>
		/// <param name="source">Source.</param>
		/// <param name="e">E.</param>
		private void OnTimedEvent (System.Object source, ElapsedEventArgs e)
		{
			userId = PreferenceConnector.ReadString (this, "last_user_id", null);//MyApplication.UserId;
			ActiveOrInActive (userId);
		}
	}
}