﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Android.Content.PM;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;

namespace RetailerAcademy.Droid
{
	[Activity (Label = "SocialMediaPage", ScreenOrientation = ScreenOrientation.Portrait)]
	public class SocialMediaPage : Activity
	{
		public Activity act;
		private Android.Webkit.WebView webView;
		private string fileUrl;
		private string socialmedia, title, description;

		/// <summary>
		/// Raises the create event.
		/// </summary>
		/// <param name="bundle">Bundle.</param>
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.SocialMediaPage);
			CommonMethods.TaskBarAppTheme (this);
			fileUrl = Intent.GetStringExtra ("fileurl");
			socialmedia = Intent.GetStringExtra ("socialmedia");
			title = Intent.GetStringExtra ("title");
			description = Intent.GetStringExtra ("description");
			act = this;
			LoadUI ();
			Finish ();
		}

		/// <summary>
		/// Called when the activity has detected the user's press of the back
		///  key.
		/// </summary>
		public override void OnBackPressed ()
		{
			base.OnBackPressed ();
		}

		/// <summary>
		/// Loads the UI.
		/// </summary>
		private void LoadUI()
		{
			try {
				webView = FindViewById<Android.Webkit.WebView> (Resource.Id.webViewId);
				if (socialmedia.Equals ("facebook")) {
					/**
					 * facebook app id  : 106667236395480,
					 * facebook account : android.raybiztech@gmail.com.
					**/
					var facebookUrl = "https://www.facebook.com/dialog/feed?app_id=" + "106667236395480" + "&display=popup&picture=" + fileUrl + 
						"&redirect_uri=https://www.facebook.com/" + "&name=" + title + "&description=" + description;
					//webView.SetWebViewClient (new MyWebViewClient ());
					webView.LoadUrl (facebookUrl);
				} else {
					//webView.SetWebViewClient (new MyWebViewClient ());
					webView.LoadUrl ("http://www.twitter.com/share?text=" + title + "\n" + description);
				}
			} catch (Exception) {				
			}			
		}

		public class MyWebViewClient : WebViewClient {

			public override bool ShouldOverrideUrlLoading (WebView view, string url)
			{
				if (url.Contains ("post_id")) {
					//TO DO for close
				}
				return false;
			}

			public override void OnPageFinished (WebView view, string url)
			{
				base.OnPageFinished (view, url);
			}
		}
	}
}
