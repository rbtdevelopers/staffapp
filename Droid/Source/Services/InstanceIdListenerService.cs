﻿using System;
using Android.App;
using Android.Gms.Gcm.Iid;
using Android.Content;

namespace RetailerAcademy.Droid
{
	[Service(Exported = false), IntentFilter(new[] { "com.google.android.gms.iid.InstanceID" })]
	class MyInstanceIDListenerService : InstanceIDListenerService
	{
		public override void OnTokenRefresh()
		{
			try {
				var intent = new Intent (this, typeof(RegistrationIntentService));
				StartService (intent);
			} catch (Exception) {				
			}
		}
	}
}