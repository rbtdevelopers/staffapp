﻿using System;
using Java.Util.Concurrent.Atomic;

namespace RetailerAcademy.Droid
{
	public class NotificationID
	{
		public NotificationID ()
		{
		}

		private static AtomicInteger integer = new AtomicInteger(0);

		public static int getID() {
			return integer.IncrementAndGet();
		}
	}
}

