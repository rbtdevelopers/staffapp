﻿using System;
using Android.App;
using Android.Gms.Gcm;
using Android.OS;
using Android.Util;
using Android.Content;
using RetailerAcademy.Droid.StaffApp;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Media;
using Android.Widget;

namespace RetailerAcademy.Droid
{
	[Service (Exported = false), IntentFilter (new [] { "com.google.android.c2dm.intent.RECEIVE" })]
	public class MyGcmListenerService : GcmListenerService
	{
		public static NotificationManager notificationManager;

		public override void OnMessageReceived (string from, Bundle data)
		{
			try {
				var message = data.GetString ("alert");
				var moduleName = data.GetString ("moduletype");
				var brandOrgId = data.GetString ("brandguid");
				var brandName = data.GetString ("brandname");
				var badgeCount = data.GetString ("badge");
				var deviceid = data.GetString ("deviceid");
				var moduleid = data.GetString ("moduleid");
				var blogcategoryid = data.GetString ("blogcategoryid");
				var integerBadgeCount = Convert.ToInt32 (badgeCount);
				PreferenceConnector.WriteInteger (this, "notificationCount", integerBadgeCount);				;
				SendNotification (message, moduleName, brandOrgId, brandName, moduleid, blogcategoryid);
				if (MainActivity.appStatus) {
					UpdateMyActivity (this, badgeCount);
				}
			} catch (Exception) {				
			}
		}

		void UpdateMyActivity (Context context, string badgeCount)
		{
			try {
				Intent intent = new Intent ("com.retaileracademy.receiver");
				//put whatever data you want to send, if any
				intent.PutExtra ("badgeCount", badgeCount);
				//send broadcast
				context.SendBroadcast (intent);
			} catch (Exception e) {
				Console.WriteLine ("Push notification error :::::::::: " + e.StackTrace);
			}
		}

		void SendNotification (string message, string moduleName, string brandOrgId, string brandName, string moduleid, string blogcategoryid)
		{
			try {
				Intent intent = new Intent (this, typeof(PushNotificationHandlerActivity));
				intent.AddFlags (ActivityFlags.ClearTop | ActivityFlags.NewTask | ActivityFlags.ClearTask);
				intent.PutExtra ("moduleName", moduleName);
				intent.PutExtra ("brandOrgId", brandOrgId);
				intent.PutExtra ("brandName", brandName);
				intent.PutExtra ("moduleid", moduleid);
				intent.PutExtra ("blogcategoryid", blogcategoryid);
				Java.Util.Random random = new Java.Util.Random ();
				int requestCode = random.NextInt (999999);
				var pendingIntent = PendingIntent.GetActivity (this, requestCode, intent, PendingIntentFlags.UpdateCurrent);
				var notificationBuilder = new Notification.Builder (this)
					.SetSmallIcon (Resource.Drawable.Icon)
					.SetContentTitle ("StaffApp")
					.SetContentText (message)
					.SetAutoCancel (true)
					.SetContentIntent (pendingIntent);
				Android.Net.Uri notification = RingtoneManager.GetDefaultUri (RingtoneType.Notification);
				notificationBuilder.SetSound (notification);
				notificationManager = (NotificationManager)GetSystemService (Context.NotificationService);
				notificationManager.Notify (NotificationID.getID (), notificationBuilder.Build ());
			} catch (Exception) {				
			}
		}
	}
}