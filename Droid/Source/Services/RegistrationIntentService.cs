﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Android.Gms.Gcm.Iid;
using Android.Gms.Gcm;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using System.Net;

namespace RetailerAcademy.Droid
{
	[Service(Exported = false)]		
	public class RegistrationIntentService : IntentService, IWebServiceDelegate
	{
		static object locker = new object();
		ISharedPreferences prefs = Application.Context.GetSharedPreferences("StaffApp", FileCreationMode.Private);

		public RegistrationIntentService() : base("RegistrationIntentService")
		{ 
		}

		#region implemented abstract members of IntentService

		protected override void OnHandleIntent (Intent intent)
		{
			try
			{
				lock (locker)
				{
					var instanceID = InstanceID.GetInstance (this);
					var token = instanceID.GetToken(GetString(Resource.String.GCM_ID), GoogleCloudMessaging.InstanceIdScope, null);
					PreferenceConnector.WriteString(this, "device_token", token);
					MyApplication.DeviceToken = token;
					SendRegistrationToAppServer (token);
					Subscribe (token);
				}
			} catch(Exception e) {
				Console.WriteLine (e.StackTrace);
				return;
			}
		}

		void SendRegistrationToAppServer (string token)
		{
			try {
				var currentUserId = MyApplication.CurrentUserId;
				var currentPassword = MyApplication.CurrentPassword;
				if (string.IsNullOrEmpty (currentUserId)) {
					currentUserId = PreferenceConnector.ReadString (this, "last_user_login", null);
				}
				if (string.IsNullOrEmpty (currentPassword)) {
					currentPassword = PreferenceConnector.ReadString (this, "last_user_password", null);
				}
				LoginData dataObjects = new LoginData ();
				dataObjects.username = currentUserId;
				dataObjects.password = currentPassword;
				dataObjects.deviceToken = token;
				dataObjects.platform = "android";
				string requestParameter = JsonConvert.SerializeObject (dataObjects);
				var jsonService = new JsonService ();
				jsonService.consumeService (CommonSharedStrings.LOGIN_URL, requestParameter, this);
			} catch (WebException) {				
			} catch (Exception) {				
			}
		}

		public void onResponse(string response)
		{
			Console.WriteLine (response);
		}

		void Subscribe (string token)
		{
			try {
				var pubSub = GcmPubSub.GetInstance (this);
				pubSub.Subscribe (token, "/topics/global", null);
			} catch (Exception) {				
			}
		}

		#endregion

		public void onResponseFailed (string response)
		{
		}
	}
}

