using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RetailerAcademy.Droid.Source.ContentDTO
{
    public class TeamDetailsDto
    {
        public string teamName { get; set; }
        public string teamCode { get; set; }
        public string description { get; set; }
    }
}