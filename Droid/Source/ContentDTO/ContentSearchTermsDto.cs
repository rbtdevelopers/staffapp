﻿using System;

namespace RetailerAcademy.Droid
{
	public class ContentSearchTermsDto : Java.Lang.Object, Java.IO.ISerializable
	{
		public ContentSearchTermsDto ()
		{
		}

		public int fileid
		{
			get;
			set;
		}

		public string type
		{
			get;
			set;
		}

		public string description
		{
			get;
			set;
		}

		public string filesname
		{
			get;
			set;
		}

		public string modifieddate
		{
			get;
			set;
		}

		public string filesurl {
			get;
			set;
		}

		public string filesize {
			get;
			set;
		}

		public bool hastags {
			get;
			set;
		}

		public bool hasread {
			get;
			set;
		}

		public string passCode {
			get;
			set;
		}
	}
}