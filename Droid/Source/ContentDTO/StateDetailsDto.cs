using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RetailerAcademy.Droid.Source.ContentDTO
{
    public class StateDetailsDto
    {
        public string statesName { get; set; }
        public string stateCode { get; set; }
        public string timeZone { get; set; }
        public string description { get; set; }  
    }
}