﻿using System;
using Java.IO;

namespace RetailerAcademy.Droid
{
	public class NotificationListObjDto : Java.Lang.Object, ISerializable
	{
		public NotificationListObjDto ()
		{
		}

		public string message 
		{
			get;
			set;
		}

		public string DateAndTime 
		{
			get;
			set;
		}

		public string moduleName 
		{
			get;
			set;
		}

		public int Id 
		{
			get;
			set;
		}

		public bool isRead 
		{
			get;
			set;
		}

		public string sendDate { get; set; }
		public int moduleId { get; set; }
		public string brandguid { get; set; }
		public string brandname { get; set; }
		public string modifieddate { get; set; }
		public string filesurl { get; set; }
		public string filesize { get; set; }
		public bool hastags { get; set; }
		public int blogcategoryid { get; set; }
	}
}

