﻿using System;

namespace RetailerAcademy.Droid
{
	public class ContentSearchDTO
	{
		public ContentSearchDTO ()
		{
		}

		public int userid
		{
			get;
			set;
		}

		public string orgid
		{
			get;
			set;
		}

		public string key
		{
			get;
			set;
		}

		public int folderid
		{
			get;
			set;
		}

		public int tagid
		{
			get;
			set;
		}
	}
}

