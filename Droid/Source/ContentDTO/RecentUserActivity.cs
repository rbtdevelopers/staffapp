﻿using System;

namespace RetailerAcademy.Droid
{
	public class RecentUserActivity
	{
		public RecentUserActivity ()
		{
		}

		public int userId {
			get;
			set;
		}

		public string brandGuid {
			get;
			set;
		}

		public string startDate {
			get;
			set;
		}

		public string endDate {
			get;
			set;
		}
	}
}
