﻿using System;
using Java.IO;

namespace RetailerAcademy.Droid
{
	public class ActivityStreamDto : Java.Lang.Object, ISerializable
	{
		public ActivityStreamDto ()
		{
		}

		public int activityStreamId
		{
			get;
			set;
		}

		public string userName
		{
			get;
			set;
		}

		public string profileImage
		{
			get;
			set;
		}

		public string activityStreamText
		{
			get;
			set;
		}

		public int likeCount
		{
			get;
			set;
		}

		public int commentCount
		{
			get;
			set;
		}

		public string date
		{
			get;
			set;
		}

		public bool hasLiked
		{
			get;
			set;
		}

		public string activityimage {
			get;
			set;
		}
	}
}