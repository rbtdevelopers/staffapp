﻿using System;

namespace RetailerAcademy.Droid
{
	public class MessageReplyDTO
	{
		public MessageReplyDTO ()
		{
		}

		public int userMessageID {
			get;
			set;
		}

		public int senderUserID {
			get;
			set;
		}

		public string senderNickName {
			get;
			set;
		}

		public string sendDateTime {
			get;
			set;
		}

		public string body {
			get;
			set;
		}

		public string attachment {
			get;
			set;
		}

		public string subject {
			get;
			set;
		}

		public string recipientName {
			get;
			set;
		}

		public int recipientUserIDs
		{
			get;
			set;
		}
		public String fileName
		{
			get;
			set;
		}
		public String extension
		{
			get;
			set;
		}
		public String imagebytes
		{
			get;
			set;
		}
	}
}

