﻿using System;

namespace RetailerAcademy.Droid
{
	public class MessageRepliesDTO
	{
		public MessageRepliesDTO ()
		{
		}

		public int userMessageID {
			get;
			set;
		}

		public int senderUserID {
			get;
			set;
		}

		public string senderNickName {
			get;
			set;
		}

		public string sendDateTime {
			get;
			set;
		}

		public string body {
			get;
			set;
		}

		public string subject {
			get;
			set;
		}

		public string attachment {
			get;
			set;
		}
	}
}