﻿using System;
using Java.IO;

namespace RetailerAcademy.Droid
{
	public class BookmarkListDto : Java.Lang.Object, ISerializable
	{
		public BookmarkListDto ()
		{
		}

		public int bookMarkId
		{
			get;
			set;
		}

		public int userId
		{
			get;
			set;
		}

		public int fileContentId
		{
			get;
			set;
		}

		public string bookmarkName
		{
			get;
			set;
		}

		public string bookmarkUrl
		{
			get;
			set;
		}

		public bool hasTags
		{
			get;
			set;
		}

		public string fileSize
		{
			get;
			set;
		}

		public string modifiedDate
		{
			get;
			set;
		}
	}
}