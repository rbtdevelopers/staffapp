﻿using System;

namespace RetailerAcademy.Droid
{
	public class ActivityStreamListDTO
	{
		public ActivityStreamListDTO ()
		{
		}

		public int userid {
			get;
			set;
		}

		public string Orgid {
			get;
			set;
		}

		public int aIDAfter {
			get;
			set;
		}
	}
}

