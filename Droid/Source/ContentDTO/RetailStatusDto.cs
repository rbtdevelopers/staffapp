using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RetailerAcademy.Droid.Source.ContentDTO
{
	public class LeaderBoardDto : Java.Lang.Object
    {
        public int leaderBoardId { get; set; }
        public string leaderBoardName { get; set; }
		public int userId { get; set; }
		public string orgId { get; set; }
    }

	public class LeaderBoardDtoWithOutJLObject
	{
		public int leaderBoardId { get; set; }
		public string leaderBoardName { get; set; }
		public int userId { get; set; }
		public string orgId { get; set; }
	}

	public class LeaderBoardUserDto
	{
		public int points { get; set; }
		public string userName { get; set; }
		public string profileImageUrl { get; set; }
		public int profileId { get; set; }
		public string userCategory { get; set; }
	}

	public class BlogDto : Java.Lang.Object
	{
		public int categoryId { get; set; }
		public string categoryName { get; set; }
		public bool hasviewed {
			get;
			set;
		}
	}
}