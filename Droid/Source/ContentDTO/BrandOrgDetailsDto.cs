using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RetailerAcademy.Droid.Source.ContentDTO
{
    public class BrandOrgDetailsDto : Java.Lang.Object
    {
        public string numberCount { get; set; }
        public string brandName { get; set; }
        public string brandImageURL { get; set; }
		public bool defaultBrand { get;	set; }
		public string userRole { get; set; }
		public int totalNewBlogs { get; set; }
		public int totalNewContent { get; set; }
		public int totalNewQuiz { get; set; }
    }
}