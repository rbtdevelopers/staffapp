﻿using System;
using Java.IO;

namespace RetailerAcademy.Droid
{
	public class BlogListObjectsDTO : Java.Lang.Object, ISerializable
	{
		public BlogListObjectsDTO ()
		{
		}

		public int blogid
		{
			get;
			set;
		}

		public string blogtitle
		{
			get;
			set;
		}

		public string imageurl
		{
			get;
			set;
		}

		public string blogdescription
		{
			get;
			set;
		}

		public int likecount
		{
			get;
			set;
		}

		public int commnetcount
		{
			get;
			set;
		}

		public string videourl
		{
			get;
			set;
		}


		public string date
		{
			get;
			set;
		}

		public bool hasLiked
		{
			get;
			set;
		}

		public bool hasviewed
		{
			get;
			set;
		}
	}
}
