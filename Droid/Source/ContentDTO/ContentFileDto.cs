﻿using System;
using Java.IO;

namespace RetailerAcademy.Droid
{
	public class ContentFileDto : Java.Lang.Object, ISerializable
	{
		public ContentFileDto ()
		{
		}

		public bool getORpost
		{
			get;
			set;
		}

		public int filesid
		{
			get;
			set;
		}
		public string filesname
		{
			get;
			set;
		}
		public string filespath
		{
			get;
			set;
		}

		public string filesextension
		{
			get;
			set;
		}

		public string createdDate
		{
			get;
			set;
		}

		public string filesize
		{
			get;
			set;
		}

		public bool hasTags
		{
			get;
			set;
		}

		public bool hasread 
		{
			get;
			set;
		}

		public string type
		{
			get;
			set;
		}

		public string description
		{
			get;
			set;
		}

		public string passCode
		{
			get;
			set;
		}
	}
}