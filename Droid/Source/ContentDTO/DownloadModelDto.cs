﻿using System;

namespace RetailerAcademy.Droid
{
	public class DownloadModelDto
	{
		public DownloadModelDto ()
		{
		}

		public string downloadFolderName {
			get;
			set;
		}

		public string downloadFileName {
			get;
			set;
		}

		public bool downloadStatus {
			get;
			set;
		}
	}
}

