using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RetailerAcademy.Droid.Source.ContentDTO
{
	public class QuizCategoryDto : Java.Lang.Object
    {
        public int categoryID { get; set; }
        public string categoryName { get; set; }
        public string categoryDescription { get; set; }
		public bool hasViewed {
			get;
			set;
		}
		public bool hasattempted {
			get;
			set;
		}
    }

    public class QuizQuestionsCallDto
    {
        public int userid { get; set; }
        public string questionid { get; set; }
        public string answersubmited { get; set; }
        public string categoryid { get; set; }
        public string attemptedQuestions { get; set; }
    }

    public class QuizAnswers
    { 
        public string questionid { get; set; }
        public string answersubmited { get; set; }
        public string questiontype { get; set; }
        public string question { get; set; }
        public List<Answers> answersArray { get; set; }
    }

    public class Answers : Java.Lang.Object
    {
        public Answers() {}
        public string answer { get; set; }
        public int answerid { get; set; }
    }
}