using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RetailerAcademy.Droid.Source.ContentDTO
{
    public class MessageComposeModel
    {
        public MessageComposeModel()
        {
        }

        public int userID
        {
            get;
            set;
        }
        public String userEmail
        {
            get;
            set;
        }
        public String fullName
        {
            get;
            set;
        }
    }

    public class MessageSend
    {
        public MessageSend()
        {
        }

        public int senderUserID
        {
            get;
            set;
        }
        public String subject
        {
            get;
            set;
        }
        public String body
        {
            get;
            set;
        }
        public String recipientUserIDs
        {
            get;
            set;
        }
        public String fileName
        {
            get;
            set;
        }
        public String extension
        {
            get;
            set;
        }
        public String imagebytes
        {
            get;
            set;
        }
    }
}