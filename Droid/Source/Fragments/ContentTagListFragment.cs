﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Support.V4.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using System.ComponentModel;
using Java.Lang;
using Newtonsoft.Json.Linq;
using Android.Views.InputMethods;
using Android.Preferences;

namespace RetailerAcademy.Droid
{
	public class ContentTagListFragment : BaseFragment, IWebServiceDelegate, Android.Text.ITextWatcher, View.IOnClickListener, ListView.IOnItemClickListener, TextView.IOnEditorActionListener
	{
		private Context context;
		private TextView headerText, cancelTV;
		private EditText searchBoxET;
		private ListView contentTagListLV;
		private ContentTagListAdapter contentTagListAdapter;
		private Dialog progressDialog, alertDialog;
		private List<ContentTags> contentTagList;
		public List<ContentTags> filteredContentTagList;
		private SwipeRefreshLayout swipeLayout;
		private int fileId;
		public bool isServiceCallCompleted;
		private JsonService jsonService;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ContentTagListFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public ContentTagListFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{			
			base.OnCreate (savedInstanceState);
			fileId = Arguments.GetInt ("fileId");
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			try {
				activity.FindViewById<ImageView> (Resource.Id.headerMenu).Visibility = ViewStates.Gone;
				activity.FindViewById<ImageView> (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;
				headerText.Visibility = ViewStates.Visible;
				headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				headerText.Text = GetString (Resource.String.content_tag_text);
				WebServiceCallForTagList ();
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Webservice call for tag list.
		/// </summary>
		private void WebServiceCallForTagList ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					ContentTag contentObj = new ContentTag ();
					contentObj.fileID = fileId;
					contentObj.userID = Convert.ToInt32 (userId);
					contentObj.brandGuid = orgId;
					string requestParameter = JsonConvert.SerializeObject (contentObj);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.TAG_LIST, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.ContentTagListFragment, container, false);
			contentTagListLV = view.FindViewById<ListView> (Resource.Id.contentTagListLV);
			cancelTV = view.FindViewById<TextView> (Resource.Id.cancelTV);
			searchBoxET = view.FindViewById<EditText> (Resource.Id.searchETId);
			swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
			try {
				cancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				searchBoxET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				searchBoxET.AddTextChangedListener (this);
				searchBoxET.SetOnEditorActionListener (this);
				cancelTV.SetOnClickListener (this);
				contentTagListLV.OnItemClickListener = this;
				swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
					Android.Resource.Color.HoloGreenLight,
					Android.Resource.Color.HoloOrangeLight,
					Android.Resource.Color.HoloRedLight);
				swipeLayout.Refresh += delegate(object sender, EventArgs e) {
					ClearSearch ();
					swipeLayout.PostDelayed (WebServiceCallForTagList, 3000);
				};
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Clears the search.
		/// </summary>
		private void ClearSearch ()
		{
			if (searchBoxET != null && !string.IsNullOrEmpty (searchBoxET.Text)) {
				searchBoxET.Text = "";
			}
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					contentTagList = new List<ContentTags> ();
					ContentTags contentTag;
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					JArray tagListArray = null;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (status) {
						if (responseObj ["contentTag"] != null) {
							tagListArray = (JArray)responseObj.SelectToken ("contentTag");
							if (tagListArray != null && tagListArray.Count > 0) {
								foreach (JObject item in tagListArray) {
									contentTag = new ContentTags ();
									contentTag.tagID = (int)item.SelectToken ("tagID");
									contentTag.tagName = (string)item.SelectToken ("tagName");
									contentTag.fileCount = (int)item.SelectToken ("fileCount");
									contentTagList.Add (contentTag);
								}
								activity.RunOnUiThread (() => {
									contentTagListAdapter = new ContentTagListAdapter (this.context, Resource.Layout.ContentTagListItem, contentTagList);
									contentTagListLV.Adapter = contentTagListAdapter;
								});
							}
						}
					} else {		
						activity.RunOnUiThread (() => {
							contentTagListAdapter = new ContentTagListAdapter (this.context, Resource.Layout.ContentTagListItem, contentTagList);
							contentTagListLV.Adapter = contentTagListAdapter;
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
						});
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		/// <param name="s">To be added.</param>
		/// <summary>
		/// Afters the text changed.
		/// </summary>
		public void AfterTextChanged (Android.Text.IEditable s)
		{
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <param name="after">To be added.</param>
		/// <summary>
		/// Befores the text changed.
		/// </summary>
		public void BeforeTextChanged (ICharSequence s, int start, int count, int after)
		{
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="before">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <summary>
		/// Raises the text changed event.
		/// </summary>
		public void OnTextChanged (ICharSequence s, int start, int before, int count)
		{
			try {
				if (contentTagList != null && contentTagList.Count > 0) {
					filteredContentTagList = contentTagList.Where (p => p.tagName.ToLower ().Contains (searchBoxET.Text.ToLower ())).ToList ();
					contentTagListAdapter = new ContentTagListAdapter (this.context, Resource.Layout.ContentTagListItem, filteredContentTagList);
					contentTagListLV.Adapter = contentTagListAdapter;
				}
			} catch (System.Exception) {					
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				switch (id) {
				case Resource.Id.cancelTV:
					searchBoxET.Text = "";
					View view = this.activity.CurrentFocus;
					if (view != null) {  
						InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
						imm.HideSoftInputFromWindow (view.WindowToken, 0);
					}
					break;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the item click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			try {
				View v = this.activity.CurrentFocus;
				if (v != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					ContentFileListFragment contentFileListFragment = new ContentFileListFragment (this.context);
					Bundle args = new Bundle ();
					args.PutString ("navigating_from", "content_tag_list_fragment");
					args.PutBoolean ("fromTag", true);
					args.PutString ("tagName", contentTagListAdapter.contentTagListItems [position].tagName);
					args.PutInt ("tagId", contentTagListAdapter.contentTagListItems [position].tagID);
					contentFileListFragment.Arguments = args;
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentFileListFragment, "content_file_list_fragment").AddToBackStack ("content_file_list_fragment").Commit ();
				}
			} catch (System.Exception) {					
			}
		}

		/// <summary>
		/// Raises the editor action event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="actionId">Action identifier.</param>
		/// <param name="e">E.</param>
		public bool OnEditorAction (TextView v, ImeAction actionId, KeyEvent e)
		{
			if (actionId == ImeAction.Search) {
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				return true;
			}
			return false;
		}
	}
}