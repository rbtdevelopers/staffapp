﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using RetailerAcademy.Droid.StaffApp;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;

namespace RetailerAcademy.Droid
{
	public class CommunicationDisplayFragment : BaseFragment, View.IOnClickListener
	{
		private Context context;
		private TextView newsFeedTV, messageTV, whatsHotTV, headerTextTV;
		public TextView orgsTitleTVId, whatsHotCountTV;
		private ImageButton newsFeedIB, whatsHotIB, messageIB;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.CommunicationDisplayFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public CommunicationDisplayFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerTextTV = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerTextTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			headerTextTV.Text = GetString (Resource.String.communication_text);
			activity.FindViewById<ImageView> (Resource.Id.headerMenu).Visibility = ViewStates.Visible;
			activity.FindViewById<ImageView> (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;
			activity.FindViewById<TextView> (Resource.Id.headerText).Visibility = ViewStates.Visible;
			orgsTitleTVId.Text = MyApplication.OrgName;
			UpdateCountInCommunicate ();
			((MainActivity)context).EnableNavigationDrawer ();
			PreferenceConnector.WriteString (this.context, "fragmentName", "communication");
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.CommunicationDisplayFragment, container, false);
			newsFeedTV = (TextView)view.FindViewById (Resource.Id.newsFeedTV);
			messageTV = (TextView)view.FindViewById (Resource.Id.messageTV);
			whatsHotTV = (TextView)view.FindViewById (Resource.Id.whatsHotTV);
			orgsTitleTVId = (TextView)view.FindViewById (Resource.Id.orgsTitleTVId);
			whatsHotCountTV = (TextView)view.FindViewById (Resource.Id.whatsHotCountTV);

			newsFeedIB = (ImageButton)view.FindViewById (Resource.Id.newsFeedIB);
			messageIB = (ImageButton)view.FindViewById (Resource.Id.messageIB);
			whatsHotIB = (ImageButton)view.FindViewById (Resource.Id.whatsHotIB);

			newsFeedIB.SetOnClickListener (this);
			messageIB.SetOnClickListener (this);
			whatsHotIB.SetOnClickListener (this);

			newsFeedTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			whatsHotCountTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			messageTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			whatsHotTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			orgsTitleTVId.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			return view;
		}

		/// <summary>
		/// Updates the count in communicate page.
		/// </summary>
		public void UpdateCountInCommunicate()
		{
			try {
				var newBlogsCount = PreferenceConnector.ReadInteger (this.context, "newBlogsCount", 0); 
				CommonMethods.CommentsOrLikesCountDisplay (newBlogsCount, whatsHotCountTV);
				if (newBlogsCount != 0) {
					whatsHotCountTV.Visibility = ViewStates.Visible;
				} else {
					whatsHotCountTV.Visibility = ViewStates.Gone;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				switch (id) {
				case Resource.Id.newsFeedIB:
					ActivityStreamListFragment activityStreamListFrag = new ActivityStreamListFragment (this.context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, activityStreamListFrag, "activity_stream_list_fragment").AddToBackStack ("activity_stream_list_fragment").Commit ();
					break;
				case Resource.Id.messageIB:
					MessagesListFragment messagesFrag = new MessagesListFragment (this.context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, messagesFrag, "messages_list_fragment").AddToBackStack ("messages_list_fragment").Commit ();
					break;
				case Resource.Id.whatsHotIB:
					BlogCategoryListFragment blogCategoryListFrag = new BlogCategoryListFragment (this.context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, blogCategoryListFrag, "blogs_category_list").AddToBackStack ("blogs_category_list").Commit ();
					break;
				}
			} catch (Exception) {					
			}
		}
	}
}