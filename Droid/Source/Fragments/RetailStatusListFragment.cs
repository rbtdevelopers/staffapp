using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json.Linq;
using RetailerAcademy.Droid.Source.ContentDTO;
using Java.Lang;
using Android.Support.V4.Widget;
using System.ComponentModel;
using Android.Views.InputMethods;

namespace RetailerAcademy.Droid.Source.Fragments
{
	public class RetailStatusListFragment : BaseFragment, IWebServiceDelegate, Android.Text.ITextWatcher, ListView.IOnItemClickListener, TextView.IOnEditorActionListener
	{
		private Context context;
		private TextView headerTextTV, cancelTV;
		private EditText searchET;
		private Dialog progressDialog;
		private ListView itemsListView;
		private List<LeaderBoardDto> retailStatusList = null;
		private LeaderBoardAdapter leaderBoardAdapter;
		private RelativeLayout searchRL;
		private SwipeRefreshLayout swipeLayout;
		public bool isServiceCallCompleted;
		private JsonService jsonService;

		public RetailStatusListFragment (Context context)
		{
			this.context = context;
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		public override void OnStart ()
		{
			base.OnStart ();
		}

		public override void OnResume ()
		{
			base.OnResume ();
			headerTextTV = (TextView)activity.FindViewById (Resource.Id.headerText);
			try {
				headerTextTV.Text = GetString (Resource.String.leaderboard_text);
				headerTextTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				ClearSearch ();
				WebServiceCall ();
			} catch (System.Exception) {				
			}
		}

		private void ClearSearch ()
		{
			if (searchET != null && !string.IsNullOrEmpty (searchET.Text)) {
				searchET.Text = "";
			}
		}

		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.LEADERBOARDCATEGORY + userId + "/" + orgId, null, this);
				}
			} catch (System.Exception) {				
			}
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.RetailStatusListFragment, container, false);
			cancelTV = view.FindViewById<TextView> (Resource.Id.cancelTV);
			searchET = view.FindViewById<EditText> (Resource.Id.searchETId);
			itemsListView = (ListView)view.FindViewById (Resource.Id.listViewId);
			searchRL = (RelativeLayout)view.FindViewById (Resource.Id.searchRLId);
			swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
			try {
				cancelTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				searchET.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				searchET.AddTextChangedListener (this);
				searchET.SetOnEditorActionListener (this);
				itemsListView.OnItemClickListener = this;	
				cancelTV.Click += cancelTV_Click;			
				swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
					Android.Resource.Color.HoloGreenLight,
					Android.Resource.Color.HoloOrangeLight,
					Android.Resource.Color.HoloRedLight);
				swipeLayout.Refresh += delegate(object sender, EventArgs e) {			
					swipeLayout.PostDelayed (WebServiceCall, 3000);				
				};
			} catch (System.Exception) {				
			}
			return view;
		}

		void cancelTV_Click (object sender, EventArgs e)
		{
			searchET.Text = "";
		}

		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					JArray leaderBoardListArray = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if (responseObj ["leaderBoardList"] != null) {
							leaderBoardListArray = (JArray)responseObj.SelectToken ("leaderBoardList");
							if (leaderBoardListArray != null && leaderBoardListArray.Count > 0) {
								retailStatusList = new List<LeaderBoardDto> ();
								foreach (JObject item in leaderBoardListArray) {
									LeaderBoardDto rsDto = new LeaderBoardDto ();
									rsDto.leaderBoardId = (int)item.SelectToken ("leaderBoardID");
									rsDto.leaderBoardName = (string)item.SelectToken ("leaderBoardName");
									retailStatusList.Add (rsDto);
								}
								if (retailStatusList.Count == 0) {
									searchRL.Visibility = ViewStates.Gone;
								}					 
								activity.RunOnUiThread (() => {
									leaderBoardAdapter = new LeaderBoardAdapter (this.context, Resource.Layout.LeaderBoardListItem, retailStatusList);
									itemsListView.Adapter = leaderBoardAdapter;
								});
							}
						}
					} else {
						searchRL.Visibility = ViewStates.Gone;
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						var message = Convert.ToString (responseObj.SelectToken ("message"));
						if (!string.IsNullOrEmpty (message)) {
							CommonMethods.ShowAlertDialog (this.context, message);
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}	
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
			});
			CommonMethods.ShowAlertDialog (this.context, response);
		}

		public void AfterTextChanged (Android.Text.IEditable s)
		{
		}

		public void BeforeTextChanged (ICharSequence s, int start, int count, int after)
		{
		}

		public void OnTextChanged (ICharSequence s, int start, int before, int count)
		{
			try {
				var leaderBoardList = new List<LeaderBoardDto> ();
				for (int i = 0; i < retailStatusList.Count; i++) {
					string title = retailStatusList [i].leaderBoardName.ToLower ();
					string searchName = s + "".ToLower ();
					if (title.Contains (searchName)) {
						Console.WriteLine ("added : " + s);
						LeaderBoardDto rsDto = new LeaderBoardDto ();
						rsDto.leaderBoardId = retailStatusList [i].leaderBoardId;
						rsDto.leaderBoardName = retailStatusList [i].leaderBoardName;
						leaderBoardList.Add (rsDto);
					}
				}
				leaderBoardAdapter = new LeaderBoardAdapter (this.context, Resource.Layout.LeaderBoardListItem, leaderBoardList);
				itemsListView.Adapter = leaderBoardAdapter;
			} catch (System.Exception) {					
			}
		}

		public bool OnEditorAction (TextView v, ImeAction actionId, KeyEvent e)
		{
			if (actionId == ImeAction.Search) {
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				return true;
			}
			return false;
		}

		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					TextView itemNameTV = (TextView)view.FindViewById (Resource.Id.itemTVId);
					LeaderBoardDto leaderBoardDto = (LeaderBoardDto)itemNameTV.Tag;					
					string[] values = { leaderBoardDto.leaderBoardName,	leaderBoardDto.leaderBoardId + "" };
					LeaderBoardUserFragment leaderBoardUserFragment = new LeaderBoardUserFragment (context, values);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, leaderBoardUserFragment, "leaderboard_user_fragment").AddToBackStack ("leaderboard_user_fragment").Commit ();
				}
			} catch (System.Exception) {					
			}
		}
	}
}