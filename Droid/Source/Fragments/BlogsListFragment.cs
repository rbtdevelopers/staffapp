﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using RetailerAcademy.Droid.StaffApp;
using Android.Support.V4.Widget;
using System.ComponentModel;
using System.Threading;
using Android.Views.InputMethods;
using System.Text.RegularExpressions;
using Java.Lang;

namespace RetailerAcademy.Droid
{
	public class BlogsListFragment : BaseFragment, IWebServiceDelegate, Android.Text.ITextWatcher, BlogServiceForLike, ListView.IOnItemClickListener, TextView.IOnEditorActionListener, ListView.IOnClickListener
	{
		private Context context;
		private TextView headerTextTV, cancelTV;
		private EditText searchET;
		private Dialog progressDialog, alertDialog;
		private ListView itemsListView;
		private int categoryId;
		private bool isLikeAreNot = false, searchEnable = false;
		private BlogListAdapter blogListAdapter;
		private List<BlogListObjectsDTO> blogsMsgsList = new List<BlogListObjectsDTO> ();
		public List<BlogListObjectsDTO> filteredBlogsList;
		private RelativeLayout searchRL;
		private SwipeRefreshLayout swipeLayout;
		private JsonService jsonService;
		private string categoryName, contentFileNameHeader;
		private BlogListObjectsDTO dto = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.BlogsListFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="categoryid">Categoryid.</param>
		/// <param name="categoryName">Category name.</param>
		public BlogsListFragment (Context context, int categoryid, string categoryName)
		{
			this.context = context;
			this.categoryId = categoryid;
			this.categoryName = categoryName;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerTextTV = (TextView)activity.FindViewById (Resource.Id.headerText);
			try {
				contentFileNameHeader = (!string.IsNullOrEmpty (categoryName)) ? categoryName : "WHAT'S HOT"; 
				headerTextTV.Text = contentFileNameHeader.ToUpper ();
				headerTextTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				activity.FindViewById<ImageView> (Resource.Id.headerMenu).Visibility = ViewStates.Visible;
				activity.FindViewById<ImageView> (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;
				headerTextTV.SetOnClickListener (this);
				var notificationCount = PreferenceConnector.ReadInteger (this.context, "notificationCount", 0);
				if (notificationCount > 0) {
					activity.FindViewById (Resource.Id.notificationCountTV).Visibility = ViewStates.Visible;				
				}
				((MainActivity)context).EnableNavigationDrawer ();
				isLikeAreNot = false;
				searchEnable = false;
				if (blogListAdapter != null) {
					blogListAdapter.blogsList = blogsMsgsList;
					blogListAdapter.NotifyDataSetChanged ();
				}
				if (searchET != null && !string.IsNullOrEmpty (searchET.Text)) {
					searchET.Text = "";
				}
				WebServiceCall ();
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Clears the search.
		/// </summary>
		private void ClearSearch ()
		{
			if (searchET != null && !string.IsNullOrEmpty (searchET.Text)) {
				searchET.Text = "";
				WebServiceCall ();
			}
		}

		/// <summary>
		/// Webservice call for blog list.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					BlogsPost blogsPostDto = new BlogsPost ();
					blogsPostDto.userId = Convert.ToInt32 (userId);
					blogsPostDto.categoryid = categoryId;
					blogsPostDto.blogid = 0;
					string requestParameter = JsonConvert.SerializeObject (blogsPostDto);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.BLOG_LIST, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.BlogsListFragment, container, false);
			cancelTV = view.FindViewById<TextView> (Resource.Id.cancelTV);
			searchET = view.FindViewById<EditText> (Resource.Id.searchETId);
			itemsListView = (ListView)view.FindViewById (Resource.Id.blogsListViewId);
			searchRL = (RelativeLayout)view.FindViewById (Resource.Id.searchRLId);
			swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
			cancelTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			searchET.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);

			try {
				searchET.AddTextChangedListener (this);
				searchET.SetOnEditorActionListener (this);
				itemsListView.OnItemClickListener = this;
				cancelTV.SetOnClickListener(this);
				swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
					Android.Resource.Color.HoloGreenLight,
					Android.Resource.Color.HoloOrangeLight,
					Android.Resource.Color.HoloRedLight);			
				swipeLayout.Refresh += delegate(object sender, EventArgs e) {	
					if (searchET != null && !string.IsNullOrEmpty (searchET.Text)) {
						searchET.Text = "";
					}
					swipeLayout.PostDelayed (WebServiceCall, 3000);
				};
				blogListAdapter = new BlogListAdapter (this.context, Resource.Layout.BlogListItem, blogsMsgsList, this);
				itemsListView.Adapter = blogListAdapter;
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);	
					bool status = false;
					JArray blogItemListArray = null;
					string statusMessage = null;
					BlogListObjectsDTO blogsListDto = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if (!isLikeAreNot) {
							if (responseObj ["blog"] != null) {
								blogItemListArray = (JArray)responseObj.SelectToken ("blog");
							}
							if (blogItemListArray != null && blogItemListArray.Count > 0) {	
								blogsMsgsList.Clear ();
								foreach (JObject item in blogItemListArray) {
									blogsListDto = new BlogListObjectsDTO ();
									blogsListDto.blogid = (int)item.SelectToken ("blogid");
									blogsListDto.blogdescription = (string)item.SelectToken ("blogdescription");
									blogsListDto.blogtitle = (string)item.SelectToken ("blogtitle");
									blogsListDto.imageurl = (string)item.SelectToken ("imageurl");
									blogsListDto.videourl = (string)item.SelectToken ("videourl");
									blogsListDto.date = (string)item.SelectToken ("date");
									blogsListDto.likecount = (int)item.SelectToken ("likecount");
									blogsListDto.commnetcount = (int)item.SelectToken ("commnetcount");
									blogsListDto.hasLiked = (bool)item.SelectToken ("hasliked");
									blogsListDto.hasviewed = (bool)item.SelectToken ("hasviewed");
									if (searchEnable) {
										string searchName = searchET.Text.ToLower ();
										if (blogsListDto.blogtitle.ToLower ().Contains (searchName)) {
											blogsMsgsList.Add (blogsListDto);
										}
									} else {
										blogsMsgsList.Add (blogsListDto);
									}
								}
								activity.RunOnUiThread (() => blogListAdapter.NotifyDataSetChanged ());
							}
						} else {
							isLikeAreNot = false;
							dto.hasLiked = !dto.hasLiked;
							int likeCount = 0, commentCount = 0;
							if (responseObj ["totalLikes"] != null) {
								likeCount = (int)responseObj.SelectToken ("totalLikes");
								dto.likecount = likeCount;
							}
							if (responseObj ["totalComments"] != null) {
								commentCount = (int)responseObj.SelectToken ("totalComments");
								dto.commnetcount = commentCount;
							}
							activity.RunOnUiThread (() => blogListAdapter.NotifyDataSetChanged ());
						}
					} else {
						/* display toast message */
						if (responseObj ["message"] != null) {
							statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
						}
						if (blogsMsgsList != null && blogsMsgsList.Count > 0) {	
							blogsMsgsList.Clear ();
							activity.RunOnUiThread (() => blogListAdapter.NotifyDataSetChanged ());
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
				CommonMethods.ShowAlertDialog (this.context, response);	
			});
		}

		/// <param name="s">To be added.</param>
		/// <summary>
		/// Afters the text changed.
		/// </summary>
		public void AfterTextChanged (Android.Text.IEditable s)
		{			
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <param name="after">To be added.</param>
		/// <summary>
		/// Befores the text changed.
		/// </summary>
		public void BeforeTextChanged (Java.Lang.ICharSequence s, int start, int count, int after)
		{			
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="before">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <summary>
		/// Raises the text changed event.
		/// </summary>
		public void OnTextChanged (Java.Lang.ICharSequence s, int start, int before, int count)
		{
			try {
				if (blogsMsgsList != null && blogsMsgsList.Count > 0) {
					filteredBlogsList = blogsMsgsList.Where (p => p.blogtitle.ToLower ().Contains (searchET.Text.ToLower ()) || p.blogdescription.ToLower ().Contains (searchET.Text.ToLower ())).ToList ();
					blogListAdapter = new BlogListAdapter (this.context, Resource.Layout.BlogListItem, filteredBlogsList, this);
					itemsListView.Adapter = blogListAdapter;
				}
			} catch (System.Exception) {				
			}
		}

		#region BlogServiceForLike implementation
		/// <summary>
		/// Webservice for like.
		/// </summary>
		/// <param name="blogInteger">Blog integer.</param>
		/// <param name="ctx">Context.</param>
		/// <param name="isLike">Is like.</param>
		/// <param name="itemPosition">Item position.</param>
		/// <param name="blogDto">Blog dto.</param>
		public void BlogServiceForLike (int blogInteger, Context ctx, string isLike, int itemPosition, BlogListObjectsDTO blogDto)
		{
			try {
				if (!CommonMethods.IsInternetConnected (ctx)) {
					CommonMethods.ShowAlertDialog (ctx, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (isLike == "like") {
						if (progressDialog == null) {
							progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
						}
						dto = blogDto;
						isLikeAreNot = true;
						if (!string.IsNullOrEmpty (searchET.Text)) {
							searchEnable = true;
						} else {
							searchEnable = false;
						}
						var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
						BlogLike blogLikeObj = new BlogLike ();
						blogLikeObj.blogid = Convert.ToInt32 (blogInteger);
						blogLikeObj.userid = Convert.ToInt32 (userId);
						string requestParameter = JsonConvert.SerializeObject (blogLikeObj);
						jsonService = new JsonService ();
						jsonService.consumeService (CommonSharedStrings.BLOG_LIKE, requestParameter, this);
					} else {
						OpenCommentsDetails (itemPosition, blogDto);
					}
				}
			} catch (System.Exception) {				
			}
		}

		#endregion
		/// <summary>
		/// This block of code is to navigate to comments details.
		/// </summary>
		/// <param name="position">Position.</param>
		/// <param name="blogListDto">Blog list dto.</param>
		private void OpenCommentsDetails (int position, BlogListObjectsDTO blogListDto)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					BlogsCommentsFragment blogsCommentFrag = new BlogsCommentsFragment (context, blogListDto);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, blogsCommentFrag, "blogs_comment_fragment").AddToBackStack ("blogs_comment_fragment").Commit ();
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the item click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					TextView likeCountTVId = view.FindViewById<TextView> (Resource.Id.likeCountTVId);
					dto = (BlogListObjectsDTO)likeCountTVId.Tag;
					BlogsListDetailsFragment blogsListDetailsFrag = new BlogsListDetailsFragment (context, dto, contentFileNameHeader, categoryId);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, blogsListDetailsFrag, "blogs_list_details_fragment").AddToBackStack ("blogs_list_details_fragment").Commit ();
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the editor action event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="actionId">Action identifier.</param>
		/// <param name="e">E.</param>
		public bool OnEditorAction (TextView v, ImeAction actionId, KeyEvent e)
		{
			if (actionId == ImeAction.Search) {
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				return true;
			}
			return false;
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				var id = v.Id;
				switch (id) {
				case Resource.Id.headerText:
					if (itemsListView != null) {
						itemsListView.SmoothScrollToPosition (0);
					}
					break;
				case Resource.Id.cancelTV:
					ClearSearch ();
					break;
				}
			} catch (System.Exception) {				
			}
		}
	}
}

