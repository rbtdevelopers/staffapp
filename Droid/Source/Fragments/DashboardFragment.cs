using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using RetailerAcademy.Droid.StaffApp;

namespace RetailerAcademy.Droid.Source.Fragments
{
	public class DashboardFragment : BaseFragment, View.IOnClickListener
	{
		private Context context;
		private TextView communicateTV, contentTV, quizMasterTV, retailStatusTV, headerTextTV;
		public TextView orgsTitleTVId, communicateCountTV, contentCountTV, quizMasterCountTV;
		private ImageButton communicateIB, contentIB, quizMasterIB, retailStatusIB;
		public CommunicationDisplayFragment communicationDisplayFragment = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.Source.Fragments.DashboardFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public DashboardFragment (Context context)
		{
			this.context = context;
		}

		public DashboardFragment ()
		{
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerTextTV = (TextView)activity.FindViewById (Resource.Id.headerText);
			try {
				headerTextTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				headerTextTV.Text = GetString (Resource.String.dashboard_text);
				activity.FindViewById<ImageView> (Resource.Id.headerMenu).Visibility = ViewStates.Visible;
				activity.FindViewById<ImageView> (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;
				activity.FindViewById<TextView> (Resource.Id.headerText).Visibility = ViewStates.Visible;
				orgsTitleTVId.Text = MyApplication.OrgName;
				UpdateCountValues();
				((MainActivity)context).EnableNavigationDrawer ();
				PreferenceConnector.WriteString (this.context, "fragmentName", "dashboard");
			} catch (Exception) {					
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.DashboardFragment, container, false);
			communicateTV = (TextView)view.FindViewById (Resource.Id.communicateTV);
			contentTV = (TextView)view.FindViewById (Resource.Id.contentTV);
			quizMasterTV = (TextView)view.FindViewById (Resource.Id.quizMasterTV);
			retailStatusTV = (TextView)view.FindViewById (Resource.Id.retailStatusTV);
			orgsTitleTVId = (TextView)view.FindViewById (Resource.Id.orgsTitleTVId);
			communicateIB = (ImageButton)view.FindViewById (Resource.Id.communicateIB);
			contentIB = (ImageButton)view.FindViewById (Resource.Id.contentIB);
			quizMasterIB = (ImageButton)view.FindViewById (Resource.Id.quizMasterIB);
			retailStatusIB = (ImageButton)view.FindViewById (Resource.Id.retailStatusIB);
			communicateCountTV = (TextView)view.FindViewById (Resource.Id.communicateCountTV);
			contentCountTV = (TextView)view.FindViewById (Resource.Id.contentCountTV);
			quizMasterCountTV = (TextView)view.FindViewById (Resource.Id.quizMasterCountTV);
			try {
				communicateIB.SetOnClickListener (this);
				contentIB.SetOnClickListener (this);
				quizMasterIB.SetOnClickListener (this);
				retailStatusIB.SetOnClickListener (this);

				communicateCountTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				contentCountTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				quizMasterCountTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				communicateTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				contentTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				quizMasterTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				retailStatusTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				orgsTitleTVId.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			} catch (Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Updates the count values.
		/// </summary>
		public void UpdateCountValues(){
			try {
				var newContentCount = PreferenceConnector.ReadInteger (this.context, "newContentCount", 0);
				var newQuizCount = PreferenceConnector.ReadInteger (this.context, "newQuizCount", 0);
				var newBlogsCount = PreferenceConnector.ReadInteger (this.context, "newBlogsCount", 0); 
				CommonMethods.CommentsOrLikesCountDisplay (newBlogsCount, communicateCountTV);
				CommonMethods.CommentsOrLikesCountDisplay (newContentCount, contentCountTV);
				CommonMethods.CommentsOrLikesCountDisplay (newQuizCount, quizMasterCountTV);
				if (newContentCount != 0) {
					contentCountTV.Visibility = ViewStates.Visible;
				} else {
					contentCountTV.Visibility = ViewStates.Gone;
				}
				if (newQuizCount != 0) {
					quizMasterCountTV.Visibility = ViewStates.Visible;
				} else {
					quizMasterCountTV.Visibility = ViewStates.Gone;
				}
				if (newBlogsCount != 0) {
					communicateCountTV.Visibility = ViewStates.Visible;
				} else {
					communicateCountTV.Visibility = ViewStates.Gone;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					switch (id) {
					case Resource.Id.communicateIB:
						communicationDisplayFragment = new CommunicationDisplayFragment (this.context);
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, communicationDisplayFragment, "communication_display_fragment").AddToBackStack ("communication_display_fragment").Commit ();
						break;
					case Resource.Id.contentIB:
						ContentFolderListFragment contentFolderFrag = new ContentFolderListFragment (this.context);	
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentFolderFrag, "content_folder_list_fragment").AddToBackStack ("content_folder_list_fragment").Commit ();
						break;
					case Resource.Id.quizMasterIB:
						QuizCategoryListFragment quizCategoryListFragment = new QuizCategoryListFragment (this.context);
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, quizCategoryListFragment, "quiz_category_list_fragment").AddToBackStack ("quiz_category_list_fragment").Commit ();
						break;
					case Resource.Id.retailStatusIB:
						LeaderBoardNewUserFragment leaderBoardNewUserFragment = new LeaderBoardNewUserFragment (this.context);
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, leaderBoardNewUserFragment, "leaderboard_status_fragment").AddToBackStack ("leaderboard_status_fragment").Commit ();
						break;
					}
				}
			} catch (Exception) {					
			}
		}
	}
}