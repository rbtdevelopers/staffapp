﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Support.V4.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Threading;
using Newtonsoft.Json.Linq;
using Java.Lang;
using Android.Views.InputMethods;
using RetailerAcademy.Droid.StaffApp;

namespace RetailerAcademy.Droid
{
	public class NotificationsListFragment : BaseFragment, IWebServiceDelegate, Android.Text.ITextWatcher, View.IOnClickListener, ListView.IOnItemClickListener, TextView.IOnEditorActionListener, AbsListView.IOnScrollListener
	{
		private Context context;
		private TextView headerText, cancelTV;
		private EditText searchBoxET;
		private ListView notificationsListLV;
		private NotificationsListAdapter notificationsListAdapter;
		private Dialog progressDialog, alertDialog;
		private List<NotificationListObjDto> notificationList = null;
		private List<NotificationListObjDto> previousNotificationList = new List<NotificationListObjDto> ();
		public List<NotificationListObjDto> filteredNotificationList;
		private SwipeRefreshLayout swipeLayout;
		public bool isServiceCallCompleted, isComingBack, clearNotificationList;
		private JsonService jsonService;
		private int preItem = 0, lastNotificationId = 0, selectionPosition = 0;
		private NotificationListObjDto notificationsObj = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.NotificationsListFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public NotificationsListFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{			
			base.OnCreate (savedInstanceState);
			notificationList = null;
			notificationsListAdapter = null;
			WebServiceCallForNotificationList (0, "");
		}

		/// <summary>
		/// Clears the search.
		/// </summary>
		private void ClearSearch ()
		{
			if (searchBoxET != null && !string.IsNullOrEmpty (searchBoxET.Text)) {
				searchBoxET.Text = "";
				WebServiceCallForNotificationList (0, "");
			}
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			try {
				headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				headerText.Text = GetString (Resource.String.notifications_text);
				headerText.Visibility = ViewStates.Visible;
				activity.FindViewById<ImageView> (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;
				activity.FindViewById<ImageView> (Resource.Id.headerMenu).Visibility = ViewStates.Visible;
				((MainActivity)context).EnableNavigationDrawer ();
				if (searchBoxET != null && !string.IsNullOrEmpty (searchBoxET.Text)) {
					searchBoxET.Text = "";
				}

				if (notificationsListAdapter != null) {
					
					notificationsListLV.Adapter = notificationsListAdapter;
//					notificationsListLV.SmoothScrollToPositionFromTop(20, 10);
					//notificationsListLV.SetSelection(selectionPosition);
					//notificationsListAdapter.NotifyDataSetChanged();
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Webs the service call for notification list.
		/// </summary>
		/// <param name="notificationLastId">Notification last identifier.</param>
		/// <param name="isRefresh">Is refresh.</param>
		private void WebServiceCallForNotificationList (int notificationLastId, string isRefresh)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					jsonService = new JsonService ();
					NotificationListDto notificationListDto = new NotificationListDto ();
					notificationListDto.userid = Convert.ToInt32 (userId);
					notificationListDto.Orgid = orgId;
					notificationListDto.idafter = notificationLastId;
					string requestParameter = JsonConvert.SerializeObject (notificationListDto);
					jsonService.consumeService (CommonSharedStrings.NOTIFICATIONLIST, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Webs the service call for notification list pull to refresh.
		/// </summary>
		private void WebServiceCallForNotificationListPullToRefresh ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					/* clear list */
					clearNotificationList = true;
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					jsonService = new JsonService ();
					NotificationListDto notificationListDto = new NotificationListDto ();
					notificationListDto.userid = Convert.ToInt32 (userId);
					notificationListDto.Orgid = orgId;
					lastNotificationId = 0;
					notificationListDto.idafter = lastNotificationId;
					string requestParameter = JsonConvert.SerializeObject (notificationListDto);
					jsonService.consumeService (CommonSharedStrings.NOTIFICATIONLIST, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.NotificationsListFragment, container, false);
			notificationsListLV = view.FindViewById<ListView> (Resource.Id.notificationsListLV);
			swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
			cancelTV = view.FindViewById<TextView> (Resource.Id.cancelTV);
			searchBoxET = view.FindViewById<EditText> (Resource.Id.searchETId);
			try {
				cancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				searchBoxET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				searchBoxET.AddTextChangedListener (this);
				searchBoxET.SetOnEditorActionListener (this);
				cancelTV.SetOnClickListener (this);
				notificationsListLV.OnItemClickListener = this;
				notificationsListLV.SetOnScrollListener (this);
				swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
					Android.Resource.Color.HoloGreenLight,
					Android.Resource.Color.HoloOrangeLight,
					Android.Resource.Color.HoloRedLight);
				swipeLayout.Refresh += delegate(object sender, EventArgs e) {			
					swipeLayout.PostDelayed (WebServiceCallForNotificationListPullToRefresh, 3000);
				};
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					PreferenceConnector.WriteInteger (this.context, "notificationCount", 0);
					UpdateMyActivity (this.context);
					isComingBack = true;
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					JArray notificationListArray = null;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if (responseObj ["listNotificaiton"] != null) {
							notificationListArray = (JArray)responseObj.SelectToken ("listNotificaiton");
						}
						if (notificationListArray != null && notificationListArray.Count > 0) {
							if (clearNotificationList) {
								clearNotificationList = false;
								notificationList.Clear ();
							}
							if (notificationList == null) {
								notificationList = new List<NotificationListObjDto> ();
							}
							foreach (JObject item in notificationListArray) {
								notificationsObj = new NotificationListObjDto ();
								notificationsObj.message = (string)item.SelectToken ("message");
								notificationsObj.DateAndTime = (string)item.SelectToken ("sendDate");
								notificationsObj.moduleName = (string)item.SelectToken ("moduleName");
								notificationsObj.Id = (int)item.SelectToken ("Id");
								notificationsObj.isRead = (bool)item.SelectToken ("isRead");
								notificationsObj.blogcategoryid = (int)item.SelectToken ("blogcategoryid");
								notificationsObj.brandguid = (string)item.SelectToken ("brandguid");
								notificationsObj.brandname = (string)item.SelectToken ("brandname");
								notificationsObj.filesize = (string)item.SelectToken ("filesize");
								notificationsObj.filesurl = (string)item.SelectToken ("filesurl");
								notificationsObj.hastags = (bool)item.SelectToken ("hastags");
								notificationsObj.modifieddate = (string)item.SelectToken ("modifieddate");
								notificationsObj.moduleId = (int)item.SelectToken ("moduleId");
								notificationList.Add (notificationsObj);
							}							
							if (notificationsListAdapter == null) {
								activity.RunOnUiThread (() => {	
									notificationsListAdapter = new NotificationsListAdapter (this.context, Resource.Layout.BookmarkListItem, notificationList);
									notificationsListLV.Adapter = notificationsListAdapter;
								});
							} else {
								activity.RunOnUiThread (() => notificationsListAdapter.NotifyDataSetChanged ());
							}
						}
					} else {
						if (responseObj ["message"] != null) {
							statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// Updates my activity.
		/// </summary>
		/// <param name="context">Context.</param>
		void UpdateMyActivity (Context context)
		{
			try {
				Intent intent = new Intent ("com.retaileracademy.receiver");
				context.SendBroadcast (intent);
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Ons the response failed.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		public void AfterTextChanged (Android.Text.IEditable s)
		{
		}

		public void BeforeTextChanged (ICharSequence s, int start, int count, int after)
		{
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="before">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <summary>
		/// Raises the text changed event.
		/// </summary>
		public void OnTextChanged (ICharSequence s, int start, int before, int count)
		{
			try {
//				if (notificationList != null && notificationList.Count > 0) {
//					filteredNotificationList = notificationList.Where (p => p.message.ToLower ().Contains (searchBoxET.Text.ToLower ())).ToList ();
//					notificationsListAdapter = new NotificationsListAdapter (this.context, Resource.Layout.BookmarkListItem, filteredNotificationList);
//					notificationsListLV.Adapter = notificationsListAdapter;
//				}
				if (notificationList != null) {
					notificationsListAdapter.SearchListItem (s.ToString ());
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				switch (id) {
				case Resource.Id.cancelTV:
					View view = this.activity.CurrentFocus;
					if (view != null) {  
						InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
						imm.HideSoftInputFromWindow (view.WindowToken, 0);
					}
					ClearSearch ();
					break;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the item click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			try {
				View v = this.activity.CurrentFocus;
				if (v != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {					
					TextView itemNameTV = (TextView)view.FindViewById (Resource.Id.bookmarkTitleTV);
					NotificationListObjDto notificationDto = (NotificationListObjDto)itemNameTV.Tag;
					var moduleName = notificationDto.moduleName;
					var moduleid = notificationDto.moduleId;
					var modifieddate = notificationDto.modifieddate;
					var filesurl = notificationDto.filesurl;
					var filesize = notificationDto.filesize;
					var blogcategoryid = notificationDto.blogcategoryid; 
					var hastags = notificationDto.hastags;
					notificationDto.isRead = true;
					selectionPosition = position;
					CommonMethods.NavigateToRespectiveFragment (this.context, fragmentManager, moduleName, Convert.ToString (moduleid), Convert.ToString (blogcategoryid));
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the editor action event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="actionId">Action identifier.</param>
		/// <param name="e">E.</param>
		public bool OnEditorAction (TextView v, ImeAction actionId, KeyEvent e)
		{
			if (actionId == ImeAction.Search) {
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				return true;
			}
			return false;
		}

		/// <summary>
		/// Raises the scroll event.
		/// </summary>
		/// <param name="view">View.</param>
		/// <param name="firstVisibleItem">First visible item.</param>
		/// <param name="visibleItemCount">Visible item count.</param>
		/// <param name="totalItemCount">Total item count.</param>
		public void OnScroll (AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
		{
			try {
				if (string.IsNullOrEmpty (searchBoxET.Text)) {
					int lastItem = firstVisibleItem + visibleItemCount;
					if (lastItem == totalItemCount && totalItemCount != 0) {
						if (preItem != lastItem) { 
							//to avoid multiple calls for last item
							if (notificationList != null && notificationList.Count > 0) {
								preItem = lastItem;
								lastNotificationId = notificationList [notificationList.Count - 1].Id;
								WebServiceCallForNotificationList (lastNotificationId, "refresh");
							}
						}
					} else {
						preItem = lastItem;
					}
				}
			} catch (ArrayIndexOutOfBoundsException) {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
			} catch (InvalidCastException) {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
			} catch (Java.Lang.Exception) {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
			}
		}

		public void OnScrollStateChanged (AbsListView view, ScrollState scrollState)
		{
		}
	}
}