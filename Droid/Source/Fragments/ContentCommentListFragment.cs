﻿
using System;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Graphics;
using RetailerAcademy.Droid.StaffApp;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Timers;
using Java.Text;
using System.Collections;
using System.Collections.Generic;
using Android.Views.InputMethods;

namespace RetailerAcademy.Droid
{
	public class ContentCommentListFragment : BaseFragment, IWebServiceDelegate, ListView.IOnItemLongClickListener, ListView.IOnClickListener, AbsListView.IOnScrollListener, View.IOnFocusChangeListener
	{
		private Context context;
		private TextView headerText, userNameTV, userLikeTV, userCommentTV, dateFormatTV, submitTV;
		private EditText commentBoxET;
		private ListView commentsListLV;
		private Dialog progressDialog, alertDialog;
		private bool isLikeAreNot, isLiked, likeCheck, isCommentDelete = false;
		private string userNameText, activityDateText, lastDate = "", commentText = "";
		private int fileId, userLikesText, userCommentsText, commentItemPosition = -1;
		private List<CommentsCls> commentsList = new List<CommentsCls> ();
		private List<CommentsCls> previousCommentsList = new List<CommentsCls> ();
		private ContentCommentListAdapter contentCommentListAdapter;
		private System.Timers.Timer timer;
		private List<int> idsList = new List<int> ();
		private int lastCommentId = 0, preItem = 0;
		private bool postedCommentMsg = false, commentBoxFocus;
		private int totalCommentsCount;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ContentCommentListFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public ContentCommentListFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{			
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the Fragment is visible to the user.
		/// </summary>
		public override void OnStart ()
		{
			base.OnStart ();
			SetTimerForService ();
		}

		/// <summary>
		/// Called when the Fragment is no longer started.
		/// </summary>
		public override void OnStop ()
		{
			base.OnStop ();
			timer.Stop ();
		}

		/// <summary>
		/// Sets the timer for service.
		/// </summary>
		private void SetTimerForService ()
		{
			timer = new System.Timers.Timer (5000);
			timer.Elapsed += OnTimedEvent;
			timer.AutoReset = true;
			timer.Enabled = true;
		}

		/// <summary>
		/// Raises the timed event event.
		/// </summary>
		/// <param name="source">Source.</param>
		/// <param name="e">E.</param>
		private void OnTimedEvent (System.Object source, ElapsedEventArgs e)
		{
			try {
				if (!commentBoxFocus) {
					isLikeAreNot = false;
					postedCommentMsg = true;
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
						var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
						Comments commentsFileObj = new Comments ();
						commentsFileObj.fileid = fileId;
						commentsFileObj.userid = Convert.ToInt32 (userId);
						commentsFileObj.comment = "";
						commentsFileObj.afterdate = lastDate;
						string requestParameter = JsonConvert.SerializeObject (commentsFileObj);
						var jsonService = new JsonService ();
						jsonService.consumeService (CommonSharedStrings.COMMENT, requestParameter, this);
					}
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			headerText.Text = GetString (Resource.String.comment_text);
			headerText.SetOnClickListener (this);
			activity.FindViewById (Resource.Id.headerMenu).Visibility = ViewStates.Gone;
			activity.FindViewById (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;
			activity.FindViewById (Resource.Id.notificationCountTV).Visibility = ViewStates.Gone;
			headerText.Visibility = ViewStates.Visible;
			((MainActivity)context).DisableNavigationDrawer ();
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.ContentCommentListFragment, container, false);
			commentsListLV = view.FindViewById<ListView> (Resource.Id.commentsListLV);
			userNameTV = view.FindViewById<TextView> (Resource.Id.userNameTV);
			userLikeTV = view.FindViewById<TextView> (Resource.Id.likeTV);
			userCommentTV = view.FindViewById<TextView> (Resource.Id.commentTV);
			dateFormatTV = view.FindViewById<TextView> (Resource.Id.dateTV);
			commentBoxET = view.FindViewById<EditText> (Resource.Id.commentETId);
			submitTV = view.FindViewById<TextView> (Resource.Id.submitTV);

			try {
				userNameTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				userLikeTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				userCommentTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				dateFormatTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				commentBoxET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				submitTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				
				contentCommentListAdapter = new ContentCommentListAdapter (this.context, Resource.Layout.ActivityStreamCommentListItem, previousCommentsList);//changed
				commentsListLV.Adapter = contentCommentListAdapter;
				
				commentBoxET.OnFocusChangeListener = this;
				submitTV.SetOnClickListener (this);
				userLikeTV.SetOnClickListener (this);
				commentsListLV.OnItemLongClickListener = this;
				commentsListLV.SetOnScrollListener (this);
				SetBunldeDataToUI ();
				WebServiceCallForContentComments (commentText);
			} catch (Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Like service call detail page.
		/// </summary>
		private void LikeServiceCallDetailPage ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					isLiked = true;
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					ContentLike contentlikeObj = new ContentLike ();
					contentlikeObj.fileid = fileId;
					contentlikeObj.userid = Convert.ToInt32 (userId); 
					if (likeCheck) {
						contentlikeObj.check = false;
					} else {
						contentlikeObj.check = true;
					}
					string requestParameter = JsonConvert.SerializeObject (contentlikeObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.CONTENT_LIKE, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Sets the bunlde data to UI.
		/// </summary>
		private void SetBunldeDataToUI ()
		{
			try {
				userNameText = Arguments.GetString ("fileName");
				userLikesText = Arguments.GetInt ("likesCount");
				userCommentsText = Arguments.GetInt ("commentsCount");
				activityDateText = Arguments.GetString ("dateFormat");
				isLikeAreNot = Arguments.GetBoolean ("hasLiked");
				fileId = Arguments.GetInt ("fileId");
				userNameTV.Text = userNameText;
				CommonMethods.CommentsOrLikesCountDisplay (userLikesText, this.userLikeTV);
				CommonMethods.CommentsOrLikesCountDisplay (userCommentsText, this.userCommentTV);
				dateFormatTV.Text = activityDateText;
				if (isLikeAreNot) {
					likeCheck = true;
					userLikeTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_checked, 0, 0, 0);
				} else {
					likeCheck = false;
					userLikeTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_unchecked, 0, 0, 0);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Webservice call for content comments.
		/// </summary>
		/// <param name="cmtText">Cmt text.</param>
		private void WebServiceCallForContentComments (string cmtText)
		{
			try {
				isLikeAreNot = false;
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					Comments commentsFileObj = new Comments ();
					commentsFileObj.fileid = fileId;
					commentsFileObj.comment = cmtText;
					commentsFileObj.afterdate = lastDate;
					commentsFileObj.userid = Convert.ToInt32 (userId);
					string requestParameter = JsonConvert.SerializeObject (commentsFileObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.COMMENT, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Gets the comments on scroll service.
		/// </summary>
		private void GetCommentsOnScroll ()
		{
			try {
				isLikeAreNot = false;
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					Comments commentsFileObj = new Comments ();
					commentsFileObj.fileid = fileId;
					commentsFileObj.comment = "";
					commentsFileObj.afterdate = "";
					commentsFileObj.lastcommentid = lastCommentId;
					string requestParameter = JsonConvert.SerializeObject (commentsFileObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.COMMENT, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		#region IWebServiceDelegate implementation
		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			activity.RunOnUiThread (() => commentBoxET.ClearFocus ());
			int latestCount = 0;
			commentsList = new List<CommentsCls> ();
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (status) {
						if (responseObj ["commentcount"] != null) {
							totalCommentsCount = (int)responseObj.SelectToken ("commentcount");
						}
						if (!isCommentDelete) {
							if (isLiked) {	
								activity.RunOnUiThread (() => {
									isLiked = false;
									if (!likeCheck) {
										likeCheck = true;						
										userLikeTV.Text = string.Format ("{0}", ++userLikesText);
										userLikeTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_checked, 0, 0, 0);
									} else {
										likeCheck = false;
										userLikeTV.Text = string.Format ("{0}", --userLikesText);
										int likesCount = Convert.ToInt32 (userLikeTV.Text);
										if (likesCount <= 0) {
											userLikeTV.Text = "0";
										}
										userLikeTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_unchecked, 0, 0, 0);
									}
									if (progressDialog != null) {
										progressDialog.Dismiss ();
										progressDialog = null;
									}
								});
								return;
							}
							if (statusMessage == "No Comments available") {
								//CommonMethods.ShowAlertDialog (this.context, Convert.ToString (responseObj.SelectToken ("message")));
							} else {
								JArray CommentsArray = null;
								if (responseObj ["comments"] != null) {
									CommentsArray = (JArray)responseObj.SelectToken ("comments");
									if (CommentsArray != null && CommentsArray.Count > 0) {
										foreach (JObject item in CommentsArray) {
											CommentsCls comments = new CommentsCls ();
											comments.dateofcomment = Convert.ToString (item.SelectToken ("dateofcomment"));
											comments.userfullname = Convert.ToString (item.SelectToken ("userfullname"));
											comments.Comment = Convert.ToString (item.SelectToken ("Comment"));
											comments.userImg = Convert.ToString (item.SelectToken ("imageurl"));
											comments.userId = (int)item.SelectToken ("userid");
											comments.commentid = (int)item.SelectToken ("commentid");
											int id = (int)item.SelectToken ("commentid");
											if (!idsList.Contains (id)) {
												idsList.Add (id);
												latestCount = 1;
												if (postedCommentMsg) {
													previousCommentsList.Add (comments);
												} else {
													commentsList.Add (comments);
												}
											}
											lastDate = Convert.ToString (item.SelectToken ("dateofcomment"));
										}
										activity.RunOnUiThread (() => {
											if (previousCommentsList != null && previousCommentsList.Count > 0) {
												previousCommentsList.AddRange (commentsList);
												var concatedList = previousCommentsList.OrderBy (x => x.commentid).ToList ();
												previousCommentsList.Clear ();
												previousCommentsList.AddRange (concatedList);
											} else {
												previousCommentsList.AddRange (commentsList);
												contentCommentListAdapter.NotifyDataSetChanged ();
												commentsListLV.SetSelection (commentsListLV.Adapter.Count - 1);
												lastCommentId = previousCommentsList [0].commentid;
											}					
										});
										activity.RunOnUiThread (() => {
											CommonMethods.CommentsOrLikesCountDisplay (totalCommentsCount, this.userCommentTV);
											int size = commentsListLV.Adapter.Count;
											if (size > 0 && latestCount > 0) {
												contentCommentListAdapter.NotifyDataSetChanged ();
												var commentListSize = commentsList.Count;
												if (!postedCommentMsg) {
													commentsListLV.SetSelection (commentListSize);
												} 
												lastCommentId = previousCommentsList [0].commentid;
											}
										});	
										postedCommentMsg = false;
									}	
								}
							}
						} else {
							/* Remove selected item from list */
							activity.RunOnUiThread (() => {
								isCommentDelete = false;
								previousCommentsList.RemoveAt (commentItemPosition);//changed
								contentCommentListAdapter.setPosition = -1;
								contentCommentListAdapter.NotifyDataSetChanged ();
								CommonMethods.CommentsOrLikesCountDisplay (totalCommentsCount, this.userCommentTV);
								if (!string.IsNullOrEmpty (statusMessage)) {
									activity.RunOnUiThread (() => {
										if (alertDialog != null) {
											alertDialog.Dismiss ();
										}
										alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
									});
								}
							});
						}
					} else {
						if (statusMessage != "No Comments available") {
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		#endregion
		/// <summary>
		/// Raises the item long click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public bool OnItemLongClick (AdapterView parent, View view, int position, long id)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					var userLoginId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					int userId = Convert.ToInt32 (userLoginId);
					var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
					if (userId == contentCommentListAdapter.contentCommentListItems [position].userId || userRole == "SuperAdmin" || userRole == "Admin") {
						commentItemPosition = position;
						var archieveRemoveTV = (TextView)view.FindViewById (Resource.Id.moveToArchiveInboxTV);
						if (archieveRemoveTV != null) {
							archieveRemoveTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							archieveRemoveTV.SetOnClickListener (this);
							contentCommentListAdapter.setPosition = position;
							contentCommentListAdapter.NotifyDataSetChanged ();
						}
					}
				}
				return true;
			} catch (Exception) {		
				return false;
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				var id = v.Id;
				switch (id) {
				case Resource.Id.headerText:
					if (commentsListLV != null) {
						commentsListLV.SmoothScrollToPosition (-1);
					}
					break;
				case Resource.Id.submitTV:
					if (commentBoxET.Text.Trim ().Length != 0) {				
						commentText = commentBoxET.Text;
						WebServiceCallForContentComments (commentText);
						commentBoxET.Text = "";
						postedCommentMsg = true;
					} else {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.COMMENT_EMPTY);
					}
					break;
				case Resource.Id.likeTV:
					LikeServiceCallDetailPage ();
					break;
				case Resource.Id.moveToArchiveInboxTV:
					var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
					var userLoginId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var commentUserId = previousCommentsList [commentItemPosition].userId;//changed123
					int userId = Convert.ToInt32 (userLoginId);
					if (userId != commentUserId) {
						if (userRole != "SuperAdmin" && userRole != "Admin") {
							CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
						} else {
							DeleteSelectedItem ();
						}
					} else {
						DeleteSelectedItem ();
					}
					break;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Deletes the selected item.
		/// </summary>
		private void DeleteSelectedItem ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					isCommentDelete = true;
					string userId = null;
					var userLoginId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					if (Convert.ToInt32 (userLoginId) != previousCommentsList [commentItemPosition].userId) {
						userId = previousCommentsList [commentItemPosition].userId + "";//changed
					} else {
						userId = userLoginId;
					}
					int commentId = previousCommentsList [commentItemPosition].commentid;//changed
					CommentsDelete commentsObj = new CommentsDelete ();
					commentsObj.commentId = commentId;
					commentsObj.fileid = fileId;
					commentsObj.userid = Convert.ToInt32 (userId);
					string requestParameter = JsonConvert.SerializeObject (commentsObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.CONTENT_DELETE_COMMENT, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Raises the scroll state changed event.
		/// </summary>
		/// <param name="view">View.</param>
		/// <param name="scrollState">Scroll state.</param>
		public void OnScrollStateChanged (AbsListView view, ScrollState scrollState)
		{			
		}

		/// <summary>
		/// Raises the scroll event.
		/// </summary>
		/// <param name="view">View.</param>
		/// <param name="firstVisibleItem">First visible item.</param>
		/// <param name="visibleItemCount">Visible item count.</param>
		/// <param name="totalItemCount">Total item count.</param>
		public void OnScroll (AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
		{
			try {
				if (!commentBoxFocus) {
					int lastFirstVisibleItem = 0;
					int lastItem = firstVisibleItem + visibleItemCount;
					if (string.IsNullOrEmpty (commentBoxET.Text)) {
						if (firstVisibleItem == 0 && previousCommentsList.Count > 0) {//changed from 19 to 0
							if (preItem != lastItem) { 
								GetCommentsOnScroll ();
								preItem = lastItem;
							}
						} else {
							preItem = lastItem;
						}
						lastFirstVisibleItem = firstVisibleItem;
					}
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Raises the focus change event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="hasFocus">If set to <c>true</c> has focus.</param>
		public void OnFocusChange (View v, bool hasFocus)
		{
			commentBoxFocus = hasFocus;
		}
	}
}