﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Graphics;
using RetailerAcademy.Droid.StaffApp;
using RetailerAcademy.Droid.Source.Utilities;
using System.ComponentModel;
using Java.Lang;
using Newtonsoft.Json.Linq;
using Android.Views.InputMethods;
using Newtonsoft.Json;

namespace RetailerAcademy.Droid
{
	public class BookmarkListFragment : BaseFragment, IWebServiceDelegate, Android.Text.ITextWatcher, View.IOnClickListener, ListView.IOnItemClickListener, TextView.IOnEditorActionListener
	{
		private Context context;
		private TextView headerText, cancelTV;
		private EditText searchBoxET;
		private ListView bookmarkListLV;
		private BookmarkListAdapter bookmarkListAdapter;
		private Dialog progressDialog, alertDialog;
		private List<BookmarkListDto> contentBookmarkList = new List<BookmarkListDto> ();
		public List<BookmarkListDto> filteredContentBookmarkList;
		private SwipeRefreshLayout swipeLayout;
		private bool deleteBookmark;
		private JsonService jsonService;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.BookmarkListFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public BookmarkListFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{			
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			headerText.Text = GetString (Resource.String.bookmark_caps_text);
			headerText.Visibility = ViewStates.Visible;
			activity.FindViewById<ImageView> (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;
			activity.FindViewById (Resource.Id.notificationCountTV).Visibility = ViewStates.Gone;
			WebServiceCallForBookmarkList ();
		}

		/// <summary>
		/// Webservice call for bookmark list.
		/// </summary>
		private void WebServiceCallForBookmarkList ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.BOOKMARK_LIST + userId, null, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Bookmark delete service call.
		/// </summary>
		/// <param name="fileContentID">File content I.</param>
		public void BookmarkDeleteService (int fileContentID)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					deleteBookmark = true;
					ContentAddBookmark contentbookmark = new ContentAddBookmark ();
					contentbookmark.fileContentId = fileContentID;
					contentbookmark.userId = Convert.ToInt32 (userId); 
					contentbookmark.removeBookMark = true;
					string requestParameter = JsonConvert.SerializeObject (contentbookmark);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.BOOKMARK_ADDREMOVE, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.BookmarkListFragment, container, false);
			bookmarkListLV = view.FindViewById<ListView> (Resource.Id.bookmarkListLV);
			cancelTV = view.FindViewById<TextView> (Resource.Id.cancelTV);
			searchBoxET = view.FindViewById<EditText> (Resource.Id.searchETId);
			swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
			try {
				cancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				searchBoxET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				searchBoxET.AddTextChangedListener (this);
				searchBoxET.SetOnEditorActionListener (this);
				cancelTV.SetOnClickListener (this);
				bookmarkListLV.OnItemClickListener = this;
				swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
					Android.Resource.Color.HoloGreenLight,
					Android.Resource.Color.HoloOrangeLight,
					Android.Resource.Color.HoloRedLight);
				swipeLayout.Refresh += delegate(object sender, EventArgs e) {
					swipeLayout.PostDelayed (WebServiceCallForBookmarkList, 3000);
				};
				bookmarkListAdapter = new BookmarkListAdapter (this.context, Resource.Layout.BookmarkListItem, contentBookmarkList);
				bookmarkListLV.Adapter = bookmarkListAdapter;
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					JArray bookmarkListArray = null;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (status) {
						// Comment delete handling
						if (deleteBookmark) {
							deleteBookmark = false;
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
							return;
						}
						if (responseObj ["bookMarkList"] != null) {
							bookmarkListArray = (JArray)responseObj.SelectToken ("bookMarkList");
						}
						BookmarkListDto bookmark;
						if (bookmarkListArray != null && bookmarkListArray.Count > 0) {
							contentBookmarkList.Clear ();
							foreach (JObject item in bookmarkListArray) {
								bookmark = new BookmarkListDto ();
								bookmark.userId = (int)item.SelectToken ("userId");
								bookmark.fileContentId = (int)item.SelectToken ("fileContentId");
								bookmark.bookMarkId = (int)item.SelectToken ("bookMarkId");
								bookmark.bookmarkName = (string)item.SelectToken ("fileName");
								bookmark.bookmarkUrl = (string)item.SelectToken ("filePath");
								bookmark.hasTags = (bool)item.SelectToken ("hasTags");
								bookmark.fileSize = (string)item.SelectToken ("fileSize");
								bookmark.modifiedDate = (string)item.SelectToken ("modifiedDate");
								contentBookmarkList.Add (bookmark);
							}
							activity.RunOnUiThread (() => bookmarkListAdapter.NotifyDataSetChanged ());
						}
					} else {
						if (!string.IsNullOrEmpty (statusMessage)) {
							activity.RunOnUiThread (() => {
								if (alertDialog != null) {
									alertDialog.Dismiss ();
								}
								alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
							});
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		/// <param name="s">To be added.</param>
		/// <summary>
		/// Afters the text changed.
		/// </summary>
		public void AfterTextChanged (Android.Text.IEditable s)
		{
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <param name="after">To be added.</param>
		/// <summary>
		/// Befores the text changed.
		/// </summary>
		public void BeforeTextChanged (ICharSequence s, int start, int count, int after)
		{
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="before">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <summary>
		/// Raises the text changed event.
		/// </summary>
		public void OnTextChanged (ICharSequence s, int start, int before, int count)
		{
			try {
				if (contentBookmarkList != null && contentBookmarkList.Count > 0) {
					filteredContentBookmarkList = contentBookmarkList.Where (p => p.bookmarkName.ToLower ().Contains (searchBoxET.Text.ToLower ())).ToList ();
					bookmarkListAdapter = new BookmarkListAdapter (this.context, Resource.Layout.BookmarkListItem, filteredContentBookmarkList);
					bookmarkListLV.Adapter = bookmarkListAdapter;
				}
			} catch (System.Exception) {					
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				switch (id) {
				case Resource.Id.cancelTV:
					searchBoxET.Text = "";
					View view = this.activity.CurrentFocus;
					if (view != null) {  
						InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
						imm.HideSoftInputFromWindow (view.WindowToken, 0);
					}
					break;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the item click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			try {				
				View v = this.activity.CurrentFocus;
				if (v != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					ContentViewFragment contentViewFragment = new ContentViewFragment (this.context, "bookmarklist");
					Bundle args = new Bundle ();
					TextView bookmarkTitle = view.FindViewById<TextView> (Resource.Id.bookmarkTitleTV);
					BookmarkListDto bookmarkFileObj = (BookmarkListDto)bookmarkTitle.Tag;
					args.PutSerializable ("bookmarkFileObj", bookmarkFileObj);
					contentViewFragment.Arguments = args;
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentViewFragment, "content_view_fragment").AddToBackStack ("content_view_fragment").Commit ();
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the editor action event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="actionId">Action identifier.</param>
		/// <param name="e">E.</param>
		public bool OnEditorAction (TextView v, ImeAction actionId, KeyEvent e)
		{
			if (actionId == ImeAction.Search) {
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				return true;
			}
			return false;
		}
	}
}