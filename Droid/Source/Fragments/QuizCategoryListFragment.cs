using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json.Linq;
using RetailerAcademy.Droid.Source.ContentDTO;
using Android.Support.V4.Widget;
using System.ComponentModel;
using Java.Lang;
using Android.Views.InputMethods;
using RetailerAcademy.Droid.StaffApp;

namespace RetailerAcademy.Droid.Source.Fragments
{
	public class QuizCategoryListFragment : BaseFragment, IWebServiceDelegate, ExpandableListView.IOnGroupExpandListener, Android.Text.ITextWatcher, IQuizCategoryPosition, TextView.IOnEditorActionListener
	{
		private ExpandableListView expandableListView;
		private QuizExpandableListAdapter expandableListAdapter;
		private List<string> expandableListTitle, titleNamesList;
		private Dictionary<string, string> expandableListDetail;
		private List<QuizCategoryDto> quizCategoryResponseList;
		private List<QuizCategoryDto> filteredQuizCategoryList;
		private Context context;
		private TextView headerTextTV, cancelTV;
		private EditText searchET;
		private Dialog progressDialog, alertDialog;
		private QuizCategoryDto quizObj;
		private RelativeLayout searchRL;
		private SwipeRefreshLayout swipeLayout;
		public bool isServiceCallCompleted;
		private JsonService jsonService;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.Source.Fragments.QuizCategoryListFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public QuizCategoryListFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerTextTV = (TextView)activity.FindViewById (Resource.Id.headerText);
			try {
				headerTextTV.Text = GetString (Resource.String.quiz_questions_text);
				activity.FindViewById<ImageView> (Resource.Id.headerMenu).Visibility = ViewStates.Visible;
				headerTextTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				((MainActivity)context).EnableNavigationDrawer ();
				ClearSearch ();
				WebServiceCall ();
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Clears the search.
		/// </summary>
		private void ClearSearch ()
		{
			if (searchET != null && !string.IsNullOrEmpty (searchET.Text)) {
				searchET.Text = "";
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.QuizCategoryListFragment, container, false);
			cancelTV = view.FindViewById<TextView> (Resource.Id.cancelTV);
			searchET = view.FindViewById<EditText> (Resource.Id.searchETId);
			searchRL = (RelativeLayout)view.FindViewById (Resource.Id.searchRLId);
			expandableListView = view.FindViewById<ExpandableListView> (Resource.Id.expandableListViewId);
			swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
			try {
				cancelTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				searchET.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				searchET.AddTextChangedListener (this);
				searchET.SetOnEditorActionListener (this);
				expandableListView.SetOnGroupExpandListener (this);
				cancelTV.Click += cancelTV_Click;
				swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
					Android.Resource.Color.HoloGreenLight,
					Android.Resource.Color.HoloOrangeLight,
					Android.Resource.Color.HoloRedLight);
				swipeLayout.Refresh += delegate(object sender, EventArgs e) {	
					ClearSearch ();
					swipeLayout.PostDelayed (WebServiceCall, 3000);
				};
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Cancels the T v click.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void cancelTV_Click (object sender, EventArgs e)
		{
			ClearSearch ();
		}

		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (quizCategoryResponseList == null) {
						quizCategoryResponseList = new List<QuizCategoryDto> ();
					} else {
						quizCategoryResponseList.Clear ();
					}
					if (expandableListDetail == null) {
						expandableListDetail = new Dictionary<string, string> ();
					} else {
						expandableListDetail.Clear ();
					}
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					searchRL.Visibility = ViewStates.Visible;
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.QUIZ_CATAGORY + userId + "/" + orgId, null, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Updates my activity.
		/// </summary>
		/// <param name="context">Context.</param>
		void UpdateMyActivity (Context context)
		{
			try {
				Intent intent = new Intent ("com.retaileracademy.receiver");
				context.SendBroadcast (intent);
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					PreferenceConnector.WriteInteger (this.context, "newQuizCount", 0);
					UpdateMyActivity (this.context);
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					JArray quizCategoryListArray = null;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if (responseObj ["quizCategories"] != null) {
							quizCategoryListArray = (JArray)responseObj.SelectToken ("quizCategories");
							if (quizCategoryListArray != null && quizCategoryListArray.Count > 0) {
								foreach (JObject item in quizCategoryListArray) {
									quizObj = new QuizCategoryDto ();
									quizObj.categoryID = (int)item.SelectToken ("categoryID");
									quizObj.categoryName = (string)item.SelectToken ("categoryName");
									quizObj.categoryDescription = (string)item.SelectToken ("categoryDescription");
									quizObj.hasattempted = (bool)item.SelectToken ("hasattempted");
									if (quizObj.hasattempted) {
										quizObj.hasViewed = true;										
									} else {
										quizObj.hasViewed = (bool)item.SelectToken ("hasViewed");
									}
									quizObj.hasViewed = (bool)item.SelectToken ("hasViewed");
									quizCategoryResponseList.Add (quizObj);
									if (expandableListDetail.ContainsKey (quizObj.categoryName)) {
										expandableListDetail.Remove (quizObj.categoryName);
									}
									expandableListDetail.Add (quizObj.categoryName, quizObj.categoryDescription);
								}
								activity.RunOnUiThread (() => {
									expandableListTitle = new List<string> (expandableListDetail.Keys);
									expandableListAdapter = new QuizExpandableListAdapter (this.context, expandableListTitle, expandableListDetail, quizCategoryResponseList, this);
									expandableListView.SetAdapter (expandableListAdapter);
								});
							} else {							
								activity.RunOnUiThread (() => searchRL.Visibility = ViewStates.Gone);							
							}
						}
					} else {
						if (responseObj ["message"] != null) {
							statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
						}
						if (expandableListTitle != null) {
							expandableListTitle.Clear ();
						}
						if (expandableListDetail != null) {
							expandableListDetail.Clear ();
						}
						if (quizCategoryResponseList != null) {
							quizCategoryResponseList.Clear ();
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// Raises the group expand event.
		/// </summary>
		/// <param name="groupPosition">Group position.</param>
		public void OnGroupExpand (int groupPosition)
		{
			try {
				int len = expandableListAdapter.GroupCount;
				for (int i = 0; i < len; i++) {
					if (i != groupPosition) {
						expandableListView.CollapseGroup (i);
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="s">To be added.</param>
		/// <summary>
		/// Afters the text changed.
		/// </summary>
		public void AfterTextChanged (Android.Text.IEditable s)
		{
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <param name="after">To be added.</param>
		/// <summary>
		/// Befores the text changed.
		/// </summary>
		public void BeforeTextChanged (ICharSequence s, int start, int count, int after)
		{
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="before">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <summary>
		/// Raises the text changed event.
		/// </summary>
		public void OnTextChanged (ICharSequence s, int start, int before, int count)
		{
			try {
				if (expandableListTitle != null && quizCategoryResponseList != null) {
					titleNamesList = new List<string> ();
					for (int i = 0; i < expandableListTitle.Count; i++) {
						string title = expandableListTitle [i].ToLower ();
						string searchName = s + "".ToLower ();
						if (title.Contains (searchName)) {
							titleNamesList.Add (expandableListTitle [i]);
						}
					}
					filteredQuizCategoryList = quizCategoryResponseList.Where (p => p.categoryName.ToLower ().Contains (searchET.Text.ToLower ())).ToList ();
					expandableListAdapter = new QuizExpandableListAdapter (this.context, titleNamesList, expandableListDetail, filteredQuizCategoryList, this);
					expandableListView.SetAdapter (expandableListAdapter);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		/// <summary>
		/// Raises the editor action event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="actionId">Action identifier.</param>
		/// <param name="e">E.</param>
		public bool OnEditorAction (TextView v, ImeAction actionId, KeyEvent e)
		{
			if (actionId == ImeAction.Search) {
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				return true;
			}
			return false;
		}

		#region IQuizCategoryPosition implementation
		/// <summary>
		/// Gets the quiz category position.
		/// </summary>
		/// <param name="categoryPosition">Category position.</param>
		public void GetQuizCategoryPosition (int categoryPosition)
		{
			try {
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					Android.Views.InputMethods.InputMethodManager imm = (Android.Views.InputMethods.InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					var itmGroup = expandableListAdapter.expandableListTitle [categoryPosition];
					var catId = expandableListAdapter.quizCategoryResponseList [categoryPosition].categoryID;
					string[] strArray = { catId + "", itmGroup };
					Bundle bundle = new Bundle ();
					bundle.PutStringArray ("categoryId", strArray);
					Intent quizArrayIntent = new Intent (this.context, typeof(QuizPageActivity));
					quizArrayIntent.AddFlags (ActivityFlags.ClearTop | ActivityFlags.NewTask);
					quizArrayIntent.PutExtras (bundle);
					StartActivity (quizArrayIntent);
				}
			} catch (System.Exception) {				
			}
		}

		#endregion
	}
}