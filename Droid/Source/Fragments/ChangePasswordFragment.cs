using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Utilities;
using Android.Graphics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using RetailerAcademy.Droid.StaffApp;

namespace RetailerAcademy.Droid.Source.Fragments
{
	public class ChangePasswordFragment : BaseFragment, IWebServiceDelegate, View.IOnClickListener
	{
		private EditText currentPasswordET, newPasswordET, confirmPasswordET;
		private Button cancelButton, requestButton;
		private Dialog progressDialog, alertDialog, dialog = null;
		private Context context;
		private TextView headerText;
		private bool logoutService;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.Source.Fragments.ChangePasswordFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public ChangePasswordFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		public override void OnResume ()
		{
			base.OnResume ();
			headerText.Text = GetString (Resource.String.change_password_text);
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.ChangePasswordFragment, container, false);
			requestButton = (Button)view.FindViewById (Resource.Id.requestBTN);
			cancelButton = (Button)view.FindViewById (Resource.Id.cancelBTN);
			currentPasswordET = (EditText)view.FindViewById (Resource.Id.currentPasswordET);
			newPasswordET = (EditText)view.FindViewById (Resource.Id.newPasswordET);
			confirmPasswordET = (EditText)view.FindViewById (Resource.Id.confirmPasswordET);
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerText.Text = activity.GetString (Resource.String.change_password_text);
			requestButton.SetOnClickListener (this);
			cancelButton.SetOnClickListener (this);
			currentPasswordET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			newPasswordET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			confirmPasswordET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			requestButton.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			cancelButton.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			return view;
		}

		/// <summary>
		/// Password change request webservice.
		/// </summary>
		private void PasswordRequestWebservice ()
		{
			try {
				string currentPasswordString = this.currentPasswordET.Text;
				string newPasswordStr = this.newPasswordET.Text;
				string confirmPassword = this.confirmPasswordET.Text;
				string changedPasswordString = "";
				var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
				var currentPassword = PreferenceConnector.ReadString (this.context, "last_user_password", null);
				Regex objAlphaPattern = new Regex ("[^A-Za-z0-9]");
				bool isValidPasswordOrNot = objAlphaPattern.IsMatch (newPasswordStr);
				if (string.IsNullOrEmpty (currentPasswordString)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CURRENT_PASSWORD_EMPTY);
				} else if (currentPasswordString != currentPassword) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CURRENT_PASSWORD);
				} else if (string.IsNullOrEmpty (newPasswordStr)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.NEW_PASSWORD_EMPTY);
				} else if (newPasswordStr.Length < 8) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.NEWPASSWORD_INVALID);
				} else if (isValidPasswordOrNot) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.NEWPASSWORD_NO_SPLCHARS);
				} else if (string.IsNullOrEmpty (confirmPassword)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CONFIRM_PASSWORD_EMPTY);
				} else if (confirmPassword.Length < 8) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CONFIRMPASSWORD_INVALID);
				} else if (this.newPasswordET.Text != this.confirmPasswordET.Text) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CONFIRM_PASSWORD_TEXT);
				} else if (this.newPasswordET.Text == currentPasswordString) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.PASSWORD_EXISTS_INVALID);
				} else if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, GetString (Resource.String.loading_text));
					}
					changedPasswordString = this.confirmPasswordET.Text;
					ChangePasswordData dataObjects = new ChangePasswordData ();
					dataObjects.Currentpassword = currentPasswordString;
					dataObjects.Newpassword = changedPasswordString;
					dataObjects.UserId = userId.ToString ();
					string requestParameter = JsonConvert.SerializeObject (dataObjects);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.CHANGE_PASSWORD_URL, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
//						if (logoutService) {
//							PreferenceConnector.WriteString (this.context, "last_user_password", null);
//							PreferenceConnector.WriteBoolean (this.context, "isLogin", false); 
//							MyApplication.CurrentPassword = "";
//							MyApplication.IsLogin = false;	
//							MyApplication.OrgId = "";
//							MyApplication.OrgName = "";
//							logoutService = false;
//							activity.RunOnUiThread (() => {
//								if (alertDialog != null) {
//									alertDialog.Dismiss ();
//								}
//								if (dialog != null) {
//									dialog.Dismiss ();
//								}
//								activity.Finish ();
//								activity.StartActivity (typeof(LoginActivity));
//							});
//							activity.RunOnUiThread (() => {
//								if (progressDialog != null) {
//									progressDialog.Dismiss ();
//									progressDialog = null;
//								}
//							});
//							return;
//						}
						PreferenceConnector.WriteString (this.context, "last_user_password", this.newPasswordET.Text);
						MyApplication.CurrentPassword = this.newPasswordET.Text;
						activity.RunOnUiThread (() => {
							AlertDialog.Builder alert = new AlertDialog.Builder (context);
							alert.SetTitle ("Alert");
							alert.SetCancelable (false);
							alert.SetMessage (CommonSharedStrings.PASSWORD_CHANGED_TEXT);
							alert.SetPositiveButton ("OK", (senderAlert, args) => {
								if (dialog != null) {
									dialog.Dismiss ();
//									logoutService = true;
//									var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
//									JsonService jsonService = new JsonService ();
//									Logout logout = new Logout ();
//									logout.userid = Convert.ToInt32 (userId);
//									logout.deviceToken = PreferenceConnector.ReadString (this.context, "device_token", null);//MyApplication.DeviceToken;
//									logout.platform = "android";
//									string requestParameter = JsonConvert.SerializeObject (logout);
//									jsonService.consumeService (CommonSharedStrings.LOGOUT, requestParameter, this);
									DashboardFragment dashboardFragment = new DashboardFragment (this.context);
									fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, dashboardFragment, "dashboard_fragment").AddToBackStack ("dashboard_fragment").Commit ();
								}
							});
							dialog = alert.Create ();
							dialog.Show ();
						});
					} else {
						//display toast message
						if (responseObj ["message"] != null) {
							statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
						}
						activity.RunOnUiThread (() => {
							this.currentPasswordET.Text = "";
							this.newPasswordET.Text = "";
							this.confirmPasswordET.Text = "";
						});
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				} 
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				var id = v.Id;
				switch (id) {
				case Resource.Id.requestBTN:
					PasswordRequestWebservice ();
					break;
				case Resource.Id.cancelBTN:
					activity.OnBackPressed ();
					break;
				}
			} catch (Exception) {				
			}
		}
	}
}