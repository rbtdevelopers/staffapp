﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using RetailerAcademy.Droid.Source.ContentDTO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace RetailerAcademy.Droid
{
	public class LeaderBoardNewUserFragment : BaseFragment, IWebServiceDelegate
	{
		private Context context;
		private TextView headerText, categoryTV, pointsTV, categoryNameTV, pointsValueTV;
		private Dialog progressDialog;
		private JsonService jsonService;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.LeaderBoardNewUserFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public LeaderBoardNewUserFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			headerText.Text = GetString (Resource.String.leaderboard_text);
			WebServiceCall ();
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.LeaderBoardNewUserFragment, container, false);
			categoryTV = view.FindViewById<TextView> (Resource.Id.categoryTV);
			pointsTV = view.FindViewById<TextView> (Resource.Id.pointsTV);
			categoryNameTV = view.FindViewById<TextView> (Resource.Id.categoryNameTV);
			pointsValueTV = view.FindViewById<TextView> (Resource.Id.pointsValueTV);
			categoryNameTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			pointsValueTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			categoryTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			pointsTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			return view;
		}

		/// <summary>
		/// Webservice call for leaderboard service UI representation.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					LeaderBoardDtoWithOutJLObject lbDto = new LeaderBoardDtoWithOutJLObject ();
					lbDto.userId = Convert.ToInt32 (userId);
					lbDto.orgId = orgId;
					string parameters = JsonConvert.SerializeObject (lbDto);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.LEADERBOARD_NEWSERVICE, parameters, this);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					JArray leaderBoardResponseArray = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if (responseObj ["leaderBoardUserList"] != null) {
							leaderBoardResponseArray = (JArray)responseObj.SelectToken ("leaderBoardUserList");
						}
						if (leaderBoardResponseArray != null && leaderBoardResponseArray.Count > 0) {
							var item = leaderBoardResponseArray [0];
							var categoryName = (string)item.SelectToken ("userCategory");
							if (categoryName.Length > 23) {
								activity.RunOnUiThread (() => categoryNameTV.Text = categoryName.Substring (0, 22) + "...");
							} else {
								activity.RunOnUiThread (() => categoryNameTV.Text = categoryName);
							}
							activity.RunOnUiThread (() => pointsValueTV.Text = (string)item.SelectToken ("points"));
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}
	}
}