﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using RetailerAcademy.Droid.StaffApp;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Android.Graphics;
using Java.IO;
using System.Net;
using System.ComponentModel;
using Java.Net;
using Android.Webkit;
using Java.Text;
using System.Text.RegularExpressions;
using Android.Accounts;

namespace RetailerAcademy.Droid
{
	public class ContentViewFragment : BaseFragment, IWebServiceDelegate, View.IOnClickListener
	{
		private Context context;
		private TextView headerText, contentTitleTV, commentCountTV, fileCreatedDateTV, fileSizeTV, dialogTitleTV, dialogActionTV, dialogDescTV, dialogAdditionalTV, cancelTV, facebookTV, twitterTV, socialmediaCancelTV, tapToOpenPdfTV;
		private Android.Webkit.WebView contentViewWV, tapToOpenPdf;
		private ImageView commentCountIV, bookmarkIV, shareContentIV, likeContentIV, tagContentIV, downloadContentIV, videoImageView;
		private Dialog progressDialog, contentViewDialogs, socialMediaDialog, alertDialog;
		private LinearLayout contentActionsLayout;
		private string eventName, fileUrlForPdf = null, fileName, fileSize, fileCreatedDate, filePath, fileUrl, comingFrom, folderName;
		private int fileId, folderId, noOfLikes, noOfComments;
		public bool likeCheck, bookmarkCheck, hasTags;
		private Button deleteBTN;
		private static ProgressBar dialogProgressBar;
		private Java.IO.File finalPath;
		public bool isDownloadCompleted = false;
		private RelativeLayout videoImageViewRL;
		private Java.IO.File dir;
		private ContentFileDto contentFileObj;
		private BookmarkListDto bookmarkListObj;
		private ContentSearchTermsDto contentSearchTermsDto;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ContentViewFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="comingFrom">Coming from.</param>
		public ContentViewFragment (Context context, string comingFrom)
		{
			this.context = context;
			this.comingFrom = comingFrom;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			/* Checking filename/image is avalable or not in local storage */
			dir = new Java.IO.File (Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/RetailAcademyData/");
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.ContentViewFragment, null);
			contentTitleTV = view.FindViewById<TextView> (Resource.Id.contentTitleTV);
			commentCountTV = view.FindViewById<TextView> (Resource.Id.commentCountTV);
			fileCreatedDateTV = view.FindViewById<TextView> (Resource.Id.fileCreatedDateTV);
			fileSizeTV = view.FindViewById<TextView> (Resource.Id.fileSizeTV);
			commentCountIV = view.FindViewById<ImageView> (Resource.Id.commentCountIV);
			bookmarkIV = view.FindViewById<ImageView> (Resource.Id.bookmarkIV);
			shareContentIV = view.FindViewById<ImageView> (Resource.Id.shareContentIV);
			likeContentIV = view.FindViewById<ImageView> (Resource.Id.likeContentIV);
			tagContentIV = view.FindViewById<ImageView> (Resource.Id.tagContentIV);
			downloadContentIV = view.FindViewById<ImageView> (Resource.Id.downloadContentIV);
			contentViewWV = view.FindViewById<Android.Webkit.WebView> (Resource.Id.contentViewWV);
			tapToOpenPdfTV = view.FindViewById<TextView> (Resource.Id.tapToOpenPdfTV);
			contentActionsLayout = view.FindViewById<LinearLayout> (Resource.Id.contentActionsLayout);
			videoImageView = (ImageView)view.FindViewById (Resource.Id.videoImageView);
			videoImageViewRL = (RelativeLayout)view.FindViewById (Resource.Id.videoImageViewRL);
			deleteBTN = view.FindViewById<Button> (Resource.Id.deleteBTN);
			dialogProgressBar = view.FindViewById<ProgressBar> (Resource.Id.dialogProgressBar);
			tapToOpenPdf = view.FindViewById<Android.Webkit.WebView> (Resource.Id.tapToOpenPdf);

			try {
				contentViewWV.Settings.DefaultZoom = Android.Webkit.WebSettings.ZoomDensity.Far;
				contentViewWV.Settings.BuiltInZoomControls = true;
				contentViewWV.Settings.DisplayZoomControls = false;
				contentViewWV.Settings.UseWideViewPort = true;
				contentViewWV.Settings.LoadWithOverviewMode = true;
				contentViewWV.Settings.JavaScriptEnabled = true;
				tapToOpenPdf.Settings.JavaScriptEnabled = true;
				videoImageViewRL.SetOnClickListener (this);
				
				contentTitleTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				fileCreatedDateTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				commentCountTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				fileSizeTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				deleteBTN.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				tapToOpenPdfTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				
				bookmarkIV.SetOnClickListener (this);
				shareContentIV.SetOnClickListener (this);
				likeContentIV.SetOnClickListener (this);
				tagContentIV.SetOnClickListener (this);
				downloadContentIV.SetOnClickListener (this);
				commentCountIV.SetOnClickListener (this);
				deleteBTN.SetOnClickListener (this);
				
				GetBundleData ();
			} catch (Exception) {				
			}

			return view;
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			try {
				headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				activity.FindViewById<ImageView> (Resource.Id.headerMenu).Visibility = ViewStates.Gone;
				activity.FindViewById<ImageView> (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;
				activity.FindViewById<TextView> (Resource.Id.headerText).Visibility = ViewStates.Visible;
				((MainActivity)context).DisableNavigationDrawer ();
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Gets the bundle data and updates the UI.
		/// </summary>
		private void GetBundleData ()
		{
			try {
				if (!string.IsNullOrEmpty (comingFrom)) {
					switch (comingFrom) {
					case "bookmarklist":
						bookmarkListObj = (BookmarkListDto)Arguments.GetSerializable ("bookmarkFileObj");
						if (bookmarkListObj != null) {
							fileId = bookmarkListObj.fileContentId; 
							folderId = bookmarkListObj.bookMarkId;
							if (folderId != 0) {
								contentActionsLayout.Visibility = ViewStates.Visible;
							} else {
								contentActionsLayout.Visibility = ViewStates.Invisible;
							}
						}
						break;
					case "contentfilelist":
						contentFileObj = (ContentFileDto)Arguments.GetSerializable ("contentFileCls");
						if (contentFileObj != null) {
							fileId = contentFileObj.filesid; 
							folderId = Arguments.GetInt ("folderId");
							if (folderId != 0) {
								contentActionsLayout.Visibility = ViewStates.Visible;
							} else {
								fileName = (!string.IsNullOrEmpty (contentFileObj.filesname)) ? contentFileObj.filesname : "";
								fileSize = (!string.IsNullOrEmpty (contentFileObj.filesize)) ? contentFileObj.filesize : "0.00";
								fileCreatedDate = (!string.IsNullOrEmpty (contentFileObj.createdDate)) ? contentFileObj.createdDate : "";
								filePath = contentFileObj.filespath;
								contentActionsLayout.Visibility = ViewStates.Invisible;
								UIUpdate (fileCreatedDate, fileSize, fileName, filePath);
								return;
							}
						}
						break;
					case "contentSearchTerms":
						contentSearchTermsDto = (ContentSearchTermsDto)Arguments.GetSerializable ("contentSearchCls");
						if (contentSearchTermsDto != null) {
							fileId = contentSearchTermsDto.fileid; 
							folderId = Arguments.GetInt ("folderId");
							if (folderId != 0) {
								contentActionsLayout.Visibility = ViewStates.Visible;
							} else {
								contentActionsLayout.Visibility = ViewStates.Invisible;
							}
						}
						break;
					case "pushNotificationApp":
						contentFileObj = (ContentFileDto)Arguments.GetSerializable ("contentFileCls");
						if (contentFileObj != null) {
							fileId = contentFileObj.filesid;
							folderId = Arguments.GetInt ("folderId");
							if (folderId != 0) {
								contentActionsLayout.Visibility = ViewStates.Visible;
							} else {
								contentActionsLayout.Visibility = ViewStates.Invisible;
							}
						}
						break;
					}
					WebServiceCall ();
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// User interface update.
		/// </summary>
		/// <param name="fileCreatedDateTxt">File created date text.</param>
		/// <param name="fileSizeTxt">File size text.</param>
		/// <param name="fileNameTxt">File name text.</param>
		/// <param name="filePathTxt">File path text.</param>
		private void UIUpdate (string fileCreatedDateTxt, string fileSizeTxt, string fileNameTxt, string filePathTxt)
		{
			try {
				string contentFileName = "";
				if (!string.IsNullOrEmpty (fileNameTxt)) {
					contentFileName = Regex.Replace (fileNameTxt, @"[^0-9a-zA-Z .]+", " ");
				}
				contentTitleTV.Text = contentFileName;
				fileCreatedDateTV.Text = fileCreatedDateTxt;
				fileSizeTV.Text = fileSizeTxt;
				string imagePathTxt = fileId + fileNameTxt;
				finalPath = new Java.IO.File (dir, imagePathTxt);
				if (finalPath.Exists ()) {
					/* OFF-LINE Process */
					downloadContentIV.SetImageResource (Resource.Drawable.download_checked);
					if (imagePathTxt.Contains (".pdf")) {
						tapToOpenPdf.Visibility = ViewStates.Invisible;
						contentViewWV.Visibility = ViewStates.Invisible;
						videoImageViewRL.Visibility = ViewStates.Invisible;
						tapToOpenPdfTV.Visibility = ViewStates.Visible;
						tapToOpenPdfTV.SetOnClickListener (this);
						OpenFile (finalPath);
						return;
					} else if (imagePathTxt.Contains (".mp4") || imagePathTxt.Contains (".wmv")) {	
						videoImageViewRL.Visibility = ViewStates.Visible;
						contentViewWV.Visibility = ViewStates.Invisible;
						tapToOpenPdf.Visibility = ViewStates.Invisible;
						tapToOpenPdfTV.Visibility = ViewStates.Invisible;
						return;
					} else {	
						contentViewWV.Visibility = ViewStates.Visible;
						videoImageViewRL.Visibility = ViewStates.Invisible;
						tapToOpenPdfTV.Visibility = ViewStates.Invisible;
						tapToOpenPdf.Visibility = ViewStates.Invisible;
						fileUrl = "file://" + finalPath.ToString ();
						contentViewWV.SetWebViewClient (new MyWebViewClient ());
						contentViewWV.LoadUrl (fileUrl);
					}
				
				} else {
					/* ON-LINE Process */
					if (!string.IsNullOrEmpty (filePathTxt)) {
						if (!CommonMethods.IsInternetConnected (this.context)) {
							CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
						} else {
							if (filePathTxt.Contains (".pdf")) {	
								tapToOpenPdf.Visibility = ViewStates.Visible;
								contentViewWV.Visibility = ViewStates.Invisible;
								videoImageViewRL.Visibility = ViewStates.Invisible;
								tapToOpenPdfTV.Visibility = ViewStates.Invisible;
								fileUrlForPdf = "https://docs.google.com/gview?embedded=true&url=" + filePathTxt;
								tapToOpenPdf.SetWebViewClient (new MyWebViewClient ());
								tapToOpenPdf.LoadUrl (fileUrlForPdf);
							} else if (filePathTxt.Contains (".mp4") || filePathTxt.Contains (".wmv") || filePathTxt.Contains (".mov")) {
								videoImageViewRL.Visibility = ViewStates.Visible;
								contentViewWV.Visibility = ViewStates.Invisible;
								tapToOpenPdf.Visibility = ViewStates.Invisible;
								tapToOpenPdfTV.Visibility = ViewStates.Invisible;
							} else {	
								contentViewWV.Visibility = ViewStates.Visible;
								tapToOpenPdfTV.Visibility = ViewStates.Invisible;
								videoImageViewRL.Visibility = ViewStates.Invisible;
								tapToOpenPdf.Visibility = ViewStates.Invisible;
								contentViewWV.SetWebViewClient (new MyWebViewClient ());
								contentViewWV.LoadData ("<html><head><style type='text/css'>body{margin:auto auto;text-align:center;} img{width:100%25;} </style></head><body><img src='" + filePathTxt + "'/></body></html>", "text/html", "UTF-8");
								/* Code block for Html content to be in center aligned */
								//string imageHtmlData = "<html style='height:100%; width:100%; padding:0; margin:0;'><body style='height:100%; width:100%; padding:0; margin:0;'><table width='100%' height='100%' border='0' cellspacing='0' cellpadding='0'><tr><td style='text-align:center'><img src='" + filePathTxt + "' align='middle' /></td></tr></table></body></html>";
								//contentViewWV.LoadData(imageHtmlData, "text/html", "UTF-8");
							}
						}
					} else {
						CommonMethods.ShowAlertDialog (context, CommonSharedStrings.STATUS_ERROR);
						return;
					}
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Webservice call to display the respective content.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					ContentDesc contentObj = new ContentDesc ();
					contentObj.fileid = fileId;
					contentObj.userid = Convert.ToInt32 (userId);
					string requestParameter = JsonConvert.SerializeObject (contentObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.CONTENT_DISPLAY, requestParameter, this);
					eventName = "contentDisplay";
				}
			} catch (Exception) {				
			}
		}

		#region IWebServiceDelegate implementation
		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					string statusMessage = null;
					bool status = false;
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (eventName == "contentDisplay") {
						if (status) {
							var likebyuser = (bool)responseObj.SelectToken ("likebyuser");
							var bookmarksByUser = (bool)responseObj.SelectToken ("bookmarkbyuser");
							noOfLikes = (int)responseObj.SelectToken ("nooflikes");
							noOfComments = (int)responseObj.SelectToken ("noofcomments");
							fileName = (string)responseObj.SelectToken ("filename");
							fileSize = (string)responseObj.SelectToken ("filesize");
							fileCreatedDate = (string)responseObj.SelectToken ("modifieddate");
							filePath = (string)responseObj.SelectToken ("filesurl");
							hasTags = (bool)responseObj.SelectToken ("hastags");
							folderName = (string)responseObj.SelectToken ("foldername");
							activity.RunOnUiThread (() => {
								if (!string.IsNullOrEmpty (folderName)) {
									var folderCapsText = folderName.ToUpper ();
									headerText.Text = folderCapsText;
									PreferenceConnector.WriteString(this.context, "folderName", folderCapsText);
								}
								UIUpdate (fileCreatedDate, fileSize, fileName, filePath);
								if (hasTags) {
									tagContentIV.SetImageResource (Resource.Drawable.tag_checked);
								} else {
									tagContentIV.SetImageResource (Resource.Drawable.tag_unchecked);
								}
							});
							activity.RunOnUiThread (() => CommonMethods.CommentsOrLikesCountDisplay (noOfComments, this.commentCountTV));
							activity.RunOnUiThread (() => {
								if (contentFileObj != null && folderId != 0) {
									contentFileObj.hasread = true;
								}
								if (likebyuser) {
									likeCheck = true;
									likeContentIV.SetImageResource (Resource.Drawable.like_checked);
								} else {
									likeCheck = false;
									likeContentIV.SetImageResource (Resource.Drawable.like_unchecked);
								}
								if (bookmarksByUser) {
									bookmarkCheck = true;
									bookmarkIV.SetImageResource (Resource.Drawable.bookmark_checked);
								} else {
									bookmarkCheck = false;
									bookmarkIV.SetImageResource (Resource.Drawable.bookmark_unchecked);
								}
							});
						} else {
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
						}
					} else if (eventName == "like") {
						if (status) {
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
							activity.RunOnUiThread (() => {
								if (!likeCheck) {
									likeCheck = true;
									noOfLikes++;
									likeContentIV.SetImageResource (Resource.Drawable.like_checked);
								} else {
									likeCheck = false;
									noOfLikes--;
									likeContentIV.SetImageResource (Resource.Drawable.like_unchecked);
								}
							});
						} else {
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
						}
					} else if (eventName == "bookmark") {
						if (status) {
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
							activity.RunOnUiThread (() => {
								if (!bookmarkCheck) {
									bookmarkCheck = true;
									bookmarkIV.SetImageResource (Resource.Drawable.bookmark_checked);
								} else {
									bookmarkCheck = false;
									bookmarkIV.SetImageResource (Resource.Drawable.bookmark_unchecked);
								}
							});
						} else {
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
						}
					} 
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		#endregion
		/// <summary>
		/// Content action dialog for bookmark and share content.
		/// </summary>
		/// <param name="contentAction">Content action.</param>
		private void ContentActionDialog (string contentAction)
		{
			try {
				contentViewDialogs = new Dialog (this.context);
				contentViewDialogs.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
				contentViewDialogs.Window.RequestFeature (WindowFeatures.NoTitle);
				contentViewDialogs.SetContentView (Resource.Layout.ContentActionDialogs);
				dialogTitleTV = (TextView)contentViewDialogs.FindViewById (Resource.Id.dialogTitleTV);
				dialogActionTV = (TextView)contentViewDialogs.FindViewById (Resource.Id.dialogActionTV);
				dialogDescTV = (TextView)contentViewDialogs.FindViewById (Resource.Id.dialogDescTV);
				dialogAdditionalTV = (TextView)contentViewDialogs.FindViewById (Resource.Id.dialogAdditionalTV);
				cancelTV = (TextView)contentViewDialogs.FindViewById (Resource.Id.cancelTV);
				View addView = (View)contentViewDialogs.FindViewById (Resource.Id.dialogAddView);
				View dialogActionView = (View)contentViewDialogs.FindViewById (Resource.Id.dialogActionView);
				dialogTitleTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				dialogActionTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				dialogDescTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				cancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				dialogAdditionalTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				contentViewDialogs.Show ();
				dialogActionTV.SetOnClickListener (this);
				dialogDescTV.SetOnClickListener (this);
				cancelTV.SetOnClickListener (this);
				dialogAdditionalTV.SetOnClickListener (this);
				switch (contentAction) {
				case "bookmarkIV":
					dialogTitleTV.Text = GetString (Resource.String.bookmark_text);
					dialogDescTV.Text = GetString (Resource.String.bookmark_list_text);
					dialogAdditionalTV.Visibility = ViewStates.Gone;
					dialogActionView.Visibility = ViewStates.Visible;
					dialogActionTV.Visibility = ViewStates.Visible;
					addView.Visibility = ViewStates.Gone;
					if (bookmarkCheck) {
						dialogActionTV.Text = GetString (Resource.String.remove_bookmark_text);
					} else {
						dialogActionTV.Text = GetString (Resource.String.add_bookmark_text);
					}
					break;
				case "shareContentIV":
					dialogTitleTV.Text = GetString (Resource.String.share_text);
					dialogDescTV.Text = GetString (Resource.String.send_via_email_text);
					dialogAdditionalTV.Text = GetString (Resource.String.social_media_text);
					dialogAdditionalTV.Visibility = ViewStates.Visible;
					addView.Visibility = ViewStates.Visible;
					dialogActionTV.Visibility = ViewStates.Gone;
					break;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Like or dislike content webservice call.
		/// </summary>
		private void LikeOrDislikeContent ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					ContentLike contentlikeObj = new ContentLike ();
					contentlikeObj.fileid = fileId;
					contentlikeObj.userid = Convert.ToInt32 (userId); 
					if (likeCheck) {
						contentlikeObj.check = false;
					} else {
						contentlikeObj.check = true;
					}
					string requestParameter = JsonConvert.SerializeObject (contentlikeObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.CONTENT_LIKE, requestParameter, this);
					eventName = "like";
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Adding bookmark or remove bookmark service.
		/// </summary>
		private void BookmarkAddOrRemove ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					ContentAddBookmark contentbookmark = new ContentAddBookmark ();
					contentbookmark.fileContentId = fileId;
					contentbookmark.userId = Convert.ToInt32 (userId); 
					if (bookmarkCheck) {
						contentbookmark.removeBookMark = true;
					} else {
						contentbookmark.removeBookMark = false;
					}
					string requestParameter = JsonConvert.SerializeObject (contentbookmark);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.BOOKMARK_ADDREMOVE, requestParameter, this);
					eventName = "bookmark";
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Opens the locally stored file.
		/// </summary>
		/// <param name="url">URL.</param>
		public void OpenFile (File url)
		{
			try {
				// Create URI
				Android.Net.Uri uri = Android.Net.Uri.FromFile (url);
				Intent intent = new Intent (Intent.ActionView);
				if (url.ToString ().Contains (".doc") || url.ToString ().Contains (".docx")) {
					// Word document
					intent.SetDataAndType (uri, "application/msword");
				} else if (url.ToString ().Contains (".pdf")) {
					// PDF file
					intent.SetDataAndType (uri, "application/pdf");
				} else if (url.ToString ().Contains (".ppt") || url.ToString ().Contains (".pptx")) {
					// Powerpoint file
					intent.SetDataAndType (uri, "application/vnd.ms-powerpoint");
				} else if (url.ToString ().Contains (".xls") || url.ToString ().Contains (".xlsx")) {
					// Excel file
					intent.SetDataAndType (uri, "application/vnd.ms-excel");
				} else if (url.ToString ().Contains (".zip") || url.ToString ().Contains (".rar")) {
					// WAV audio file
					intent.SetDataAndType (uri, "application/x-wav");
				} else if (url.ToString ().Contains (".rtf")) {
					// RTF file
					intent.SetDataAndType (uri, "application/rtf");
				} else if (url.ToString ().Contains (".wav") || url.ToString ().Contains (".mp3")) {
					// WAV audio file
					intent.SetDataAndType (uri, "audio/x-wav");
				} else if (url.ToString ().Contains (".gif")) {
					// GIF file
					intent.SetDataAndType (uri, "image/gif");
				} else if (url.ToString ().Contains (".jpg") || url.ToString ().Contains (".jpeg") || url.ToString ().Contains (".png")) {
					// JPG file
					intent.SetDataAndType (uri, "image/jpeg");
				} else if (url.ToString ().Contains (".txt")) {
					// Text file
					intent.SetDataAndType (uri, "text/plain");
				} else if (url.ToString ().Contains (".3gp") || url.ToString ().Contains (".mpg") || url.ToString ().Contains (".mpeg") || url.ToString ().Contains (".mpe") || url.ToString ().Contains (".mp4") || url.ToString ().Contains (".avi")) {
					// Video files
					intent.SetDataAndType (uri, "video/*");
				} else {
					// Other files
					intent.SetDataAndType (uri, "*/*");
				}
				intent.AddFlags (ActivityFlags.NewTask);
				StartActivity (intent);
			} catch (ActivityNotFoundException) {
				CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.NOT_SUPPORTED_TEXT);
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				switch (id) {
				case Resource.Id.commentCountIV:
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						ContentCommentListFragment contentCommentListFragment = new ContentCommentListFragment (this.context);
						Bundle contentCommentArgs = new Bundle ();
						contentCommentArgs.PutInt ("fileId", fileId);
						contentCommentArgs.PutInt ("commentsCount", noOfComments);
						contentCommentArgs.PutInt ("likesCount", noOfLikes);
						contentCommentArgs.PutString ("dateFormat", fileCreatedDate);
						contentCommentArgs.PutString ("fileName", fileName);
						contentCommentArgs.PutBoolean ("hasLiked", likeCheck);
						contentCommentListFragment.Arguments = contentCommentArgs;
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentCommentListFragment, "content_comment_list_fragment").AddToBackStack ("content_comment_list_fragment").Commit ();
					}
					break;
				case Resource.Id.bookmarkIV:
					ContentActionDialog ("bookmarkIV");
					break;
				case Resource.Id.shareContentIV:
					ContentActionDialog ("shareContentIV");
					break;
				case Resource.Id.likeContentIV:
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						LikeOrDislikeContent ();
					}
					break;
				case Resource.Id.tagContentIV:
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						ContentTagListFragment contentTagListFragment = new ContentTagListFragment (this.context);
						Bundle args = new Bundle ();
						args.PutInt ("fileId", fileId);
						args.PutString ("navigating_from", "content_view_fragment");
						contentTagListFragment.Arguments = args;
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentTagListFragment, "content_tag_list_fragment").AddToBackStack ("content_tag_list_fragment").Commit ();
					}
					break;
				case Resource.Id.downloadContentIV:
					if (finalPath.Exists ()) {
						deleteBTN.Visibility = ViewStates.Visible;
					} else {
						if (!CommonMethods.IsInternetConnected (this.context)) {
							CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
						} else {
							DownloadContentFileFragment downloadContentFileFragment = new DownloadContentFileFragment (this.context);
							Bundle downloadArgs = new Bundle ();
							downloadArgs.PutString ("navigating_from", "content_view_fragment");
							downloadArgs.PutString ("fileName", fileName);
							downloadArgs.PutString ("contentFilePath", filePath);
							downloadArgs.PutInt ("fileId", fileId);
							downloadContentFileFragment.Arguments = downloadArgs;
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, downloadContentFileFragment, "download_content_file_fragment").AddToBackStack ("download_content_file_fragment").Commit ();
						}
					}
					break;
				case Resource.Id.cancelTV:
					contentViewDialogs.Dismiss ();
					break;
				case Resource.Id.dialogActionTV:
					contentViewDialogs.Dismiss ();
					if (dialogTitleTV.Text == GetString (Resource.String.bookmark_text)) {
						BookmarkAddOrRemove ();
					}
					break;
				case Resource.Id.dialogDescTV:
					contentViewDialogs.Dismiss ();
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						if (dialogTitleTV.Text == GetString (Resource.String.bookmark_text)) {
							BookmarkListFragment bookmarkListFragment = new BookmarkListFragment (this.context);
							Bundle bookmarkArgs = new Bundle ();
							bookmarkArgs.PutString ("navigating_from", "content_view_fragment");
							bookmarkListFragment.Arguments = bookmarkArgs;
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, bookmarkListFragment, "bookmark_list_fragment").AddToBackStack ("bookmark_list_fragment").Commit ();
						} else if (dialogTitleTV.Text == GetString (Resource.String.share_text)) {
							Intent email = null;
							if (CommonMethods.IsPackageInstalled ("com.google.android.gm", context)) {
								email = new Intent ();
								email.SetType ("text/plain");
								email.SetPackage ("com.google.android.gm");
								email.PutExtra (Android.Content.Intent.ExtraSubject, fileName);
								email.PutExtra (Android.Content.Intent.ExtraText, filePath);
								context.StartActivity (email);
							} else {
								email = new Intent (Android.Content.Intent.ActionSend);
								email.PutExtra (Android.Content.Intent.ExtraSubject, fileName);
								email.PutExtra (Android.Content.Intent.ExtraText, filePath);
								email.SetType ("message/rfc822");
								context.StartActivity (email);
							}
						}
					}
					break;
				case Resource.Id.dialogAdditionalTV:
					contentViewDialogs.Dismiss ();
					OpenSocialMediaDialog ();
					break;
				case Resource.Id.deleteBTN:
					if (finalPath.Exists ()) {
						finalPath.Delete ();
						deleteBTN.Visibility = ViewStates.Gone;
						downloadContentIV.SetImageResource (Resource.Drawable.download_unchecked);
					}
					break;
				case Resource.Id.tapToOpenPdfTV:
					if (finalPath.Exists ()) {
						OpenFile (finalPath);
					} else {
						if (!CommonMethods.IsInternetConnected (this.context)) {
							CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
						} else {
							tapToOpenPdfTV.Visibility = ViewStates.Invisible;
							tapToOpenPdf.Visibility = ViewStates.Visible;
							tapToOpenPdf.Settings.SetPluginState (WebSettings.PluginState.On);
							string path = "https://docs.google.com/gview?embedded=true&url=" + filePath;
							tapToOpenPdf.LoadUrl (path);
						}
					}
					break;
				case Resource.Id.facebookTV:
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						Intent socialMediaIntentFB = new Intent (activity, typeof(SocialMediaPage));
						socialMediaIntentFB.PutExtra ("fileurl", filePath);
						socialMediaIntentFB.PutExtra ("socialmedia", "facebook");
						socialMediaIntentFB.PutExtra ("title", fileName);
						socialMediaIntentFB.PutExtra ("description", filePath);
						StartActivity (socialMediaIntentFB);
					}
					socialMediaDialog.Dismiss ();
					break;
				case Resource.Id.twitterTV:
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						Intent socialMediaIntentTwitter = new Intent (activity, typeof(SocialMediaPage));
						socialMediaIntentTwitter.PutExtra ("fileurl", filePath);
						socialMediaIntentTwitter.PutExtra ("socialmedia", "twitter");
						socialMediaIntentTwitter.PutExtra ("title", fileName);
						socialMediaIntentTwitter.PutExtra ("description", filePath);
						StartActivity (socialMediaIntentTwitter);
					}
					socialMediaDialog.Dismiss ();
					break;
				case Resource.Id.socialmediaCancelTV:
					socialMediaDialog.Dismiss ();
					break;
				case Resource.Id.videoImageViewRL:
					if (!CommonMethods.IsInternetConnected (activity)) {
						CommonMethods.ShowAlertDialog (activity, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						Intent videoIntent = new Intent (activity, typeof(VideoPlayerActivity));
						videoIntent.PutExtra ("videoUrl", filePath);
						StartActivity (videoIntent);
					}
					break;
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Opens the social media dialog.
		/// </summary>
		private void OpenSocialMediaDialog ()
		{
			try {
				socialMediaDialog = new Dialog (this.context);
				socialMediaDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
				socialMediaDialog.Window.RequestFeature (WindowFeatures.NoTitle);
				socialMediaDialog.SetContentView (Resource.Layout.SocialMediaPopup);
				facebookTV = (TextView)socialMediaDialog.FindViewById (Resource.Id.facebookTV);
				twitterTV = (TextView)socialMediaDialog.FindViewById (Resource.Id.twitterTV);
				socialmediaCancelTV = (TextView)socialMediaDialog.FindViewById (Resource.Id.socialmediaCancelTV);
				facebookTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				twitterTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				socialmediaCancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				facebookTV.SetOnClickListener (this);
				twitterTV.SetOnClickListener (this);
				socialmediaCancelTV.SetOnClickListener (this);
				socialMediaDialog.Show ();
			} catch (Exception) {				
			}
		}

		private class MyWebViewClient : WebViewClient
		{
			public override bool ShouldOverrideUrlLoading (WebView view, string url)
			{
				view.LoadUrl (url);
				return true;
			}

			public override void OnPageStarted (WebView view, string url, Android.Graphics.Bitmap favicon)
			{
				ContentViewFragment.dialogProgressBar.Visibility = ViewStates.Visible;
				base.OnPageStarted (view, url, favicon);
			}

			public override void OnPageFinished (WebView view, string url)
			{
				ContentViewFragment.dialogProgressBar.Visibility = ViewStates.Gone;
				base.OnPageFinished (view, url);
			}
		}
	}
}