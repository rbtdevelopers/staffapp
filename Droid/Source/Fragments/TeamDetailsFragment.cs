using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.ContentDTO;
using RetailerAcademy.Droid.Source.Utilities;
using Android.Graphics;
using Newtonsoft.Json.Linq;
using RetailerAcademy.Droid.Source.Adapters;
using Android.Support.V4.Widget;
using System.ComponentModel;
using Java.Lang;

namespace RetailerAcademy.Droid.Source.Fragments
{
	public class TeamDetailsFragment : BaseFragment, IWebServiceDelegate, ListView.IOnClickListener
	{
		private Context context;
		private TextView headerText;
		private ListView teamDetailsLV;
		private Dialog progressDialog, alertDialog;
		private List<TeamDetailsDto> teamDetailsList;
		private SwipeRefreshLayout swipeLayout;
		public bool isServiceCallCompleted;
		private JsonService jsonService;

		public TeamDetailsFragment (Context context)
		{
			this.context = context;
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			headerText.Text = GetString (Resource.String.team_details_text);
			headerText.SetOnClickListener (this);
			WebServiceCall ();
		}

		/// <summary>
		/// Webservice call for team details list.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.TEAM_DETAILS + userId + "/" + orgId, null, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.TeamDetailsFragment, container, false);
			teamDetailsLV = view.FindViewById<ListView> (Resource.Id.teamDetailsLV);
			swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
			try {
				swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
					Android.Resource.Color.HoloGreenLight,
					Android.Resource.Color.HoloOrangeLight,
					Android.Resource.Color.HoloRedLight);
				swipeLayout.Refresh += delegate(object sender, EventArgs e) {			
					swipeLayout.PostDelayed (WebServiceCall, 3000);
				};
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					teamDetailsList = new List<TeamDetailsDto> ();
					bool status = false;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						JArray teamsArray = null;
						if (responseObj ["teams"] != null) {
							teamsArray = (JArray)responseObj.SelectToken ("teams");
							if (teamsArray != null && teamsArray.Count > 0) {
								foreach (JObject item in teamsArray) {
									TeamDetailsDto teamDetailsObj = new TeamDetailsDto ();
									teamDetailsObj.teamCode = (string)item.SelectToken ("teamCode");
									teamDetailsObj.teamName = (string)item.SelectToken ("teamName");
									teamDetailsObj.description = (string)item.SelectToken ("description");
									teamDetailsList.Add (teamDetailsObj);
								}
								activity.RunOnUiThread (() => {
									TeamDetailsListAdapter detailsListAdapter = new TeamDetailsListAdapter (this.context, Resource.Layout.DetailsListViewItem, teamDetailsList);
									teamDetailsLV.Adapter = detailsListAdapter;
								});
							}
						}
					} else {
						activity.RunOnUiThread (() => teamDetailsLV.Adapter = new TeamDetailsListAdapter (this.context, Resource.Layout.DetailsListViewItem, this.teamDetailsList));
						if (responseObj ["message"] != null) {
							statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
							if (!string.IsNullOrEmpty (statusMessage)) {
								if (alertDialog != null) {
									alertDialog.Dismiss ();
								}
								alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
							}
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						isServiceCallCompleted = false;
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});	
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				var id = v.Id;
				switch (id) {
				case Resource.Id.headerText:
					if (teamDetailsLV != null) {
						teamDetailsLV.SmoothScrollToPosition (0);
					}
					break;
				}
			} catch (System.Exception) {				
			}
		}
	}
}