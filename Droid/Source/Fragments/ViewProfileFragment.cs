using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Newtonsoft.Json.Linq;
using RetailerAcademy.Droid.Source.Utilities;
using Com.Nostra13.Universalimageloader.Core;

namespace RetailerAcademy.Droid.Source.Fragments
{

	public class ViewProfileFragment : BaseFragment, IWebServiceDelegate
	{
		private Context context;
		private TextView nameIdTV, emailIdTV, phoneIdTV, landlineIdTV, headerText;
		private Button editProfileBTN;
		private Dialog progressDialog, alertDialog;
		private CircularView avatarImageIV;
		private ImageLoader imageLoader;
		private DisplayImageOptions options;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.Source.Fragments.ViewProfileFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public ViewProfileFragment (Context context)
		{
			this.context = context;
			imageLoader = ImageLoader.Instance;
			options = CommonMethods.ReturnDisplayOptions (Resource.Drawable.user_profile);
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText.Text = GetString (Resource.String.profile_text);   
			WebServiceCall ();
		}

		/// <summary>
		/// Webservice call for the user details to retrieve from the server.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var jsonService = new JsonService ();
					string url = CommonSharedStrings.USER_DETRAILS + userId;
					jsonService.consumeService (url, null, this);
				}
			} catch (Exception) {				
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.ViewProfileFragment, container, false);
			nameIdTV = (TextView)view.FindViewById (Resource.Id.nameIdTV);
			emailIdTV = (TextView)view.FindViewById (Resource.Id.emailIdTV);
			phoneIdTV = (TextView)view.FindViewById (Resource.Id.phoneIdTV);
			landlineIdTV = (TextView)view.FindViewById (Resource.Id.landlineIdTV);
			editProfileBTN = (Button)view.FindViewById (Resource.Id.editProfileBTN);
			avatarImageIV = (CircularView)view.FindViewById (Resource.Id.avatarImageIV);
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			try {
				headerText.Text = activity.GetString (Resource.String.profile_text);
				editProfileBTN.Click += editProfileBTN_Click;
				nameIdTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				emailIdTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				phoneIdTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				landlineIdTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				editProfileBTN.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			} catch (Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Editprofile button click to navigate to the respective page.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void editProfileBTN_Click (object sender, EventArgs e)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					EditProfileFragment editProfileFragment = new EditProfileFragment (this.context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, editProfileFragment, "edit_profile_fragment").AddToBackStack ("edit_profile_fragment").Commit ();
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					string statusMessage = null;
					bool status = false;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						string name, email, phone, landline, avatarImageUrl, firstName, lastName;
						name = Convert.ToString (responseObj.SelectToken ("userfullname"));
						email = Convert.ToString (responseObj.SelectToken ("emailid"));
						phone = Convert.ToString (responseObj.SelectToken ("mobilenumber"));
						landline = Convert.ToString (responseObj.SelectToken ("landnumber"));
						avatarImageUrl = Convert.ToString (responseObj.SelectToken ("avatarurl"));
						firstName = Convert.ToString (responseObj.SelectToken ("firstname"));
						lastName = Convert.ToString (responseObj.SelectToken ("lastname"));
						activity.RunOnUiThread (() => {
							this.nameIdTV.Text = (!string.IsNullOrEmpty (name)) ? name : GetString (Resource.String.profile_name_text);
							this.emailIdTV.Text = (!string.IsNullOrEmpty (email)) ? email : GetString (Resource.String.profile_email_text);
							this.phoneIdTV.Text = (!string.IsNullOrEmpty (phone)) ? phone : GetString (Resource.String.profile_phone_text);
							this.landlineIdTV.Text = (!string.IsNullOrEmpty (landline)) ? landline : GetString (Resource.String.profile_landline_text);
							if (!string.IsNullOrEmpty (avatarImageUrl)) { 
								imageLoader.DisplayImage (avatarImageUrl, avatarImageIV, options);
							} else {
								avatarImageIV.SetImageResource (Resource.Drawable.user_profile);
							}
						});
						PreferenceConnector.WriteString (this.context, "edit_profile_first_name", firstName);
						PreferenceConnector.WriteString (this.context, "edit_profile_last_name", lastName);
						PreferenceConnector.WriteString (this.context, "edit_profile_mobile_phone", phone);
						PreferenceConnector.WriteString (this.context, "edit_profile_land_phone", landline);
						PreferenceConnector.WriteString (this.context, "edit_profile_profile_pic", avatarImageUrl);
					} else {
						if (responseObj ["message"] != null) {
							statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}	
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}	
				CommonMethods.ShowAlertDialog (this.context, response);
			});	
		}
	}
}