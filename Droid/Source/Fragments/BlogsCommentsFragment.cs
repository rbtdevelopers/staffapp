﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Android.Graphics;
using Android.Views.InputMethods;
using RetailerAcademy.Droid.StaffApp;
using System.Timers;
using System.ComponentModel;
using Java.Lang;
using Java.Text;

namespace RetailerAcademy.Droid
{
	public class BlogsCommentsFragment : BaseFragment, IWebServiceDelegate, ListView.IOnItemLongClickListener,  ListView.IOnClickListener, AbsListView.IOnScrollListener, View.IOnFocusChangeListener
	{
		private Context context;
		private TextView dateTV, titleNameTV, likeCountTV, commentCountTV, submitTV, headerText;
		private EditText commentET;
		private ListView commentsListLV;
		private Dialog progressDialog, alertDialog;
		private List<CommentsCls> previousCommentsList = new List<CommentsCls> ();
		private List<CommentsCls> commentsArr;
		private bool likeCheck, isCommentDeleted = false, isLikeService = false;
		private int commentItemPosition, likesCount = 0, blogId;
		private string blogpost = "", lastDate = "", commentText = "";
		private CommentsCls comments;
		private BlogCommentsAdapter blogCommentAdapter;
		private System.Timers.Timer timer;
		private BlogListObjectsDTO blogsListDto;
		private List<int> idsList = new List<int> ();
		private int lastCommentId = 0, preItem = 0;
		private bool postedCommentMsg = false, commentBoxFocus;
		private int totalCommentsCount;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.BlogsCommentsFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="blogsListDto">Blogs list dto.</param>
		public BlogsCommentsFragment (Context context, BlogListObjectsDTO blogsListDto)
		{
			this.context = context;
			this.blogsListDto = blogsListDto;
		}

		/// <summary>
		/// Called when the Fragment is visible to the user.
		/// </summary>
		public override void OnStart ()
		{
			base.OnStart ();
			SetTimerForService ();
		}

		/// <summary>
		/// Called when the Fragment is no longer started.
		/// </summary>
		public override void OnStop ()
		{
			base.OnStop ();
			timer.Stop ();
		}

		/// <summary>
		/// Sets the timer for service.
		/// </summary>
		private void SetTimerForService ()
		{
			timer = new System.Timers.Timer (5000);
			timer.Elapsed += OnTimedEvent;
			timer.AutoReset = true;
			timer.Enabled = true;
		}

		/// <summary>
		/// Raises the timed event event.
		/// </summary>
		/// <param name="source">Source.</param>
		/// <param name="e">E.</param>
		private void OnTimedEvent (System.Object source, ElapsedEventArgs e)
		{
			if (!commentBoxFocus) {
				try {
					postedCommentMsg = true;
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
						BlogComments commentsObj = new BlogComments ();
						commentsObj.userid = Convert.ToInt32 (userId);
						commentsObj.blogpost = "";
						commentsObj.blogid = blogId;
						commentsObj.afterdate = lastDate;
						string requestParameter = JsonConvert.SerializeObject (commentsObj);
						var jsonService = new JsonService ();
						jsonService.consumeService (CommonSharedStrings.BLOG_COMMENTS, requestParameter, this);
					}
				} catch (System.Exception) {					
				}
			}
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			headerText.Text = GetString (Resource.String.comment_text);
			headerText.SetOnClickListener (this);
			activity.FindViewById (Resource.Id.headerMenu).Visibility = ViewStates.Gone;
			activity.FindViewById (Resource.Id.notificationCountTV).Visibility = ViewStates.Gone;
			activity.FindViewById (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;	
			headerText.Visibility = ViewStates.Visible;
			((MainActivity)context).DisableNavigationDrawer ();
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.BlogsCommentsFragment, null);
			titleNameTV = view.FindViewById<TextView> (Resource.Id.titleTVId);
			likeCountTV = view.FindViewById<TextView> (Resource.Id.likeCountTV);
			commentCountTV = view.FindViewById<TextView> (Resource.Id.commentCountTV);
			dateTV = view.FindViewById<TextView> (Resource.Id.dateTV);
			submitTV = view.FindViewById<TextView> (Resource.Id.submitTVId);
			commentET = view.FindViewById<EditText> (Resource.Id.commentETId);
			commentsListLV = view.FindViewById<ListView> (Resource.Id.commentsLVId);
			titleNameTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			likeCountTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			commentCountTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			dateTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			submitTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			commentET.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			commentET.OnFocusChangeListener = this;
			try {
				blogCommentAdapter = new BlogCommentsAdapter (this.context, Resource.Layout.BlogsCommentsListItem, previousCommentsList);//changed
				commentsListLV.Adapter = blogCommentAdapter;				
				blogId = blogsListDto.blogid;
				titleNameTV.Text = blogsListDto.blogtitle;
				likesCount = blogsListDto.likecount;
				var blogsListDtoLikeCount = blogsListDto.likecount;
				var blogsListDtoCommentCount = blogsListDto.commnetcount;
				CommonMethods.CommentsOrLikesCountDisplay (blogsListDtoLikeCount, this.likeCountTV);
				CommonMethods.CommentsOrLikesCountDisplay (blogsListDtoCommentCount, this.commentCountTV);
				titleNameTV.SetOnClickListener (this);				
				if (blogsListDto.hasLiked) {
					likeCountTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_checked, 0, 0, 0);
					likeCheck = true;
				} else {
					likeCountTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_unchecked, 0, 0, 0);
					likeCheck = false;
				}				
				dateTV.Text = blogsListDto.date;
				likeCountTV.SetOnClickListener (this);
				submitTV.SetOnClickListener (this);
				commentsListLV.OnItemLongClickListener = this;
				commentsListLV.SetOnScrollListener (this);
				CommentsListService (blogpost);
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Likes the service.
		/// </summary>
		/// <param name="context">Context.</param>
		private void LikeService (Context context)
		{
			try {
				isLikeService = true;
				if (!CommonMethods.IsInternetConnected (context)) {
					CommonMethods.ShowAlertDialog (context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					BlogLike blogLikeObj = new BlogLike ();
					blogLikeObj.blogid = blogId;
					blogLikeObj.userid = Convert.ToInt32 (userId);
					string requestParameter = JsonConvert.SerializeObject (blogLikeObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.BLOG_LIKE, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Commentses the list service.
		/// </summary>
		/// <param name="blogpost">Blogpost.</param>
		private void CommentsListService (string blogpost)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					BlogComments commentsObj = new BlogComments ();
					commentsObj.userid = Convert.ToInt32 (userId);
					commentsObj.blogpost = blogpost;
					commentsObj.blogid = blogId;
					commentsObj.afterdate = lastDate;
					string requestParameter = JsonConvert.SerializeObject (commentsObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.BLOG_COMMENTS, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Gets the comments on scroll.
		/// </summary>
		private void GetCommentsOnScroll ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					BlogComments commentsObj = new BlogComments ();
					commentsObj.lastcommentid = lastCommentId;
					commentsObj.blogpost = "";
					commentsObj.blogid = blogId;
					commentsObj.afterdate = "";
					string requestParameter = JsonConvert.SerializeObject (commentsObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.BLOG_COMMENTS, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		#region IWebServiceDelegate implementation
		/// <summary>
		/// Ons the response.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			int latestCount = 0;
			activity.RunOnUiThread (() => commentET.ClearFocus ());
			commentsArr = new List<CommentsCls> ();
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					JArray blogCommentsListArray = null;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (status) {
						if (responseObj ["commentcount"] != null) {
							totalCommentsCount = (int)responseObj.SelectToken ("commentcount");
						}
						if (!isCommentDeleted) {
							if (isLikeService) {
								isLikeService = false;
								if (!likeCheck) {
									likeCheck = true;
									activity.RunOnUiThread (() => {
										CommonMethods.CommentsOrLikesCountDisplay (likesCount + 1, this.likeCountTV);
										likesCount = likesCount + 1;
										likeCountTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_checked, 0, 0, 0);
									});
								} else {
									likeCheck = false;
									activity.RunOnUiThread (() => {
										CommonMethods.CommentsOrLikesCountDisplay (likesCount - 1, this.likeCountTV);
										likesCount = likesCount - 1;
										likeCountTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_unchecked, 0, 0, 0);
									});
								}
								activity.RunOnUiThread (() => {
									if (progressDialog != null) {
										progressDialog.Dismiss ();
										progressDialog = null;
									}
								});
								return;
							} 
							if (statusMessage.Equals ("No Comments available")) {
								//CommonMethods.ShowAlertDialog (this.context, Convert.ToString (responseObj.SelectToken ("message")));
							} else {
								/* For displaying list of comments */
								if (responseObj ["blogcomments"] != null) {
									blogCommentsListArray = (JArray)responseObj.SelectToken ("blogcomments");
									if (blogCommentsListArray != null && blogCommentsListArray.Count > 0) {
										foreach (JObject item in blogCommentsListArray) {
											comments = new CommentsCls ();
											comments.dateofcomment = Convert.ToString (item.SelectToken ("commenteddate"));
											comments.userfullname = Convert.ToString (item.SelectToken ("username"));
											comments.Comment = Convert.ToString (item.SelectToken ("comment"));
											comments.userImg = Convert.ToString (item.SelectToken ("profileimage"));
											comments.userId = (int)item.SelectToken ("userid");
											comments.commentid = (int)item.SelectToken ("commentid");
											int id = (int)item.SelectToken ("commentid");
											if (!idsList.Contains (id)) {
												idsList.Add (id);
												latestCount = 1;
												if (postedCommentMsg) {
													previousCommentsList.Add (comments);
												} else {
													commentsArr.Add (comments);
												}
											}
											lastDate = Convert.ToString (item.SelectToken ("commenteddate"));
										}
										activity.RunOnUiThread (() => {
											if (previousCommentsList != null && previousCommentsList.Count > 0) {
												previousCommentsList.AddRange (commentsArr);
												var concatedList = previousCommentsList.OrderBy (x => x.commentid).ToList ();
												previousCommentsList.Clear ();
												previousCommentsList.AddRange (concatedList);
											} else {
												previousCommentsList.AddRange (commentsArr);
												blogCommentAdapter.NotifyDataSetChanged ();
												commentsListLV.SetSelection (commentsListLV.Adapter.Count - 1);
												lastCommentId = previousCommentsList [0].commentid;
											}	
										});
										activity.RunOnUiThread (() => {
											CommonMethods.CommentsOrLikesCountDisplay (totalCommentsCount, this.commentCountTV);	
											int size = commentsListLV.Adapter.Count;
											if (size > 0 && latestCount > 0) {
												blogCommentAdapter.NotifyDataSetChanged ();
												var commentListSize = commentsArr.Count;
												if (!postedCommentMsg) {
													commentsListLV.SetSelection (commentListSize);
												}
												lastCommentId = previousCommentsList [0].commentid;
											}
										});
										postedCommentMsg = false;
									}
								}
							}
						} else {
							/* Remove selected item from list */
							activity.RunOnUiThread (() => {
								isCommentDeleted = false;
								previousCommentsList.RemoveAt (commentItemPosition);
								blogCommentAdapter.setPosition = -1;
								blogCommentAdapter.NotifyDataSetChanged ();
								CommonMethods.CommentsOrLikesCountDisplay (totalCommentsCount, this.commentCountTV);
								if (!string.IsNullOrEmpty (statusMessage)) {
									activity.RunOnUiThread (() => {
										if (alertDialog != null) {
											alertDialog.Dismiss ();
										}
										alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
									});
								}
							});
						}
					} else {
						if (!string.IsNullOrEmpty (statusMessage)) {
							activity.RunOnUiThread (() => {
								if (alertDialog != null) {
									alertDialog.Dismiss ();
								}
								alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
							});
						}
					}			
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// Ons the response failed.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		#endregion
		/// <summary>
		/// Raises the item long click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public bool OnItemLongClick (AdapterView parent, View view, int position, long id)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					var userLoginID = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					int userId = Convert.ToInt32 (userLoginID);
					var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
					if (userId == blogCommentAdapter.blogsCommentListItems [position].userId || userRole == "SuperAdmin" || userRole == "Admin") {
						commentItemPosition = position;
						var archieveRemoveTV = (TextView)view.FindViewById (Resource.Id.moveToArchiveInboxTVId);
						if (archieveRemoveTV != null) {
							archieveRemoveTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							if (archieveRemoveTV != null) {
								archieveRemoveTV.SetOnClickListener (this);
								blogCommentAdapter.setPosition = position;
								blogCommentAdapter.NotifyDataSetChanged ();
							} 
						}
					}
				}
				return true;
			} catch (System.Exception) {
				return false;
			}
		}

		/// <summary>
		/// Deletes the comment service.
		/// </summary>
		private void DeleteCommentService ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					isCommentDeleted = true;
					int commentId = previousCommentsList [commentItemPosition].commentid;
					string userId = null;
					var userLoginID = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					if (Convert.ToInt32 (userLoginID) != previousCommentsList [commentItemPosition].userId) {
						userId = previousCommentsList [commentItemPosition].userId + "";
					} else {
						userId = userLoginID;
					}
					CommentsDeleteBlogs commentsObj = new CommentsDeleteBlogs ();
					commentsObj.commentId = commentId;
					commentsObj.userid = Convert.ToInt32 (userId);
					commentsObj.blogid = blogId;
					string requestParameter = JsonConvert.SerializeObject (commentsObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.BLOG_DELETE_COMMENTS, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				var id = v.Id;
				switch (id) {
				case Resource.Id.headerText:
					if (commentsListLV != null) {
						commentsListLV.SmoothScrollToPosition (0);
					}
					break;
				case Resource.Id.likeCountTV:
					LikeService (context);
					break;
				case Resource.Id.submitTVId:
					if (commentET.Text.Trim ().Length != 0) {
						commentText = commentET.Text;
						CommentsListService (commentText);
						commentET.Text = "";
						postedCommentMsg = true;
					} else {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.COMMENT_EMPTY);
					}
					break;
				case Resource.Id.moveToArchiveInboxTVId:
					var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
					var commentUserId = previousCommentsList [commentItemPosition].userId;
					var userLoginID = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					int userId = Convert.ToInt32 (userLoginID);
					if (userId != commentUserId) {
						if (userRole != "SuperAdmin" && userRole != "Admin") {
							CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
						} else {
							DeleteCommentService ();
						}
					} else {
						DeleteCommentService ();
					}
					break;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the scroll state changed event.
		/// </summary>
		/// <param name="view">View.</param>
		/// <param name="scrollState">Scroll state.</param>
		public void OnScrollStateChanged (AbsListView view, ScrollState scrollState)
		{

		}

		/// <summary>
		/// Raises the scroll event.
		/// </summary>
		/// <param name="view">View.</param>
		/// <param name="firstVisibleItem">First visible item.</param>
		/// <param name="visibleItemCount">Visible item count.</param>
		/// <param name="totalItemCount">Total item count.</param>
		public void OnScroll (AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
		{
			try {
				if (!commentBoxFocus) {
					int lastFirstVisibleItem = 0;
					int lastItem = firstVisibleItem + visibleItemCount;
					if (string.IsNullOrEmpty (commentET.Text)) {
						if (firstVisibleItem == 0 && previousCommentsList.Count > 0) { //changed from 19 to 0
							if (preItem != lastItem) { 
								GetCommentsOnScroll ();
								preItem = lastItem;
							}
						} else {
							preItem = lastItem;
						}
						lastFirstVisibleItem = firstVisibleItem;
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the focus change event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="hasFocus">If set to <c>true</c> has focus.</param>
		public void OnFocusChange (View v, bool hasFocus)
		{
			commentBoxFocus = hasFocus;
		}
	}
}