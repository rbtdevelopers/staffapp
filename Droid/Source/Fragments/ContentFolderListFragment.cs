﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Support.V4.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using System.ComponentModel;
using Newtonsoft.Json.Linq;
using Java.Lang;
using Android.Views.InputMethods;
using Android.Preferences;
using Newtonsoft.Json;

namespace RetailerAcademy.Droid
{
	public class ContentFolderListFragment : BaseFragment, IWebServiceDelegate, Android.Text.ITextWatcher, View.IOnClickListener, ListView.IOnItemClickListener, TextView.IOnEditorActionListener
	{
		private Context context;
		private TextView headerText, cancelTV, okTV, dialogCancelTV;
		private EditText searchBoxET;
		private ListView contentFolderListLV;
		private ContentFolderListAdapter contentFolderListAdapter;
		private Dialog progressDialog, passcodeDialog, alertDialog;
		private List<ContentFolderCls> contentFolderList;
		public List<ContentFolderCls> filteredContentFolderList;
		private List<ContentSearchTermsDto> searchItemsList;
		public bool isReadAreNot, isFromTextChange;
		private SwipeRefreshLayout swipeLayout;
		public bool isServiceCallCompleted;
		private JsonService jsonService;
		private string fromQueryString;
		private SearchListItemsAdapter searchItemsListAdapter = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ContentFolderListFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public ContentFolderListFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{			
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			try {
				headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				headerText.Text = GetString (Resource.String.content_navigator_text);
				headerText.SetOnClickListener (this);
				if (string.IsNullOrEmpty (searchBoxET.Text)) {
					WebServiceCall ();
				} else {
					TextChangeService (searchBoxET.Text);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Clears the search.
		/// </summary>
		private void ClearSearch ()
		{
			if (searchBoxET != null && !string.IsNullOrEmpty (searchBoxET.Text)) {
				searchBoxET.Text = "";
				WebServiceCall ();
			}
		}

		/// <summary>
		/// Webservice call for content folders.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					fromQueryString = "";
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.CONTENT_FOLDERS + userId + "/" + orgId, null, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.ContentFolderListFragment, container, false);
			contentFolderListLV = view.FindViewById<ListView> (Resource.Id.contentFolderListLV);
			cancelTV = view.FindViewById<TextView> (Resource.Id.cancelTV);
			searchBoxET = view.FindViewById<EditText> (Resource.Id.searchETId);
			swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
			try {
				cancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				searchBoxET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				searchBoxET.AddTextChangedListener (this);
				searchBoxET.SetOnEditorActionListener (this);
				cancelTV.SetOnClickListener (this);
				contentFolderListLV.OnItemClickListener = this;
				swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
					Android.Resource.Color.HoloGreenLight,
					Android.Resource.Color.HoloOrangeLight,
					Android.Resource.Color.HoloRedLight);
				swipeLayout.Refresh += delegate(object sender, EventArgs e) {
					ClearSearch ();
					swipeLayout.PostDelayed (WebServiceCall, 3000);
				};
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					if (fromQueryString == "text_change") {
						string statusMessageTextChange = null;
						bool statusTextChange = false;
						if (responseObj ["status"] != null) {
							statusTextChange = Convert.ToBoolean (responseObj.SelectToken ("status"));
						}
						if (responseObj ["message"] != null) {
							statusMessageTextChange = Convert.ToString (responseObj.SelectToken ("message"));
						}
						if (statusTextChange) {
							JArray contentFolderSearch = null;
							if (responseObj ["files"] != null) {
								contentFolderSearch = (JArray)responseObj.SelectToken ("files");
								if (contentFolderSearch != null && contentFolderSearch.Count > 0) {
									searchItemsList = new List<ContentSearchTermsDto> ();
									searchItemsList.Clear ();
									foreach (JObject item in contentFolderSearch) {
										ContentSearchTermsDto contentSearchTerms = new ContentSearchTermsDto ();
										contentSearchTerms.fileid = (int)item.SelectToken ("fileid");
										contentSearchTerms.filesize = (string)item.SelectToken ("filesize");
										contentSearchTerms.filesname = (string)item.SelectToken ("filesname");
										contentSearchTerms.filesurl = (string)item.SelectToken ("filesurl");
										contentSearchTerms.description = (string)item.SelectToken ("description");
										contentSearchTerms.hasread = (bool)item.SelectToken ("hasread");
										contentSearchTerms.hastags = (bool)item.SelectToken ("hastags");
										contentSearchTerms.modifieddate = (string)item.SelectToken ("modifieddate");
										contentSearchTerms.passCode = (string)item.SelectToken ("passCode");
										contentSearchTerms.type = (string)item.SelectToken ("type");
										searchItemsList.Add (contentSearchTerms);
									}
									activity.RunOnUiThread (() => {
										searchItemsListAdapter = new SearchListItemsAdapter (this.context, Resource.Layout.SearchListItemContent, searchItemsList);
										contentFolderListLV.Adapter = searchItemsListAdapter;
									});
								}
								activity.RunOnUiThread (() => {
									if (progressDialog != null) {
										progressDialog.Dismiss ();
										progressDialog = null;
									}
									if (swipeLayout != null) {
										swipeLayout.Refreshing = false;
									}
								});
								isFromTextChange = true;
								return;
							}
						} else {
							if (searchItemsList != null && searchItemsList.Count > 0) {
								searchItemsList.Clear ();
								searchItemsListAdapter = new SearchListItemsAdapter (this.context, Resource.Layout.SearchListItemContent, searchItemsList);
								contentFolderListLV.Adapter = searchItemsListAdapter;
							} else {
								if (!string.IsNullOrEmpty (statusMessageTextChange)) {
									activity.RunOnUiThread (() => {
										if (alertDialog != null) {
											alertDialog.Dismiss ();
										}
										alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessageTextChange);
										if (contentFolderList != null && contentFolderList.Count > 0) {
											contentFolderList.Clear ();
										}
									});
								}
							}
							activity.RunOnUiThread (() => {
								if (progressDialog != null) {
									progressDialog.Dismiss ();
									progressDialog = null;
								}
								if (swipeLayout != null) {
									swipeLayout.Refreshing = false;
								}
							});
							isFromTextChange = true;
							return;
						}
					} 
					contentFolderList = new List<ContentFolderCls> ();
					ContentFolderCls contentFolderCls = null;
					var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
					if (userRole == "SuperAdmin" || userRole == "Admin") {
						contentFolderCls = new ContentFolderCls ();
						contentFolderCls.foldername = "My Content";
						contentFolderCls.folderDesc = "Uploaded content";
						contentFolderCls.folderid = 0;
						contentFolderCls.passCode = "";
						contentFolderCls.isFolderRead = true;
						contentFolderList.Add (contentFolderCls);
					}
					bool status = false;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (status) {	
						isFromTextChange = false;
						JArray contentFolderListArray = null;
						if (responseObj ["folder"] != null) {
							contentFolderListArray = (JArray)responseObj.SelectToken ("folder");
							if (contentFolderListArray != null && contentFolderListArray.Count > 0) {
								foreach (JObject item in contentFolderListArray) {
									contentFolderCls = new ContentFolderCls ();
									contentFolderCls.foldername = (string)item.SelectToken ("folderName");
									contentFolderCls.folderDesc = (string)item.SelectToken ("folderDescription");
									contentFolderCls.folderid = (int)item.SelectToken ("folderID");
									contentFolderCls.passCode = (string)item.SelectToken ("passCode");
									contentFolderCls.isFolderRead = (bool)item.SelectToken ("isFolderRead");
									contentFolderList.Add (contentFolderCls);
								}
								activity.RunOnUiThread (() => {
									contentFolderListAdapter = new ContentFolderListAdapter (this.context, Resource.Layout.ContentFolderListItem, contentFolderList);
									contentFolderListLV.Adapter = contentFolderListAdapter;
								});
							}
						}
					} else {
						isFromTextChange = false;
						//display toast message
						if (!string.IsNullOrEmpty (statusMessage)) {
							activity.RunOnUiThread (() => {
								if (alertDialog != null) {
									alertDialog.Dismiss ();
								}
								alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
							});
						}
						activity.RunOnUiThread (() => {
							if (contentFolderList != null && contentFolderList.Count > 0) {
								contentFolderList.Clear ();
							}
							contentFolderListAdapter = new ContentFolderListAdapter (this.context, Resource.Layout.ContentFolderListItem, contentFolderList);
							contentFolderListLV.Adapter = contentFolderListAdapter;
						});
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		/// <param name="s">To be added.</param>
		/// <summary>
		/// Afters the text changed.
		/// </summary>
		public void AfterTextChanged (Android.Text.IEditable s)
		{			
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <param name="after">To be added.</param>
		/// <summary>
		/// Befores the text changed.
		/// </summary>
		public void BeforeTextChanged (ICharSequence s, int start, int count, int after)
		{
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="before">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <summary>
		/// Raises the text changed event.
		/// </summary>
		public void OnTextChanged (ICharSequence s, int start, int before, int count)
		{
			try {
				if (!string.IsNullOrEmpty (searchBoxET.Text)) {
					TextChangeService (s.ToString ());
				} else {
					WebServiceCall ();
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		///  Global search on edit text change on every character service.
		/// </summary>
		/// <param name="charSeq">Char seq.</param>
		private void TextChangeService (string charSeq)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					var searchKey = searchBoxET.Text;
					fromQueryString = "text_change";
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					var jsonService = new JsonService ();
					ContentSearchDTO contentDataObjects = new ContentSearchDTO ();
					contentDataObjects.userid = Convert.ToInt32 (userId);
					contentDataObjects.orgid = orgId;
					contentDataObjects.key = charSeq;
					contentDataObjects.folderid = 0;
					contentDataObjects.tagid = 0;
					string requestParameter = JsonConvert.SerializeObject (contentDataObjects);
					jsonService.consumeService (CommonSharedStrings.CONTENT_SEARCH, requestParameter, this);
					contentDataObjects = null;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			int id = v.Id;
			View view = this.activity.CurrentFocus;
			if (view != null) {  
				InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
				imm.HideSoftInputFromWindow (view.WindowToken, 0);
			}
			try {
				switch (id) {
				case Resource.Id.headerText:
					if (contentFolderListLV != null) {
						contentFolderListLV.SmoothScrollToPosition (0);
					}
					break;
				case Resource.Id.cancelTV:
					ClearSearch ();
					break;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the item click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			View v = this.activity.CurrentFocus;
			if (v != null) {  
				InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
				imm.HideSoftInputFromWindow (view.WindowToken, 0);
			}
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (!isFromTextChange) {
						if (contentFolderListAdapter.contentFolderListItems [position].folderid == 0) {
							var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
							if (userRole != "SuperAdmin" && userRole != "Admin") {
								CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
								return;
							}
						}
						if (contentFolderListAdapter.contentFolderListItems [position].passCode.Length != 0) {
							passcodeDialog = new Dialog (this.context);
							passcodeDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
							passcodeDialog.Window.RequestFeature (WindowFeatures.NoTitle);
							passcodeDialog.SetContentView (Resource.Layout.PasswordAlertBox);
							TextView alertTV = (TextView)passcodeDialog.FindViewById (Resource.Id.alertTV);
							TextView pleaseEnterTV = (TextView)passcodeDialog.FindViewById (Resource.Id.enterPasscodeTV);
							EditText passcodeET = (EditText)passcodeDialog.FindViewById (Resource.Id.passcodeET);
							dialogCancelTV = (TextView)passcodeDialog.FindViewById (Resource.Id.cancelTV);
							okTV = (TextView)passcodeDialog.FindViewById (Resource.Id.okTV);
				
							alertTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							pleaseEnterTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							dialogCancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							okTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							passcodeET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				
							okTV.Click += delegate(object sender, EventArgs e) {						
								if (passcodeET.Text == contentFolderListAdapter.contentFolderListItems [position].passCode) {
									passcodeDialog.Dismiss ();
									ContentFileListFragment contentFileListFragment = new ContentFileListFragment (this.context);
									Bundle args = new Bundle ();
									contentFileListFragment.selectedFolderPosition = position;
									args.PutInt ("folderId", contentFolderListAdapter.contentFolderListItems [position].folderid);
									args.PutString ("folderName", contentFolderListAdapter.contentFolderListItems [position].foldername);
									PreferenceConnector.WriteString (this.context, "folderName", contentFolderListAdapter.contentFolderListItems [position].foldername);
									contentFileListFragment.Arguments = args;
									fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentFileListFragment, "content_file_list_fragment").AddToBackStack ("content_file_list_fragment").Commit ();
								} else {
									CommonMethods.ShowAlertDialog (this.context, GetString (Resource.String.please_enter_valid_passcode_text));
								}
							};
				
							dialogCancelTV.Click +=	delegate(object sender, EventArgs e) {
								passcodeDialog.Dismiss ();
							};
							passcodeDialog.Show ();
						} else {
							ContentFileListFragment contentFileListFragment = new ContentFileListFragment (this.context);
							Bundle args = new Bundle ();
							contentFileListFragment.selectedFolderPosition = position;
							args.PutInt ("folderId", contentFolderListAdapter.contentFolderListItems [position].folderid);
							args.PutString ("folderName", contentFolderListAdapter.contentFolderListItems [position].foldername);
							PreferenceConnector.WriteString (this.context, "folderName", contentFolderListAdapter.contentFolderListItems [position].foldername);
							contentFileListFragment.Arguments = args;
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentFileListFragment, "content_file_list_fragment").AddToBackStack ("content_file_list_fragment").Commit ();
						}
					} else {
						TextView contentDotIVId = view.FindViewById<TextView> (Resource.Id.contentDotIVId);
						ContentSearchTermsDto contentSearchDtoObj = (ContentSearchTermsDto)contentDotIVId.Tag;
						if (contentSearchDtoObj.type.Equals ("folder")) {
							if (searchItemsListAdapter.contentSearchListItems [position].passCode.Length != 0) {
								passcodeDialog = new Dialog (this.context);
								passcodeDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
								passcodeDialog.Window.RequestFeature (WindowFeatures.NoTitle);
								passcodeDialog.SetContentView (Resource.Layout.PasswordAlertBox);
								TextView alertTV = (TextView)passcodeDialog.FindViewById (Resource.Id.alertTV);
								TextView pleaseEnterTV = (TextView)passcodeDialog.FindViewById (Resource.Id.enterPasscodeTV);
								EditText passcodeET = (EditText)passcodeDialog.FindViewById (Resource.Id.passcodeET);
								dialogCancelTV = (TextView)passcodeDialog.FindViewById (Resource.Id.cancelTV);
								okTV = (TextView)passcodeDialog.FindViewById (Resource.Id.okTV);
				
								alertTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
								pleaseEnterTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
								dialogCancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
								okTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
								passcodeET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				
								okTV.Click += delegate(object sender, EventArgs e) {						
									if (passcodeET.Text == searchItemsListAdapter.contentSearchListItems [position].passCode) {
										passcodeDialog.Dismiss ();
										ContentFileListFragment contentFileListFragment = new ContentFileListFragment (this.context);
										Bundle args = new Bundle ();
										contentFileListFragment.selectedFolderPosition = position;
										args.PutInt ("folderId", searchItemsListAdapter.contentSearchListItems [position].fileid);
										args.PutString ("folderName", searchItemsListAdapter.contentSearchListItems [position].filesname);
										PreferenceConnector.WriteString (this.context, "folderName", searchItemsListAdapter.contentSearchListItems [position].filesname);
										contentFileListFragment.Arguments = args;
										fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentFileListFragment, "content_file_list_fragment").AddToBackStack ("content_file_list_fragment").Commit ();
									} else {
										CommonMethods.ShowAlertDialog (this.context, GetString (Resource.String.please_enter_valid_passcode_text));
									}
								};
				
								dialogCancelTV.Click +=	delegate(object sender, EventArgs e) {
									passcodeDialog.Dismiss ();
								};
								passcodeDialog.Show ();
							} else {
								ContentFileListFragment contentFileListFragment = new ContentFileListFragment (this.context);
								Bundle args = new Bundle ();
								contentFileListFragment.selectedFolderPosition = position;
								args.PutInt ("folderId", searchItemsListAdapter.contentSearchListItems [position].fileid);
								args.PutString ("folderName", searchItemsListAdapter.contentSearchListItems [position].filesname);
								PreferenceConnector.WriteString (this.context, "folderName", searchItemsListAdapter.contentSearchListItems [position].filesname);
								contentFileListFragment.Arguments = args;
								fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentFileListFragment, "content_file_list_fragment").AddToBackStack ("content_file_list_fragment").Commit ();
							}
						} else {
							Bundle args = new Bundle ();
							ContentViewFragment contentViewFragment = new ContentViewFragment (this.context, "contentSearchTerms");
							args.PutSerializable ("contentSearchCls", contentSearchDtoObj);
							args.PutInt ("folderId", contentSearchDtoObj.fileid);
							contentViewFragment.Arguments = args;
							fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentViewFragment, "content_view_fragment").AddToBackStack ("content_view_fragment").Commit ();
						}
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the editor action event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="actionId">Action identifier.</param>
		/// <param name="e">E.</param>
		public bool OnEditorAction (TextView v, ImeAction actionId, KeyEvent e)
		{
			if (actionId == ImeAction.Search) {
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				return true;
			}
			return false;
		}
	}
}