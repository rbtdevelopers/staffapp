﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using RetailerAcademy.Droid.Source.ContentDTO;
using Newtonsoft.Json;
using Android.Support.V4.Widget;
using System.ComponentModel;
using Java.Lang;
using Newtonsoft.Json.Linq;
using RetailerAcademy.Droid.Source.Adapters;

namespace RetailerAcademy.Droid
{
	public class LeaderBoardUserFragment : BaseFragment, IWebServiceDelegate
	{
		private Context context;
		private string[] leaderBoardValues;
		private TextView headerText;
		private Dialog progressDialog;
		private ListView leaderBoardDetailsLV;
		private SwipeRefreshLayout swipeLayout;
		private List<LeaderBoardUserDto> lbUserList;
		private Dictionary<string, List<LeaderBoardUserDto>> dictionary;
		private List<string> hList;
		public bool isServiceCallCompleted;
		private JsonService jsonService;

		public LeaderBoardUserFragment (Context context, string[] values)
		{
			this.context = context;
			this.leaderBoardValues = values;
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			headerText.Text = GetString (Resource.String.leaderboard_text);
			WebServiceCall ();
		}

		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					string lbName = leaderBoardValues [0];
					string lbId = leaderBoardValues [1];
				
					LeaderBoardDtoWithOutJLObject lbDto = new LeaderBoardDtoWithOutJLObject ();
					lbDto.userId = Convert.ToInt32 (userId);
					lbDto.orgId = orgId;
					lbDto.leaderBoardId = Convert.ToInt32 (lbId);
					lbDto.leaderBoardName = lbName;
				
					string parameters = JsonConvert.SerializeObject (lbDto);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.LEADERBOARD_LIST, parameters, this);
				}
			} catch (System.Exception) {				
			}
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.LeaderBoardUserDetailsFragment, container, false);
			leaderBoardDetailsLV = view.FindViewById<ListView> (Resource.Id.leaderBoardDetailsLV);
			swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
			try {
				swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
					Android.Resource.Color.HoloGreenLight,
					Android.Resource.Color.HoloOrangeLight,
					Android.Resource.Color.HoloRedLight);
				swipeLayout.Refresh += delegate(object sender, EventArgs e) {			
					swipeLayout.PostDelayed (WebServiceCall, 3000);
				};
			} catch (System.Exception) {				
			}
			return view;
		}

		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					lbUserList = new List<LeaderBoardUserDto> ();
					if (Convert.ToBoolean (responseObj.SelectToken ("status"))) {
						JArray lbUserArray = (JArray)responseObj.SelectToken ("leaderBoardUserList");
						foreach (JObject item in lbUserArray) {
							LeaderBoardUserDto leaderBoardOrgs = new LeaderBoardUserDto ();
							leaderBoardOrgs.profileId = (int)item.SelectToken ("UserId");
							leaderBoardOrgs.points = (int)item.SelectToken ("points");
							leaderBoardOrgs.profileImageUrl = (string)item.SelectToken ("ProfileImage");
							leaderBoardOrgs.userName = (string)item.SelectToken ("UserName");
							leaderBoardOrgs.userCategory = (string)item.SelectToken ("userCategory");
							lbUserList.Add (leaderBoardOrgs);
						}
						SortList ();
						activity.RunOnUiThread (() => {
							List<string> categoriesName = lbUserList
								.Select (e => e.userCategory)
								.Distinct ()
								.ToList ();

							HashSet<string> hashSet = new HashSet<string> ();
							for (int i = 0; i < lbUserList.Count; i++) {
								hashSet.Add (lbUserList [i].userCategory);
							}
							hList = hashSet.ToList ();

							LeaderBoardUserListAdapter lbUserListAdapter = new LeaderBoardUserListAdapter (this.context, Resource.Layout.LeaderBoardUserListItem, hList);
							lbUserListAdapter.setData (dictionary);
							leaderBoardDetailsLV.Adapter = lbUserListAdapter;
						});
					} else {
						activity.RunOnUiThread (() => {
							hList = new List<string> ();
							leaderBoardDetailsLV.Adapter = new LeaderBoardUserListAdapter (this.context, Resource.Layout.LeaderBoardUserListItem, hList);
							var message = Convert.ToString (responseObj.SelectToken ("message"));
							if (!string.IsNullOrEmpty (message)) {
								CommonMethods.ShowAlertDialog (this.context, message);
							}
						});
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		private void SortList ()
		{
			try {
				if (lbUserList.Count > 0) {
					HashSet<string> set = new HashSet<string> ();
					for (int i = 0; i < lbUserList.Count; i++) {
						set.Add (lbUserList [i].userCategory);
					}
					dictionary = new Dictionary<string, List<LeaderBoardUserDto>> ();
					foreach (string s in set) {
						List<LeaderBoardUserDto> list = new List<LeaderBoardUserDto> ();
						for (int k = 0; k < lbUserList.Count; k++) {
							if (s.Equals (lbUserList [k].userCategory)) {
								list.Add (lbUserList [k]);
							}
						}
						dictionary.Add (s, list);
					}
				}
			} catch (System.Exception) {				
			}
		}

		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}
	}
}

