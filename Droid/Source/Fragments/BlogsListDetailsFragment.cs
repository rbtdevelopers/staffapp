﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RetailerAcademy.Droid.StaffApp;
using Java.IO;
using Java.Text;
using System.Text.RegularExpressions;

namespace RetailerAcademy.Droid
{
	public class BlogsListDetailsFragment : BaseFragment, IWebServiceDelegate, View.IOnClickListener
	{
		private Context context;
		private TextView titleTV, likeCountTV, commentCountTV, dateTV, descrptionTV, headerTextTV;
		private BlogListObjectsDTO blogsListDto;
		private Android.Webkit.WebView imageWebView;
		private Dialog progressDialog;
		private List<BlogListObjectsDTO> blogDetailMsgs;
		private bool likeCheck;
		private int likesCount = 0;
		private int commentsCount = 0;
		private int blogId, categoryId;
		private bool isLikeService = false;
		private JsonService jsonService;
		private RelativeLayout videoImageViewRL;
		private string videoUrl, contentFileNameHeader;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.BlogsListDetailsFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="blogsListDto">Blogs list dto.</param>
		/// <param name="contentFileNameHeader">Content file name header.</param>
		/// <param name="categoryId">Category identifier.</param>
		public BlogsListDetailsFragment (Context context, BlogListObjectsDTO blogsListDto, string contentFileNameHeader, int categoryId)
		{
			this.context = context;
			this.blogsListDto = blogsListDto;
			this.contentFileNameHeader = contentFileNameHeader;
			this.categoryId = categoryId;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerTextTV = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerTextTV.Text = contentFileNameHeader.ToUpper ();
			headerTextTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			activity.FindViewById<ImageView> (Resource.Id.headerMenu).Visibility = ViewStates.Gone;
			activity.FindViewById<ImageView> (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;
			activity.FindViewById (Resource.Id.notificationCountTV).Visibility = ViewStates.Gone;
			((MainActivity)context).DisableNavigationDrawer ();
		}

		/// <summary>
		/// Webs the service call for the blog to display.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					BlogsPost blogsPostDto = new BlogsPost ();
					blogsPostDto.userId = Convert.ToInt32 (userId);
					blogsPostDto.categoryid = categoryId;
					blogsPostDto.blogid = blogsListDto.blogid;
					string requestParameter = JsonConvert.SerializeObject (blogsPostDto);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.BLOG_LIST, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		#region IWebServiceDelegate implementation
		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					blogDetailMsgs = new List<BlogListObjectsDTO> ();
					bool status = false;
					JArray blogDetailsArray = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if(blogsListDto != null){
							blogsListDto.hasviewed = true;
						}
						if (!isLikeService) {
							if (responseObj ["blog"] != null) {
								blogDetailsArray = (JArray)responseObj.SelectToken ("blog");
								if (blogDetailsArray != null && blogDetailsArray.Count > 0) {
									foreach (JObject item in blogDetailsArray) {	
										blogId = (int)item.SelectToken ("blogid");
										var blogDescription = (string)item.SelectToken ("blogdescription");
										var webUrl = (string)item.SelectToken ("videourl");
										var blogTitle = (string)item.SelectToken ("blogtitle");
										var imageUrlStr = (string)item.SelectToken ("imageurl");
										var date = (string)item.SelectToken ("date");
										likesCount = (int)item.SelectToken ("likecount");
										commentsCount = (int)item.SelectToken ("commnetcount");
										var liked = (bool)item.SelectToken ("hasliked");
										blogsListDto.blogid = (int)item.SelectToken ("blogid");
										blogsListDto.blogdescription = (string)item.SelectToken ("blogdescription");
										blogsListDto.videourl = (string)item.SelectToken ("videourl");
										blogsListDto.blogtitle = (string)item.SelectToken ("blogtitle");
										blogsListDto.imageurl = (string)item.SelectToken ("imageurl");
										blogsListDto.date = (string)item.SelectToken ("date");
										videoUrl = webUrl;				
										if (!string.IsNullOrEmpty (blogDescription)) {
											activity.RunOnUiThread (() => {
												descrptionTV.Visibility = ViewStates.Visible;
												descrptionTV.Text = blogDescription;
											});
										} else {
											activity.RunOnUiThread (() => descrptionTV.Visibility = ViewStates.Gone);
										}
										activity.RunOnUiThread (() => {
											titleTV.Text = blogTitle;
											dateTV.Text = date;
											CommonMethods.CommentsOrLikesCountDisplay (likesCount, this.likeCountTV);
											CommonMethods.CommentsOrLikesCountDisplay (commentsCount, this.commentCountTV);
										});
										if (!string.IsNullOrEmpty (webUrl)) {
											if (webUrl.Contains (".mp4") || webUrl.Contains (".wmv") || webUrl.Contains (".mov")) {
												activity.RunOnUiThread (() => videoImageViewRL.Visibility = ViewStates.Visible);
											}
										} else {
											activity.RunOnUiThread (() => videoImageViewRL.Visibility = ViewStates.Gone);
										}
										if (!string.IsNullOrEmpty (imageUrlStr)) {
											activity.RunOnUiThread (() => {
												imageWebView.Visibility = ViewStates.Visible;
												string imageString = "<head><style type='text/css'>"
												                     + "body{margin:auto auto;text-align:center;} img{height:200px;} </style></head>"
												                     + "<body><img src=" + imageUrlStr + "/></body>";
												imageWebView.LoadDataWithBaseURL (null, imageString, "text/html", "utf-8", null);
											});
										} else {
											activity.RunOnUiThread (() => imageWebView.Visibility = ViewStates.Gone);
										}
										likeCheck = liked;
										blogsListDto.likecount = likesCount;
										blogsListDto.commnetcount = commentsCount;
										if (!liked) {
											blogsListDto.hasLiked = false;
											activity.RunOnUiThread (() => likeCountTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_unchecked, 0, 0, 0));
										} else {
											blogsListDto.hasLiked = true;
											activity.RunOnUiThread (() => likeCountTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_checked, 0, 0, 0));
										}
									}
								}
							}
						} else {
							isLikeService = false;
							if (!likeCheck) {
								likeCheck = true;
								likesCount = likesCount + 1;
								blogsListDto.likecount = likesCount;
								blogsListDto.hasLiked = true;	
								activity.RunOnUiThread (() => {
									CommonMethods.CommentsOrLikesCountDisplay (likesCount, this.likeCountTV);
									likeCountTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_checked, 0, 0, 0);
								});
							} else {
								likeCheck = false;
								likesCount = likesCount - 1;
								blogsListDto.likecount = likesCount;
								blogsListDto.hasLiked = false;
								activity.RunOnUiThread (() => {
									CommonMethods.CommentsOrLikesCountDisplay (likesCount, this.likeCountTV);
									likeCountTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_unchecked, 0, 0, 0);
								});
							}
						}
					} 
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed or on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		#endregion
		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.BlogsListDetailsFragment, null);
			titleTV = view.FindViewById<TextView> (Resource.Id.titleTVId);
			likeCountTV = view.FindViewById<TextView> (Resource.Id.likeCountTV);
			commentCountTV = view.FindViewById<TextView> (Resource.Id.commentCountTV);
			dateTV = view.FindViewById<TextView> (Resource.Id.dateTV);
			videoImageViewRL = view.FindViewById<RelativeLayout> (Resource.Id.videoImageViewRL);
			imageWebView = view.FindViewById<Android.Webkit.WebView> (Resource.Id.imageWebView);
			descrptionTV = view.FindViewById<TextView> (Resource.Id.descriptionTVId);
			titleTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			likeCountTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			commentCountTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			dateTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			descrptionTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);

			likeCountTV.SetOnClickListener (this);
			commentCountTV.SetOnClickListener (this);
			videoImageViewRL.SetOnClickListener (this);
			WebServiceCall ();
			return view;
		}

		/// <summary>
		/// Webservice for the like.
		/// </summary>
		private void LikeService ()
		{
			try {
				isLikeService = true;
				if (!CommonMethods.IsInternetConnected (context)) {
					CommonMethods.ShowAlertDialog (context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					BlogLike blogLikeObj = new BlogLike ();
					blogLikeObj.blogid = Convert.ToInt32 (blogId);
					blogLikeObj.userid = Convert.ToInt32 (userId);
					string requestParameter = JsonConvert.SerializeObject (blogLikeObj);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.BLOG_LIKE, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Called when the Fragment is no longer started.
		/// </summary>
		public override void OnStop ()
		{
			base.OnStop ();
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				var id = v.Id;
				switch (id) {
				case Resource.Id.commentCountTV:
					if (!CommonMethods.IsInternetConnected (context)) {
						CommonMethods.ShowAlertDialog (context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						BlogsCommentsFragment blogsCommentFrag = new BlogsCommentsFragment (context, blogsListDto);
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, blogsCommentFrag, "blogs_comment_fragment").AddToBackStack ("blogs_comment_fragment").Commit ();
					}
					break;
				case Resource.Id.likeCountTV:
					LikeService ();
					break;
				case Resource.Id.videoImageViewRL:
					if (!CommonMethods.IsInternetConnected (activity)) {
						CommonMethods.ShowAlertDialog (activity, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						if (!string.IsNullOrEmpty (videoUrl)) {
							Intent videoIntent = new Intent (activity, typeof(VideoPlayerActivity));
							videoIntent.PutExtra ("videoUrl", videoUrl);
							StartActivity (videoIntent);
						}
					}
					break;
				}
			} catch (Exception) {				
			}
		}
	}
}