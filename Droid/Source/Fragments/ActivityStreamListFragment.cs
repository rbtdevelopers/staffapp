﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Support.V4.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using System.ComponentModel;
using Java.Lang;
using Newtonsoft.Json.Linq;
using Android.Views.InputMethods;
using RetailerAcademy.Droid.StaffApp;

namespace RetailerAcademy.Droid
{
	public class ActivityStreamListFragment : BaseFragment, IWebServiceDelegate, Android.Text.ITextWatcher, View.IOnClickListener, ListView.IOnItemClickListener, IActivityStreamLikeServiceCall, TextView.IOnEditorActionListener, AbsListView.IOnScrollListener
	{
		private Context context;
		private TextView headerText, cancelTV;
		private ImageView composeIV;
		private EditText searchBoxET;
		private ListView activityStreamListLV;
		private ActivityStreamListAdapter activityStreamListAdapter;
		private Dialog progressDialog, alertDialog;
		private List<ActivityStreamDto> activityStreamList = new List<ActivityStreamDto> ();
		public List<ActivityStreamDto> filteredActivityList;
		public bool isLikeAreNot, hideCompose, searchEnable = false, isComingBack, pullToRefresh;
		private SwipeRefreshLayout swipeLayout;
		private JsonService jsonService;
		private ActivityStreamDto activityStream;
		private int preItem = 0;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ActivityStreamListFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public ActivityStreamListFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the ActivityStreamListFragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of ActivityStreamListFragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{			
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the ActivityStreamListFragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			headerText.SetOnClickListener (this);
			headerText.Text = GetString (Resource.String.activity_stream_text);
			activity.FindViewById (Resource.Id.headerMenu).Visibility = ViewStates.Visible;
			activity.FindViewById (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;
			ClearSearch ();
			((MainActivity)context).EnableNavigationDrawer ();
			if (MyApplication.isActivityUploaded) {
				WebServiceCallForActivity (0, "");
			}
			searchEnable = false;
		}

		/// <summary>
		/// Clears the search.
		/// </summary>
		private void ClearSearch ()
		{
			if (searchBoxET != null && !string.IsNullOrEmpty (searchBoxET.Text)) {
				searchBoxET.Text = "";
			}
		}

		/// <summary>
		/// Web service call for activity stream list items.
		/// </summary>
		/// <param name="activityLastId">Activity last identifier.</param>
		/// <param name="isRefresh">Is refresh.</param>
		private void WebServiceCallForActivity (int activityLastId, string isRefresh)
		{
			try {
				isLikeAreNot = false;
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					jsonService = new JsonService ();
					ActivityStreamListDTO activityStreamDto = new ActivityStreamListDTO ();
					activityStreamDto.userid = Convert.ToInt32 (userId);
					activityStreamDto.Orgid = orgId;
					if (activityLastId <= 0 && !isRefresh.Equals ("refresh")) {
						activityStreamDto.aIDAfter = 0;
					} else {
						if (activityStreamList != null && activityStreamList.Count > 0) {
							activityStreamDto.aIDAfter = activityStreamList [activityStreamList.Count - 1].activityStreamId;
						} else {
							activityStreamDto.aIDAfter = 0;
						}
					}
					string requestParameter = JsonConvert.SerializeObject (activityStreamDto);
					jsonService.consumeService (CommonSharedStrings.ACTIVITYSTREAM_LIST, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Webservice call for activity list items on pull to refresh.
		/// </summary>
		private void WebServiceCallForActivityPullToRefresh ()
		{
			try {
				isLikeAreNot = false;
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					MyApplication.isActivityUploaded = true;
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					jsonService = new JsonService ();
					ActivityStreamListDTO activityStreamDto = new ActivityStreamListDTO ();
					activityStreamDto.userid = Convert.ToInt32 (userId);
					activityStreamDto.Orgid = orgId;
					activityStreamDto.aIDAfter = 0;
					string requestParameter = JsonConvert.SerializeObject (activityStreamDto);
					jsonService.consumeService (CommonSharedStrings.ACTIVITYSTREAM_LIST, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.ActivityStreamFragment, container, false);
			activityStreamListLV = view.FindViewById<ListView> (Resource.Id.activityStreamListLV);
			cancelTV = view.FindViewById<TextView> (Resource.Id.cancelTV);
			composeIV = view.FindViewById<ImageView> (Resource.Id.composeMessageIV);
			searchBoxET = view.FindViewById<EditText> (Resource.Id.searchETId);
			cancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			searchBoxET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			searchBoxET.AddTextChangedListener (this);
			searchBoxET.SetOnEditorActionListener (this);
			cancelTV.SetOnClickListener (this);
			composeIV.SetOnClickListener (this);
			activityStreamListLV.OnItemClickListener = this;
			activityStreamListLV.SetOnScrollListener (this);
			try {
				activityStreamListAdapter = new ActivityStreamListAdapter (this.context, Resource.Layout.ActivityStreamListItem, activityStreamList, this);
				activityStreamListLV.Adapter = activityStreamListAdapter;
				var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
				if (userRole == "SuperAdmin" || userRole == "Admin") {
					hideCompose = true;
					cancelTV.Visibility = ViewStates.Gone;
					composeIV.Visibility = ViewStates.Visible;
					composeIV.SetOnClickListener (this);
				}
				swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
				swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
					Android.Resource.Color.HoloGreenLight,
					Android.Resource.Color.HoloOrangeLight,
					Android.Resource.Color.HoloRedLight);
				swipeLayout.Refresh += delegate(object sender, EventArgs e) {	
					ClearSearch ();
					swipeLayout.PostDelayed (WebServiceCallForActivityPullToRefresh, 3000);
				};
				if (!isComingBack) {
					WebServiceCallForActivity (0, "");
				}
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// On the response from the service this call back method of interface and gets all the information of the ActivityStreamListFragment upon the scuccess of service.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				isComingBack = true;
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					JArray activityStreamListArray = null;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if (!isLikeAreNot) {
							if (responseObj ["activityStreams"] != null) {
								activityStreamListArray = (JArray)responseObj.SelectToken ("activityStreams");
							}
							if (activityStreamListArray != null && activityStreamListArray.Count > 0) {
								if (MyApplication.isActivityUploaded) {
									MyApplication.isActivityUploaded = false;
									activityStreamList.Clear ();
								}
								foreach (JObject item in activityStreamListArray) {
									activityStream = new ActivityStreamDto ();
									activityStream.activityStreamId = (int)item.SelectToken ("activityStreamId");
									activityStream.activityStreamText = (string)item.SelectToken ("activityStreamText");
									activityStream.likeCount = (int)item.SelectToken ("likeCount");
									activityStream.commentCount = (int)item.SelectToken ("commentCount");
									activityStream.date = (string)item.SelectToken ("date");
									activityStream.hasLiked = (bool)item.SelectToken ("hasLiked");
									activityStream.profileImage = (string)item.SelectToken ("profileImage");
									activityStream.userName = (string)item.SelectToken ("userName");	
									activityStream.activityimage = (string)item.SelectToken ("activityimage");
									activityStreamList.Add (activityStream);
								}
								activity.RunOnUiThread (() => activityStreamListAdapter.NotifyDataSetChanged ());
								if (activityStreamList.Count != activityStreamListAdapter.Count) {
									activity.RunOnUiThread (() => {
										activityStreamListAdapter = new ActivityStreamListAdapter (this.context, Resource.Layout.ActivityStreamListItem, activityStreamList, this);
										activityStreamListLV.Adapter = activityStreamListAdapter;
									});
								}
							}
						} else {
							if (responseObj ["activityStreams"] != null) {
								activityStreamListArray = (JArray)responseObj.SelectToken ("activityStreams");
							}
							if (activityStreamListArray != null && activityStreamListArray.Count > 0) {
								foreach (JObject item in activityStreamListArray) {
									activityStreamListAdapter.activityStreamDto.activityStreamId = (int)item.SelectToken ("activityStreamId");
									activityStreamListAdapter.activityStreamDto.activityStreamText = (string)item.SelectToken ("activityStreamText");
									activityStreamListAdapter.activityStreamDto.likeCount = (int)item.SelectToken ("likeCount");
									activityStreamListAdapter.activityStreamDto.commentCount = (int)item.SelectToken ("commentCount");
									activityStreamListAdapter.activityStreamDto.date = (string)item.SelectToken ("date");
									activityStreamListAdapter.activityStreamDto.hasLiked = (bool)item.SelectToken ("hasLiked");
									activityStreamListAdapter.activityStreamDto.profileImage = (string)item.SelectToken ("profileImage");
									activityStreamListAdapter.activityStreamDto.userName = (string)item.SelectToken ("userName");	
									activityStreamListAdapter.activityStreamDto.activityimage = (string)item.SelectToken ("activityimage");
								}
								activity.RunOnUiThread (() => activityStreamListAdapter.NotifyDataSetChanged ());
							}
						}
					} else {
						if (responseObj ["message"] != null) {
							statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// Ons the response failed, On the response failure of ActivityStreamListFragment upon the failure of service.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		/// <param name="s">To be added.</param>
		/// <summary>
		/// Afters the text changed.
		/// </summary>
		public void AfterTextChanged (Android.Text.IEditable s)
		{	
			if (hideCompose) {
				if (searchBoxET.Text.Length <= 0) {
					composeIV.Visibility = ViewStates.Visible;
					cancelTV.Visibility = ViewStates.Gone;
				} else {
					composeIV.Visibility = ViewStates.Gone;
					cancelTV.Visibility = ViewStates.Visible;
				}
			}		
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <param name="after">To be added.</param>
		/// <summary>
		/// Befores the text changed.
		/// </summary>
		public void BeforeTextChanged (ICharSequence s, int start, int count, int after)
		{
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="before">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <summary>
		/// Raises the text changed event.
		/// </summary>
		public void OnTextChanged (ICharSequence s, int start, int before, int count)
		{
			try {
				if (activityStreamList != null && activityStreamList.Count > 0) {
					filteredActivityList = activityStreamList.Where (p => p.userName.ToLower ().Contains (searchBoxET.Text.ToLower ()) || p.activityStreamText.ToLower ().Contains (searchBoxET.Text.ToLower ())).ToList ();
					activityStreamListAdapter = new ActivityStreamListAdapter (this.context, Resource.Layout.ActivityStreamListItem, filteredActivityList, this);
					activityStreamListLV.Adapter = activityStreamListAdapter;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				switch (id) {
				case Resource.Id.headerText:
					if (activityStreamListLV != null) {
						activityStreamListLV.SmoothScrollToPosition (0);
					}
					break;
				case Resource.Id.cancelTV:
					searchBoxET.Text = "";
					if (hideCompose) {
						composeIV.Visibility = ViewStates.Visible;
						cancelTV.Visibility = ViewStates.Gone;
					}
					View view = this.activity.CurrentFocus;
					if (view != null) {  
						InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
						imm.HideSoftInputFromWindow (view.WindowToken, 0);
					}
					break;
				case Resource.Id.composeMessageIV:
					var isUserRoleChanged = PreferenceConnector.ReadString (this.context, "user_role", null);
					if (isUserRoleChanged != "SuperAdmin" && isUserRoleChanged != "Admin") {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
					} else {
						context.StartActivity (typeof(MessageComposeActivityStreamActivity));
					}
					break;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the item click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					View v = this.activity.CurrentFocus;
					if (v != null) {  
						InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
						imm.HideSoftInputFromWindow (view.WindowToken, 0);
					}
					TextView userLikeTV = view.FindViewById<TextView> (Resource.Id.likeTV);
					ActivityStreamDto activityStreamDto = (ActivityStreamDto)userLikeTV.Tag;
					CommentsDisplayFragment commentsDisplayFragment = new CommentsDisplayFragment (this.context);
					Bundle args = new Bundle ();
					args.PutSerializable ("activityStreamDto", activityStreamDto);
					commentsDisplayFragment.Arguments = args;
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, commentsDisplayFragment, "comments_display_fragment").AddToBackStack ("comments_display_fragment").Commit ();
				}
			} catch (System.Exception) {				
			}
		}

		#region IActivityStreamLikeServiceCall implementation
		/// <summary>
		/// Webs the service call for activity like.
		/// </summary>
		/// <param name="likeItemPostion">Like item postion.</param>
		public void WebServiceCallForActivityLike (int likeItemPostion)
		{
			try {
				isLikeAreNot = true;
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					searchEnable = true;
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);//MyApplication.UserId;
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);//MyApplication.OrgId;
					ActivityLike activityLikeObj = new ActivityLike ();
					activityLikeObj.activityStreamId = likeItemPostion;
					activityLikeObj.userId = Convert.ToInt32 (userId);
					activityLikeObj.orgId = orgId;
					string requestParameter = JsonConvert.SerializeObject (activityLikeObj);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.ACTIVITYSTREAM_LIKE, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		#endregion
		/// <summary>
		/// Raises the scroll event.
		/// </summary>
		/// <param name="view">View.</param>
		/// <param name="firstVisibleItem">First visible item.</param>
		/// <param name="visibleItemCount">Visible item count.</param>
		/// <param name="totalItemCount">Total item count.</param>
		public void OnScroll (AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
		{
			try {
				int lastItem = firstVisibleItem + visibleItemCount;
				if (string.IsNullOrEmpty (searchBoxET.Text)) {
					if (lastItem == totalItemCount && totalItemCount != 0) {
						if (preItem != lastItem) { 
							//to avoid multiple calls for last item
							if (activityStreamList != null && activityStreamList.Count > 0) {
								preItem = lastItem;
								int activityIdCount = activityStreamList [activityStreamList.Count - 1].activityStreamId;
								WebServiceCallForActivity (activityIdCount, "refresh");
							}
						}
					} else {
						preItem = lastItem;
					}
				}
			} catch (ArrayIndexOutOfBoundsException) {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
				});
			} catch (InvalidCastException) {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
				});
			} catch (Java.Lang.Exception) {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
				});
			}
		}
		/// <summary>
		/// Raises the scroll state changed event.
		/// </summary>
		/// <param name="view">View.</param>
		/// <param name="scrollState">Scroll state.</param>
		public void OnScrollStateChanged (AbsListView view, ScrollState scrollState)
		{
		}
		/// <summary>
		/// Raises the editor action event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="actionId">Action identifier.</param>
		/// <param name="e">E.</param>
		public bool OnEditorAction (TextView v, ImeAction actionId, KeyEvent e)
		{
			if (actionId == ImeAction.Search) {
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				return true;
			}
			return false;
		}
	}
}