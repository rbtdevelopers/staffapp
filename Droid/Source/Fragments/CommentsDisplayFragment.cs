﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Support.V4.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Java.Lang;
using Android.Views.InputMethods;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using RetailerAcademy.Droid.StaffApp;
using System.Timers;
using Com.Nostra13.Universalimageloader.Core;
using Android.Provider;
using Android.Content.PM;
using System.IO;
using Java.IO;
using Java.Text;
using Android.Text.Method;
using Android.Support.V4.Content;
using Android;
using Android.Support.V4.App;

namespace RetailerAcademy.Droid
{
	public class CommentsDisplayFragment : BaseFragment, IWebServiceDelegate, ListView.IOnItemLongClickListener, View.IOnClickListener, AbsListView.IOnScrollListener, View.IOnFocusChangeListener
	{
		private Context context;
		private TextView headerText, userNameTV, userActivityDescTV, userLikeTV, userCommentTV, dateFormatTV, submitTV, cancelAttachmentTV;
		private ImageView userImageIV, photoUploadIV, commentImageAttachment, commentAttachmentIV;
		private EditText commentBoxET;
		private ListView commentsListLV;
		private Dialog progressDialog, photoDialog, alertDialog, permissionsDialog = null;
		private bool isLikeAreNot, isLiked, likeCheck;
		private string userNameText, userDescText, activityDateText, userProfileImage, lastDate = "", commentText = "", fileName, extension, activityImageUrl;
		private int activityStreamId, userLikesText, userCommentsText;
		private List<CommentsCls> commentsList = new List<CommentsCls> ();
		private List<CommentsCls> previousCommentsList = new List<CommentsCls> ();
		private ActivityCommentListAdapter activityCommentListAdapter;
		private bool isCommentDelete = false;
		private int commentItemPosition = -1;
		private System.Timers.Timer timer;
		private ImageLoader imageLoader;
		private DisplayImageOptions options;
		private List<int> idsList = new List<int> ();
		private RelativeLayout commentPictureBox;
		private int lastCommentId = 0, preItem = 0;
		private bool postedCommentMsg = false, commentBoxFocus;
		private int totalCommentsCount;

		private Java.IO.File _file;
		private Java.IO.File _dir;
		private const int CROP_IMAGE_REQUEST_CODE = 111;
		private const int FROM_CAMERA_INTENT_CODE = 222;
		private const int MY_PERMISSIONS_REQUEST_CAMERA = 333;
		private const int PERMISSIONS_REQUEST_WRITE_STORAGE = 444;
		private const int PERMISSIONS_REQUEST_READ_STORAGE = 555;
		private string encodedImage = null;
		private ActivityStreamDto activityStreamDto;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.CommentsDisplayFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public CommentsDisplayFragment (Context context)
		{
			this.context = context;
			imageLoader = ImageLoader.Instance;
			options = CommonMethods.ReturnDisplayOptions (Resource.Drawable.no_image);
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{			
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the Fragment is visible to the user.
		/// </summary>
		public override void OnStart ()
		{
			base.OnStart ();
			SetTimerForService ();
		}

		/// <summary>
		/// Called when the Fragment is no longer started.
		/// </summary>
		public override void OnStop ()
		{
			base.OnStop ();
			timer.Stop ();
		}

		/// <summary>
		/// Sets the timer for service.
		/// </summary>
		private void SetTimerForService ()
		{
			timer = new System.Timers.Timer (5000);
			timer.Elapsed += OnTimedEvent;
			timer.AutoReset = true;
			timer.Enabled = true;
		}

		/// <summary>
		/// Raises the timed event on timer complete.
		/// </summary>
		/// <param name="source">Source.</param>
		/// <param name="e">E.</param>
		private void OnTimedEvent (System.Object source, ElapsedEventArgs e)
		{
			try {
				if (!commentBoxFocus) {
					isLikeAreNot = false;
					postedCommentMsg = true;
					if (!CommonMethods.IsInternetConnected (this.context)) {				 
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);				 
					} else {				 
						commentText = "";				 
						var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);				 
						ActivityComments commentsObj = new ActivityComments ();				 
						commentsObj.activityStreamId = activityStreamId;				 
						commentsObj.userid = Convert.ToInt32 (userId);				 
						commentsObj.comment = "";				 
						commentsObj.afterdate = lastDate;				 
						string requestParameter = JsonConvert.SerializeObject (commentsObj);				 
						var jsonService = new JsonService ();				 
						jsonService.consumeService (CommonSharedStrings.ACTIVITYSTREAM_COMMENTSLIST, requestParameter, this);					 
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			headerText.Text = GetString (Resource.String.comment_text);
			headerText.SetOnClickListener (this);
			activity.FindViewById (Resource.Id.headerMenu).Visibility = ViewStates.Gone;
			activity.FindViewById (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;
			activity.FindViewById (Resource.Id.notificationCountTV).Visibility = ViewStates.Gone;
			headerText.Visibility = ViewStates.Visible;
			((MainActivity)context).DisableNavigationDrawer ();
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.CommentsDisplayFragment, container, false);
			commentsListLV = view.FindViewById<ListView> (Resource.Id.commentsListLV);
			userNameTV = view.FindViewById<TextView> (Resource.Id.userNameTV);
			//userActivityDescTV = view.FindViewById<TextView> (Resource.Id.activityDescTV);
			userLikeTV = view.FindViewById<TextView> (Resource.Id.likeTV);
			userCommentTV = view.FindViewById<TextView> (Resource.Id.commentTV);
			dateFormatTV = view.FindViewById<TextView> (Resource.Id.dateTV);
			userImageIV = view.FindViewById<ImageView> (Resource.Id.userImageView);
			commentBoxET = view.FindViewById<EditText> (Resource.Id.commentETId);
			submitTV = view.FindViewById<TextView> (Resource.Id.submitTV);
			photoUploadIV = view.FindViewById<ImageView> (Resource.Id.photoUploadImageView);
			commentImageAttachment = view.FindViewById<ImageView> (Resource.Id.commentUploadImageView);
			cancelAttachmentTV = view.FindViewById<TextView> (Resource.Id.cancelAttachmentTV);
			commentPictureBox = view.FindViewById<RelativeLayout> (Resource.Id.commentPictureBox);
			commentAttachmentIV = view.FindViewById<ImageView> (Resource.Id.commentAttachmentIV);
			try {
				userNameTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				//userActivityDescTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				userLikeTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				userCommentTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				dateFormatTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				commentBoxET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				submitTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				
				//userActivityDescTV.MovementMethod = new ScrollingMovementMethod ();
				commentBoxET.OnFocusChangeListener = this;
				
				activityCommentListAdapter = new ActivityCommentListAdapter (this.context, Resource.Layout.ActivityStreamCommentListItem, previousCommentsList);//changed
				commentsListLV.Adapter = activityCommentListAdapter;
				
				photoUploadIV.SetOnClickListener (this);
				cancelAttachmentTV.SetOnClickListener (this);
				
				userLikeTV.SetOnClickListener (this);
				submitTV.SetOnClickListener (this);
				commentsListLV.OnItemLongClickListener = this;
				commentsListLV.SetOnScrollListener (this);
				var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);//MyApplication.UserRole;
				if (userRole == "SuperAdmin" || userRole == "Admin") {
					photoUploadIV.Visibility = ViewStates.Visible;
				} else {
					photoUploadIV.Visibility = ViewStates.Invisible;
				}
				
				if (progressDialog == null) {
					progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
				}
				SetBunldeDataToUI ();
				WebServiceCallForActivityComments ("", "", "", "", "");
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Sets the bunlde data to UI.
		/// </summary>
		private void SetBunldeDataToUI ()
		{
			try {
				activityStreamDto = (ActivityStreamDto)Arguments.GetSerializable ("activityStreamDto");
				if (activityStreamDto != null) {
					userNameText = activityStreamDto.userName;
					userDescText = activityStreamDto.activityStreamText;
					userLikesText = activityStreamDto.likeCount;
					userCommentsText = activityStreamDto.commentCount;
				
					activityDateText = activityStreamDto.date;
					isLikeAreNot = activityStreamDto.hasLiked;
					userProfileImage = activityStreamDto.profileImage;
					activityStreamId = activityStreamDto.activityStreamId;
					activityImageUrl = activityStreamDto.activityimage;
				
					userNameTV.Text = userNameText;
					//userActivityDescTV.Text = userDescText;
					CommonMethods.CommentsOrLikesCountDisplay (userLikesText, this.userLikeTV);
					CommonMethods.CommentsOrLikesCountDisplay (userCommentsText, this.userCommentTV);
				
					dateFormatTV.Text = activityDateText;
				
					if (!string.IsNullOrEmpty (userProfileImage)) {
						imageLoader.DisplayImage (userProfileImage, userImageIV, options);
					} else {
						userImageIV.SetImageResource (Resource.Drawable.no_image);
					}
				
					if (isLikeAreNot) {
						likeCheck = true;
						userLikeTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_checked, 0, 0, 0);
					} else {
						likeCheck = false;
						userLikeTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_unchecked, 0, 0, 0);
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Webservice call for activity comments.
		/// </summary>
		/// <param name="commentData">Comment data.</param>
		/// <param name="lastCommentDate">Last comment date.</param>
		/// <param name="encodedAttachedImg">Encoded attached image.</param>
		/// <param name="fileNameImg">File name image.</param>
		/// <param name="fileExtnsnImg">File extnsn image.</param>
		private void WebServiceCallForActivityComments (string commentData, string lastCommentDate, string encodedAttachedImg, string fileNameImg, string fileExtnsnImg)
		{
			try {
				isLikeAreNot = false;
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					ActivityComments commentsObj = new ActivityComments ();
					commentsObj.activityStreamId = activityStreamId;
					commentsObj.userid = Convert.ToInt32 (userId);
					commentsObj.comment = commentData;
					commentsObj.afterdate = lastCommentDate;
					if (commentPictureBox.Visibility == ViewStates.Visible) {
						commentsObj.file = encodedAttachedImg;
						commentsObj.filename = fileNameImg;
						commentsObj.extension = fileExtnsnImg;
						commentPictureBox.Visibility = ViewStates.Gone;
					} else {
						commentsObj.file = "";
						commentsObj.filename = "";
						commentsObj.extension = "";
					}					
					string requestParameter = JsonConvert.SerializeObject (commentsObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.ACTIVITYSTREAM_COMMENTSLIST, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Gets the comments on scroll service call.
		/// </summary>
		private void GetCommentsOnScroll ()
		{
			try {
				isLikeAreNot = false;
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					ActivityComments commentsObj = new ActivityComments ();
					commentsObj.activityStreamId = activityStreamId;
					commentsObj.lastcommentid = lastCommentId;
					commentsObj.comment = "";
					commentsObj.afterdate = "";
					if (commentPictureBox.Visibility == ViewStates.Visible) {
						commentsObj.file = "";
						commentsObj.filename = "";
						commentsObj.extension = "";
						commentPictureBox.Visibility = ViewStates.Gone;
					} else {
						commentsObj.file = "";
						commentsObj.filename = "";
						commentsObj.extension = "";
					}					
					string requestParameter = JsonConvert.SerializeObject (commentsObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.ACTIVITYSTREAM_COMMENTSLIST, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		#region IWebServiceDelegate implementation
		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			activity.RunOnUiThread (() => commentBoxET.ClearFocus ());
			int latestCount = 0;
			commentsList = new List<CommentsCls> ();
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);	
					if (responseObj ["commentcount"] != null) {
						totalCommentsCount = (int)responseObj.SelectToken ("commentcount");
					}
					bool status = false;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (status) {
						if (!isCommentDelete) {
							if (isLiked) {
								isLiked = false;
								if (!likeCheck) {
									likeCheck = true;	
									activity.RunOnUiThread (() => {
										CommonMethods.CommentsOrLikesCountDisplay (++userLikesText, this.userLikeTV);
										userLikeTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_checked, 0, 0, 0);
										activityStreamDto.hasLiked = true;
										activityStreamDto.likeCount = Convert.ToInt32 (this.userLikeTV.Text);
									});
								} else {
									likeCheck = false;
									activity.RunOnUiThread (() => {
										CommonMethods.CommentsOrLikesCountDisplay (--userLikesText, this.userLikeTV);
										int likesCount = Convert.ToInt32 (userLikeTV.Text);
										if (likesCount <= 0) {
											userLikeTV.Text = "0";
										}
										userLikeTV.SetCompoundDrawablesWithIntrinsicBounds (Resource.Drawable.like_unchecked, 0, 0, 0);
										activityStreamDto.hasLiked = false;
										activityStreamDto.likeCount = Convert.ToInt32 (this.userLikeTV.Text);
									});
								}
								activity.RunOnUiThread (() => {
									if (progressDialog != null) {
										progressDialog.Dismiss ();
										progressDialog = null;
									}
								});
								return;
							}
							if (statusMessage == "No Comments available") {
								//CommonMethods.ShowAlertDialog (this.context, Convert.ToString (responseObj.SelectToken ("message")));
							} else {
								JArray CommentsArray = null;
								if (responseObj ["asCommentList"] != null) {
									CommentsArray = (JArray)responseObj.SelectToken ("asCommentList");
									if (CommentsArray != null && CommentsArray.Count > 0) {
										foreach (JObject item in CommentsArray) {
											CommentsCls comments = new CommentsCls ();
											comments.commentid = (int)item.SelectToken ("commentID");
											comments.userId = (int)item.SelectToken ("userId");
											comments.dateofcomment = Convert.ToString (item.SelectToken ("commentedDate"));
											comments.userfullname = Convert.ToString (item.SelectToken ("userName"));
											comments.Comment = Convert.ToString (item.SelectToken ("comment"));
											comments.userImg = Convert.ToString (item.SelectToken ("profileImage"));
											comments.commentAttachmentImg = Convert.ToString (item.SelectToken ("CommentImage"));
											int id = (int)item.SelectToken ("commentID");
											if (!idsList.Contains (id)) {
												idsList.Add (id);
												latestCount = 1;
												if (postedCommentMsg) {
													previousCommentsList.Add (comments);
												} else {
													commentsList.Add (comments);
												}
											}
											lastDate = Convert.ToString (item.SelectToken ("commentedDate"));
										}
										activity.RunOnUiThread (() => {
											if (previousCommentsList != null && previousCommentsList.Count > 0) {
												previousCommentsList.AddRange (commentsList);
												var concatedList = previousCommentsList.OrderBy (x => x.commentid).ToList ();
												previousCommentsList.Clear ();
												previousCommentsList.AddRange (concatedList);
											} else {
												previousCommentsList.AddRange (commentsList);
												activityCommentListAdapter.NotifyDataSetChanged ();
												commentsListLV.SetSelection (commentsListLV.Adapter.Count - 1);
												lastCommentId = previousCommentsList [0].commentid;
											}					
										});
										activity.RunOnUiThread (() => {	
											CommonMethods.CommentsOrLikesCountDisplay (totalCommentsCount, this.userCommentTV);
											activityStreamDto.commentCount = totalCommentsCount;
											int size = commentsListLV.Adapter.Count;
											if (size > 0 && latestCount > 0) {
												activityCommentListAdapter.NotifyDataSetChanged ();
												var commentListSize = commentsList.Count;
												if (!postedCommentMsg) {
													commentsListLV.SetSelection (commentListSize);
												}
												lastCommentId = previousCommentsList [0].commentid;
											}
										});
										postedCommentMsg = false;
									}
								}
							}
						} else {
							/* Remove selected item from list */
							activity.RunOnUiThread (() => {
								isCommentDelete = false;
								previousCommentsList.RemoveAt (commentItemPosition);
								activityCommentListAdapter.setPosition = -1;
								activityCommentListAdapter.NotifyDataSetChanged ();
								CommonMethods.CommentsOrLikesCountDisplay (totalCommentsCount, this.userCommentTV);
								activityStreamDto.commentCount = totalCommentsCount;
								if (!string.IsNullOrEmpty (statusMessage)) {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								}
							});
						}
					} else {
						if (!string.IsNullOrEmpty (statusMessage)) {
							activity.RunOnUiThread (() => {
								if (alertDialog != null) {
									alertDialog.Dismiss ();
								}
								alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
							});
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
					CommonMethods.ShowAlertDialog (this.context, response);
				}
			});
		}

		#endregion
		/// <summary>
		/// Raises the item long click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public bool OnItemLongClick (AdapterView parent, View view, int position, long id)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					var userLoginId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					int userId = Convert.ToInt32 (userLoginId);
					var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
					if (userId == activityCommentListAdapter.activityStreamCommentListItems [position].userId || userRole == "SuperAdmin" || userRole == "Admin") {
						commentItemPosition = position;
						var archieveRemoveTV = (TextView)view.FindViewById (Resource.Id.moveToArchiveInboxTV);
						if (archieveRemoveTV != null) {
							archieveRemoveTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							archieveRemoveTV.SetOnClickListener (this);
							activityCommentListAdapter.setPosition = position;
							activityCommentListAdapter.NotifyDataSetChanged ();
						}
					}
				}
				return true;
			} catch (System.Exception) {
				return false;
			}
		}

		/// <summary>
		/// Deletes the selected item service.
		/// </summary>
		private void DeleteSelectedItem ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					isCommentDelete = true;
					string userId = null;
					var userLoginId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					if (Convert.ToInt32 (userLoginId) != previousCommentsList [commentItemPosition].userId) {
						userId = previousCommentsList [commentItemPosition].userId + "";
					} else {
						userId = userLoginId;
					}
					int commentId = previousCommentsList [commentItemPosition].commentid;
					CommentsDeleteActivityStream commentsObj = new CommentsDeleteActivityStream ();
					commentsObj.commentId = commentId;
					commentsObj.activityStreamId = activityStreamId;
					commentsObj.userid = Convert.ToInt32 (userId);
					string requestParameter = JsonConvert.SerializeObject (commentsObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.DELETECOMMENT, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Activitystream like in detail page webservice call.
		/// </summary>
		private void ActivityStreamLikeInDetailPage ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					isLiked = true;
					ActivityLike activityLikeObj = new ActivityLike ();
					activityLikeObj.activityStreamId = activityStreamId;
					activityLikeObj.userId = Convert.ToInt32 (userId);
					activityLikeObj.orgId = orgId;
					string requestParameter = JsonConvert.SerializeObject (activityLikeObj);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.ACTIVITYSTREAM_LIKE, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				var id = v.Id;
				switch (id) {
				case Resource.Id.headerText:
					if (commentsListLV != null) {
						commentsListLV.SmoothScrollToPosition (-1);
					}
					break;
				case Resource.Id.likeTV:
					ActivityStreamLikeInDetailPage ();
					break;
				case Resource.Id.submitTV:
					var isUserRoleChanged = PreferenceConnector.ReadString (this.context, "user_role", null);
					if (commentPictureBox.Visibility == ViewStates.Visible) {
						if (isUserRoleChanged != "SuperAdmin" && isUserRoleChanged != "Admin") {
							CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
							return;
						}
					}
					if (commentBoxET.Text.Trim ().Length != 0) {
						commentText = commentBoxET.Text;
						WebServiceCallForActivityComments (commentBoxET.Text, lastDate, encodedImage, fileName, extension);
						commentBoxET.Text = "";
						postedCommentMsg = true;
					} else {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.COMMENT_EMPTY);
					}
					break;
				case Resource.Id.moveToArchiveInboxTV:
					var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
					var userLoginId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var commentUserId = previousCommentsList [commentItemPosition].userId;
					int userId = Convert.ToInt32 (userLoginId);
					if (userId != commentUserId) {
						if (userRole != "SuperAdmin" && userRole != "Admin") {
							CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
						} else {
							DeleteSelectedItem ();
						}
					} else {
						DeleteSelectedItem ();
					}
					break;
				case Resource.Id.photoUploadImageView:
					photoDialog = new Dialog (this.context);
					photoDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
					photoDialog.Window.RequestFeature (WindowFeatures.NoTitle);
					photoDialog.SetContentView (Resource.Layout.PhotoDialogBox);
					TextView cameraTV = (TextView)photoDialog.FindViewById (Resource.Id.cameraTV);
					TextView photoLibraryTV = (TextView)photoDialog.FindViewById (Resource.Id.photoLibraryTV);
					TextView cancelTV = (TextView)photoDialog.FindViewById (Resource.Id.cancelTV);
					cameraTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
					photoLibraryTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
					cancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
					photoDialog.Show ();
				
					cameraTV.SetOnClickListener (this);
					photoLibraryTV.SetOnClickListener (this);
					cancelTV.SetOnClickListener (this);
					break;
				case Resource.Id.photoLibraryTV:
					photoDialog.Dismiss ();
					RequestReadStoragePermission ();	
					break;
				case Resource.Id.cameraTV:
					photoDialog.Dismiss ();
					RequestWriteStoragePermission ();
					break;
				case Resource.Id.cancelTV:
					photoDialog.Dismiss ();
					break;
				case Resource.Id.cancelAttachmentTV:
					commentPictureBox.Visibility = ViewStates.Gone;
					encodedImage = null;
					break;
				case Resource.Id.okAlertOKTV:
					if (permissionsDialog != null) {
						permissionsDialog.Dismiss ();
					}
					break;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Intent call for photo library with crop funtionality.
		/// </summary>
		private void IntentCallForPhotoLibrary ()
		{
			try {
				int height = 450;
				int width = Resources.DisplayMetrics.WidthPixels;
				Intent intent = new Intent (Intent.ActionPick, Android.Provider.MediaStore.Images.Media.ExternalContentUri);	
				intent.SetType ("image/*");
				intent.PutExtra ("crop", "true");  
				intent.PutExtra ("aspectX", 1);  
				intent.PutExtra ("aspectY", 1);  
				intent.PutExtra ("outputX", width);  
				intent.PutExtra ("outputY", height);  
				intent.PutExtra ("noFaceDetection", true);  
				intent.PutExtra ("return-data", true); 
				intent.PutExtra ("scale", true);   
				intent.PutExtra("scaleUpIfNeeded", true);
				intent.PutExtra("outputFormat", Bitmap.CompressFormat.Jpeg.ToString());
				StartActivityForResult (intent, CROP_IMAGE_REQUEST_CODE);
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Requests the write storage permission.
		/// </summary>
		private void RequestWriteStoragePermission ()
		{
			// Here, thisActivity is the current activity
			if (ContextCompat.CheckSelfPermission (activity, Manifest.Permission.WriteExternalStorage) != Permission.Granted) {
				// Should we show an explanation?
				if (ShouldShowRequestPermissionRationale (Manifest.Permission.WriteExternalStorage)) {
					// Show an expanation to the user *asynchronously* -- don't block
					// this thread waiting for the user's response! After the user
					// sees the explanation, try again to request the permission.
					RequestPermissions (new System.String[]{ Manifest.Permission.WriteExternalStorage }, PERMISSIONS_REQUEST_WRITE_STORAGE);
				} else {
					// No explanation needed, we can request the permission.
					RequestPermissions (new System.String[]{ Manifest.Permission.WriteExternalStorage }, PERMISSIONS_REQUEST_WRITE_STORAGE);
					// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
					// app-defined int constant. The callback method gets the
					// result of the request.
				}
			} else {
				//Call camera directly, permissions are already accepted.
				if (IsThereAnAppToTakePictures ()) {
					CreateDirectoryForPictures ();
					CallCamera ();
				}
			}
		}

		/// <summary>
		/// Requests the read storage permission.
		/// </summary>
		private void RequestReadStoragePermission ()
		{
			// Here, thisActivity is the current activity
			if (ContextCompat.CheckSelfPermission (activity, Manifest.Permission.ReadExternalStorage) != Permission.Granted) {
				// Should we show an explanation?
				if (ShouldShowRequestPermissionRationale (Manifest.Permission.ReadExternalStorage)) {
					// Show an expanation to the user *asynchronously* -- don't block
					// this thread waiting for the user's response! After the user
					// sees the explanation, try again to request the permission.
					RequestPermissions (new System.String[]{ Manifest.Permission.ReadExternalStorage }, PERMISSIONS_REQUEST_READ_STORAGE);
				} else {
					// No explanation needed, we can request the permission.
					RequestPermissions (new System.String[]{ Manifest.Permission.ReadExternalStorage }, PERMISSIONS_REQUEST_READ_STORAGE);
					// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
					// app-defined int constant. The callback method gets the
					// result of the request.
				}
			} else {
				//Call camera directly, permissions are already accepted.
				IntentCallForPhotoLibrary ();
			}
		}

		/// <summary>
		/// Raises the request permissions result event.
		/// </summary>
		/// <param name="requestCode">Request code.</param>
		/// <param name="permissions">Permissions.</param>
		/// <param name="grantResults">Grant results.</param>
		public override void OnRequestPermissionsResult (int requestCode, string[] permissions, Permission[] grantResults)
		{
			switch (requestCode) {
			case PERMISSIONS_REQUEST_WRITE_STORAGE:
				{
					// If request is cancelled, the result arrays are empty.
					if (grantResults.Length > 0	&& grantResults [0] == Permission.Granted) {
						// permission was granted, yay! Do the
						// camera task you need to do.
						if (IsThereAnAppToTakePictures ()) {
							CreateDirectoryForPictures ();
							CallCamera ();
						}
					} else {
						// permission denied, boo! Disable the
						// functionality that depends on this permission.
						if (permissionsDialog != null) {
							permissionsDialog.Dismiss ();
						}
						permissionsDialog = CommonMethods.AlertDialogForPermissions (context, Arial, this, true);
					}
					return;
				}
			case PERMISSIONS_REQUEST_READ_STORAGE:
				{
					// If request is cancelled, the result arrays are empty.
					if (grantResults.Length > 0	&& grantResults [0] == Permission.Granted) {
						// permission was granted, yay! Do the
						// camera task you need to do.
						IntentCallForPhotoLibrary ();
					} else {
						// permission denied, boo! Disable the
						// functionality that depends on this permission.
						if (permissionsDialog != null) {
							permissionsDialog.Dismiss ();
						}
						permissionsDialog = CommonMethods.AlertDialogForPermissions (context, Arial, this, true);
					}
					return;
				}
			}
		}

		/// <summary>
		/// Determines whether this instance is there an app to take pictures.
		/// </summary>
		/// <returns><c>true</c> if this instance is there an app to take pictures; otherwise, <c>false</c>.</returns>
		private bool IsThereAnAppToTakePictures ()
		{
			try {
				Intent intent = new Intent (MediaStore.ActionImageCapture);
				IList<ResolveInfo> availableActivities = context.PackageManager.QueryIntentActivities (intent, PackageInfoFlags.MatchDefaultOnly);
				return availableActivities != null && availableActivities.Count > 0;
			} catch (System.Exception) {		
				return false;
			}
		}

		/// <summary>
		/// Creates the directory for pictures.
		/// </summary>
		private void CreateDirectoryForPictures ()
		{
			try {
				_dir = new Java.IO.File (Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/RetailAcademyData/");
				if (!_dir.Exists ()) {
					_dir.Mkdirs ();
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Calls the camera.
		/// </summary>
		private void CallCamera ()
		{
			try {
				Intent intent = new Intent (MediaStore.ActionImageCapture);
				_file = new Java.IO.File (_dir, System.String.Format ("StaffApp_{0}.jpg", Guid.NewGuid ()));
				intent.PutExtra (MediaStore.ExtraOutput, Android.Net.Uri.FromFile (_file));
				StartActivityForResult (intent, 222);
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Gets the real path from URI.
		/// </summary>
		/// <returns>The real path from URI.</returns>
		/// <param name="contentUri">Content URI.</param>
		public string GetRealPathFromURI (Android.Net.Uri contentUri)
		{
			try {
				string[] filePathColumn = { MediaStore.MediaColumns.Data };
				var cursor = context.ContentResolver.Query (contentUri, filePathColumn, null, null, null);
				cursor.MoveToFirst ();
				int columnIndex = cursor.GetColumnIndex (filePathColumn [0]);
				string picturePath = cursor.GetString (columnIndex);
				cursor.Close ();
				return picturePath;
			} catch (System.Exception) {
				return null;
			}
		}

		/// <summary>
		/// Gets the image URI.
		/// </summary>
		/// <returns>The image URI.</returns>
		/// <param name="inImage">In image.</param>
		public Android.Net.Uri GetImageUri (Bitmap inImage)
		{
			try {
				MemoryStream bytes = new MemoryStream ();
				inImage.Compress (Bitmap.CompressFormat.Jpeg, 100, bytes);
				string path = Android.Provider.MediaStore.Images.Media.InsertImage (context.ContentResolver, inImage, "Title", null);
				return Android.Net.Uri.Parse (path);
			} catch (System.Exception) {
				return null;
			}
		}

		/// <param name="requestCode">The integer request code originally supplied to
		///  startActivityForResult(), allowing you to identify who this
		///  result came from.</param>
		/// <param name="resultCode">The integer result code returned by the child activity
		///  through its setResult().</param>
		/// <param name="data">An Intent, which can return result data to the caller
		///  (various data can be attached to Intent "extras").</param>
		/// <summary>
		/// Raises the activity result event.
		/// </summary>
		public override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);
			if (resultCode == Result.Canceled) {
				return;
			} else {
				try {
					// make it available in the gallery
					string imagePath = null;
					Android.Net.Uri contentUri = null;
					Intent mediaScanIntent = new Intent (Intent.ActionMediaScannerScanFile);
					if (_file != null && requestCode == FROM_CAMERA_INTENT_CODE) {
						contentUri = Android.Net.Uri.FromFile (_file);
						mediaScanIntent.SetData (contentUri);
						context.SendBroadcast (mediaScanIntent);
						int height = commentImageAttachment.Height;
						int width = Resources.DisplayMetrics.WidthPixels;
						if (height <= 0) {
							height = 450;
						}
						Bitmap bitmap = null;
						using (bitmap = _file.Path.LoadAndResizeBitmapCommentsActivityStream (width, height)) {
							commentPictureBox.Visibility = ViewStates.Visible;
							commentImageAttachment.SetImageBitmap (bitmap);
							MemoryStream stream = new MemoryStream ();
							bitmap.Compress (Bitmap.CompressFormat.Jpeg, 100, stream);
							byte[] imageByteArray = stream.ToArray ();
							encodedImage = Convert.ToBase64String (imageByteArray);
						}
						_file = null;
					} else if (data != null) {
						Bitmap thumbnailBitmap = null;
						if (requestCode == CROP_IMAGE_REQUEST_CODE && data.Extras != null) {
							Bundle bundle = data.Extras;
							thumbnailBitmap = (Bitmap)bundle.GetParcelable ("data");
							contentUri = GetImageUri (thumbnailBitmap);
						} else {
							contentUri = data.Data;
							thumbnailBitmap = Android.Provider.MediaStore.Images.Media.GetBitmap (context.ContentResolver, contentUri);
						}
						commentPictureBox.Visibility = ViewStates.Visible;
						commentImageAttachment.SetImageBitmap (thumbnailBitmap);
						MemoryStream stream = new MemoryStream ();
						thumbnailBitmap.Compress (Bitmap.CompressFormat.Jpeg, 100, stream);
						byte[] imageByteArray = stream.ToArray ();
						encodedImage = Convert.ToBase64String (imageByteArray);
					}
					if (contentUri != null) {
						if (!contentUri.ToString ().Contains ("file")) {
							imagePath = GetRealPathFromURI (contentUri);
						} else {
							imagePath = contentUri.ToString ();
						}
						var imagefileName = imagePath.Substring (imagePath.LastIndexOf ("/") + 1);
						string[] imageNameArray = imagefileName.Split ('.');
						fileName = imageNameArray [0];
						extension = imagefileName.Substring (imagefileName.LastIndexOf ("."));
					}
				} catch (Java.Lang.ArithmeticException) {					
				} catch (IllegalArgumentException) {					
				} catch (Java.Lang.Exception) {					
				}
			}
		}

		/// <summary>
		/// Raises the scroll state changed event.
		/// </summary>
		/// <param name="view">View.</param>
		/// <param name="scrollState">Scroll state.</param>
		public void OnScrollStateChanged (AbsListView view, ScrollState scrollState)
		{
		}

		/// <summary>
		/// Raises the scroll event.
		/// </summary>
		/// <param name="view">View.</param>
		/// <param name="firstVisibleItem">First visible item.</param>
		/// <param name="visibleItemCount">Visible item count.</param>
		/// <param name="totalItemCount">Total item count.</param>
		public void OnScroll (AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
		{
			try {
				if (!commentBoxFocus) {
					int lastFirstVisibleItem = 0;
					int lastItem = firstVisibleItem + visibleItemCount;
					if (string.IsNullOrEmpty (commentBoxET.Text)) {
						if (firstVisibleItem == 0 && previousCommentsList.Count > 0) {//changed from 19 to 0
							if (preItem != lastItem) { 
								GetCommentsOnScroll ();
								preItem = lastItem;
							}
						} else {
							preItem = lastItem;
						}
						lastFirstVisibleItem = firstVisibleItem;
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the focus change event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="hasFocus">If set to <c>true</c> has focus.</param>
		public void OnFocusChange (View v, bool hasFocus)
		{
			commentBoxFocus = hasFocus;
		}
	}

	public static class BitmapHelpersCommentsDisplay
	{
		/// <summary>
		/// Load and resize bitmap in the comments activity stream page.
		/// </summary>
		/// <returns>The and resize bitmap comments activity stream.</returns>
		/// <param name="fileName">File name.</param>
		/// <param name="width">Width.</param>
		/// <param name="height">Height.</param>
		public static Bitmap LoadAndResizeBitmapCommentsActivityStream (this string fileName, int width, int height)
		{
			try {
				// First we get the the dimensions of the file on disk
				BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
				BitmapFactory.DecodeFile (fileName, options);

				// Next we calculate the ratio that we need to resize the image by
				// in order to fit the requested dimensions.
				int outHeight = options.OutHeight;
				int outWidth = options.OutWidth;
				int inSampleSize = 1;

				try {
					if (outHeight > height || outWidth > width) {
						inSampleSize = outWidth > outHeight	? outHeight / height : outWidth / width;
					}
				} catch (System.ArithmeticException) {
				} catch (System.Exception) {
				}

				// Now we will load the image and have BitmapFactory resize it for us.
				options.InSampleSize = inSampleSize;
				options.InJustDecodeBounds = false;
				Bitmap resizedBitmap = null;
				resizedBitmap = BitmapFactory.DecodeFile (fileName, options);

				return resizedBitmap;
			} catch (Java.Lang.ArithmeticException) {
				return null;
			} catch (Java.Lang.IllegalArgumentException) {
				return null;
			} catch (Java.Lang.Exception) {
				return null;
			}
		}
	}
}