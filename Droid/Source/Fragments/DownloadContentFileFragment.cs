﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using System.Net;
using Android.Graphics;
using System.ComponentModel;
using RetailerAcademy.Droid.Source.Utilities;
using System.Threading.Tasks;
using Android.Preferences;
using Java.Net;
using Java.IO;
using Org.Apache.Http.Util;
using System.IO;
using System.Threading;
using Android.Support.V4.Content;
using Android;
using Android.Content.PM;

namespace RetailerAcademy.Droid
{
	public class DownloadContentFileFragment : BaseFragment, AdapterView.IOnItemClickListener, View.IOnClickListener
	{
		private Context context;
		private TextView headerText, overallProgressTV;
		private ListView downloadListLV;
		private DownloadContentFileAdapter downloadContentFileAdapter;
		private ProgressBar downloadProgress;
		private Dialog progressDialog, permissionsDialog = null;
		private List<DownloadModelDto> downloadList = new List<DownloadModelDto> ();
		private WebClient client;
		private String filePath, fileName, folderName, contentFilePath;
		private int fileId;
		private const int PERMISSIONS_REQUEST_WRITE_STORAGE = 444;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.DownloadContentFileFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public DownloadContentFileFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			try {
				fileName = Arguments.GetString ("fileName");
				folderName = PreferenceConnector.ReadString(this.context, "folderName", null);
				if(string.IsNullOrEmpty(folderName)){
					folderName = "KNOWLEDGE";
				}
				contentFilePath = Arguments.GetString ("contentFilePath");
				fileId = Arguments.GetInt ("fileId");
				if (progressDialog == null) {
					progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			headerText.Text = GetString (Resource.String.download_text);
			activity.FindViewById<TextView> (Resource.Id.headerText).Visibility = ViewStates.Visible;
			activity.FindViewById<ImageView> (Resource.Id.headerOrgs).Visibility = ViewStates.Gone;
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.DownloadContentFileFragment, container, false);
			downloadListLV = view.FindViewById<ListView> (Resource.Id.downloadListLV);
			overallProgressTV = view.FindViewById<TextView> (Resource.Id.overallProgressTV);
			downloadProgress = view.FindViewById<ProgressBar> (Resource.Id.downloadProgress);
			try {
				overallProgressTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				downloadListLV.OnItemClickListener = this;
				DownloadModelDto model = new DownloadModelDto ();
				model.downloadFileName = fileName;
				model.downloadFolderName = folderName;
				model.downloadStatus = false;
				downloadList.Add (model);
				downloadContentFileAdapter = new DownloadContentFileAdapter (activity, Resource.Layout.DownloadListItem, downloadList);
				downloadListLV.Adapter = downloadContentFileAdapter;
				RequestWriteStoragePermission ();
			} catch (Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Downloads the file from the given url.
		/// </summary>
		private void DownloadFileContent(){
			try {
				new System.Threading.Thread (new System.Threading.ThreadStart (() => {
					client = new WebClient ();
					client.DownloadProgressChanged += new DownloadProgressChangedEventHandler (client_DownloadProgressChanged);
					client.DownloadStringCompleted += Client_DownloadStringCompleted;
					client.DownloadStringAsync (new Uri (contentFilePath));
				})).Start ();
			} catch (WebException) {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (activity, CommonSharedStrings.STATUS_ERROR);
			}
		}

		/// <summary>
		/// Requests the write storage permission.
		/// </summary>
		private void RequestWriteStoragePermission ()
		{
			// Here, thisActivity is the current activity
			if (ContextCompat.CheckSelfPermission (activity, Manifest.Permission.WriteExternalStorage) != Permission.Granted) {
				// Should we show an explanation?
				if (ShouldShowRequestPermissionRationale (Manifest.Permission.WriteExternalStorage)) {
					// Show an expanation to the user *asynchronously* -- don't block
					// this thread waiting for the user's response! After the user
					// sees the explanation, try again to request the permission.
					RequestPermissions (new System.String[]{ Manifest.Permission.WriteExternalStorage }, PERMISSIONS_REQUEST_WRITE_STORAGE);
				} else {
					// No explanation needed, we can request the permission.
					RequestPermissions (new System.String[]{ Manifest.Permission.WriteExternalStorage }, PERMISSIONS_REQUEST_WRITE_STORAGE);
					// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
					// app-defined int constant. The callback method gets the
					// result of the request.
				}
			} else {
				//Call camera directly, permissions are already accepted.
				DownloadFileContent();
			}
		}

		/// <summary>
		/// Raises the request permissions result event.
		/// </summary>
		/// <param name="requestCode">Request code.</param>
		/// <param name="permissions">Permissions.</param>
		/// <param name="grantResults">Grant results.</param>
		public override void OnRequestPermissionsResult (int requestCode, string[] permissions, Permission[] grantResults)
		{
			switch (requestCode) {
			case PERMISSIONS_REQUEST_WRITE_STORAGE:
				{
					// If request is cancelled, the result arrays are empty.
					if (grantResults.Length > 0	&& grantResults [0] == Permission.Granted) {
						// permission was granted, yay! Do the
						// camera task you need to do.
						DownloadFileContent();
					} else {
						// permission denied, boo! Disable the
						// functionality that depends on this permission.
						if (permissionsDialog != null) {
							permissionsDialog.Dismiss ();
						}
						permissionsDialog = CommonMethods.AlertDialogForPermissions (context, Arial, this, false);
					}
					return;
				}
			}
		}

		/// <summary>
		/// Response after the download string completed.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		async void Client_DownloadStringCompleted (object sender, DownloadStringCompletedEventArgs e)
		{
			try {
				var text = e.Result;
				/* Creating custom local path for images */
				var dir = new Java.IO.File (Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/RetailAcademyData/");
				if (!dir.Exists ()) {
					dir.Mkdirs ();
				}
				filePath = System.IO.Path.Combine (dir.ToString (), fileId + fileName);
				string localPath = System.IO.Path.Combine (dir.ToString (), filePath);
				if (client != null) {
					client = new WebClient ();
				}
				var url = new Uri (contentFilePath);
				byte[] bytes = null;
				try {
					bytes = await client.DownloadDataTaskAsync (url); 
				} catch (TaskCanceledException) {
					System.Console.WriteLine ("Task Canceled!");
					return;
				}
				FileStream fs = new FileStream (localPath, FileMode.OpenOrCreate);
				await fs.WriteAsync (bytes, 0, bytes.Length);
				fs.Close ();
			} catch (WebException) {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (activity, CommonSharedStrings.STATUS_ERROR);
				});
			} catch (InvalidCastException) {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
				});
			} catch (Java.Lang.Exception) {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (activity, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// Download progress change event to update the progress.
		/// </summary>
		/// <param name="sender">Sender.</param>
		/// <param name="e">E.</param>
		void client_DownloadProgressChanged (object sender, DownloadProgressChangedEventArgs e)
		{
			try {
				double bytesIn = Convert.ToInt32 (e.BytesReceived.ToString ());
				double totalBytes = Convert.ToInt32 (e.TotalBytesToReceive.ToString ());
				int percentage = Convert.ToInt32 ((bytesIn / totalBytes) * 100);
				activity.RunOnUiThread (() => this.downloadProgress.Progress = percentage);
				if (percentage == 100) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							Thread.Sleep (3000);
							CommonMethods.ShowAlertDialog (activity, "Download completed.");
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				}
			} catch (WindowManagerBadTokenException){
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Called when the fragment is no longer in use.
		/// </summary>
		public override void OnDestroy ()
		{
			base.OnDestroy ();
			if (client != null) {
				client.CancelAsync ();
			}
		}

		/// <summary>
		/// Raises the destroy view event.
		/// </summary>
		public override void OnDestroyView ()
		{
			base.OnDestroyView ();
			if (client != null) {
				client.CancelAsync ();
			}
		}

		/// <summary>
		/// Raises the item click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			activity.OnBackPressed ();
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				switch (id) {
				case Resource.Id.okAlertOKTV:
					if (permissionsDialog != null) {
						permissionsDialog.Dismiss ();
					}
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					activity.OnBackPressed ();
					break;
				}
			} catch (Exception) {				
			}
		}
	}
}