using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Utilities;
using Android.Graphics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using Android.Provider;
using Java.IO;
using Android.Content.PM;
using System.Threading;
using Com.Nostra13.Universalimageloader.Core;
using Android.Support.V4.Content;
using Android;
using Android.Support.V4.App;

namespace RetailerAcademy.Droid.Source.Fragments
{
	public class EditProfileFragment : BaseFragment, IWebServiceDelegate, View.IOnClickListener
	{
		private Context context;
		private EditText firstNameET, lastNameET, mobilePhoneET, landPhoneET;
		private Button changePhotoBTN, changePasswordBTN, updateBTN;
		private Dialog progressDialog, photoDialog, alertDialog;
		private CircularView avatarImageIV;
		private TextView headerText;
		private string encodedImage = null;
		private Java.IO.File _file;
		private Java.IO.File _dir;
		private ImageLoader imageLoader;
		private DisplayImageOptions options;
		private bool isDetailsService;
		private const int CROP_IMAGE_REQUEST_CODE = 111;
		private const int FROM_CAMERA_INTENT_CODE = 222;
		private const int MY_PERMISSIONS_REQUEST_CAMERA = 333;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.Source.Fragments.EditProfileFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public EditProfileFragment (Context context)
		{
			this.context = context;
			imageLoader = ImageLoader.Instance;
			options = CommonMethods.ReturnDisplayOptions (Resource.Drawable.user_profile);
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText.Text = GetString (Resource.String.editProfile_text);
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.EditProfileFragment, container, false);
			firstNameET = (EditText)view.FindViewById (Resource.Id.firstNameET);
			lastNameET = (EditText)view.FindViewById (Resource.Id.lastNameET);
			mobilePhoneET = (EditText)view.FindViewById (Resource.Id.mobilePhoneET);
			landPhoneET = (EditText)view.FindViewById (Resource.Id.landPhoneET);
			changePhotoBTN = (Button)view.FindViewById (Resource.Id.changePhotoBTN);
			changePasswordBTN = (Button)view.FindViewById (Resource.Id.changePasswordBTN);
			updateBTN = (Button)view.FindViewById (Resource.Id.updateBTN);
			avatarImageIV = (CircularView)view.FindViewById (Resource.Id.avatarImageIV);
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);

			try {
				headerText.Text = activity.GetString (Resource.String.editProfile_text);
				changePhotoBTN.SetOnClickListener (this);
				changePasswordBTN.SetOnClickListener (this);
				updateBTN.SetOnClickListener (this);
				avatarImageIV.SetOnClickListener (this);
				
				firstNameET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				lastNameET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				mobilePhoneET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				landPhoneET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				changePhotoBTN.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				changePasswordBTN.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				updateBTN.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				WebServiceCall ();
			} catch (Exception) {				
			}

			return view;
		}

		/// <summary>
		/// Webservice call for getting user details.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					isDetailsService = true;
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var jsonService = new JsonService ();
					string url = CommonSharedStrings.USER_DETRAILS + userId;
					jsonService.consumeService (url, null, this);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Intent call to crop image.
		/// </summary>
		private void IntentCallToCropImage(){
			try {
				Intent intent = new Intent (Intent.ActionPick, Android.Provider.MediaStore.Images.Media.ExternalContentUri);	
				intent.SetType ("image/*");
				intent.PutExtra ("crop", "true");  
				intent.PutExtra ("aspectX", 1);  
				intent.PutExtra ("aspectY", 1);  
				intent.PutExtra ("outputX", 100);  
				intent.PutExtra ("outputY", 100);  
				intent.PutExtra ("noFaceDetection", true);  
				intent.PutExtra ("return-data", true); 
				intent.PutExtra ("scale", true);   
				StartActivityForResult (intent, CROP_IMAGE_REQUEST_CODE);
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Determines whether this instance is there an app to take pictures.
		/// </summary>
		/// <returns><c>true</c> if this instance is there an app to take pictures; otherwise, <c>false</c>.</returns>
		private bool IsThereAnAppToTakePictures ()
		{
			try {
				Intent intent = new Intent (MediaStore.ActionImageCapture);
				IList<ResolveInfo> availableActivities = context.PackageManager.QueryIntentActivities (intent, PackageInfoFlags.MatchDefaultOnly);
				return availableActivities != null && availableActivities.Count > 0;
			} catch (Exception ex) {
				return false;
			}
		}

		/// <summary>
		/// Creates the directory for pictures.
		/// </summary>
		private void CreateDirectoryForPictures ()
		{
			try {
				_dir = new Java.IO.File (Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + "/RetailAcademyData/");
				if (!_dir.Exists ()) {
					_dir.Mkdirs ();
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Calls the camera.
		/// </summary>
		private void CallCamera ()
		{
			try {
				Intent intent = new Intent (MediaStore.ActionImageCapture);
				_file = new Java.IO.File (_dir, String.Format ("StaffApp_{0}.jpg", Guid.NewGuid ()));
				intent.PutExtra (MediaStore.ExtraOutput, Android.Net.Uri.FromFile (_file));
				StartActivityForResult (intent, 222);
			} catch (Exception) {				
			}
		}

		/// <param name="requestCode">The integer request code originally supplied to
		///  startActivityForResult(), allowing you to identify who this
		///  result came from.</param>
		/// <param name="resultCode">The integer result code returned by the child activity
		///  through its setResult().</param>
		/// <param name="data">An Intent, which can return result data to the caller
		///  (various data can be attached to Intent "extras").</param>
		/// <summary>
		/// Raises the activity result event.
		/// </summary>
		public override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);
			try {
				if (resultCode == Result.Canceled) {
					return;
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, GetString (Resource.String.loading_text));
					}
					// make it available in the gallery
					Intent mediaScanIntent = new Intent (Intent.ActionMediaScannerScanFile);
					if (_file != null && requestCode == FROM_CAMERA_INTENT_CODE) {
						Android.Net.Uri contentUri = Android.Net.Uri.FromFile (_file);
						mediaScanIntent.SetData (contentUri);
						context.SendBroadcast (mediaScanIntent);
						int height = avatarImageIV.Height;
						int width = Resources.DisplayMetrics.WidthPixels;
						Bitmap bitmap = null;
						using (bitmap = _file.Path.LoadAndResizeBitmap (width, height)) {
							avatarImageIV.SetImageBitmap (bitmap);
							MemoryStream stream = new MemoryStream ();
							bitmap.Compress (Bitmap.CompressFormat.Jpeg, 100, stream);
							byte[] imageByteArray = stream.ToArray ();
							encodedImage = Convert.ToBase64String (imageByteArray);
						}
						_file = null;
					} else if (data != null) {
						Bitmap thumbnailBitmap = null;
						if (requestCode == CROP_IMAGE_REQUEST_CODE && data.Extras != null) {
							Bundle bundle = data.Extras;
							thumbnailBitmap = (Bitmap)bundle.GetParcelable ("data");
						} else {
							Android.Net.Uri imageUri = data.Data;
							thumbnailBitmap = Android.Provider.MediaStore.Images.Media.GetBitmap (context.ContentResolver, imageUri);
						}
						avatarImageIV.SetImageBitmap (thumbnailBitmap);
						MemoryStream stream = new MemoryStream ();
						thumbnailBitmap.Compress (Bitmap.CompressFormat.Jpeg, 100, stream);
						byte[] imageByteArray = stream.ToArray ();
						encodedImage = Convert.ToBase64String (imageByteArray);
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					EditProfileImageData editProfileImageData = new EditProfileImageData ();
					editProfileImageData.userid = Convert.ToInt32 (userId);
					editProfileImageData.imagebytes = encodedImage;
					string requestParameter = JsonConvert.SerializeObject (editProfileImageData);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.EDIT_PROFILE, requestParameter, this);
				}
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					string statusMessage = null;
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (isDetailsService) {
						isDetailsService = false;
						bool status = false;
						if (responseObj ["status"] != null) {
							status = Convert.ToBoolean (responseObj.SelectToken ("status"));
							if (status) {
								string name, email, phone, landline, avatarImageUrl, firstName, lastName;
								name = Convert.ToString (responseObj.SelectToken ("userfullname"));
								email = Convert.ToString (responseObj.SelectToken ("emailid"));
								phone = Convert.ToString (responseObj.SelectToken ("mobilenumber"));
								landline = Convert.ToString (responseObj.SelectToken ("landnumber"));
								avatarImageUrl = Convert.ToString (responseObj.SelectToken ("avatarurl"));
								firstName = Convert.ToString (responseObj.SelectToken ("firstname"));
								lastName = Convert.ToString (responseObj.SelectToken ("lastname"));
								activity.RunOnUiThread (() => {
									this.firstNameET.Text = (!string.IsNullOrEmpty (firstName)) ? firstName : "";
									this.lastNameET.Text = (!string.IsNullOrEmpty (lastName)) ? lastName : "";
									this.mobilePhoneET.Text = (!string.IsNullOrEmpty (phone)) ? phone : "";
									this.landPhoneET.Text = (!string.IsNullOrEmpty (landline)) ? landline : "";
									if (!string.IsNullOrEmpty (avatarImageUrl)) { 
										imageLoader.DisplayImage (avatarImageUrl, avatarImageIV, options);
									} else {
										avatarImageIV.SetImageResource (Resource.Drawable.user_profile);
									}
								});
							} else {
								if (!string.IsNullOrEmpty (statusMessage)) {
									activity.RunOnUiThread (() => {
										if (alertDialog != null) {
											alertDialog.Dismiss ();
										}
										alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
									});
								}
							}
						}
					} else {
						if (!string.IsNullOrEmpty (statusMessage)) {
							activity.RunOnUiThread (() => {
								if (alertDialog != null) {
									alertDialog.Dismiss ();
								}
								alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
							});
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		/// <summary>
		/// Photo upload dialog display.
		/// </summary>
		private void PhotoUploadDialog(){
			try {
				photoDialog = new Dialog (this.context);
				photoDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
				photoDialog.Window.RequestFeature (WindowFeatures.NoTitle);
				photoDialog.SetContentView (Resource.Layout.PhotoDialogBox);
				TextView cameraTV = (TextView)photoDialog.FindViewById (Resource.Id.cameraTV);
				TextView photoLibraryTV = (TextView)photoDialog.FindViewById (Resource.Id.photoLibraryTV);
				TextView cancelTV = (TextView)photoDialog.FindViewById (Resource.Id.cancelTV);
				cameraTV.SetOnClickListener (this);
				photoLibraryTV.SetOnClickListener (this);
				cancelTV.SetOnClickListener (this);
				cameraTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				photoLibraryTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				cancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				photoDialog.Show ();
			} catch (Exception) {				
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{			
			try {
				int id = v.Id;
				switch (id) {
				case Resource.Id.changePhotoBTN:
					PhotoUploadDialog ();
					break;
				case Resource.Id.avatarImageIV:
					PhotoUploadDialog ();
					break;
				case Resource.Id.changePasswordBTN:				
					ChangePasswordFragment changePasswordFragment = new ChangePasswordFragment (this.context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, changePasswordFragment, "change_password_fragment").AddToBackStack ("change_password_fragment").Commit ();
					break;
				case Resource.Id.cameraTV:
					photoDialog.Dismiss ();
					if (IsThereAnAppToTakePictures ()) {
						CreateDirectoryForPictures ();
						CallCamera ();
					}
					break;
				case Resource.Id.photoLibraryTV:
					photoDialog.Dismiss ();
					IntentCallToCropImage ();
					break;
				case Resource.Id.cancelTV:
					photoDialog.Dismiss ();
					break;
				case Resource.Id.updateBTN:
					string firstName = this.firstNameET.Text.Trim ();
					string lastname = this.lastNameET.Text.Trim ();
					string phonenumber = this.landPhoneET.Text.Trim ();
					string mobileNumber = this.mobilePhoneET.Text.Trim ();				
					if (string.IsNullOrEmpty (firstName) || firstName.Length < 2) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.FIRSTNAME);
					} else if (string.IsNullOrEmpty (lastname) || lastname.Length < 2) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.LASTNAME);
					} else if (mobileNumber.Length != 0 && mobileNumber.Length < 10) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.MOBILENUMBER);
					} else if (phonenumber.Length != 0 && phonenumber.Length < 10) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.LANDNUMBER);
					} else if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						if (progressDialog == null) {
							progressDialog = CommonMethods.GetProgressDialog (this.context, GetString (Resource.String.loading_text));
						}
						isDetailsService = false;
						var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
						firstName = firstName.Trim ();
						lastname = lastname.Trim ();
						EditProfileData dataObjects = new EditProfileData ();
						dataObjects.firstname = firstName;
						dataObjects.lastname = lastname;
						dataObjects.phonenumber = phonenumber;
						dataObjects.mobilenumber = mobileNumber;
						dataObjects.userid = Convert.ToInt32 (userId);
						string requestParameter = JsonConvert.SerializeObject (dataObjects);
						var jsonService = new JsonService ();
						jsonService.consumeService (CommonSharedStrings.EDIT_PROFILE_INFO, requestParameter, this);
					}
					break;
				}
			} catch (Exception) {				
			}
		}
	}

	public static class BitmapHelpers
	{
		/// <summary>
		/// Loads and resize bitmap.
		/// </summary>
		/// <returns>The and resize bitmap.</returns>
		/// <param name="fileName">File name.</param>
		/// <param name="width">Width.</param>
		/// <param name="height">Height.</param>
		public static Bitmap LoadAndResizeBitmap (this string fileName, int width, int height)
		{
			try {
				// First we get the the dimensions of the file on disk
				BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
				BitmapFactory.DecodeFile (fileName, options);

				// Next we calculate the ratio that we need to resize the image by
				// in order to fit the requested dimensions.
				int outHeight = options.OutHeight;
				int outWidth = options.OutWidth;
				int inSampleSize = 1;

				try {
					if (outHeight > height || outWidth > width) {
						inSampleSize = outWidth > outHeight	? outHeight / height : outWidth / width;
					}
				} catch (System.ArithmeticException) {					
				} catch (Exception) {
				}

				// Now we will load the image and have BitmapFactory resize it for us.
				options.InSampleSize = inSampleSize;
				options.InJustDecodeBounds = false;
				Bitmap resizedBitmap = null;
				resizedBitmap = BitmapFactory.DecodeFile (fileName, options);

				return resizedBitmap;
			} catch (Java.Lang.ArithmeticException) {
				return null;
			} catch (Java.Lang.IllegalArgumentException) {
				return null;
			} catch (Java.Lang.Exception) {
				return null;
			}
		}
	}
}