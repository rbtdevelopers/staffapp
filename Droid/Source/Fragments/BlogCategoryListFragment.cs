﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json.Linq;
using RetailerAcademy.Droid.Source.ContentDTO;
using Android.Support.V4.Widget;
using System.ComponentModel;
using System.Threading;
using Android.Views.InputMethods;
using Java.Lang;

namespace RetailerAcademy.Droid
{
	public class BlogCategoryListFragment : BaseFragment, IWebServiceDelegate, Android.Text.ITextWatcher, ListView.IOnItemClickListener, TextView.IOnEditorActionListener, ListView.IOnClickListener
	{
		private Context context;
		private TextView headerTextTV, cancelTV;
		private Dialog progressDialog, alertDialog;
		private EditText searchET;
		private ListView itemsListView;
		private BlogCategoryListAdapter blogCategoryListAdapter;
		private List<BlogDto> blogCategoryList = new List<BlogDto> ();
		private List<BlogDto> filteredBlogCategoryList;
		private SwipeRefreshLayout swipeLayout;
		private JsonService jsonService;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.BlogCategoryListFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public BlogCategoryListFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			if (blogCategoryListAdapter != null) {
				blogCategoryListAdapter.blogCategoryListItems = blogCategoryList;
				blogCategoryListAdapter.NotifyDataSetChanged ();
			}
			headerTextTV = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerTextTV.Text = GetString (Resource.String.whats_hot_text);
			headerTextTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			headerTextTV.SetOnClickListener (this);
			ClearSearch ();
			WebServiceCall ();
		}

		/// <summary>
		/// Clears the search.
		/// </summary>
		private void ClearSearch ()
		{
			if (searchET != null && !string.IsNullOrEmpty (searchET.Text)) {
				searchET.Text = "";
			}
		}

		/// <summary>
		/// Webs the service call.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.BLOG_CATEGORIES + "/" + userId, null, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.BlogCategoryListFragment, container, false);
			cancelTV = view.FindViewById<TextView> (Resource.Id.cancelTV);
			searchET = view.FindViewById<EditText> (Resource.Id.searchETId);
			itemsListView = (ListView)view.FindViewById (Resource.Id.listViewId);
			cancelTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			searchET.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
			searchET.AddTextChangedListener (this);
			searchET.SetOnEditorActionListener (this);

			cancelTV.SetOnClickListener (this);

			swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
			swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
				Android.Resource.Color.HoloGreenLight,
				Android.Resource.Color.HoloOrangeLight,
				Android.Resource.Color.HoloRedLight);
			swipeLayout.Refresh += delegate(object sender, EventArgs e) {
				ClearSearch ();
				swipeLayout.PostDelayed (WebServiceCall, 3000);
			};
			return view;
		}

		/// <summary>
		/// Ons the response.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);	
					bool status = false;
					JArray blogListArray = null;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						blogCategoryList.Clear ();
						if (responseObj ["blogcategorylist"] != null) {
							blogListArray = (JArray)(responseObj.SelectToken ("blogcategorylist"));
							if (blogListArray != null && blogListArray.Count > 0) {
								foreach (JObject item in blogListArray) {
									BlogDto blogDto = new BlogDto ();
									blogDto.categoryId = (int)item.SelectToken ("categoryid");
									blogDto.categoryName = (string)item.SelectToken ("blogcategory");
									blogDto.hasviewed = (bool)item.SelectToken ("hasviewed");
									itemsListView.SetTag (Resource.Layout.BlogCategoryListItem, blogDto.categoryId);
									blogCategoryList.Add (blogDto);
								}
							}
						}	
						activity.RunOnUiThread (() => {
							blogCategoryListAdapter = new BlogCategoryListAdapter (this.context, Resource.Layout.BlogCategoryListItem, blogCategoryList);
							itemsListView.Adapter = blogCategoryListAdapter;
							itemsListView.OnItemClickListener = this;
						});
					} else {
						if (responseObj ["message"] != null) {
							statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			}
		}

		/// <summary>
		/// Raises the item click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					View v = this.activity.CurrentFocus;
					if (v != null) {  
						InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
						imm.HideSoftInputFromWindow (view.WindowToken, 0);
					}
					TextView itemNameTV = (TextView)view.FindViewById (Resource.Id.itemTVId);
					BlogDto blogDto = (BlogDto)itemNameTV.Tag;
					int categoryId = blogDto.categoryId;
					var categoryName = blogDto.categoryName;
					BlogsListFragment blogsListFragment = new BlogsListFragment (context, categoryId, categoryName);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, blogsListFragment, "blogs_list_fragment").AddToBackStack ("blogs_list_fragment").Commit ();
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Ons the response failed.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}
				public void AfterTextChanged (Android.Text.IEditable s)
		{			
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <param name="after">To be added.</param>
		/// <summary>
		/// Befores the text changed.
		/// </summary>
		public void BeforeTextChanged (Java.Lang.ICharSequence s, int start, int count, int after)
		{			
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="before">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <summary>
		/// Raises the text changed event.
		/// </summary>
		public void OnTextChanged (Java.Lang.ICharSequence s, int start, int before, int count)
		{
			try {
				if (blogCategoryList != null && blogCategoryList.Count > 0) {
					filteredBlogCategoryList = blogCategoryList.Where (p => p.categoryName.ToLower ().Contains (searchET.Text.ToLower ())).ToList ();
					blogCategoryListAdapter = new BlogCategoryListAdapter (this.context, Resource.Layout.BlogCategoryListItem, filteredBlogCategoryList);
					itemsListView.Adapter = blogCategoryListAdapter;
				}
			} catch (System.Exception) {					
			}
		}

		/// <summary>
		/// Raises the editor action event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="actionId">Action identifier.</param>
		/// <param name="e">E.</param>
		public bool OnEditorAction (TextView v, ImeAction actionId, KeyEvent e)
		{
			if (actionId == ImeAction.Search) {
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				return true;
			}
			return false;
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			var id = v.Id;
			switch (id) {
			case Resource.Id.headerText:
				if (itemsListView != null) {
					itemsListView.SmoothScrollToPosition (0); 
				}
				break;
			case Resource.Id.cancelTV:
				searchET.Text = "";
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				break;
			}
		}
	}
}