using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Utilities;
using Android.Support.V4.Widget;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RetailerAcademy.Droid.Source.Adapters;
using System.ComponentModel;
using Java.Lang;
using RetailerAcademy.Droid.Source.StaffApp;
using Android.Graphics;
using Android.Views.InputMethods;

namespace RetailerAcademy.Droid.Source.Fragments
{
	public class MessagesListFragment : BaseFragment, IWebServiceDelegate, View.IOnClickListener, ListView.IOnItemClickListener, Android.Text.ITextWatcher, ListView.IOnItemLongClickListener, AbsListView.IOnScrollListener, TextView.IOnEditorActionListener
	{
		private Context context;
		private TextView headerTextTV, inboxTV, archivedTV, sentTV, cancelTV;
		private ListView messagesListLV;
		private Dialog progressDialog, alertDialog;
		private List<CommunicationMessage> communicationMessagesList;
		public List<CommunicationMessage> filteredCommMessages;
		private SwipeRefreshLayout swipeLayout;
		private EditText searchET;
		private ImageView inboxIV, archivedIV, sentIV, composeMessageIV;
		private MessageListAdapter messageListAdapter;
		private string lastMsgDate;
		public string messageType, userId;
		public bool messageMoved, isSentList, loadingMore, isCompose, isRefresh;
		private int userMessageId;
		private JsonService jsonService;
		private int preItem = 0;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.Source.Fragments.MessagesListFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public MessagesListFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			messageType = "inbox";
			userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerTextTV = (TextView)activity.FindViewById (Resource.Id.headerText);
			try {
				headerTextTV.Text = GetString (Resource.String.messages_text);
				headerTextTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				headerTextTV.SetOnClickListener (this);
				ClearSearch ();
				messageListAdapter = null;
				CommunicationService (messageType, null);
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Clears the search.
		/// </summary>
		private void ClearSearch ()
		{
			if (searchET != null && !string.IsNullOrEmpty (searchET.Text)) {
				searchET.Text = "";
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.MessagesListFragment, container, false);
			searchET = view.FindViewById<EditText> (Resource.Id.searchETId);
			messagesListLV = view.FindViewById<ListView> (Resource.Id.messagesListLV);
			inboxIV = view.FindViewById<ImageView> (Resource.Id.inboxIV);
			archivedIV = view.FindViewById<ImageView> (Resource.Id.archivedIV);
			sentIV = view.FindViewById<ImageView> (Resource.Id.sentIV);
			inboxTV = view.FindViewById<TextView> (Resource.Id.inboxTV);
			sentTV = view.FindViewById<TextView> (Resource.Id.sentTV);
			archivedTV = view.FindViewById<TextView> (Resource.Id.archivedTV);
			cancelTV = view.FindViewById<TextView> (Resource.Id.cancelTV);
			composeMessageIV = view.FindViewById<ImageView> (Resource.Id.composeMessageIV);
			swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
			try {
				searchET.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				inboxTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				sentTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				archivedTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				cancelTV.SetTypeface (OswaldRegular, Android.Graphics.TypefaceStyle.Normal);
				inboxIV.SetOnClickListener (this);
				sentIV.SetOnClickListener (this);
				archivedIV.SetOnClickListener (this);
				messagesListLV.OnItemClickListener = this;
				messagesListLV.OnItemLongClickListener = this;
				messagesListLV.SetOnScrollListener (this);
				searchET.AddTextChangedListener (this);
				searchET.SetOnEditorActionListener (this);
				composeMessageIV.SetOnClickListener (this);
				cancelTV.SetOnClickListener (this);
				swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
					Android.Resource.Color.HoloGreenLight,
					Android.Resource.Color.HoloOrangeLight,
					Android.Resource.Color.HoloRedLight);
				swipeLayout.Refresh += delegate(object sender, EventArgs e) {		
					ClearSearch ();	
					swipeLayout.PostDelayed (CommunicationServicePullToRefresh, 3000);
				};
				inboxIV.SetImageResource (Resource.Drawable.inbox_blue);
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Communicationservice which will be called on pull to refresh.
		/// </summary>
		public void CommunicationServicePullToRefresh ()
		{
			try {
				string msgType = null, first = null;
				if (msgType == null) {
					msgType = messageType;
				}
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (msgType == "inbox") {
				
						if (progressDialog == null) {
							progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
						}
						Communication commObj = new Communication ();
						commObj.userID = Convert.ToInt32 (userId);
						if (first == "refresh") {
							isRefresh = true;
							commObj.filterDate = lastMsgDate;
						} else {
							isRefresh = false;
							communicationMessagesList = new List<CommunicationMessage> ();
							commObj.filterDate = null;
						}
						string requestParameter = JsonConvert.SerializeObject (commObj);
						jsonService = new JsonService ();
						jsonService.consumeService (CommonSharedStrings.COMMUNICATION_LIST, requestParameter, this);
						commObj = null;
					} else if (msgType == "sent") {
						
						if (progressDialog == null) {
							progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
						}
						Communication commObj = new Communication ();
						commObj.userID = Convert.ToInt32 (userId);
				
						if (first == "refresh") {
							isRefresh = true;
							commObj.filterDate = lastMsgDate;
						} else {
							isRefresh = false;
							communicationMessagesList = new List<CommunicationMessage> ();
							commObj.filterDate = null;
						}
						string requestParameter = JsonConvert.SerializeObject (commObj);
						jsonService = new JsonService ();
						jsonService.consumeService (CommonSharedStrings.COMMUNICATION_SENT_MESSAGE, requestParameter, this);
						commObj = null;
					} else if (msgType == "archive") {
						//Archieve
						if (progressDialog == null) {
							progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
						}
						CommunicationArchieve commObj = new CommunicationArchieve ();
						commObj.userID = Convert.ToInt32 (userId);
						commObj.isArchived = true;
				
						if (first == "refresh") {
							isRefresh = true;
							commObj.filterDate = lastMsgDate;
						} else {
							isRefresh = false;
							communicationMessagesList = new List<CommunicationMessage> ();
							commObj.filterDate = null;
						}
						string requestParameter = JsonConvert.SerializeObject (commObj);
						jsonService = new JsonService ();
						jsonService.consumeService (CommonSharedStrings.COMMUNICATION_ARCHIEVE_MESSAGE, requestParameter, this);
						commObj = null;
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// This method is used for making the functional call for the webservice for fist time.
		/// </summary>
		/// <param name="msgType"></param>
		/// <param name="first"></param>
		public void CommunicationService (string msgType, string first)
		{
			try {
				if (msgType == null) {
					msgType = messageType;
				}
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (msgType == "inbox") {
						//inbox
						if (progressDialog == null) {
							progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
						}
						Communication commObj = new Communication ();
						commObj.userID = Convert.ToInt32 (userId);
						if (first == "refresh") {
							isRefresh = true;
							commObj.filterDate = lastMsgDate;
						} else {
							isRefresh = false;
							communicationMessagesList = new List<CommunicationMessage> ();
							commObj.filterDate = null;
						}
						string requestParameter = JsonConvert.SerializeObject (commObj);
						jsonService = new JsonService ();
						jsonService.consumeService (CommonSharedStrings.COMMUNICATION_LIST, requestParameter, this);
						commObj = null;
					} else if (msgType == "sent") {
						//sent
						if (progressDialog == null) {
							progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
						}
						Communication commObj = new Communication ();
						commObj.userID = Convert.ToInt32 (userId);
				
						if (first == "refresh") {
							isRefresh = true;
							commObj.filterDate = lastMsgDate;
						} else {
							isRefresh = false;
							communicationMessagesList = new List<CommunicationMessage> ();
							commObj.filterDate = null;
						}
						string requestParameter = JsonConvert.SerializeObject (commObj);
						jsonService = new JsonService ();
						jsonService.consumeService (CommonSharedStrings.COMMUNICATION_SENT_MESSAGE, requestParameter, this);
						commObj = null;
					} else if (msgType == "archive") {
						//Archieve
						if (progressDialog == null) {
							progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
						}
						CommunicationArchieve commObj = new CommunicationArchieve ();
						commObj.userID = Convert.ToInt32 (userId);
						commObj.isArchived = true;
				
						if (first == "refresh") {
							isRefresh = true;
							commObj.filterDate = lastMsgDate;
						} else {
							isRefresh = false;
							communicationMessagesList = new List<CommunicationMessage> ();
							commObj.filterDate = null;
						}
						string requestParameter = JsonConvert.SerializeObject (commObj);
						jsonService = new JsonService ();
						jsonService.consumeService (CommonSharedStrings.COMMUNICATION_ARCHIEVE_MESSAGE, requestParameter, this);
						commObj = null;
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response"></param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					activity.RunOnUiThread (() => {
						DisabledImagesForFooter ();
						switch (messageType) {
						case "inbox":
							inboxIV.SetImageResource (Resource.Drawable.inbox_blue);
							break;
						case "archive":
							archivedIV.SetImageResource (Resource.Drawable.archiev_blue);
							break;
						case "sent":
							sentIV.SetImageResource (Resource.Drawable.sent_blue);
							break;
						}
					});
					if (status) {
						if (messageMoved) {
							messageMoved = false;
							messageListAdapter.removeItem (messageListAdapter.setPosition, userMessageId);
							activity.RunOnUiThread (() => {
								if (progressDialog != null) {
									progressDialog.Dismiss ();
									progressDialog = null;
								}
							});
							if (!string.IsNullOrEmpty (statusMessage)) {
								activity.RunOnUiThread (() => {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								});
							}
							return;
						}
						JArray messageListArray = null;
						if (responseObj ["messagelist"] != null) {						
							messageListArray = (JArray)responseObj.SelectToken ("messagelist");
							if (messageListArray != null && messageListArray.Count > 0) {							
								foreach (JObject item in messageListArray) {
									CommunicationMessage msg = new CommunicationMessage ();
									msg.subject = (string)item.SelectToken ("subject");
									msg.body = (string)item.SelectToken ("body");
									msg.userMessageID = (int)item.SelectToken ("userMessageID");
									msg.senderNickName = (string)item.SelectToken ("senderNickName");
									msg.recipientName = (string)item.SelectToken ("recipientName");
									msg.messageIsRead = (bool)item.SelectToken ("messageIsRead");
									msg.recipientUserIDs = (string)item.SelectToken ("recipientUserIDs");
									msg.attachment = (string)item.SelectToken ("attachment");
									msg.sendDateTime = (string)item.SelectToken ("sendDateTime");
									lastMsgDate = (string)item.SelectToken ("sendDateTime");
									communicationMessagesList.Add (msg);
									msg = null;
								}
							}
						}
						activity.RunOnUiThread (() => {
							if (messageListAdapter == null) {
								messageListAdapter = new MessageListAdapter (this.context, Resource.Layout.MessageListItem, communicationMessagesList);
								if (messageType == "sent") {
									messageListAdapter.isSentList = true;
								}
								messagesListLV.Adapter = messageListAdapter;
							} else {
								messageListAdapter.NotifyDataSetChanged ();
							}

						});
					} else {
						activity.RunOnUiThread (() => {
							if (communicationMessagesList.Count <= 0) {
								if (!string.IsNullOrEmpty (statusMessage)) {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								}
								communicationMessagesList = new List<CommunicationMessage> ();
								messageListAdapter = new MessageListAdapter (this.context, Resource.Layout.MessageListItem, communicationMessagesList);
								messagesListLV.Adapter = messageListAdapter;
							}
						});
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// To make common images for the footer layout.
		/// </summary>
		public void DisabledImagesForFooter ()
		{
			inboxIV.SetImageResource (Resource.Drawable.inbox_gray);
			archivedIV.SetImageResource (Resource.Drawable.archiev_gray);
			sentIV.SetImageResource (Resource.Drawable.sent_gray);
		}

		/// <summary>
		/// Onclick event for all the click functions in the fragment.
		/// </summary>
		/// <param name="v"></param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				switch (id) {
				case Resource.Id.headerText:
					if (messagesListLV != null) {
						messagesListLV.SmoothScrollToPosition (0);
					}
					break;
				case Resource.Id.inboxIV:
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						DisabledImagesForFooter ();
						ClearSearch ();
						inboxIV.SetImageResource (Resource.Drawable.inbox_blue);
						messageType = "inbox";
						lastMsgDate = null;
						CommunicationService (messageType, null);//inbox
						communicationMessagesList = new List<CommunicationMessage> ();
						messageListAdapter = null;
					}
					break;
				case Resource.Id.archivedIV:
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						DisabledImagesForFooter ();
						ClearSearch ();
						archivedIV.SetImageResource (Resource.Drawable.archiev_blue);
						messageType = "archive";
						lastMsgDate = null;
						CommunicationService (messageType, null);//archive
						communicationMessagesList = new List<CommunicationMessage> ();
						messageListAdapter = null;
					}
					break;
				case Resource.Id.sentIV:
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						DisabledImagesForFooter ();
						ClearSearch ();
						sentIV.SetImageResource (Resource.Drawable.sent_blue);
						messageType = "sent";
						lastMsgDate = null;
						CommunicationService (messageType, null);//sent
						communicationMessagesList = new List<CommunicationMessage> ();
						messageListAdapter = null;
					}
					break;
				case Resource.Id.moveToArchiveInboxTV:
					if (messageType == "archive") {
						MessageMoveToService (false);
					} else if (messageType == "inbox") {
						MessageMoveToService (true);
					}
					break;
				case Resource.Id.composeMessageIV:
					PreferenceConnector.WriteBoolean (this.context, "isCompose", true);
					context.StartActivity (typeof(MessageViewActivity));
					break;
				case Resource.Id.cancelTV:
					searchET.Text = "";
					composeMessageIV.Visibility = ViewStates.Visible;
					cancelTV.Visibility = ViewStates.Gone;
					break;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Method used to call the service to move the message from inbox to archive or archive to inbox.
		/// </summary>
		/// <param name="isarchieved"></param>
		public void MessageMoveToService (bool isarchieved)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					messageMoved = true;
					CommunicationMovetoArchieve commObj = new CommunicationMovetoArchieve ();
					commObj.userID = Convert.ToInt32 (userId);
					commObj.archive = isarchieved;
					commObj.messageSumID = userMessageId;
					string requestParameter = JsonConvert.SerializeObject (commObj);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.COMMUNICATION_MOVETOARCHIEVE, requestParameter, this);
					commObj = null;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// On listview item click to display the message as detailed content in the next activity.
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="view"></param>
		/// <param name="position"></param>
		/// <param name="id"></param>
		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					Intent intent = new Intent (Activity, typeof(MessageViewActivity));
					if (messageType == "inbox" || messageType == "archive") {
						var userMessageID = messageListAdapter.messageListItems [position].userMessageID;
						var recipientName = messageListAdapter.messageListItems [position].senderNickName;
						var recipientUserIDs = messageListAdapter.messageListItems [position].senderUserID;
						var attachment = messageListAdapter.messageListItems [position].attachment;
						intent.PutExtra ("userMessageID", Convert.ToString (userMessageID));
						intent.PutExtra ("messageType", messageType);
						intent.PutExtra ("recipientName", recipientName);
						intent.PutExtra ("recipientUserIDs", recipientUserIDs);
						intent.PutExtra ("attachment", attachment);
						intent.PutExtra ("comingFromPush", "no");
					} else if (messageType == "sent") {
						var messageID = messageListAdapter.messageListItems [position].userMessageID;
						var fromIds = messageListAdapter.messageListItems [position].recipientName;
						var subject = messageListAdapter.messageListItems [position].subject;
						var body = messageListAdapter.messageListItems [position].body;
						var attachment = messageListAdapter.messageListItems [position].attachment;
						intent.PutExtra ("userMessageID", Convert.ToString (messageID));
						intent.PutExtra ("fromIds", fromIds);
						intent.PutExtra ("subject", subject);
						intent.PutExtra ("body", body);
						intent.PutExtra ("attachment", attachment);
						intent.PutExtra ("messageType", messageType);
					}
					StartActivityForResult (intent, 1000);
					PreferenceConnector.WriteBoolean (this.context, "isCompose", false);
					intent = null;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Called on response failed from the server to display the same in alert dialog.
		/// </summary>
		/// <param name="response"></param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		/// <param name="requestCode">The integer request code originally supplied to
		///  startActivityForResult(), allowing you to identify who this
		///  result came from.</param>
		/// <param name="resultCode">The integer result code returned by the child activity
		///  through its setResult().</param>
		/// <param name="data">An Intent, which can return result data to the caller
		///  (various data can be attached to Intent "extras").</param>
		/// <summary>
		/// Raises the activity result event.
		/// </summary>
		public override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);
			try {
				if (resultCode == Result.Ok) {
					DashboardFragment dashboardFrag = new DashboardFragment (this.context);
					fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, dashboardFrag, "dashboard_fragment").AddToBackStack ("dashboard_fragment").Commit ();
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="s">To be added.</param>
		/// <summary>
		/// Afters the text changed.
		/// </summary>
		public void AfterTextChanged (Android.Text.IEditable s)
		{
			if (searchET.Text.Length > 0) {
				composeMessageIV.Visibility = ViewStates.Gone;
				cancelTV.Visibility = ViewStates.Visible;
			} else {
				composeMessageIV.Visibility = ViewStates.Visible;
				cancelTV.Visibility = ViewStates.Gone;
			}
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <param name="after">To be added.</param>
		/// <summary>
		/// Befores the text changed.
		/// </summary>
		public void BeforeTextChanged (ICharSequence s, int start, int count, int after)
		{
		}

		/// <summary>
		/// Used to filter the listview items based on the search key threshold.
		/// </summary>
		/// <param name="s"></param>
		/// <param name="start"></param>
		/// <param name="before"></param>
		/// <param name="count"></param>
		public void OnTextChanged (ICharSequence s, int start, int before, int count)
		{
			try {
				if (communicationMessagesList != null) {
//					filteredCommMessages = communicationMessagesList.Where (p => p.subject.ToLower ().Contains (searchET.Text.ToLower ()) || p.body.ToLower ().Contains (searchET.Text.ToLower ()) || p.senderNickName.ToLower ().Contains (searchET.Text.ToLower ())).ToList ();
//					messageListAdapter = new MessageListAdapter (activity, Resource.Layout.MessageListItem, filteredCommMessages);
					messageListAdapter.SearchListItem(s.ToString());
					if (messageType == "sent") {
						messageListAdapter.isSentList = true;
					}
//					messagesListLV.Adapter = messageListAdapter;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Used to display the move button from inbox to archive or archive to inbox on long press on item.
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="view"></param>
		/// <param name="position"></param>
		/// <param name="id"></param>
		/// <returns></returns>
		public bool OnItemLongClick (AdapterView parent, View view, int position, long id)
		{
			try {
				bool valueToReturn = false;
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (messageType != "sent") {
						userMessageId = messageListAdapter.messageListItems [position].userMessageID;
						var moveToArchiveInboxTV = (TextView)view.FindViewById (Resource.Id.moveToArchiveInboxTV);
						moveToArchiveInboxTV.SetTypeface (OswaldLight, TypefaceStyle.Normal);
						if (messageType == "inbox") {
							moveToArchiveInboxTV.Text = "Move to Archive";
						} else if (messageType == "archive") {
							moveToArchiveInboxTV.Text = "Move to Inbox";
						}
						moveToArchiveInboxTV.SetOnClickListener (this);
						messageListAdapter.setPosition = position;
						messageListAdapter.NotifyDataSetChanged ();
						valueToReturn = true;
					} else {
						valueToReturn = false;
					}
				}
				return valueToReturn;
			} catch (System.Exception) {	
				return false;
			}
		}

		/// <summary>
		/// Raises the scroll event.
		/// </summary>
		/// <param name="view">View.</param>
		/// <param name="firstVisibleItem">First visible item.</param>
		/// <param name="visibleItemCount">Visible item count.</param>
		/// <param name="totalItemCount">Total item count.</param>
		public void OnScroll (AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
		{
			try {
				if (string.IsNullOrEmpty (searchET.Text)) {
					int lastItem = firstVisibleItem + visibleItemCount;
					if (lastItem == totalItemCount && totalItemCount != 0) {
						if (preItem != lastItem) { 
							//to avoid multiple calls for last item
							preItem = lastItem;
							CommunicationService (null, "refresh");
						}
					} else {
						preItem = lastItem;
					}
				}
			} catch (System.Exception) {					
			}
		}

		/// <summary>
		/// Raises the scroll state changed event.
		/// </summary>
		/// <param name="view">View.</param>
		/// <param name="scrollState">Scroll state.</param>
		public void OnScrollStateChanged (AbsListView view, ScrollState scrollState)
		{
		}

		/// <summary>
		/// Raises the editor action event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="actionId">Action identifier.</param>
		/// <param name="e">E.</param>
		public bool OnEditorAction (TextView v, ImeAction actionId, KeyEvent e)
		{
			if (actionId == ImeAction.Search) {
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				return true;
			}
			return false;
		}
	}
}