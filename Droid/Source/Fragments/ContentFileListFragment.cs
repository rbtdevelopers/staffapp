﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Support.V4.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json;
using System.ComponentModel;
using Newtonsoft.Json.Linq;
using Java.Lang;
using Android.Views.InputMethods;
using System.Text.RegularExpressions;

namespace RetailerAcademy.Droid
{
	public class ContentFileListFragment : BaseFragment, IWebServiceDelegate, Android.Text.ITextWatcher, View.IOnClickListener, ListView.IOnItemClickListener, ListView.IOnItemLongClickListener, TextView.IOnEditorActionListener
	{
		private Context context;
		private TextView headerText, cancelTV, uploadContentTV, okTV, dialogCancelTV;
		private EditText searchBoxET;
		private ListView contentFileListLV;
		private ContentFileListAdapter contentFileListAdapter;
		private Dialog progressDialog, alertDialog, passcodeDialog;
		private List<ContentFileDto> contentFileList = new List<ContentFileDto> ();
		public List<ContentFileDto> filteredContentFileList;
		private List<ContentSearchTermsDto> searchItemsList;
		private RelativeLayout footerRLId;
		public bool isReadAreNot, fromTag, isFileDeleted = false, loadingMore, isServiceCallCompleted, isFromTextChange = false;
		private SwipeRefreshLayout swipeLayout;
		private int folderId, fileId, tagId;
		public int selectedFolderPosition = -1;
		private int contentItemPosition = -1;
		private JsonService jsonService;
		private ContentFileDto contentFileCls;
		private string folderName, ifEmptyFolderName, fromQueryString, isContentTag;
		private SearchListItemsAdapter searchItemsListAdapter = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ContentFileListFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public ContentFileListFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{			
			base.OnCreate (savedInstanceState);
			try {
				folderId = Arguments.GetInt ("folderId");
				fromTag = Arguments.GetBoolean ("fromTag");
				ifEmptyFolderName = Arguments.GetString ("folderName");
				isContentTag = Arguments.GetString ("navigating_from");
				folderName = (!string.IsNullOrEmpty (ifEmptyFolderName)) ? ifEmptyFolderName : "KNOWLEDGE"; 
				if (fromTag) {
					folderId = Arguments.GetInt ("tagId");
					tagId = Arguments.GetInt ("tagId");
				} else {
					folderId = Arguments.GetInt ("folderId");
				}
				MyApplication.isComingFromUpload = false;
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			try {
				headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				if (!string.IsNullOrEmpty (isContentTag) && isContentTag.Equals ("content_tag_list_fragment")) {
					headerText.Text = GetString (Resource.String.content_tag_text);
				} else {
					var splCharsRemoved = Regex.Replace (folderName, @"[^0-9a-zA-Z .]+", " ");
					headerText.Text = splCharsRemoved.ToUpper ();
				}
				activity.FindViewById<ImageView> (Resource.Id.headerMenu).Visibility = ViewStates.Visible;
				activity.FindViewById<ImageView> (Resource.Id.headerOrgs).Visibility = ViewStates.Visible;
				var notificationCount = PreferenceConnector.ReadInteger (this.context, "notificationCount", 0);
				if (notificationCount > 0) {
					activity.FindViewById (Resource.Id.notificationCountTV).Visibility = ViewStates.Visible;				
				}
				headerText.Visibility = ViewStates.Visible;
				headerText.SetOnClickListener (this);
				if (!MyApplication.isComingFromUpload) {
					if (string.IsNullOrEmpty (searchBoxET.Text)) {
						WebServiceCall ();
					} else {
						TextChangeService (searchBoxET.Text);
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Clears the search.
		/// </summary>
		private void ClearSearch ()
		{
			if (searchBoxET != null && !string.IsNullOrEmpty (searchBoxET.Text)) {
				searchBoxET.Text = "";
				WebServiceCall ();
			}
		}

		/// <summary>
		/// Webservice call for content file list.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					fromQueryString = "";
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					jsonService = new JsonService ();
					if (fromTag) {
						ContentFileWithTag contentTag = new ContentFileWithTag ();
						contentTag.userid = Convert.ToInt32 (userId);
						contentTag.brandGuid = orgId;
						contentTag.tagid = folderId;
						string requestParameter = JsonConvert.SerializeObject (contentTag);
						jsonService.consumeService (CommonSharedStrings.CONTENT_FILES_TAGLIST, requestParameter, this);
					} else {
						if (folderId != 0) {
							ContentFile contentFileObj = new ContentFile ();
							contentFileObj.folderid = folderId;
							contentFileObj.userid = Convert.ToInt32 (userId);
							string requestParameter = JsonConvert.SerializeObject (contentFileObj);
							jsonService.consumeService (CommonSharedStrings.CONTENT_FILES, requestParameter, this);
						} else {
							jsonService.consumeService (CommonSharedStrings.CONTENT_UPLOAD + "/" + userId, null, this);
						}
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.ContentFileListFragment, container, false);
			contentFileListLV = view.FindViewById<ListView> (Resource.Id.contentFilesListLV);
			cancelTV = view.FindViewById<TextView> (Resource.Id.cancelTV);
			uploadContentTV = view.FindViewById<TextView> (Resource.Id.uploadContentTV);
			footerRLId = view.FindViewById<RelativeLayout> (Resource.Id.footerRLId);
			searchBoxET = view.FindViewById<EditText> (Resource.Id.searchETId);
			try {
				cancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				searchBoxET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				uploadContentTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				searchBoxET.AddTextChangedListener (this);
				searchBoxET.SetOnEditorActionListener (this);
				cancelTV.SetOnClickListener (this);
				uploadContentTV.SetOnClickListener (this);
				contentFileListLV.OnItemClickListener = this;
				contentFileListLV.OnItemLongClickListener = this;
				if (folderId != 0) {
					footerRLId.Visibility = ViewStates.Gone;
				} else {
					footerRLId.Visibility = ViewStates.Visible;
				}
				contentFileListAdapter = new ContentFileListAdapter (this.context, Resource.Layout.ContentFileListItem, contentFileList);
				contentFileListLV.Adapter = contentFileListAdapter;
				swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
				swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
					Android.Resource.Color.HoloGreenLight,
					Android.Resource.Color.HoloOrangeLight,
					Android.Resource.Color.HoloRedLight);
				swipeLayout.Refresh += delegate(object sender, EventArgs e) {			
					swipeLayout.PostDelayed (WebServiceCall, 3000);
				};
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					if (fromQueryString == "text_change") {
						string statusMessageTextChange = null;
						bool statusTextChange = false;
						if (responseObj ["status"] != null) {
							statusTextChange = Convert.ToBoolean (responseObj.SelectToken ("status"));
						}
						if (responseObj ["message"] != null) {
							statusMessageTextChange = Convert.ToString (responseObj.SelectToken ("message"));
						}
						if (statusTextChange) {
							JArray contentFolderSearch = null;
							if (responseObj ["files"] != null) {
								contentFolderSearch = (JArray)responseObj.SelectToken ("files");
								if (contentFolderSearch != null && contentFolderSearch.Count > 0) {
									searchItemsList = new List<ContentSearchTermsDto> ();
									searchItemsList.Clear ();
									foreach (JObject item in contentFolderSearch) {
										ContentSearchTermsDto contentSearchTerms = new ContentSearchTermsDto ();
										contentSearchTerms.fileid = (int)item.SelectToken ("fileid");
										contentSearchTerms.filesize = (string)item.SelectToken ("filesize");
										contentSearchTerms.filesname = (string)item.SelectToken ("filesname");
										contentSearchTerms.filesurl = (string)item.SelectToken ("filesurl");
										contentSearchTerms.description = (string)item.SelectToken ("description");
										contentSearchTerms.hasread = (bool)item.SelectToken ("hasread");
										contentSearchTerms.hastags = (bool)item.SelectToken ("hastags");
										contentSearchTerms.modifieddate = (string)item.SelectToken ("modifieddate");
										contentSearchTerms.passCode = (string)item.SelectToken ("passCode");
										contentSearchTerms.type = (string)item.SelectToken ("type");
										searchItemsList.Add (contentSearchTerms);
									}
									activity.RunOnUiThread (() => {
										searchItemsListAdapter = new SearchListItemsAdapter (this.context, Resource.Layout.SearchListItemContent, searchItemsList);
										contentFileListLV.Adapter = searchItemsListAdapter;
										contentFileListAdapter = null;
									});
								}
								activity.RunOnUiThread (() => {
									if (progressDialog != null) {
										progressDialog.Dismiss ();
										progressDialog = null;
									}
									if (swipeLayout != null) {
										swipeLayout.Refreshing = false;
									}
								});
								isFromTextChange = true;
								return;
							}
						} else {
							if (searchItemsList != null && searchItemsList.Count > 0) {
								searchItemsList.Clear ();
								searchItemsListAdapter = new SearchListItemsAdapter (this.context, Resource.Layout.SearchListItemContent, searchItemsList);
								contentFileListLV.Adapter = searchItemsListAdapter;
								contentFileListAdapter = null;
							} else {
								if (!string.IsNullOrEmpty (statusMessageTextChange)) {
									activity.RunOnUiThread (() => {
										if (alertDialog != null) {
											alertDialog.Dismiss ();
										}
										alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessageTextChange);
										if (contentFileList != null && contentFileList.Count > 0) {
											contentFileList.Clear ();
										}
									});
								}
							}
							activity.RunOnUiThread (() => {
								if (progressDialog != null) {
									progressDialog.Dismiss ();
									progressDialog = null;
								}
								if (swipeLayout != null) {
									swipeLayout.Refreshing = false;
								}
							});
							isFromTextChange = true;
							return;
						}
					} 
					bool status = false;
					JArray contentFileListArray = null;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (status) {
						isFromTextChange = false;
						if (isFileDeleted) {
							activity.RunOnUiThread (() => {
								isFileDeleted = false;
								if (contentFileListAdapter != null) {
									contentFileListAdapter.removeItem (contentFileListAdapter.setPosition);
								}
								foreach (ContentFileDto dto in contentFileList.ToList()) {
									if (dto.filesid == fileId) {
										contentFileList.Remove (dto);
									}
								}
								if (progressDialog != null) {
									progressDialog.Dismiss ();
									progressDialog = null;
								}
								if (swipeLayout != null) {
									swipeLayout.Refreshing = false;
								}
								if (!string.IsNullOrEmpty (statusMessage)) {									
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								}
							});
							return;
						} else {
							if (folderId != 0) {
								if (responseObj ["files"] != null) {
									contentFileListArray = (JArray)responseObj.SelectToken ("files");
									if (contentFileListArray != null && contentFileListArray.Count > 0) {
										contentFileList.Clear ();
										foreach (JObject item in contentFileListArray) {
											contentFileCls = new ContentFileDto ();
											contentFileCls.filesid = (int)item.SelectToken ("fileid");
											var filesname = (string)item.SelectToken ("filesname");
											contentFileCls.filesname = filesname;
											contentFileCls.filespath = (string)item.SelectToken ("filesurl");
											contentFileCls.createdDate = (string)item.SelectToken ("modifieddate");
											contentFileCls.filesize = (string)item.SelectToken ("filesize");
											contentFileCls.hasTags = (bool)item.SelectToken ("hastags");
											contentFileCls.hasread = (bool)item.SelectToken ("hasread");
											contentFileCls.type = (string)item.SelectToken ("type");
											contentFileCls.description = (string)item.SelectToken ("description");
											contentFileCls.passCode = (string)item.SelectToken ("passCode");
											contentFileList.Add (contentFileCls);
										}
									}
								}
							} else {
								if (responseObj ["myfiles"] != null) {
									contentFileListArray = (JArray)responseObj.SelectToken ("myfiles");
									if (contentFileListArray != null && contentFileListArray.Count > 0) {
										contentFileList.Clear ();
										foreach (JObject item in contentFileListArray) {
											contentFileCls = new ContentFileDto ();
											contentFileCls.filesid = (int)item.SelectToken ("mycontentid");
											contentFileCls.filesname = (string)item.SelectToken ("contenttitle");
											contentFileCls.filespath = (string)item.SelectToken ("fileurl");
											contentFileCls.createdDate = (string)item.SelectToken ("date");
											contentFileCls.filesize = (string)item.SelectToken ("filesize");
											contentFileCls.type = (string)item.SelectToken ("type");
											contentFileList.Add (contentFileCls);
										}
									}
								}
							}
							activity.RunOnUiThread (() => {
								if (contentFileListAdapter == null || contentFileList.Count != contentFileListAdapter.Count) {
									contentFileListAdapter = new ContentFileListAdapter (this.context, Resource.Layout.ContentFileListItem, contentFileList);
									contentFileListLV.Adapter = contentFileListAdapter;
								} else {
									contentFileListAdapter.NotifyDataSetChanged ();
								}
							});
						}
					} else {
						isFromTextChange = false;
						if (!string.IsNullOrEmpty (statusMessage)) {
							activity.RunOnUiThread (() => {
								if (alertDialog != null) {
									alertDialog.Dismiss ();
								}
								alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
							});
						}
						activity.RunOnUiThread (() => {
							if (contentFileList != null && contentFileList.Count > 0) {
								contentFileList.Clear ();
							}
							if (contentFileListAdapter == null || contentFileList.Count != contentFileListAdapter.Count) {
								contentFileListAdapter = new ContentFileListAdapter (this.context, Resource.Layout.ContentFileListItem, contentFileList);
								contentFileListLV.Adapter = contentFileListAdapter;
							} else {
								contentFileListAdapter.NotifyDataSetChanged ();
							}
						});
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				if (alertDialog != null) {
					alertDialog.Dismiss ();
				}
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
				alertDialog = CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		/// <param name="s">To be added.</param>
		/// <summary>
		/// Afters the text changed.
		/// </summary>
		public void AfterTextChanged (Android.Text.IEditable s)
		{			
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <param name="after">To be added.</param>
		/// <summary>
		/// Befores the text changed.
		/// </summary>
		public void BeforeTextChanged (ICharSequence s, int start, int count, int after)
		{
		}

		/// <param name="s">To be added.</param>
		/// <param name="start">To be added.</param>
		/// <param name="before">To be added.</param>
		/// <param name="count">To be added.</param>
		/// <summary>
		/// Raises the text changed event.
		/// </summary>
		public void OnTextChanged (ICharSequence s, int start, int before, int count)
		{
			try {
				if (folderId == 0) {
					if (contentFileList != null) {
						filteredContentFileList = contentFileList.Where (p => p.filesname.ToLower ().Contains (searchBoxET.Text.ToLower ())).ToList ();
						contentFileListAdapter = new ContentFileListAdapter (this.context, Resource.Layout.ContentFileListItem, filteredContentFileList);
						contentFileListLV.Adapter = contentFileListAdapter;
					}
				} else {
					if (!string.IsNullOrEmpty (searchBoxET.Text)) {
						TextChangeService (s.ToString ());
					} else {
						WebServiceCall ();
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Global search on edit text change on every character service.
		/// </summary>
		/// <param name="charSeq">Char seq.</param>
		private void TextChangeService (string charSeq)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					var searchKey = searchBoxET.Text;
					fromQueryString = "text_change";
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					var orgId = PreferenceConnector.ReadString (this.context, "org_id", null);
					var jsonService = new JsonService ();
					ContentSearchDTO contentDataObjects = new ContentSearchDTO ();
					contentDataObjects.userid = Convert.ToInt32 (userId);
					contentDataObjects.orgid = orgId;
					contentDataObjects.key = charSeq;
					if (tagId != 0) {
						contentDataObjects.tagid = tagId;					
					}
					contentDataObjects.folderid = folderId;
					string requestParameter = JsonConvert.SerializeObject (contentDataObjects);
					jsonService.consumeService (CommonSharedStrings.CONTENT_SEARCH, requestParameter, this);
					contentDataObjects = null;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {				
				int id = v.Id;
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				if (folderId == 0) {
					var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
					if (userRole != "SuperAdmin" && userRole != "Admin") {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
						return;
					}
				}
				switch (id) {
				case Resource.Id.cancelTV:
					ClearSearch ();
					break;
				case Resource.Id.uploadContentTV:
					if (!CommonMethods.IsInternetConnected (this.context)) {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
					} else {
						ContentUploadFragment contentUploadFragment = new ContentUploadFragment (this.context);
						fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentUploadFragment, "content_upload_fragment").AddToBackStack ("content_upload_fragment").Commit ();
					}
					break;
				case Resource.Id.deleteTV:
					DeleteContentFileService ();
					break;
				case Resource.Id.headerText:
					if (contentFileListLV != null) {
						contentFileListLV.SmoothScrollToPosition (0);
					}
					break;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the item click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public void OnItemClick (AdapterView parent, View view, int position, long id)
		{
			try {
				View v = this.activity.CurrentFocus;
				if (v != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					TextView contentDotIVId = view.FindViewById<TextView> (Resource.Id.contentDotIVId);
					Java.Lang.Object obj = contentDotIVId.Tag;
					string passCode = null;
					if (contentFileListAdapter != null && contentFileListAdapter.contentFileListItems.Count > 0) {
						passCode = contentFileListAdapter.contentFileListItems [position].passCode;
					}
					if (!isFromTextChange) {
						if (folderId == 0) {
							var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
							if (userRole != "SuperAdmin" && userRole != "Admin") {
								CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
								return;
							}
						}
						if (!string.IsNullOrEmpty (passCode) && passCode.Length != 0) {
							passcodeDialog = new Dialog (this.context);
							passcodeDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
							passcodeDialog.Window.RequestFeature (WindowFeatures.NoTitle);
							passcodeDialog.SetContentView (Resource.Layout.PasswordAlertBox);
							TextView alertTV = (TextView)passcodeDialog.FindViewById (Resource.Id.alertTV);
							TextView pleaseEnterTV = (TextView)passcodeDialog.FindViewById (Resource.Id.enterPasscodeTV);
							EditText passcodeET = (EditText)passcodeDialog.FindViewById (Resource.Id.passcodeET);
							dialogCancelTV = (TextView)passcodeDialog.FindViewById (Resource.Id.cancelTV);
							okTV = (TextView)passcodeDialog.FindViewById (Resource.Id.okTV);

							alertTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							pleaseEnterTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							dialogCancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							okTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							passcodeET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);

							okTV.Click += delegate(object sender, EventArgs e) {						
								if (passcodeET.Text == contentFileListAdapter.contentFileListItems [position].passCode) {
									passcodeDialog.Dismiss ();
									ContentFileListFragment contentFileListFragment = new ContentFileListFragment (this.context);
									Bundle args = new Bundle ();
									contentFileListFragment.selectedFolderPosition = position;
									args.PutInt ("folderId", contentFileListAdapter.contentFileListItems [position].filesid);
									args.PutString ("folderName", contentFileListAdapter.contentFileListItems [position].filesname);
									PreferenceConnector.WriteString (this.context, "folderName", contentFileListAdapter.contentFileListItems [position].filesname);
									contentFileListFragment.Arguments = args;
									fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentFileListFragment, "content_file_list_fragment").AddToBackStack ("content_file_list_fragment").Commit ();
								} else {
									CommonMethods.ShowAlertDialog (this.context, GetString (Resource.String.please_enter_valid_passcode_text));
								}
							};

							dialogCancelTV.Click +=	delegate(object sender, EventArgs e) {
								passcodeDialog.Dismiss ();
							};
							passcodeDialog.Show ();
						} else {
							Bundle args = new Bundle ();
							if (obj.GetType () == typeof(ContentFileDto)) {
								ContentFileDto contentFileDtoObj = (ContentFileDto)obj;
								if (folderId != 0) {
									if (contentFileDtoObj.type != null && contentFileDtoObj.type.Equals ("folder")) {
										ContentFileListFragment contentFileListFragment = new ContentFileListFragment (this.context);
										args.PutInt ("folderId", contentFileDtoObj.filesid);
										args.PutString ("folderName", contentFileDtoObj.filesname);
										contentFileListFragment.Arguments = args;
										fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentFileListFragment, "content_file_list_fragment").AddToBackStack ("content_file_list_fragment").Commit ();
									} else {
										ContentViewFragment contentViewFragment = new ContentViewFragment (this.context, "contentfilelist");
										args.PutSerializable ("contentFileCls", contentFileDtoObj);
										args.PutInt ("folderId", folderId);
										contentViewFragment.Arguments = args;
										fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentViewFragment, "content_view_fragment").AddToBackStack ("content_view_fragment").Commit ();
									}
								} else {
									ContentViewFragment contentViewFragment = new ContentViewFragment (this.context, "contentfilelist");
									args.PutSerializable ("contentFileCls", contentFileDtoObj);
									args.PutInt ("folderId", folderId);
									contentViewFragment.Arguments = args;
									fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentViewFragment, "content_view_fragment").AddToBackStack ("content_view_fragment").Commit ();
								}
							}	
						}
					} else {
						if (folderId == 0) {
							var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
							if (userRole != "SuperAdmin" && userRole != "Admin") {
								CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
								return;
							}
						}
						if (!string.IsNullOrEmpty (passCode) && passCode.Length != 0) {
							passcodeDialog = new Dialog (this.context);
							passcodeDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
							passcodeDialog.Window.RequestFeature (WindowFeatures.NoTitle);
							passcodeDialog.SetContentView (Resource.Layout.PasswordAlertBox);
							TextView alertTV = (TextView)passcodeDialog.FindViewById (Resource.Id.alertTV);
							TextView pleaseEnterTV = (TextView)passcodeDialog.FindViewById (Resource.Id.enterPasscodeTV);
							EditText passcodeET = (EditText)passcodeDialog.FindViewById (Resource.Id.passcodeET);
							dialogCancelTV = (TextView)passcodeDialog.FindViewById (Resource.Id.cancelTV);
							okTV = (TextView)passcodeDialog.FindViewById (Resource.Id.okTV);

							alertTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							pleaseEnterTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							dialogCancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							okTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
							passcodeET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);

							okTV.Click += delegate(object sender, EventArgs e) {						
								if (passcodeET.Text == contentFileListAdapter.contentFileListItems [position].passCode) {
									passcodeDialog.Dismiss ();
									ContentFileListFragment contentFileListFragment = new ContentFileListFragment (this.context);
									Bundle args = new Bundle ();
									contentFileListFragment.selectedFolderPosition = position;
									args.PutInt ("folderId", contentFileListAdapter.contentFileListItems [position].filesid);
									args.PutString ("folderName", contentFileListAdapter.contentFileListItems [position].filesname);
									PreferenceConnector.WriteString (this.context, "folderName", contentFileListAdapter.contentFileListItems [position].filesname);
									contentFileListFragment.Arguments = args;
									fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentFileListFragment, "content_file_list_fragment").AddToBackStack ("content_file_list_fragment").Commit ();
								} else {
									CommonMethods.ShowAlertDialog (this.context, GetString (Resource.String.please_enter_valid_passcode_text));
								}
							};

							dialogCancelTV.Click +=	delegate(object sender, EventArgs e) {
								passcodeDialog.Dismiss ();
							};
							passcodeDialog.Show ();
						} else {
							if (obj.GetType () == typeof(ContentSearchTermsDto)) {
								ContentSearchTermsDto contentSearchDtoObj = (ContentSearchTermsDto)obj;
								Bundle args = new Bundle ();
								if (contentSearchDtoObj.type.Equals ("folder")) {
									ContentFileListFragment contentFileListFragment = new ContentFileListFragment (this.context);
									args.PutInt ("folderId", contentSearchDtoObj.fileid);
									args.PutString ("folderName", contentSearchDtoObj.filesname);
									contentFileListFragment.Arguments = args;
									fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentFileListFragment, "content_file_list_fragment").AddToBackStack ("content_file_list_fragment").Commit ();
								} else {
									ContentViewFragment contentViewFragment = new ContentViewFragment (this.context, "contentSearchTerms");
									args.PutSerializable ("contentSearchCls", contentSearchDtoObj);
									args.PutInt ("folderId", folderId);
									contentViewFragment.Arguments = args;
									fragmentManager.BeginTransaction ().Replace (Resource.Id.childContainer, contentViewFragment, "content_view_fragment").AddToBackStack ("content_view_fragment").Commit ();
								}
							}
						}
					}
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the item long click event.
		/// </summary>
		/// <param name="parent">Parent.</param>
		/// <param name="view">View.</param>
		/// <param name="position">Position.</param>
		/// <param name="id">Identifier.</param>
		public bool OnItemLongClick (AdapterView parent, View view, int position, long id)
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					contentItemPosition = position;
					var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
					if (folderId == 0) {
						if (userRole != "SuperAdmin" && userRole != "Admin") {
							CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
						} else {
							if (contentFileListAdapter != null) {
								fileId = contentFileListAdapter.contentFileListItems [position].filesid;
								var deleteTV = (TextView)view.FindViewById (Resource.Id.deleteTV);
								deleteTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
								deleteTV.SetOnClickListener (this);
								contentFileListAdapter.setPosition = position;
								contentFileListAdapter.NotifyDataSetChanged ();
							}
						}
					}
				}
				return true;
			} catch (System.Exception) {
				return false;
			}
		}

		/// <summary>
		/// Deletes the content file item service.
		/// </summary>
		private void DeleteContentFileService ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					isFileDeleted = true;
					var userLoginId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					int userId = Convert.ToInt32 (userLoginId);
					ContentDelete contentObj = new ContentDelete ();
					contentObj.mycontentid = fileId;
					contentObj.userid = Convert.ToInt32 (userId);
					string requestParameter = JsonConvert.SerializeObject (contentObj);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.MYCONTENT_DELETE, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Raises the editor action event.
		/// </summary>
		/// <param name="v">V.</param>
		/// <param name="actionId">Action identifier.</param>
		/// <param name="e">E.</param>
		public bool OnEditorAction (TextView v, ImeAction actionId, KeyEvent e)
		{
			if (actionId == ImeAction.Search) {
				View view = this.activity.CurrentFocus;
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				return true;
			}
			return false;
		}
	}
}