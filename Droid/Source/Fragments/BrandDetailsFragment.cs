using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Adapters;
using RetailerAcademy.Droid.Source.Utilities;
using Newtonsoft.Json.Linq;
using RetailerAcademy.Droid.Source.ContentDTO;
using Android.Support.V4.Widget;
using System.ComponentModel;
using Java.Lang;

namespace RetailerAcademy.Droid.Source.Fragments
{
	public class BrandDetailsFragment : BaseFragment, IWebServiceDelegate, ListView.IOnClickListener
	{
		private Context context;
		private TextView headerText;
		private ListView brandDetailsLV;
		private Dialog progressDialog, alertDialog;
		private List<BrandOrgDetailsDto> brandOrgsList;
		private SwipeRefreshLayout swipeLayout;
		public bool isServiceCallCompleted;
		private JsonService jsonService;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.Source.Fragments.BrandDetailsFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public BrandDetailsFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			headerText.Text = GetString (Resource.String.brand_details_text);
			headerText.SetOnClickListener (this);
			WebServiceCall ();
		}

		/// <summary>
		/// Webservice call for the brand details list.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (activity, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.USER_ORGS + userId, null, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.BrandDetailsFragment, container, false);
			brandDetailsLV = view.FindViewById<ListView> (Resource.Id.brandDetailsLV);
			swipeLayout = view.FindViewById<SwipeRefreshLayout> (Resource.Id.swipe_container);
			swipeLayout.SetColorSchemeResources (Android.Resource.Color.HoloBlueBright,
				Android.Resource.Color.HoloGreenLight,
				Android.Resource.Color.HoloOrangeLight,
				Android.Resource.Color.HoloRedLight);
			swipeLayout.Refresh += delegate(object sender, EventArgs e) {			
				swipeLayout.PostDelayed (WebServiceCall, 3000);
			};
			return view;
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					brandOrgsList = new List<BrandOrgDetailsDto> ();
					bool status = false;
					JArray brandsArray = null;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (status) {
						if (responseObj ["brands"] != null) {
							brandsArray = (JArray)responseObj.SelectToken ("brands");
							if (brandsArray != null && brandsArray.Count > 0) {
								foreach (JObject item in brandsArray) {
									BrandOrgDetailsDto brandOrgs = new BrandOrgDetailsDto ();
									brandOrgs.numberCount = (string)item.SelectToken ("orgid");
									brandOrgs.brandImageURL = (string)item.SelectToken ("orglogo");
									brandOrgs.brandName = (string)item.SelectToken ("orgname");
									brandOrgsList.Add (brandOrgs);
								}
								activity.RunOnUiThread (() => {
									BrandDetailsListAdapter detailsListAdapter = new BrandDetailsListAdapter (this.context, Resource.Layout.DetailsListViewItem, brandOrgsList);
									brandDetailsLV.Adapter = detailsListAdapter;
								});
							}
						}
					} else {
						activity.RunOnUiThread (() => {
							brandDetailsLV.Adapter = new BrandDetailsListAdapter (this.context, Resource.Layout.DetailsListViewItem, this.brandOrgsList);
							if (responseObj ["message"] != null) {
								statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
								if (!string.IsNullOrEmpty (statusMessage)) {
									if (alertDialog != null) {
										alertDialog.Dismiss ();
									}
									alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
								}
							}
						});
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (ArrayIndexOutOfBoundsException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (InvalidCastException) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						if (swipeLayout != null) {
							swipeLayout.Refreshing = false;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					if (swipeLayout != null) {
						swipeLayout.Refreshing = false;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and status erron.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				if (swipeLayout != null) {
					swipeLayout.Refreshing = false;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				var id = v.Id;
				switch (id) {
				case Resource.Id.headerText:
					if (brandDetailsLV != null) {
						brandDetailsLV.SmoothScrollToPosition (0);
					}
					break;
				}
			} catch (System.Exception) {				
			}
		}
	}
}