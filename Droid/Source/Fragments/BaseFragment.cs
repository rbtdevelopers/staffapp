using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace RetailerAcademy.Droid.Source.Fragments
{
    public class BaseFragment : Fragment
    {
        public Activity activity;
        public Typeface OswaldBold, OswaldLight, OswaldRegular, Arial;
		public Android.App.FragmentManager fragmentManager;

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            activity = Activity;
			fragmentManager = Activity.FragmentManager;
			//font styles applied
            OswaldBold = Typeface.CreateFromAsset(Application.Context.Assets, "Fonts/oswald_bold.ttf");
            OswaldLight = Typeface.CreateFromAsset(Application.Context.Assets, "Fonts/oswald_light.ttf");
            OswaldRegular = Typeface.CreateFromAsset(Application.Context.Assets, "Fonts/oswald_regular.ttf");
			Arial = Typeface.CreateFromAsset (Application.Context.Assets, "Fonts/arial.ttf");
        }
    }
}