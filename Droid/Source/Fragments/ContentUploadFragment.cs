﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using RetailerAcademy.Droid.Source.Fragments;
using Android.Support.V4.Widget;
using Android.Graphics;
using RetailerAcademy.Droid.Source.Utilities;
using System.ComponentModel;
using Newtonsoft.Json.Linq;
using Java.Lang;
using Android.Views.InputMethods;
using Newtonsoft.Json;
using System.IO;
using Android.Provider;
using Android.Database;
using System.IO.Compression;
using Java.IO;
using System.Net;
using Android.Support.V4.Content;
using Android;
using Android.Content.PM;

namespace RetailerAcademy.Droid
{
	public class ContentUploadFragment : BaseFragment, IWebServiceDelegate, View.IOnClickListener
	{
		private Context context;
		private TextView headerText, descriptionLabelTV, imageNameTV, contentTitleLabelTV;
		private Button selectContentBTN, uploadBTN;
		private EditText descripitonET, contentTitleET;
		private Dialog progressDialog, photoDialog, alertDialog, permissionsDialog = null;
		private string encodedImage = "", fileName, extension;
		private const int FROM_GALLERY_INTENT_CODE = 111;
		private const int CROP_IMAGE_REQUEST_CODE = 222;
		private const int FROM_VIDEOS_INTENT_CODE = 333;
		private const int PERMISSIONS_REQUEST_WRITE_STORAGE = 444;
		private const int PERMISSIONS_REQUEST_READ_STORAGE = 555;
		private bool photos;

		/// <summary>
		/// Initializes a new instance of the <see cref="RetailerAcademy.Droid.ContentUploadFragment"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		public ContentUploadFragment (Context context)
		{
			this.context = context;
		}

		/// <param name="savedInstanceState">If the fragment is being re-created from
		///  a previous saved state, this is the state.</param>
		/// <summary>
		/// Called to do initial creation of a fragment.
		/// </summary>
		public override void OnCreate (Bundle savedInstanceState)
		{			
			base.OnCreate (savedInstanceState);
		}

		/// <summary>
		/// Called when the fragment is visible to the user and actively running.
		/// </summary>
		public override void OnResume ()
		{
			base.OnResume ();
			headerText = (TextView)activity.FindViewById (Resource.Id.headerText);
			headerText.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
			headerText.Text = GetString (Resource.String.content_navigator_text);
			MyApplication.isComingFromUpload = false;
		}

		/// <summary>
		/// Webservice call for the content upload to the server.
		/// </summary>
		private void WebServiceCall ()
		{
			try {
				if (contentTitleET.Text.Trim ().Length == 0) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CONTENT_TITLE_ERROR);
				} else if (descripitonET.Text.Trim ().Length == 0) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CONTENT_DESC_ERROR);
				} else if (encodedImage.Length == 0) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CONTENT_FILE_ERROR);
				} else if (!CommonMethods.IsInternetConnected (this.context)) {
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.INTERNET_ERROR_MESSAGE);
				} else {
					if (progressDialog == null) {
						progressDialog = CommonMethods.GetProgressDialog (this.context, "Loading, please wait...");
					}
					var userId = PreferenceConnector.ReadString (this.context, "last_user_id", null);
					ContentUploadReq dataObjects = new ContentUploadReq ();
					dataObjects.userid = Convert.ToInt32 (userId);
					dataObjects.filename = fileName;
					dataObjects.extension = extension;
					dataObjects.file = encodedImage;
					dataObjects.description = descripitonET.Text;
					dataObjects.contenttitle = contentTitleET.Text;
					string requestParameter = JsonConvert.SerializeObject (dataObjects);
					var jsonService = new JsonService ();
					jsonService.consumeService (CommonSharedStrings.CONTENT_UPLOAD, requestParameter, this);
				}
			} catch (System.Exception) {				
			}
		}

		/// <param name="inflater">The LayoutInflater object that can be used to inflate
		///  any views in the fragment,</param>
		/// <param name="container">If non-null, this is the parent view that the fragment's
		///  UI should be attached to. The fragment should not add the view itself,
		///  but this can be used to generate the LayoutParams of the view.</param>
		/// <param name="savedInstanceState">If non-null, this fragment is being re-constructed
		///  from a previous saved state as given here.</param>
		/// <summary>
		/// Called to have the fragment instantiate its user interface view.
		/// </summary>
		/// <returns>To be added.</returns>
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var view = inflater.Inflate (Resource.Layout.ContentUploadFragment, container, false);
			contentTitleLabelTV = view.FindViewById<TextView> (Resource.Id.contentTitleLabelTV);
			descriptionLabelTV = view.FindViewById<TextView> (Resource.Id.descriptionLabelTV);
			descripitonET = view.FindViewById<EditText> (Resource.Id.descriptionET);
			contentTitleET = view.FindViewById<EditText> (Resource.Id.contentTitleET);
			imageNameTV = view.FindViewById<TextView> (Resource.Id.imageNameTV);
			selectContentBTN = view.FindViewById<Button> (Resource.Id.selectContentBTN);
			uploadBTN = view.FindViewById<Button> (Resource.Id.uploadBTN);

			try {
				contentTitleLabelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				descriptionLabelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				descripitonET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				contentTitleET.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				imageNameTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				selectContentBTN.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				uploadBTN.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
				
				uploadBTN.SetOnClickListener (this);
				selectContentBTN.SetOnClickListener (this);
				imageNameTV.SetOnClickListener (this);
			} catch (System.Exception) {				
			}
			return view;
		}

		/// <summary>
		/// Response for the web service calls and parsing code block and displaying the same content to UI.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponse (string response)
		{			
			if (!string.IsNullOrEmpty (response)) {
				try {
					JObject responseObj = JObject.Parse (response);
					bool status = false;
					string statusMessage = null;
					if (responseObj ["status"] != null) {
						status = Convert.ToBoolean (responseObj.SelectToken ("status"));
					}
					if (responseObj ["message"] != null) {
						statusMessage = Convert.ToString (responseObj.SelectToken ("message"));
					}
					if (status) {
						activity.RunOnUiThread (() => {
							if (!string.IsNullOrEmpty (statusMessage)) {
								AlertDialog.Builder alert = new AlertDialog.Builder (context);
								alert.SetTitle ("Alert");
								alert.SetCancelable (false);
								alert.SetMessage (statusMessage);
								Dialog dialog = null;
								alert.SetPositiveButton ("OK", (senderAlert, args) => {
									if (dialog != null) {
										dialog.Dismiss ();
									}
									activity.OnBackPressed ();
								});
								dialog = alert.Create ();
								dialog.Show ();
							}
						});
					} else {
						//display toast message							
						if (!string.IsNullOrEmpty (statusMessage)) {
							activity.RunOnUiThread (() => {
								if (alertDialog != null) {
									alertDialog.Dismiss ();
								}
								alertDialog = CommonMethods.ShowAlertDialog (this.context, statusMessage);
							});
						}
					}
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
					});
				} catch (Java.Lang.Exception) {
					activity.RunOnUiThread (() => {
						if (progressDialog != null) {
							progressDialog.Dismiss ();
							progressDialog = null;
						}
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
					});
				}
			} else {
				activity.RunOnUiThread (() => {
					if (progressDialog != null) {
						progressDialog.Dismiss ();
						progressDialog = null;
					}
					CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.STATUS_ERROR);
				});
			}
		}

		/// <summary>
		/// On the response failed and on status error.
		/// </summary>
		/// <param name="response">Response.</param>
		public void onResponseFailed (string response)
		{
			activity.RunOnUiThread (() => {
				if (progressDialog != null) {
					progressDialog.Dismiss ();
					progressDialog = null;
				}
				CommonMethods.ShowAlertDialog (this.context, response);
			});
		}

		/// <summary>
		/// Raises the click event.
		/// </summary>
		/// <param name="v">V.</param>
		public void OnClick (View v)
		{
			try {
				int id = v.Id;
				View view = this.activity.CurrentFocus;
				var userRole = PreferenceConnector.ReadString (this.context, "user_role", null);
				if (view != null) {  
					InputMethodManager imm = (InputMethodManager)context.GetSystemService (Context.InputMethodService);
					imm.HideSoftInputFromWindow (view.WindowToken, 0);
				}
				switch (id) {
				case Resource.Id.uploadBTN:
					if (userRole != "SuperAdmin" && userRole != "Admin") {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
					} else {
						WebServiceCall ();
					}
					break;
				case Resource.Id.selectContentBTN:
					if (userRole != "SuperAdmin" && userRole != "Admin") {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
					} else {
						photoDialog = new Dialog (this.context);
						photoDialog.Window.SetBackgroundDrawableResource (Android.Resource.Color.Transparent);
						photoDialog.Window.RequestFeature (WindowFeatures.NoTitle);
						photoDialog.SetContentView (Resource.Layout.SelectContentDialogBox);
						TextView photosTV = (TextView)photoDialog.FindViewById (Resource.Id.photosTV);
						TextView videosTV = (TextView)photoDialog.FindViewById (Resource.Id.videosTV);
						TextView cancelTV = (TextView)photoDialog.FindViewById (Resource.Id.cancelTV);
						photosTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
						videosTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
						cancelTV.SetTypeface (OswaldRegular, TypefaceStyle.Normal);
						photoDialog.Show ();
						photosTV.SetOnClickListener (this);
						videosTV.SetOnClickListener (this);
						cancelTV.SetOnClickListener (this);
					}
					break;
				case Resource.Id.photosTV:
					photoDialog.Dismiss ();
					if (userRole != "SuperAdmin" && userRole != "Admin") {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
					} else {
						photos = true;
						RequestReadStoragePermission ();
					}
					break;
				case Resource.Id.videosTV:
					photoDialog.Dismiss ();
					if (userRole != "SuperAdmin" && userRole != "Admin") {
						CommonMethods.ShowAlertDialog (this.context, CommonSharedStrings.CHANGE_IN_USER_ROLE);
					} else {
						photos = false;
						RequestReadStoragePermission ();
					}
					break;
				case Resource.Id.cancelTV:
					photoDialog.Dismiss ();
					break;
				case Resource.Id.imageNameTV:
					imageNameTV.Text = "";
					imageNameTV.Visibility = ViewStates.Invisible;
					encodedImage = "";
					break;
				case Resource.Id.okAlertOKTV:
					if (permissionsDialog != null) {
						permissionsDialog.Dismiss ();
					}
					break;
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Intent call for photo library.
		/// </summary>
		private void IntentCallForPhotoLibrary ()
		{
			try {
				if (photos) {
					int height = 450;
					int width = Resources.DisplayMetrics.WidthPixels;
					Intent intent = new Intent (Intent.ActionPick, Android.Provider.MediaStore.Images.Media.ExternalContentUri);	
					intent.SetType ("image/*");
					intent.PutExtra ("crop", "true");  
					intent.PutExtra ("aspectX", 1);  
					intent.PutExtra ("aspectY", 1);  
					intent.PutExtra ("outputX", 100);  
					intent.PutExtra ("outputY", 100);  
					intent.PutExtra ("noFaceDetection", true);  
					intent.PutExtra ("return-data", true); 
					intent.PutExtra ("scale", true);   
					intent.PutExtra("scaleUpIfNeeded", true);
					intent.PutExtra("outputFormat", Bitmap.CompressFormat.Jpeg.ToString());
					StartActivityForResult (intent, CROP_IMAGE_REQUEST_CODE);
				} else {
					Intent i = new Intent (Intent.ActionPick, Android.Provider.MediaStore.Video.Media.ExternalContentUri);
					StartActivityForResult (i, FROM_VIDEOS_INTENT_CODE);
				}
			} catch (System.Exception) {				
			}
		}

		/// <summary>
		/// Requests the read storage permission.
		/// </summary>
		private void RequestReadStoragePermission ()
		{
			// Here, thisActivity is the current activity
			if (ContextCompat.CheckSelfPermission (activity, Manifest.Permission.ReadExternalStorage) != Permission.Granted) {
				// Should we show an explanation?
				if (ShouldShowRequestPermissionRationale (Manifest.Permission.ReadExternalStorage)) {
					// Show an expanation to the user *asynchronously* -- don't block
					// this thread waiting for the user's response! After the user
					// sees the explanation, try again to request the permission.
					RequestPermissions (new System.String[]{ Manifest.Permission.ReadExternalStorage }, PERMISSIONS_REQUEST_READ_STORAGE);
				} else {
					// No explanation needed, we can request the permission.
					RequestPermissions (new System.String[]{ Manifest.Permission.ReadExternalStorage }, PERMISSIONS_REQUEST_READ_STORAGE);
					// MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
					// app-defined int constant. The callback method gets the
					// result of the request.
				}
			} else {
				//Call camera directly, permissions are already accepted.
				IntentCallForPhotoLibrary ();
			}
		}

		/// <summary>
		/// Raises the request permissions result event.
		/// </summary>
		/// <param name="requestCode">Request code.</param>
		/// <param name="permissions">Permissions.</param>
		/// <param name="grantResults">Grant results.</param>
		public override void OnRequestPermissionsResult (int requestCode, string[] permissions, Permission[] grantResults)
		{
			switch (requestCode) {
			case PERMISSIONS_REQUEST_READ_STORAGE:
				{
					// If request is cancelled, the result arrays are empty.
					if (grantResults.Length > 0	&& grantResults [0] == Permission.Granted) {
						// permission was granted, yay! Do the
						// camera task you need to do.
						IntentCallForPhotoLibrary ();
					} else {
						// permission denied, boo! Disable the
						// functionality that depends on this permission.
						if (permissionsDialog != null) {
							permissionsDialog.Dismiss ();
						}
						permissionsDialog = CommonMethods.AlertDialogForPermissions (context, Arial, this, true);
					}
					return;
				}
			}
		}

		/// <summary>
		/// Gets the real path from UR.
		/// </summary>
		/// <returns>The real path from UR.</returns>
		/// <param name="contentUri">Content URI.</param>
		public string GetRealPathFromURI (Android.Net.Uri contentUri)
		{
			try {
				string[] filePathColumn = { MediaStore.MediaColumns.Data };
				var cursor = context.ContentResolver.Query (contentUri, filePathColumn, null, null, null);
				cursor.MoveToFirst ();
				int columnIndex = cursor.GetColumnIndex (filePathColumn [0]);
				string picturePath = cursor.GetString (columnIndex);
				cursor.Close ();
				return picturePath;
			} catch (System.Exception) {	
				return null;			
			}
		}

		/// <param name="requestCode">The integer request code originally supplied to
		///  startActivityForResult(), allowing you to identify who this
		///  result came from.</param>
		/// <param name="resultCode">The integer result code returned by the child activity
		///  through its setResult().</param>
		/// <param name="data">An Intent, which can return result data to the caller
		///  (various data can be attached to Intent "extras").</param>
		/// <summary>
		/// Raises the activity result event.
		/// </summary>
		public override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			if (data != null) {
				try {
					Android.Net.Uri imageVideoUri = data.Data;
					string imagePath = null, imagefileName = null;
					if (imageVideoUri != null) {
						if (!imageVideoUri.ToString ().Contains ("file")) {
							imagePath = GetRealPathFromURI (imageVideoUri);
						} else {
							imagePath = imageVideoUri.ToString ();
						}
						imagefileName = imagePath.Substring (imagePath.LastIndexOf ("/") + 1);
						string[] imageNameArray = imagefileName.Split ('.');
						fileName = imageNameArray [0];
						extension = imagefileName.Substring (imagefileName.LastIndexOf ("."));
						imageNameTV.Text = imagefileName;
						imageNameTV.Visibility = ViewStates.Visible;
						if (requestCode == FROM_VIDEOS_INTENT_CODE) {
							byte[] newBytes = System.IO.File.ReadAllBytes (imagePath);
							encodedImage = Convert.ToBase64String(newBytes);
							//encodedImage = Base64.EncodeToString (newBytes, Base64Flags.Default);					
						} else if (requestCode == CROP_IMAGE_REQUEST_CODE) {
							Bitmap thumbnailBitmap = Android.Provider.MediaStore.Images.Media.GetBitmap (activity.ContentResolver, imageVideoUri);
							MemoryStream stream = new MemoryStream ();
							thumbnailBitmap.Compress (Bitmap.CompressFormat.Jpeg, 50, stream);
							byte[] imageByteArray = stream.ToArray ();
							encodedImage = Convert.ToBase64String (imageByteArray);
						} else {
							if (permissionsDialog != null) {
								permissionsDialog.Dismiss ();
							}
							permissionsDialog = CommonMethods.AlertDialogForPermissions (context, Arial, this, true);
						}
					} else {
						imageNameTV.Text = "";
						imageNameTV.Visibility = ViewStates.Invisible;
					}					
				} catch (System.Exception e) {					
					/* Getting Out of Memory Error for less mobiles */
					imageNameTV.Text = "";
					string error = e.StackTrace;
					imageNameTV.Visibility = ViewStates.Invisible;
					CommonMethods.ShowAlertDialog (context, GetString (Resource.String.not_upload));
				}
			}
		}
	}
}