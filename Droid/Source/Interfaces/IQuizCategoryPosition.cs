﻿using System;

namespace RetailerAcademy.Droid
{
	public interface IQuizCategoryPosition
	{
		void GetQuizCategoryPosition(int categoryPosition);
	}
}

